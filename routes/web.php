<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Contractor;
use App\Mail\sendQR;
use App\TasksView;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

Route::prefix('/admin')->namespace('Admin')->group(function(){

    // Manage contractor
    Route::get('/' ,'AdminController@index')->name('admin.index');
    Route::match(array('GET', 'POST'),'/contractor' ,'AdminContractorController@contractorIndex')->name('admin.contractor.index');
    Route::get('/contractor/enterprise' ,'AdminContractorController@contractorEnterpriseIndex')->name('admin.contractor.enterprise.index');
    Route::get('/checkCitizen' ,'AdminContractorController@checkCitizen')->name('admin.checkCitizen');
    Route::get('/checkEnterprise' ,'AdminContractorController@checkEnterprise')->name('admin.checkEnterprise');
    Route::match(array('GET', 'POST'),'/contractor/edit/{id?}' ,'AdminContractorController@contractorEdit')->name('admin.contractor.edit');
    Route::match(array('GET', 'POST'),'/contractor/enterprise/edit/{id?}' ,'AdminContractorController@contractorEnterpriseEdit')->name('admin.contractor.enterprise.edit');
    Route::match(array('GET', 'POST'),'/contractor/delete/{id?}' ,'AdminContractorController@contractorDelete')->name('admin.contractor.delete');
    Route::match(array('GET', 'POST'),'/contractor/enterprise/delete/{id?}' ,'AdminContractorController@enterpriseDelete')->name('admin.contractor.enterprise.delete');
    Route::match(array('GET', 'POST'),'/getContractorAdmin', 'AdminContractorController@getContractorAdmin')->name('getContractorAdmin');
    // Manage leader
    Route::get('/leader' ,'AdminLeaderController@leaderIndex')->name('admin.leader.index');
    Route::match(array('GET', 'POST'),'/leader/edit/{id?}' ,'AdminLeaderController@leaderEdit')->name('admin.leader.edit');
    Route::match(array('GET', 'POST'),'/leader/delete/{id?}' ,'AdminLeaderController@leaderDelete')->name('admin.leader.delete');
    Route::get('/checkEmailLeader' ,'AdminLeaderController@checkEmailLeader')->name('admin.checkEmailLeader');
    // Manage Admin
    Route::get('/admin' ,'AdminController@adminIndex')->name('admin.admin.index');
    Route::match(array('GET', 'POST'),'/admin/edit/{id?}' ,'AdminController@adminEdit')->name('admin.admin.edit');
    Route::match(array('GET', 'POST'),'/admin/delete/{id?}' ,'AdminController@adminDelete')->name('admin.admin.delete');

    // Manage Task
    Route::get('/task/workpermit' ,'AdminTaskController@taskIndex')->name('admin.task.index');
    Route::match(array('GET', 'POST'),'/task/all' ,'AdminTaskController@taskAll')->name('admin.task.allTask');
    Route::match(array('GET', 'POST'),'/getAllTask' ,'AdminTaskController@allTaskJson')->name('admin.getAllTask');
    Route::get('/task/all/qecode/{task_id?}' ,'AdminTaskController@taskAllQRcode')->name('admin.task.allTask.qrcode');
    Route::get('/task/detail/{id?}' ,'AdminTaskController@taskDetail')->name('admin.task.detail');
    Route::match(array('GET', 'POST'),'/task/resultTaskDetail/{task_id?}/{task_detail_id?}' ,'AdminTaskController@resultTaskDetail')->name('admin.task.resultTaskDetail');
    Route::match(array('GET', 'POST'),'/task/resultTaskDetail/{task_id?}/{task_detail_id?}' ,'AdminTaskController@resultTaskDetail')->name('admin.task.resultTaskDetail');
    Route::get('/task/resultTaskDetailPDF/{task_id?}/{task_detail_id?}' ,'AdminTaskController@resultTaskDetailPDF')->name('admin.task.resultTaskDetailPDF');
    Route::match(array('GET', 'POST'),'/task/delete/{id?}' ,'AdminTaskController@taskDelete')->name('admin.task.delete');
    Route::match(array('GET', 'POST'),'/task/edit/{id?}' ,'AdminTaskController@taskEdit')->name('admin.task.edit');
    Route::get('/task/changeStatusTask{task_id?}' ,'AdminTaskController@changeStatusTask')->name('admin.task.change.status');
    Route::match(array('GET', 'POST'),'/task/finish/{task_detail_id?}' ,'AdminTaskController@taskFinish')->name('admin.finish.task');

    // Manage Mail List
    Route::get('/mail_list' ,'AdminMailController@mailIndex')->name('admin.mail.list.index');
    Route::match(array('GET', 'POST'),'/mail_list/edit/{id?}' ,'AdminMailController@mailEdit')->name('admin.mail.list.edit');
    Route::match(array('GET', 'POST'),'/mail_list/delete/{id?}' ,'AdminMailController@mailDelete')->name('admin.mail.list.delete');

    // Export Task
    Route::get('/export/task/index' ,'AdminExportController@exportTaskIndex')->name('admin.export.task.index');
    Route::get('/export/contractor/index' ,'AdminExportController@exportContractorIndex')->name('admin.export.contractor.index');
    Route::match(array('GET', 'POST'),'/export/task' ,'AdminExportController@exportTask')->name('admin.export.task');
    Route::match(array('GET', 'POST'),'/export/contractor' ,'AdminExportController@exportContractor')->name('admin.export.contractor');
    Route::get('/export/leader/index' ,'AdminExportController@exportLeaderIndex')->name('admin.export.leader.index');
    Route::match(array('GET', 'POST'),'/export/leader' ,'AdminExportController@exportLeader')->name('admin.export.leader');

    // Manage Training
    Route::get('/training/index' ,'AdminTrainingController@trainingIndex')->name('admin.training.index');
    Route::match(array('GET', 'POST'),'/training/detail/{id?}' ,'AdminTrainingController@trainingDetail')->name('admin.training.detail');
    Route::get('/training/detail/delete/{id?}' ,'AdminTrainingController@trainingDetailDelete')->name('admin.training.detail.delete');
    Route::match(array('GET', 'POST'),'/training/detail/edit/{training_id?}/{training_detail_id?}' ,'AdminTrainingController@trainingDetailEdit')->name('admin.training.detail.edit');
    Route::match(array('GET', 'POST'),'/training/edit/{id?}','AdminTrainingController@trainingEdit')->name('admin.training.edit');
    Route::match(array('GET', 'POST'),'/training/delete/{id?}' ,'AdminTrainingController@trainingDelete')->name('admin.training.delete');
    Route::get('/trainer' ,'AdminTrainingController@trainerIndex')->name('admin.trainer.index');
    Route::match(array('GET', 'POST'),'/trainer/edit/{id?}' ,'AdminTrainingController@trainerEdit')->name('admin.trainer.edit');
    Route::match(array('GET', 'POST'),'/trainer/delete/{id?}' ,'AdminTrainingController@trainerDelete')->name('admin.trainer.delete');
    Route::get('/safetyTraining/index' ,'AdminTrainingController@safetyTrainingIndex')->name('admin.trainer.safetyTraining.index');
    Route::match(array('GET', 'POST'),'/safetyTraining/edit/{id?}' ,'AdminTrainingController@safetyTrainingEdit')->name('admin.trainer.safetyTraining.edit');

    Route::namespace('Auth')->group(function(){
        //Login Routes
        Route::get('/login','LoginController@showLoginForm')->name('admin.login');
        Route::post('/login','LoginController@login');
        Route::post('/logout','LoginController@logout')->name('admin.logout');

        //Forgot Password Routes
        Route::get('/password/reset','ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
        Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');

        //Reset Password Routes
        Route::get('/password/reset/{token}','ResetPasswordController@showResetForm')->name('admin.password.reset');
        Route::post('/password/reset','ResetPasswordController@reset')->name('admin.password.update');

    });
});

Route::prefix('/leader')->namespace('Leader')->group(function(){
    Route::get('/task/contractor' ,'LeaderTaskController@index')->name('leader.task.index');
    Route::get('/task/contractor/detail/{id?}' ,'LeaderTaskController@taskDetail')->name('leader.task.detail');
    Route::match(array('GET', 'POST'),'/task/confirmTask' ,'LeaderConfirmController@confirmTask')->name('leader.task.confirm');
    Route::get('/task/confirmIndex' ,'LeaderConfirmController@confirmIndex')->name('leader.task.confirm.index');
    Route::get('/task/finishIndex' ,'LeaderConfirmController@finishIndex')->name('leader.task.finish.index');
    Route::match(array('GET', 'POST'),'/task/finishTask/{id?}' ,'LeaderConfirmController@finishTask')->name('leader.task.finish');
    Route::get('/task/changeStatusTask{task_id?}' ,'LeaderTaskController@changeStatusTask')->name('leader.task.change.status');
    Route::match(array('GET', 'POST'),'/task/checkTaskDetail/{id?}/{time?}' ,'LeaderTaskController@checkTaskDetail')->name('leader.checkTaskDetail');
    Route::match(array('GET', 'POST'),'/task/resultTaskDetail/{task_id?}/{task_detail_id?}' ,'LeaderTaskController@resultTaskDetail')->name('leader.resultTaskDetail');
    Route::match(array('GET', 'POST'),'/task/check/{task_id?}' ,'LeaderTaskController@checkTask')->name('leader.task.check');
    Route::match(array('GET', 'POST'),'/task/delete/{id?}' ,'LeaderTaskController@taskDelete')->name('leader.task.delete');
    Route::match(array('GET', 'POST'),'/task/cancel/{id?}' ,'LeaderTaskController@taskCancel')->name('leader.task.cancel');

    Route::get('/task/workpermit' ,'LeaderTaskController@taskWorkPermit')->name('leader.task.workpermit');
    Route::match(array('GET', 'POST'),'/task/all' ,'LeaderTaskController@taskAll')->name('leader.task.allTask');
    Route::get('/task/all/qecode/{task_id?}' ,'LeaderTaskController@taskAllQRcode')->name('leader.task.allTask.qrcode');
    Route::match(array('GET', 'POST'),'/getAllTask' ,'LeaderTaskController@allTaskJson')->name('leader.getAllTask');
    Route::match(array('GET', 'POST'),'/task/edit/{id?}' ,'LeaderTaskController@taskEdit')->name('leader.task.edit');
    Route::get('/task/detail/{id?}' ,'LeaderTaskController@taskDetailGroup')->name('leader.task.detailGroup');
    Route::match(array('GET', 'POST'),'/task/resultTaskDetailGroup/{task_id?}/{task_detail_id?}' ,'LeaderTaskController@resultTaskDetailGroup')->name('leader.task.resultTaskDetailGroup');
    Route::get('/task/resultTaskDetailGroupPDF/{task_id?}/{task_detail_id?}' ,'LeaderTaskController@resultTaskDetailGroupPDF')->name('leader.task.resultTaskDetailGroupPDF');

    //export
    Route::get('/export/task/index' ,'LeaderExportController@exportTaskIndex')->name('leader.export.task.index');
    Route::match(array('GET', 'POST'),'/export/task' ,'LeaderExportController@exportTask')->name('leader.export.task');

    Route::namespace('Auth')->group(function(){
        //Login Routes
        Route::get('/login','LoginController@showLoginForm')->name('leader.login');
        Route::post('/login','LoginController@login');
        Route::post('/logout','LoginController@logout')->name('leader.logout');

        //Forgot Password Routes
        Route::get('/password/reset','ForgotPasswordController@showLinkRequestForm')->name('leader.password.request');
        Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('leader.password.email');

        //Reset Password Routes
        Route::get('/password/reset/{token}','ResetPasswordController@showResetForm')->name('leader.password.reset');
        Route::post('/password/reset','ResetPasswordController@reset')->name('leader.password.update');

    });
});
Auth::routes();

Route::get('/', 'HomeController@index_task_type')->name('home');
//Route::get('/v2', 'HomeController@index')->name('home_v2');
Route::post('/register', 'HomeController@register_task_type')->name('register');
//Route::post('/register/v2', 'HomeController@register')->name('register_v2');
Route::match(array('GET', 'POST'),'/getContractor', 'HomeController@getContractors')->name('getContractor');
Route::match(array('GET', 'POST'),'/getLeader', 'HomeController@getLeaders')->name('getLeader');
Route::get('/contractor/scan/open/task/{task_id?}', 'HomeController@openTask')->name('contractor.scanQR.openTask');
Route::get('/contractor/scan/workpermit/{taskDetail_id?}' ,'HomeController@workpermit')->name('contractor.scanQR.workPermit');
Route::get('/contractor/search/task/index' ,'HomeController@searchIndex')->name('contractor.search.task.index');
Route::match(array('GET', 'POST'),'/contractor/search/task' ,'HomeController@searchTask')->name('contractor.search.task');
Route::get('/task/workpermit/pdf/{id?}' ,'HomeController@workpermitPDF')->name('admin.workPermit.pdf');
Route::match(array('GET', 'POST'),'/task/checkTaskDetail/{id?}' ,'HomeController@checkTaskDetail')->name('contractor.checkTaskDetail');
Route::match(array('GET', 'POST'),'/searchTraining' ,'HomeController@searchTraining')->name('searchTraining');
Route::match(array('GET', 'POST'),'/searchSafetyTraining' ,'HomeController@searchSafetyTraining')->name('searchSafetyTraining');
Route::match(array('GET', 'POST'),'/resultPage' ,'HomeController@resultPage')->name('resultPage');

Route::get('/preview', function () {
    //preview workpermit
//    $task= tasksView::where('task_id',54)->first();
//    return new App\Mail\sendMailConfirmTask($task,2,2);

    //preview QR
    $taskDetail = tasksView::where('task_id',2704)->first();
//    return new sendQR($taskDetail);
    return  Mail::to('Phugagza@hotmail.com')->send(
        new sendQR($taskDetail)
    );
});

//
Route::match(array('GET', 'POST'),'/test', function (Request $request) {

    $allRequest = $request->except('_method', '_token');
    $keyword = $request->keyword ? $request->keyword : '';
    $task_detail_status = $request->task_detail_status ? $request->task_detail_status :'';
    $date_start = $request->date_start ? Carbon::create($request->date_start) -> format('Y-m-d') :'';
    $date_end = $request->date_end ? Carbon::create($request->date_end) -> format('Y-m-d') :'';
    $perPageDefault = $request-> perPageDefault ? $request-> perPageDefault : 15;
    if($keyword != '' && $date_start == '' && $task_detail_status == ''){
        $tasks = DB::table('group_all_task_view')
            ->where(function ($query) use($keyword) {
                $query->orwhere('task_title','like',"%".$keyword."%")
                    ->orWhere('task_location','like',"%".$keyword."%")
                    ->orWhere('contractor_first_name','like',"%".$keyword."%")
                    ->orWhere('leader_first_name','like',"%".$keyword."%");
            })
            ->orderBy('task_id','desc')
            ->paginate($perPageDefault);
    }
    if($date_start != '' && $task_detail_status == '' || $date_end){
        $tasks = DB::table('group_all_task_view')
            ->whereBetween('task_working_date' ,[$date_start,$date_end])
            ->orwhereBetween('register_task_detail_date' ,[$date_start,$date_end])
            ->where(function ($query) use($keyword) {
                $query->orwhere('task_title','like',"%".$keyword."%")
                    ->orWhere('task_location','like',"%".$keyword."%")
                    ->orWhere('contractor_first_name','like',"%".$keyword."%")
                    ->orWhere('leader_first_name','like',"%".$keyword."%");
            })
            ->orderBy('task_id','desc')
            ->paginate($perPageDefault);
    }
    if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start == ''){
        $tasks = DB::table('group_all_task_view')
            ->where('task_detail_status' ,$task_detail_status)
            ->where(function ($query) use($keyword) {
                $query->orwhere('task_title','like',"%".$keyword."%")
                    ->orWhere('task_location','like',"%".$keyword."%")
                    ->orWhere('contractor_first_name','like',"%".$keyword."%")
                    ->orWhere('leader_first_name','like',"%".$keyword."%");
            })
            ->orderBy('task_id','desc')
            ->paginate($perPageDefault);
    }
    if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start != ''){
        $tasks = DB::table('group_all_task_view')
            ->where('task_detail_status' ,$task_detail_status)
            ->whereBetween('register_task_detail_date' ,[$date_start,$date_end])
            ->where(function ($query) use($keyword) {
                $query->orwhere('task_title','like',"%".$keyword."%")
                    ->orWhere('task_location','like',"%".$keyword."%")
                    ->orWhere('contractor_first_name','like',"%".$keyword."%")
                    ->orWhere('leader_first_name','like',"%".$keyword."%");
            })
            ->orderBy('task_id','desc')
            ->paginate($perPageDefault);

    }

    if ($task_detail_status == 'registered' && $date_start == ''){
        $tasks = DB::table('group_all_task_view')
            ->where('task_type' ,null)
            ->where('task_detail_status' ,null)
            ->orderBy('task_id','desc')
            ->where(function ($query) use($keyword) {
                $query->orwhere('task_title','like',"%".$keyword."%")
                    ->orWhere('task_location','like',"%".$keyword."%")
                    ->orWhere('contractor_first_name','like',"%".$keyword."%")
                    ->orWhere('leader_first_name','like',"%".$keyword."%");
            })
            ->paginate($perPageDefault);
    }
    if ($task_detail_status == 'registered' && $date_start != ''){
        $tasks = DB::table('group_all_task_view')
            ->where('task_type' ,null)
            ->where('task_detail_status' ,null)
            ->whereBetween('task_working_date' ,[$date_start,$date_end])
            ->where(function ($query) use($keyword) {
                $query->orwhere('task_title','like',"%".$keyword."%")
                    ->orWhere('task_location','like',"%".$keyword."%")
                    ->orWhere('contractor_first_name','like',"%".$keyword."%")
                    ->orWhere('leader_first_name','like',"%".$keyword."%");
            })
            ->orderBy('task_id','desc')
            ->paginate($perPageDefault);
    }
    if ($task_detail_status == '5rule' && $date_start == ''){
        $tasks = DB::table('group_all_task_view')
            -> where('task_type' ,'<>',null)
            -> where('task_detail_status' ,null)
            ->where(function ($query) use($keyword) {
                $query->orwhere('task_title','like',"%".$keyword."%")
                    ->orWhere('task_location','like',"%".$keyword."%")
                    ->orWhere('contractor_first_name','like',"%".$keyword."%")
                    ->orWhere('leader_first_name','like',"%".$keyword."%");
            })
            ->orderBy('task_id','desc')
            ->paginate($perPageDefault);
    }
    if ($task_detail_status == '5rule' && $date_start != ''){
        $tasks = DB::table('group_all_task_view')
            -> where('task_type' ,'<>',null)
            -> where('task_detail_status' ,null)
            ->whereBetween('task_working_date' ,[$date_start,$date_end])
            ->where(function ($query) use($keyword) {
                $query->orwhere('task_title','like',"%".$keyword."%")
                    ->orWhere('task_location','like',"%".$keyword."%")
                    ->orWhere('contractor_first_name','like',"%".$keyword."%")
                    ->orWhere('leader_first_name','like',"%".$keyword."%");
            })
            ->orderBy('task_id','desc')
            ->paginate($perPageDefault);
    }
    if($task_detail_status == '' && $date_start =='' && $date_end =='' && $keyword == ''){
        $tasks = DB::table('group_all_task_view')
            ->orderBy('task_id','desc')
            ->paginate($perPageDefault);
    }
    return view('test',compact('tasks','allRequest','perPageDefault','keyword','task_detail_status','date_start','date_end'));

});

