<?php
return[

    'task'=>[
        'status' =>[
            '1' => 'QR OK',
            '2' => 'QR Close'
        ],
    ],

    'task_type'=>[
        '1'=>'งานไฟฟ้า',
        '2'=>'งานเครื่องปรับอากาศ',
        '3'=>'งานเชื่อม',
        '4'=>'งานติดตั้งนั่งร้าน',
        '5'=>'งานอับอากาศ',
        '6'=>'อื่นๆ',
    ],

    'task_detail'=>[
        'status' =>[
            '1' => ['color'=>'warning','status_name'=>'รอเข้างาน'],
            '2' => ['color'=>'primary','status_name'=>'อนุมัติ'],
            '3' => ['color'=>'primary','status_name'=>'เริ่มทำงาน'],
            '4' => ['color'=>'warning','status_name'=>'ตรวจสอบ 1'],
            '5' => ['color'=>'warning','status_name'=>'ตรวจสอบ 2'],
            '6' => ['color'=>'warning','status_name'=>'ตรวจสอบปิดงาน'],
            '7' => ['color'=>'success','status_name'=>'ปิดงานสำเร็จ'],
            '8' => ['color'=>'danger','status_name'=>'ปิดงานไม่สำเร็จ'],
            '9' => ['color'=>'danger','status_name'=>'ยกเลิก'],
            'registered' => ['color'=>'info','status_name'=>'ลงทะเบียน'],
            '5rule' => ['color'=>'info','status_name'=>'5 Job Rule'],
            'default' => ['color'=>'primary','status_name'=>'-'],
        ],
        'status_value'=>[
            'WaitForApprove' => '1',
            'Approved ' => '2',
            'StartWork' => '3',
            'CheckTask1' => '4',
            'CheckTask2' => '5',
            'CheckWorkDone' => '6',
            'WorkDone' => '7',
            'WorkNotDone' => '8',
            'WorkCancel' => '9',
        ]
    ],

    'training'=>[
        'SafetyTraining'=>'Safety Training',
        'IndoorElectricity'=>'ช่างไฟไฟ้าภายในอาคาร',
        'IndoorAirConditioner'=>'ช่างเครื่องปรับอากาศภายในบ้านและการพาณิชย์ขนาดเล็ก',
        'MetalArc'=>'ช่างเชื่อมอาร์กโลหะด้วยมือ',
        'TigWelding'=>'ช่างเชื่อมทิก',
        'AlloyWelding'=>'ช่างเชื่อมแม็ก',
        'Scaffolding'=>'ติดตั้งนั่งร้าน',
        'WorkerConfine'=>'ผู้ปฏิบัติงาน ในงานอับอากาศ',
        'AssistantConfine'=>'ผู้ช่วยเหลือ ในงานอับอากาศ',
        'ControllerConfine'=>'ผู้ควบคุม ในงานอับอากาศ',
        'WarrantorConfine'=>'ผู้อนุญาต ในงานอับอากาศ',
    ],
    //ED_ - EE_ + ME_ + EET_
    'leader_ED_group' =>[
        'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
        'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
        'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
        'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
        'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT',
        'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
        'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
        'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
        'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
        'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
        'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
        'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
        '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri',
        'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
        'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
        'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
    ],
    //EE_ - EE1 EE2 EE3 EE5 PCSI
    'leader_EE_group' =>[
        'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
        'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
        'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
        'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
        'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT'
    ],
    //ME_ - ME1 ME2 ME3 ME4 ME5 MAG MRG MUG
    'leader_ME_group' =>[
        'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
        'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
        'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
        'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
        'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
        'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
        'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
        '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri'
    ],
    //EET_ - ENGINEERING PM ENERGY
    'leader_EET_group' =>[
        'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
        'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
        'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
    ],


];
