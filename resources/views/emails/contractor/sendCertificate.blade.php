<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Taviraj&display=swap');
        html , body ,center{
            margin: 0;
            padding: 0;
            height: 100%;
            /*background: #5d5d5a;*/
            font-family: 'Taviraj', serif;
        }




        .grid-container{
            display: grid;
            grid-template-rows: 50% 50%;
            margin-top: 50px;
            width: 500px;
            height: 500px;
            border-radius: 10px;
            background-color: white;
        }

        .grid-item:nth-child(1){

        }

        .grid-item:nth-child(2){
            border-radius: 0 0 10px 10px;

        }






    </style>
</head>
<body>
<center>
    <div class="grid-container">
        <div class="grid-item" {{--style="background-color: #a748ca"--}}>
{{--            <img src="{{asset('img/bg/ns-logo.png')}}" alt="logo">--}}
            <h3>แจ้งเตือนวันหมดอายุ{{$title}}</h3>
            <div class="grid-item" {{--style="background-color: #ca9c21"--}}>
                {{$contractor -> prefix.$contractor -> first_name." ".$contractor -> last_name}}
            </div>
            <div class="grid-item" {{--style="background-color: #ca9c21"--}}>
                กำลังจะหมดอายุในวันที่ : <span style="font-weight: bold; color: red">{{$contractor -> safety_expired_date ? ConvertThaiDate($contractor -> safety_expired_date) : '-'}} </span>
                อีก <span style="font-weight: bold; color: red">{{$contractor -> count_safety_date}}</span> วัน
            </div>
            <div class="grid-item" {{--style="background-color: #ca9c21"--}}>
                <a href="{{route('searchSafetyTraining')}}">ตรวจสอบการอบรม</a>
            </div>
        </div>
    </div>
</center>
</body>
</html>
