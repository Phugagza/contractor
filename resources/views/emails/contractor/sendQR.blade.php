<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Preview</title>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Taviraj&display=swap');
        body, html ,center{
            margin: 0;
            padding: 0;
            height: 100%;
            font-family: 'Taviraj', serif;
        }

        .box-head{
            background-color: #ffa45c;
            width: 750px;
            height: 250px;
            text-align: center;
        }

        .box-head > img {
            margin: 30px 10px 10px 10px;
            width:  calc(100% - 350px);
            max-height: 350px;
        }

        .box-footer{
            background-color: #5d5d5a;
            width: 750px;
            height:75%;
            text-align: center;
            max-height:  100%;
        }
        .group-info , h1.title{
            display: inline-block;
            margin: 0 0;
            padding-top: 70px;
            color: white;
            max-width: 550px;
        }

        h1.title{
            font-size: 45px;
        }

        p.info{
            word-break: break-word;
        }


        p.register-date{
            font-size: 20px;
        }

        @media screen and (max-width: 991px) {
            .box-head{
                background-color: #ffa45c;
                width: auto;
                height: 200px;
                text-align: center;
            }

            .box-head > img {
                width: calc(100% - 500px);
            }

            .box-footer{
                width: auto;
            }
            .group-info , h1.title{
                padding-top: 100px;
            }

            h1.title{
                font-size: 50px;
            }

            p.info{
                font-size: 20px;
            }

            p.register-date{
                font-size: 20px;
            }

        }

        @media screen and (max-width: 767px) {
            .box-head{
                background-color: #ffa45c;
                width: auto;
                height: 200px;
                text-align: center;
            }

            .box-head > img {
                width: calc(100% - 250px);
            }

            .box-footer{
                width: auto;
            }
            .group-info , h1.title{
                padding-top: 100px;
            }

            h1.title{
                font-size: 45px;
            }

            p.info{
                font-size: 20px;
            }
            p.register-date{
                font-size: 20px;
            }

        }

        @media screen and (max-width: 479px){

            .box-head{
                background-color: #ffa45c;
                width: auto;
                height: 200px;
                text-align: center;
            }

            .box-head > img {
                width: calc(100% - 50px);
            }

            .box-footer{
                width: auto;
                max-height: 100%;

            }
            .group-info , h1.title{
                padding-top: 100px;

            }

            h1.title{
                font-size: 30px;
            }

            p.info{
                font-size: 20px;
            }

            p.register-date{
                font-size: 15px;
            }

        }

    </style>
</head>
<body>
        <center>
            <div class="box-head">
                <img src="{!!$message->embedData(
                                                  QrCode::format('png')
                                                  ->size(450)
                                                  ->generate(route('contractor.scanQR.openTask',['id' => $taskDetail -> task_id])),
                                                  'QrCode(สำหรับสแกนเข้างาน).png',
                                                  'image/png')!!}"
                >
            </div>
            <div class="box-footer">
                <div class="group-info">
                    <h1 class="title">{{$taskDetail -> task_title}}</h1>
                    <p class="info">{{$taskDetail -> task_descrition}}</p>
                    <p class="register-date"> วันที่เข้างาน :  {{ConvertThaiDate($taskDetail -> task_working_date)}}</p>
                    <p class="register-date"> ผู้ควบคุมงาน :  {{$taskDetail -> leader_perfix}}{{$taskDetail -> leader_first_name}} {{$taskDetail -> leader_last_name}}</p>
                    <p class="register-date"> หัวหน้าผู้รับเหมา : {{$taskDetail -> contractor_prefix}}{{$taskDetail -> contractor_first_name}} {{$taskDetail -> contractor_last_name}}</p>
                </div>
            </div>
        </center>
</body>
</html>
