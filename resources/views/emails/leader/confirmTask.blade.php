<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Taviraj&display=swap');
        html , body ,center{
            margin: 0;
            padding: 0;
            height: 100%;
            /*background: #5d5d5a;*/
            font-family: 'Taviraj', serif;
        }

        h1 , .success{
            font-size: 70px;
            color: #b2d430;
            margin: 70px;
        }

        h1 , .fail{
            font-size: 60px;
            color: #e2434b;
            margin: 70px;
        }
        p{
            word-break: break-word;
        }

        p , .register-date{
            font-size: 20px;
            color: #3e4a61;
            margin: 0;
        }

        p , .footer-warning{
            font-size: 20px;
            color: red;
            margin-top: 10px;
        }


        .grid-container{
            display: grid;
            grid-template-rows: 50% 50%;
            margin-top: 50px;
            width: 500px;
            height: 500px;
            border-radius: 10px;
            background-color: white;
        }

        .grid-item:nth-child(1){

        }

        .grid-item:nth-child(2){
            border-radius: 0 0 10px 10px;

        }

        table{
            margin: 20px 0 20px 0;

        }

         tr .tr-title{
             max-width: 30%;
             font-size: 20px;
             color: #4e413b;
        }

         td{
             max-width: 30%;
             font-size: 20px;
             color: #4e413b;
         }

         a{
            width: 250px;
            height: 25px;
            display: block;
            padding: 10px;
            background-color: #c3bf31;
            border-radius: 5px;
            color: #FFFFFF;
            text-decoration: none;
            text-align: center;
         }

        a:hover {
            background-color: #ccc806;
        }


        @media screen and (max-width: 991px){
            h1 , .success{
                font-size: 70px;
                margin: 70px;
            }

            h1 , .fail{
                font-size: 60px;
                margin: 70px;
            }
            p{
                word-break: break-word;
            }

            p , .register-date{
                font-size: 20px;
                margin: 0;
            }

            p , .footer-warning{
                font-size: 20px;
                margin-top: 10px;
            }


            .grid-container{
                margin-top: 50px;
                width: 500px;
                height: 500px;
            }

            .grid-item:nth-child(1){

            }

            .grid-item:nth-child(2){
                border-radius: 0 0 10px 10px;

            }

            table{
                margin: 20px 0 20px 0;

            }

            tr .tr-title{
                max-width: 30%;
                font-size: 20px;
            }

            td{
                max-width: 30%;
                font-size: 20px;
            }

            a{
                width: 250px;
                height: 25px;
                padding: 10px;
                color: #FFFFFF;
                text-decoration: none;
                text-align: center;
            }
        }

        @media screen and (max-width: 479px){
            h1 , .success{
                font-size: 40px;
                margin: 85px;
            }

            h1 , .fail{
                font-size: 30px;
                margin: 85px;
            }
            p{
                word-break: break-word;
            }

            p , .footer-warning{
                font-size: 15px;
                margin-top: 10px;
            }


            .grid-container{
                margin-top: 50px;
                width: auto;
                height: 450px;
            }

            .grid-item:nth-child(1){

            }

            .grid-item:nth-child(2){
                border-radius: 0 0 10px 10px;

            }

            table{
                margin: 10px 0 20px 0;

            }

            tr .tr-title{
                font-size: 15px;
            }

            td{
                font-size: 15px;
            }

            a{
                width: 250px;
                height: 25px;
                padding: 5px;
                color: white;
                text-decoration: none;
                text-align: center;
            }

        }

    </style>
</head>
<body>
        <center>
            <div class="grid-container">
                <div class="grid-item">
                    <h1 class="{{$status == 2 ? 'success' : 'fail'}}">{{$status == 2 ? 'ผ่านอนุมัติ' : 'ไม่ผ่านอนุมัติ'}}</h1>

                </div>
                <div class="grid-item">
                    <hr>
                    <table>
                        <tr>
                            <td align="right" class="tr-title"> หัวข้อ : </td>
                            <td align="left"> {{$task -> task_title}} </td>
                        </tr>
                        <tr>
                            <td align="right" class="tr-title"> รายละเอียด : </td>
                            <td align="left"> {{$task -> task_description}} </td>
                        </tr>
                        <tr>
                            <td align="right" class="tr-title"> หัวหน้าผู้รับเหมา : </td>
                            <td align="left">{{$task -> contractor_first_name}} {{$task -> contractor_last_name}} </td>
                        </tr>
                    </table>
                    @if($status == 3)
                        <a href="{{route('home')}}" target="_blank">ลงทะเบียนใหม่</a>
                    @endif
                    @if($status == 2)
                        <a href="{{route('admin.workPermit.pdf',['id' => $taskDetail_id])}}" target="_blank">ใบ Workpermit</a>
{{--                        <p class="footer-warning">*นำ QR Code ที่ได้รับทางอีเมล์ สแกน ณ หน้างานเท่านั้น</p>--}}
                    @endif
                </div>
            </div>
        </center>
</body>
</html>
