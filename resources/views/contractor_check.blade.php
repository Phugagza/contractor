@extends('layouts.paper')
@section('title','แบบตรวจสอบระหว่างปฏิบัติงาน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">แบบตรวจสอบระหว่างปฏิบัติงาน</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form method="POST" action="" id="check_form" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                แบบตรวจสอบระหว่างปฏิบัติงานครั้งที่ {{$contractorCheckTaskDetail -> time}}
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="5%">เลือก</th>
                                        <th class="text-center" width="50%">รายการตรวจสอบที่เกี่ยว</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="equipment_personal" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">1.สวมอุปกรณ์ป้องกันอันตรายส่วนบุคคล</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="safety_glasses" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">2.แว่นตานิรภัย (Safety glasses)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="safety_belt" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">3.เข็มขัดนิรภัย (Safety belt)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="scba" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">4.อุปกรณ์ช่วยหายใจ (SCBA)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="oximeter" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">5.เครื่องวัดออกซิเจนครบถ้วน</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="fan" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">6.พัดลมระบายอากาศครบถ้วน</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="acid_mask" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">7.หน้ากากป้องกันไอกรด (Acid fume protecting mash)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="sling" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">8.สลิงอยู่ในสภาพพรอ้มใช้งาน (เป็นไปตามมาตรฐาน NS-SUS)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="electric1" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">9.เครื่องเชื่อมไฟฟ้า</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="electric1_1" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">9.1.สภาพสายไฟ(ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด,จุดต่อต้องมั่นคงแข็งแรง)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="electric1_2" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">9.1.สภาพสาย Ground (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด,จุดต่อต้องมั่นคงแข็งแรง)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="cut_gas1" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">10.เครื่องเชื่อม ตัด เผา Gas</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="cut_gas1_1" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">10.1.Gauge ไม่แตก / สามารถอ่านค่าได ้</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="cut_gas1_2" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">10.2. ต้องมีอุปกรณ์ป้องกันไฟย้อนกลับ (Flash back Arrestor) 4 ตำแหน่ง
                                            และมีสติกเกอร์แสดงการตรวจสอบติดอยู่</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="cut_gas1_3" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">10.3.สาย GAS อยู่ในสภาพดีไม่แตกลายงา และมีการยึดสายกับข้อต่อที่ถัง GAS
                                            และ หัว GAS แน่นหนา ไม่รั่วซึม</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="cut_gas1_4" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">10.4. จุดยึดถัง GAS ต้องมันคงแข็งแรงและมีโซ่หรือเชือกมัดถัง ไม่เสี่ยงต่อการล้ม
                                            ของถัง GAS</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="cut_hand1" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">11.เครื่องตัด เจียรมือ</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="cut_hand1_1" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">11.1.สภาพสายไฟ (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด, จุดต่อต้องมั่นคงแข็งแรง)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="cut_hand1_2" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">11.2.สภาพสาย Ground (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด, จุดต่อต้องมั่นคงแข็งแรง)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="cut_hand1_3" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">11.3. มี Guard ป้องกัน (Guard ต้องยึดอย่างมั่นคง ไม่หลวมคลอน)</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="fire_ready" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">12.อุปกรณ์ดับเพลิงมีพร้อมและตู้น้ำดับเพลิงอยู่ในสภาพพร้อมใช้งาน</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="fire_amount" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">13.จำนวนถังดับเพลิงมีครบถ้วน และตรวจสอบล่าสุด</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="fire_resis" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">14.มีการจัดเตรียมแผ่นวัสดุทนไฟ สำหรับป้องกันสะเก็ดไฟกระเด็น
                                            (วัสดุป้องกันสะเก็ดไฟต้องไม่ติดไฟ)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" name="safety" value="1" type="checkbox">
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">15.มีสภาพการปฏิบัตงานที่ปลอดภัย

                                        </td>
                                    </tr>
                                    <tr class="table-warning">
                                        <td class="text-center">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label font-weight-bold">
                                                    <input class="form-check-input" type="checkbox"  id="contractor" name="contractor" value="1" required>
                                                    <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="font-weight-bold">หัวหน้างานผู้รับเหมายืนยัน

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" id="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    <script>
        $(document).ready(function() {

            $("#check_form").on("submit", function(){
                let contractor_confirm = document.getElementById("contractor").checked;
                if(!contractor_confirm){
                    Swal.fire({
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'ตกลง',
                        title: 'กรุณาเลือกยืนยันช่องสุดท้าย',
                    })
                }

                if(contractor_confirm){
                    Swal.fire({
                        title: 'กรุณารอบันทึกข้อมูลสักครู่',
                        onOpen: () => {
                            swal.showLoading();
                        }
                    })
                    document.getElementById('submit').className = "btn btn-primary btn-lg disabled"
                }

            });//submit

        });

    </script>
@stop
