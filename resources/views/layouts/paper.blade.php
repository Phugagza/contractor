<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/ns.png')}}">
    <link rel="icon" type="image/png" href="{{asset('img/ns.png')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @yield('title')
    </title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <!-- CSS Files -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/paper-dashboard.css')}}" rel="stylesheet" />
    <script src="{{asset('js/core/jquery.min.js')}}" type="text/javascript"></script>
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
    <script src=" {{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
    {{--select2--}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
</head>
<body>
<div class="wrapper ">
    <div class="sidebar" data-color="brown" data-active-color="danger">
        <div class="logo">
            <a href="#" class="simple-text logo-mini">
                <div class="logo-image-small">
                    <img src="{{asset('img/ns.jpg')}}">
                </div>
            </a>
            <a href="{{route('home')}}" class="simple-text logo-normal">
                @if(Auth::guard('admin')->check())
                    @if(Auth::guard('admin')->user()->level == 2)
                        ผู้ฝึกอบรม
                    @else
                        แอดมิน
                    @endif
                @elseif(Auth::guard('leader')->check())
                    ผู้ควบคุมงาน
                @else
                    ผู้รับเหมา
                @endif
            </a>
        </div>
        <div class="sidebar-wrapper">
            <div class="user">
                <div class="photo">
                    <img src="{{asset('img/ns.png')}}" />
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                        <span>
                             @if(Auth::guard('admin')->check())
                                {{Auth::guard('admin')->user()->first_name}}
                            @elseif(Auth::guard('leader')->check())
                                {{Auth::guard('leader')->user()->first_name}}
                            @else
                                 ระบบลงทะเบียน
                            @endif
                        </span>
                    </a>
                </div>
            </div>
            @if(Auth::guard('admin')->check())
                <ul class="nav">
                    @if(Auth::guard('admin')->user()->level == 3)
                    <li class="{{Request::is('admin/contractor*') ? 'active' : ''}}">
                        <a data-toggle="collapse" href="#editContractor" aria-expanded="true">
                            <i class="nc-icon nc-single-02"></i>
                            <p>
                                ผู้รับเหมา
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{Request::is('admin/contractor*') ? 'show' : ''}}" id="editContractor">
                            <ul class="nav">
                                <li class="{{Request::is('admin/contractor') || Request::is('admin/contractor/edit*') ? 'active' : ''}}">
                                    <a href="{{route('admin.contractor.index')}}">
                                        <span class="sidebar-mini-icon">M</span>
                                        <span class="sidebar-normal"> จัดการข้อมูลผู้รับเหมา </span>
                                    </a>
                                </li>
                                <li class="{{Request::is('admin/contractor/enterprise*') ? 'active' : ''}}">
                                    <a href="{{route('admin.contractor.enterprise.index')}}">
                                        <span class="sidebar-mini-icon">E</span>
                                        <span class="sidebar-normal"> จัดการข้อมูลบริษัทผู้รับเหมา </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="{{Request::is('admin/leader*') ? 'active' : ''}}">
                        <a data-toggle="collapse" href="#editLeader" aria-expanded="true">
                            <i class="nc-icon nc-circle-10"></i>
                            <p>
                                ผู้ควบคุมงาน
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{Request::is('admin/leader*') ? 'show' : ''}}" id="editLeader">
                            <ul class="nav">
                                <li class="{{Request::is('admin/leader*') ? 'active' : ''}}">
                                    <a href="{{route('admin.leader.index')}}">
                                        <span class="sidebar-mini-icon">M</span>
                                        <span class="sidebar-normal"> จัดการข้อมูลผู้ควบคุมงาน </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="{{Request::is('admin/admin*') ? 'active' : ''}}">
                        <a data-toggle="collapse" href="#editAdmin" aria-expanded="true">
                            <i class="nc-icon nc-circle-10"></i>
                            <p>
                                แอดมิน
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{Request::is('admin/admin*') ? 'show' : ''}}" id="editAdmin">
                            <ul class="nav">
                                <li class="{{Request::is('admin/admin*') ? 'active' : ''}}">
                                    <a href="{{route('admin.admin.index')}}">
                                        <span class="sidebar-mini-icon">M</span>
                                        <span class="sidebar-normal"> จัดการข้อมูลแอดมิน </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @endif
                    @if(Auth::guard('admin')->user()->level == 2 || Auth::guard('admin')->user()->level == 3)
                    <li class="{{Request::is('admin/trainer*') ? 'active' : ''}}">
                        <a data-toggle="collapse" href="#editTrainer" aria-expanded="true">
                            <i class="nc-icon nc-circle-10"></i>
                            <p>
                                ผู้ฝึกอบรม
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{Request::is('admin/trainer*') ? 'show' : ''}}" id="editTrainer">
                            <ul class="nav">
                                <li class="{{Request::is('admin/trainer*') ? 'active' : ''}}">
                                    <a href="{{route('admin.trainer.index')}}">
                                        <span class="sidebar-mini-icon">M</span>
                                        <span class="sidebar-normal"> จัดการข้อมูลผู้ฝึกอบรม </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @endif
                    @if(Auth::guard('admin')->user()->level == 3)
                    <li class="{{Request::is('admin/mail_list*') ? 'active' : ''}}">
                        <a data-toggle="collapse" href="#editMailList" aria-expanded="true">
                            <i class="nc-icon nc-email-85"></i>
                            <p>
                                อีเมล์
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{Request::is('admin/mail_list*') ? 'show' : ''}}" id="editMailList">
                            <ul class="nav">
                                <li class="{{Request::is('admin/mail_list*') ? 'active' : ''}}">
                                    <a href="{{route('admin.mail.list.index')}}">
                                        <span class="sidebar-mini-icon">M</span>
                                        <span class="sidebar-normal"> อีเมล์สำหรับส่งใบ Workpermit </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="{{Request::is('admin/task*') ? 'active' : ''}}">
                        <a data-toggle="collapse" href="#editTask" aria-expanded="true">
                            <i class="nc-icon nc-tap-01"></i>
                            <p>
                                งาน
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{Request::is('admin/task*') ? 'show' : ''}}" id="editTask">
                            <ul class="nav">
                                <li class="{{Request::is('admin/task/all') || Request::is('admin/task/all/qecode/*') || Request::is('admin/task/detail*') || Request::is('admin/task/resultTaskDetail*') || Request::is('admin/task/edit*')? 'active' : ''}}">
                                    <a href="{{route('admin.task.allTask')}}">
                                        <span class="sidebar-mini-icon">T</span>
                                        <span class="sidebar-normal"> งานที่ลงทะเบียนทั้งหมด </span>
                                    </a>
                                </li>
                                <li class="{{Request::is('admin/task/workpermit') ? 'active' : ''}}">
                                    <a href="{{route('admin.task.index')}}">
                                        <span class="sidebar-mini-icon">M</span>
                                        <span class="sidebar-normal"> งานที่ได้รับอนุมัติ </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @endif
                    @if(Auth::guard('admin')->user()->level == 2)
{{--                    <li class="{{Request::is('admin/training*') ? 'active' : ''}}">--}}
{{--                        <a data-toggle="collapse" href="#editTraining" aria-expanded="true">--}}
{{--                            <i class="nc-icon nc-bullet-list-67"></i>--}}
{{--                            <p>--}}
{{--                                การอบรม--}}
{{--                                <b class="caret"></b>--}}
{{--                            </p>--}}
{{--                        </a>--}}
{{--                        <div class="collapse {{Request::is('admin/training/index') || Request::is('admin/training/edit*') || Request::is('admin/training/detail*') ? 'show' : ''}}" id="editTraining">--}}
{{--                            <ul class="nav">--}}
{{--                                <li class="{{Request::is('admin/training/index') || Request::is('admin/training/edit*') || Request::is('admin/training/detail*') ? 'active' : ''}}">--}}
{{--                                    <a href="{{route('admin.training.index')}}">--}}
{{--                                        <span class="sidebar-mini-icon">T</span>--}}
{{--                                        <span class="sidebar-normal"> ข้อมูลการอบรม </span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </li>--}}
                            <li class="{{Request::is('admin/safetyTraining*') ? 'active' : ''}}">
                                <a href="{{route('admin.trainer.safetyTraining.index')}}">
                                    <i class="nc-icon nc-calendar-60"></i>
                                    <p>Safety Training ผู้รับเหมา</p>
                                </a>
                            </li>
                    @endif
                    @if(Auth::guard('admin')->user()->level == 3)
                    <li class="{{Request::is('admin/export*') ? 'active' : ''}}">
                        <a data-toggle="collapse" href="#editExpost" aria-expanded="true">
                            <i class="nc-icon nc-single-copy-04"></i>
                            <p>
                                Export
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{Request::is('admin/export*') ? 'show' : ''}}" id="editExpost">
                            <ul class="nav">
                                <li class="{{Request::is('admin/export/task/index') ? 'active' : ''}}">
                                    <a href="{{route('admin.export.task.index')}}">
                                        <span class="sidebar-mini-icon">T</span>
                                        <span class="sidebar-normal"> Export ข้อมูลงาน </span>
                                    </a>
                                </li>
                                <li class="{{Request::is('admin/export/contractor/index') ? 'active' : ''}}">
                                    <a href="{{route('admin.export.contractor.index')}}">
                                        <span class="sidebar-mini-icon">C</span>
                                        <span class="sidebar-normal"> Export ข้อมูลผู้รับเหมา </span>
                                    </a>
                                </li>
                                <li class="{{Request::is('admin/export/leader/index') ? 'active' : ''}}">
                                    <a href="{{route('admin.export.leader.index')}}">
                                        <span class="sidebar-mini-icon">C</span>
                                        <span class="sidebar-normal"> Export ข้อมูลผู้ควบคุมงาน </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif
                </ul>
            @elseif(Auth::guard('leader')->check())
                <nav class="nav">
                    <li class="{{Request::is('leader/task*') || Request::is('leader/export*')  || Request::is('/') ? 'active' : ''}}">
                        <a data-toggle="collapse" href="#confirmTask" aria-expanded="true">
                            <i class="nc-icon nc-single-copy-04"></i>
                            <p>
                                งาน
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{Request::is('leader/task*') || Request::is('leader/export*') || Request::is('/')   ? 'show' : ''}}" id="confirmTask">
                            <ul class="nav">
                                <li class="{{Request::is('/') || Request::is('register') ? 'active' : ''}}">
                                    <a href="{{route('home')}}">
                                        <span class="sidebar-mini-icon">R</span>
                                        <span class="sidebar-normal"> ลงทะเบียน </span>
                                    </a>
                                </li>
                                <li class="{{Request::is('leader/task/contractor*') || Request::is('leader/task/checkTaskDetail*') || Request::is('leader/task/resultTaskDetail/*') || Request::is('leader/task/check*') ? 'active' : ''}}">
                                    <a href="{{route('leader.task.index')}}">
                                        <span class="sidebar-mini-icon">M</span>
                                        <span class="sidebar-normal"> งานที่ลงทะเบียน </span>
                                    </a>
                                </li>
                                <li class="{{Request::is('leader/task/confirm*') ? 'active' : ''}}">
                                    <a href="{{route('leader.task.confirm.index')}}">
                                        <span class="sidebar-mini-icon">C</span>
                                        <span class="sidebar-normal"> ยืนยันการเข้างาน </span>
                                    </a>
                                </li>
                                <li class="{{Request::is('leader/task/finish*') ? 'active' : ''}}">
                                    <a href="{{route('leader.task.finish.index')}}">
                                        <span class="sidebar-mini-icon">F</span>
                                        <span class="sidebar-normal"> ยืนยันการปิดงาน </span>
                                    </a>
                                </li>
                                <li class="{{Request::is('leader/task/all') || Request::is('leader/task/all/qecode/*') || Request::is('leader/task/detail*') || Request::is('leader/task/resultTaskDetailGroup*') || Request::is('leader/task/edit*')? 'active' : ''}}">
                                    <a href="{{route('leader.task.allTask')}}">
                                        <span class="sidebar-mini-icon">T</span>
                                        <span class="sidebar-normal"> งานที่ลงทะเบียนทั้งหมด </span>
                                    </a>
                                </li>
                                <li class="{{Request::is('leader/task/workpermit') ? 'active' : ''}}">
                                    <a href="{{route('leader.task.workpermit')}}">
                                        <span class="sidebar-mini-icon">M</span>
                                        <span class="sidebar-normal"> งานที่ได้รับอนุมัติ </span>
                                    </a>
                                </li>
                                <li class="{{Request::is('leader/export/task/index') ? 'active' : ''}}">
                                    <a href="{{route('leader.export.task.index')}}">
                                        <span class="sidebar-mini-icon">T</span>
                                        <span class="sidebar-normal"> Export ข้อมูลงาน </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </nav>
            @else
                <nav class="nav">
                    <li class="{{Request::is('/') || Request::is('register') ? 'active' : ''}}">
                        <a href="{{route('home')}}">
                            <i class="nc-icon nc-paper"></i>
                            <p>ลงทะเบียน</p>
                        </a>
                    </li>
                    <li class="{{Request::is('searchSafetyTraining') ? 'active' : ''}}">
                        <a href="{{route('searchSafetyTraining')}}">
                            <i class="nc-icon nc-calendar-60"></i>
                            <p>ตรวจสอบการอบรม</p>
                        </a>
                    </li>
{{--                    <li class="{{Request::is('searchTraining') ? 'active' : ''}}">--}}
{{--                        <a href="{{route('searchTraining')}}">--}}
{{--                            <i class="nc-icon nc-calendar-60"></i>--}}
{{--                            <p>ตรวจสอบการอบรมอื่นๆ</p>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </nav>
            @endif
        </div>
    </div>
</div>
<div class="main-panel">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-icon btn-round">
                        <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
                        <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
                    </button>
                </div>
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <a class="navbar-brand" href="#pablo"></a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
                <span class="navbar-toggler-bar navbar-kebab"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                <ul class="navbar-nav">
                    <li class="nav-item btn-rotate dropdown">
                        <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="nc-icon nc-settings-gear-65"></i>
                            <p>
                                <span class="d-lg-none d-md-block">ระบบ</span>
                            </p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            @if(Auth::guard('admin')->check())
                            <a class="dropdown-item" href="#" onclick="event.preventDefault();document.querySelector('#admin-logout-form').submit();">ออกจากระบบ ({{Auth::guard('admin')->user()->first_name}})</a>
                            <form id="admin-logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            @elseif(Auth::guard('leader')->check())
                                <a class="dropdown-item" href="#" onclick="event.preventDefault();document.querySelector('#leader-logout-form').submit();">ออกจากระบบ ({{Auth::guard('leader')->user()->first_name}}) </a>
                                <form id="leader-logout-form" action="{{ route('leader.logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            @else
                                <a class="dropdown-item" href="{{route('admin.login')}}">สำหรับพนักงาน</a>
                            @endif
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <!-- <div class="panel-header panel-header-sm">


</div> -->
    @yield('content')

</div>

<!--   Core JS Files   -->
<script src="{{asset('js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/core/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/plugins/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/plugins/moment.min.js')}}"></script>
<!-- Forms Validations Plugin -->
<script src="{{asset('js/plugins/jquery.validate.min.js')}}" ></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="{{asset('js/plugins/jquery.bootstrap-wizard.js')}}"></script>
<!--    Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="{{asset('js/plugins/bootstrap-selectpicker.js')}}" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src=" {{asset('js/plugins/bootstrap-switch.js')}}"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src=" {{asset('js/plugins/bootstrap-datetimepicker.js')}}"></script>
<!--    Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src=" {{asset('js/plugins/bootstrap-tagsinput.js')}}"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src=" {{asset('js/plugins/jasny-bootstrap.min.js')}}"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src=" {{asset('js/plugins/fullcalendar.min.js')}}"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src=" {{asset('js/plugins/jquery-jvectormap.js')}}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src=" {{asset('js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
<!--  Google Maps Plugin
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>-->
<!-- Chart JS -->
<script src=" {{asset('js/plugins/chartjs.min.js')}}"></script>
<!--  Notifications Plugin    -->
<script src=" {{asset('js/plugins/bootstrap-notify.js')}}"></script>
<!-- Control Center for Paper Dashboard -->
<script src=" {{asset('js/paper-dashboard.js?v=2.0.1')}}" type="text/javascript"></script>
<!--  Plugin for Sweet Alert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.29.0/dist/sweetalert2.all.min.js"></script>
<!-- <script src="{{asset('js/plugins/sweetalert2.min.js')}}"></script> -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"  >
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>

<script src="{{asset('demo/demo.js')}}"></script>
</body>
</html>
