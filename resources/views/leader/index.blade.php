@extends('layouts.paper')
@section('title','จัดการการลงทะเบียน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <div class="card mt-5">
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="10%">ชื่องาน</th>
                                                <th class="text-center" width="10%">QR Code</th>
                                                <th class="text-center" width="10%">เข้างานล่าสุด</th>
                                                <th class="text-center" width="10%">วันที่ทำการลงทะเบียน</th>
                                                <th class="text-center" width="20%">หัวหน้าผู้รับเหมา</th>
                                                <th class="text-center" width="5%">สถานะ QR Code</th>
                                                <th class="text-center" width="5%">สถานะล่าสุด</th>
                                                <th class="text-center" class="disabled-sorting text-center" width="15%">Action</th>
                                                <th class="text-center" class="disabled-sorting text-center" width="5%">ยกเลิก</th>
                                                <th class="text-center" class="disabled-sorting text-center" width="5%">ลบ</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($tasks as $index => $task)
                                                <tr>
                                                    <td class="text-center"> {{$task -> task_title}}</td>
                                                    <td class="text-center">
                                                        <a href="{{route('leader.task.allTask.qrcode',['id'=> $task -> task_id])}}"
                                                           class="btn btn-primary"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="ดู">
                                                            <i class="nc-icon nc-zoom-split"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-center"> {{$task -> register_task_detail_date ? ConvertThaiDate($task -> register_task_detail_date) : '-'}}</td>
                                                    <td class="text-center"> {{ConvertThaiDate($task -> task_register_date)}}</td>
                                                    <td class="text-center"> {{$task -> contractor_prefix}}{{$task -> contractor_first_name}} {{$task -> contractor_last_name}}</td>
                                                    <td class="text-center">
                                                        @if($task -> task_status == 1)
                                                            <h5>
                                                                <span class="badge badge-success">QR OK</span>
                                                            </h5>
                                                        @elseif($task -> task_status == 2)
                                                            <h5>
                                                                <span class="badge badge-danger">QR Close</span>
                                                            </h5>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        @if($task -> task_detail_status == 9)
                                                            <h5>
                                                                <span class="badge badge-{{config("constants.task_detail.status.9.color")}}">{{config("constants.task_detail.status.9.status_name")}}</span>
                                                            </h5>
                                                        @elseif($task -> task_type == null)
                                                            <h5>
                                                                <span class="badge badge-{{config("constants.task_detail.status.registered.color")}}">{{config("constants.task_detail.status.registered.status_name")}}</span>
                                                            </h5>
                                                        @elseif($task -> task_type != null && $task -> task_detail_status == null)
                                                            <h5>
                                                                <span class="badge badge-{{config("constants.task_detail.status.5rule.color")}}">{{config("constants.task_detail.status.5rule.status_name")}}</span>
                                                            </h5>
                                                        @elseif($task -> task_type != null && $task -> task_detail_status != null)
{{--                                                            @if($task -> task_detail_status == config("constants.task_detail.status.{$task -> task_detail_status}"))--}}
                                                                <h5>
                                                                    <span class="badge badge-{{config("constants.task_detail.status.{$task -> task_detail_status}.color")}}">{{config("constants.task_detail.status.{$task -> task_detail_status}.status_name")}}</span>
                                                                </h5>
{{--                                                            @elseif($task -> task_detail_status == 2)--}}
{{--                                                                <h5>--}}
{{--                                                                    <span class="badge badge-primary">อนุมัติ</span>--}}
{{--                                                                </h5>--}}
{{--                                                            @elseif($task -> task_detail_status == 3)--}}
{{--                                                                <h5>--}}
{{--                                                                    <span class="badge badge-primary">เริ่มทำงาน</span>--}}
{{--                                                                </h5>--}}
{{--                                                            @elseif($task -> task_detail_status == 4)--}}
{{--                                                                <h5>--}}
{{--                                                                    <span class="badge badge-warning">ตรวจสอบ 1</span>--}}
{{--                                                                </h5>--}}
{{--                                                            @elseif($task -> task_detail_status == 5)--}}
{{--                                                                <h5>--}}
{{--                                                                    <span class="badge badge-warning">ตรวจสอบ 2</span>--}}
{{--                                                                </h5>--}}
{{--                                                            @elseif($task -> task_detail_status == 6)--}}
{{--                                                                <h5>--}}
{{--                                                                    <span class="badge badge-warning">ตรวจสอบปิดงาน</span>--}}
{{--                                                                </h5>--}}
{{--                                                            @elseif($task -> task_detail_status == 7)--}}
{{--                                                                <h5>--}}
{{--                                                                    <span class="badge badge-success">ปิดงานสำเร็จ</span>--}}
{{--                                                                </h5>--}}
{{--                                                            @elseif($task -> task_detail_status == 8)--}}
{{--                                                                <h5>--}}
{{--                                                                    <span class="badge badge-danger">ปิดงานไม่สำเร็จ</span>--}}
{{--                                                                </h5>--}}
{{--                                                            @elseif($task -> task_detail_status == 9)--}}
{{--                                                                <h5>--}}
{{--                                                                    <span class="badge badge-danger">ยกเลิก</span>--}}
{{--                                                                </h5>--}}
{{--                                                            @endif--}}
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="{{route('leader.task.check',['id'=> $task -> task_id])}}"
                                                           class="btn btn-success btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="แบบตรวจสอบผู้ควบคุมงาน">
                                                            <i class="nc-icon nc-paper"></i>
                                                        </a>
                                                        <a href="{{route('leader.task.detail',['id'=> $task -> task_id])}}"
                                                           class="btn btn-secondary btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="ดูรายละเอียดงาน">
                                                            <i class="nc-icon nc-bullet-list-67"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-center">
                                                        @if($task -> task_detail_status == null)
                                                            <a href="#"
                                                               onclick="cancelTask({{$task -> task_id}}); return false"
                                                               class="btn btn-warning btn-icon btn-sm like"
                                                               id="hover"
                                                               data-toggle="tooltip"
                                                               data-placement="top"
                                                               title="ยกเลิกงาน">
                                                                <i class="nc-icon nc-refresh-69"></i>
                                                            </a>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#"
                                                           class="btn btn-danger btn-icon btn-sm like"
                                                           id="hover"
                                                           onclick="DeleteTask({{$task -> task_id}}); return false"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="ลบข้อมูลลงทะเบียน">
                                                            <i class="nc-icon nc-simple-remove"></i>
                                                        </a>
                                                    </td>
                                                </tr>

                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <script>
        $('#datatable').DataTable({
            responsive: true,
            "order": [[ 3, "desc" ]]
        });

        DeleteTask=(id)=>{
            Swal.fire({
                title: 'ต้องการลบข้อมูลใช่หรือไม่ ?',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#c8d2dd',
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if(result.dismiss == 'cancel'){
                    Swal.hideLoading();
                }else if(result.dismiss == 'overlay'){
                    Swal.hideLoading();
                } else{
                    Swal.fire({
                        title: 'กรุณารอสักครู่',
                        onOpen: () => {
                            swal.showLoading();
                        }
                    })
                }
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url: "{{ route('leader.task.delete') }}",
                        method: 'post',
                        data: {
                            id: id
                        },
                        success: function(response){
                            if (response.success == 'Yes') {
                                Swal.fire({
                                    type: 'success',
                                    title: 'บันทึกข้อมูลเรียบร้อย',
                                    showConfirmButton: true,
                                    confirmButtonText: "OK!"
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                            else if(response.success == 'No'){
                                Swal.fire({
                                    type: 'error',
                                    title: 'ล้มเหลว',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({
                                    type: 'error',
                                    title: 'เกิดข้อผิดพลาด',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
        cancelTask= async (id)=>{
            const  { value: cause } = await Swal.fire({
                title: 'ต้องการยกเลิกงานใช่หรือไม่',
                showCancelButton: true,
                cancelButtonText: 'ยกเลิก',
                input: 'text',
                inputPlaceholder: 'สาเหตุ',
                inputAttributes: {
                    maxlength: 255,
                    required: true
                },
                inputValidator: (value) => {
                    if (!value) {
                        return 'โปรดระบุสาเหตุ'
                    }
                }
            })
            console.log(cause)
            if (cause) {
                Swal.fire({
                    title: `สาเหตุที่ต้องการยกเลิก \n ${cause}`,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#c8d2dd',
                    confirmButtonText: 'ใช่',
                    cancelButtonText: 'ยกเลิก',
                }).then((result) => {
                    console.table(result)
                    if(result.dismiss == 'cancel'){
                        Swal.hideLoading();
                    }else if(result.dismiss == 'overlay'){
                        Swal.hideLoading();
                    } else{
                        Swal.fire({
                            title: 'กรุณารอสักครู่',
                            onOpen: () => {
                                swal.showLoading();
                            }
                        })
                    }
                    console.table(result.value)
                    if (result.value === true) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        jQuery.ajax({
                            url: "{{ route('leader.task.cancel') }}",
                            method: 'post',
                            data: {
                                id: id,
                                cause: cause ? cause : ''
                            },
                            success: function(response){
                                console.log(response.success)
                                if (response.success == 'Yes') {
                                    Swal.fire({
                                        type: 'success',
                                        title: 'บันทึกข้อมูลเรียบร้อย',
                                        showConfirmButton: true,
                                        confirmButtonText: "OK!"
                                    }).then((result) => {
                                        if (result.value) {
                                            location.reload();
                                        }
                                    });
                                }
                                else if(response.success == 'No'){
                                    Swal.fire({
                                        type: 'error',
                                        title: 'ล้มเหลว',
                                        text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                        timer: 2000,
                                        showConfirmButton: false
                                    }).then((result) => {
                                        if (result.value) {
                                            location.reload();
                                        }
                                    });
                                }else{
                                    Swal.fire({
                                        type: 'error',
                                        title: 'เกิดข้อผิดพลาด',
                                        text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                        timer: 2000,
                                        showConfirmButton: false
                                    }).then((result) => {
                                        if (result.value) {
                                            location.reload();
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
        }

    </script>
@stop
