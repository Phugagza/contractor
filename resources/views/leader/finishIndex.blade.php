@extends('layouts.paper')
@section('title','ยืนยันการปิดงาน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">ยืนยันการปิดงาน</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <div class="card mt-5">
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="20%">ชื่องาน</th>
                                                <th class="text-center" width="10%" class="disabled-sorting text-center">ปิดงาน</th>
                                                <th class="text-center" width="20%">หัวหน้าผู้รับเหมา</th>
                                                <th class="text-center" width="10%">No.</th>
                                                <th class="text-center" width="10%">สถานะ</th>
                                                <th class="text-center" width="20%">วันที่สแกนเข้างาน</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($taskDetails as $taskDetail)
                                                <tr>
                                                    <td class="text-center"> {{$taskDetail -> task_title}}</td>
                                                    <td class="text-center">
                                                        <a href="{{route('leader.task.finish',['id' => $taskDetail -> taskDetail_id])}}"
                                                           class="btn btn-success btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="ตรวจสอบ">
                                                            <i class="nc-icon nc-paper"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-center">
                                                        {{$taskDetail -> contractor_prefix}}{{$taskDetail -> contractor_first_name}} {{$taskDetail -> contractor_last_name}}
                                                    </td>
                                                    <td class="text-center"> {{$taskDetail -> taskDetail_id}}</td>
                                                    <td class="text-center">
                                                        @if($taskDetail -> task_status == 6)
                                                            <h5>
                                                                <span class="badge badge-warning">ตรวจสอบปิดงาน</span>
                                                            </h5>
                                                        @endif
                                                    </td>
                                                    <td class="text-center"> {{ConvertThaiDate($taskDetail -> task_scan_date)}}</td>


                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <script>
        $('#datatable').DataTable({
            responsive: true,
        });

        finishTask=(taskDetail_id)=>
        {
            Swal.fire({
                title: 'ปิดงานใช่หรือไม่ ?',
                type: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#c8d2dd',
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ยกเลิก',
            }).then((result) => {
                if(result.dismiss == 'cancel'){
                    Swal.hideLoading();
                }else if(result.dismiss == 'overlay'){
                    Swal.hideLoading();
                } else{
                    Swal.fire({
                        title: 'กรุณารอสักครู่',
                        onOpen: () => {
                            swal.showLoading();
                        }
                    })
                }
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url: "{{ route('leader.task.finish') }}",
                        method: 'post',
                        data: {
                            taskDetail_id: taskDetail_id,
                            status: 5
                        },
                        success: function(response){
                            if (response.success == 'Yes') {
                                Swal.fire({
                                    type: 'success',
                                    title: 'บันทึกข้อมูลเรียบร้อย',
                                    showConfirmButton: true,
                                    confirmButtonText: "OK!"
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                            else if(response.success == 'No'){
                                Swal.fire({
                                    type: 'error',
                                    title: 'ล้มเหลว',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({
                                    type: 'error',
                                    title: 'เกิดข้อผิดพลาด',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }

        cancelTask=(taskDetail_id)=>
        {
            Swal.fire({
                title: 'ปิดงานไม่สำเร็จใช่หรือไม่ ?',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#c8d2dd',
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if(result.dismiss == 'cancel'){
                    Swal.hideLoading();
                }else if(result.dismiss == 'overlay'){
                    Swal.hideLoading();
                } else{
                    Swal.fire({
                        title: 'กรุณารอสักครู่',
                        onOpen: () => {
                            swal.showLoading();
                        }
                    })
                }
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url: "{{ route('leader.task.finish') }}",
                        method: 'post',
                        data: {
                            taskDetail_id: taskDetail_id,
                            // task_type: "",
                            status: 8
                        },
                        success: function(response){
                            if (response.success == 'Yes') {
                                Swal.fire({
                                    type: 'success',
                                    title: 'บันทึกข้อมูลเรียบร้อย',
                                    showConfirmButton: true,
                                    confirmButtonText: "OK!"
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                            else if(response.success == 'No'){
                                Swal.fire({
                                    type: 'error',
                                    title: 'แพ็คเกจนี้มีการใช้งานอยู่',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({
                                    type: 'error',
                                    title: 'เกิดข้อผิดพลาด',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    </script>
@stop
