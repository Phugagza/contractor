@extends('layouts.paper')
@section('title','สรุปแบบตรวจสอบระหว่างปฏิบัติงาน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item" aria-current="page">รายละเอียดการทำงาน</li>
                <li class="breadcrumb-item" aria-current="page">
                    {{$getTask -> task_title}}
                    (Work Controller : {{$getTask -> leader_prefix.$getTask -> leader_first_name." ".$getTask -> leader_last_name}})
                </li>
                <li class="breadcrumb-item active" aria-current="page">สรุปแบบตรวจสอบระหว่างปฏิบัติงาน(No.{{$checkResultView[0] -> task_detail_id}})</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card-header ">
                        <h5 class="m-md-2">
                            ผู้รับเหมา
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" width="5%">ลำดับ</th>
                                    <th class="text-center" width="50%">รายการ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">1.</td>
                                    <td>ผู้ขออนุญาต(หัวหน้าผู้รับเหมา):&nbsp;
                                        <span class="font-weight-bold">
                                            {{$checkResultView[0] -> contractor_prefix.$checkResultView[0] -> contractor_first_name." ".$checkResultView[0] -> contractor_last_name}}
                                        </span>
                                        &nbsp;เบอร์โทร:&nbsp;
                                        <span class="font-weight-bold">
                                            {{$checkResultView[0] -> contractor_tel ? $checkResultView[0] -> contractor_tel : '-'}}
                                        </span>
                                        &nbsp;บริษัท:&nbsp;
                                        <span class="font-weight-bold">
                                            {{$checkResultView[0] -> enterprise_name ? $checkResultView[0] -> enterprise_name : '-'}}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">2.</td>
                                    <td>งานที่ปฏิบัติ:&nbsp;
                                        <span class="font-weight-bold">
                                            {{$checkResultView[0] -> task_title}}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">3.</td>
                                    <td>สถานที่ปฏิบัติงาน:&nbsp;
                                        <span class="font-weight-bold">
                                            {{$checkResultView[0] -> task_location}}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">4.</td>
                                    <td>รายละเอียดงาน:&nbsp;
                                        <span class="font-weight-bold">
                                            {{$checkResultView[0] -> task_description ? $checkResultView[0] -> task_description : "-"}}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">5.</td>
                                    <td>วันที่ปฏิบัติ:&nbsp;
                                        <span class="font-weight-bold">
                                            {{ConvertThaiDate($checkResultView[0] -> task_detail_date)}}
                                        </span>
                                        ช่วงเวลาที่ปฏิบัติงาน:&nbsp;
                                        <span class="font-weight-bold">
                                            {{ConvertThaiDateHour($checkResultView[0] -> task_detail_start)}}
                                        </span>
                                        &nbsp;ถึง&nbsp;
                                        <span class="font-weight-bold">
                                            {{ConvertThaiDateHour($checkResultView[0] -> task_detail_end)}}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">6.</td>
                                    <td>ประเภทงานตาม License:&nbsp;
                                        <span class="font-weight-bold">
                                            {{$checkResultView[0] -> task_type ? config("constants.task_type.{$checkResultView[0] -> task_type}")  : "อื่นๆ"}}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">7.</td>
                                    <td>จำนวนผู้ปฏิบัติงาน:&nbsp;
                                        <span class="font-weight-bold">
                                            {{count($subcontractors) + count($contractors) + count($assistant_subcontractors)}}
                                        </span>
                                        &nbsp;คน
                                        @if(count($subcontractors) > 0 || count($contractors) > 0)
                                            <ul>
                                                @foreach($contractors as $contractor)
                                                    <li>{{$contractor -> prefix.$contractor -> first_name." ".$contractor -> last_name}} (หัวหน้าผู้รับเหมา)</li>
                                                @endforeach
                                                ----------------
                                                @foreach($subcontractors as $subcontractor)
                                                    <li>{{$subcontractor -> prefix.$subcontractor -> first_name." ".$subcontractor -> last_name}} (ช่าง)</li>
                                                @endforeach
                                                ----------------
                                                @if(count($assistant_subcontractors) > 0 )
                                                    @foreach($assistant_subcontractors as $assistant_subcontractor)
                                                        <li>{{$assistant_subcontractor -> prefix.$assistant_subcontractor -> first_name." ".$assistant_subcontractor -> last_name}} (ผู้ช่วยช่าง)</li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        @endif

                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card-header ">
                        <h5 class="m-md-2">
                            ผู้ควบคุม
                        </h5>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5"> 6.1 เลือกประเภทงาน อ้างอิงจากกฏการตัดสินใจสำหรับผู้ปฏิบัติงาน (เฉพาะตัว) ฯ </span>
                        <div class="row ml-4">
{{--                            <div class="col-sm-10 checkbox-radios">--}}
{{--                                <div class="form-check">--}}
{{--                                    <label class="form-check-label font-weight-bold">--}}
{{--                                        <input class="form-check-input" name="type1" value="1" type="checkbox" {{$checkResultView[0]  -> type1 == 1 ? 'checked' : ''}} disabled>--}}
{{--                                        <span class="form-check-sign"></span>--}}
{{--                                        งานประเภท 1 : งานปกติทำประจำ (หากไม่มี WI ให้พิจารณาเป็นข้อ 2)--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <div class="form-check">--}}
{{--                                    <label class="form-check-label font-weight-bold">--}}
{{--                                        <input class="form-check-input" name="type2" value="1" type="checkbox" {{$checkResultView[0]  -> type2 == 1 ? 'checked' : ''}} disabled>--}}
{{--                                        <span class="form-check-sign"></span>--}}
{{--                                        งานประเภท 2 : งานปกติผู้ปฏิบัติงานไม่ได้ทำ > 1 เดือน--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <div class="form-check">--}}
{{--                                    <label class="form-check-label font-weight-bold">--}}
{{--                                        <input class="form-check-input" name="type3" value="1" type="checkbox" {{$checkResultView[0]  -> type3 == 1 ? 'checked' : ''}} disabled>--}}
{{--                                        <span class="form-check-sign"></span>--}}
{{--                                        งานประเภท 3 : งานไม่ปกติ เคยทำไม่เกิน 1 เดือน หรือซ้อมทบทวนทุกเดือน--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <div class="form-check">--}}
{{--                                    <label class="form-check-label font-weight-bold">--}}
{{--                                        <input class="form-check-input" name="type4" value="1" type="checkbox" {{$checkResultView[0]  -> type4 == 1 ? 'checked' : ''}} disabled>--}}
{{--                                        <span class="form-check-sign"></span>--}}
{{--                                        งานประเภท 4 : งานไม่ปกติ ไม่เคยทำหรือไม่ได้ซ้อมทบทวนทุกเดือน--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <div class="form-check">--}}
{{--                                    <label class="form-check-label font-weight-bold">--}}
{{--                                        <input class="form-check-input" name="type5" value="1" type="checkbox" {{$checkResultView[0]  -> type5 == 1 ? 'checked' : ''}} disabled>--}}
{{--                                        <span class="form-check-sign"></span>--}}
{{--                                        งานประเภท 5 : กิจกรรมหรืองานอื่นๆ ที่ไม่มี WI--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="col-sm-6 col-lg-12">
                                <div class="form-check-radio">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="type1" value="1" {{$checkResultView[0]  -> type1 == 1 ? 'checked' : ''}} disabled> งานประเภท 1 : งานปกติทำประจำ (หากไม่มี WI ให้พิจารณาเป็นข้อ 2)
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <div class="form-check-radio">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="type1" value="2" {{$checkResultView[0]  -> type1 == 2 ? 'checked' : ''}} disabled> งานประเภท 2 : งานปกติผู้ปฏิบัติงานไม่ได้ทำ > 1 เดือน
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <div class="form-check-radio">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="type1" value="3" {{$checkResultView[0]  -> type1 == 3 ? 'checked' : ''}} disabled> งานประเภท 3 : งานไม่ปกติ เคยทำไม่เกิน 1 เดือน หรือซ้อมทบทวนทุกเดือน
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <div class="form-check-radio">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="type1" value="4" {{$checkResultView[0]  -> type1 == 4 ? 'checked' : ''}} disabled> งานประเภท 4 : งานไม่ปกติ ไม่เคยทำหรือไม่ได้ซ้อมทบทวนทุกเดือน
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <div class="form-check-radio">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="type1" value="5" {{$checkResultView[0]  -> type1 == 5 ? 'checked' : ''}} disabled> งานประเภท 5 : กิจกรรมหรืองานอื่นๆ ที่ไม่มี WI
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">6.2 แนบสำเนาใบตรวจสอบอุปกรณ์ป้องกันอันตรายส่วนบุคคล (PPE)</span>
                        <div class="row ml-4">
                            <div class="col-sm-10 checkbox-radios">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="ppe" value="1" type="checkbox" {{$checkResultView[0] -> ppe == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        แนบสำเนาใบตรวจสอบอุปกรณ์ป้องกันอันตรายส่วนบุคคล (PPE)
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">6.3 การใช้เครนประเภทเคลี่อนที่ (รถเฮี๊ยบ รถเครน ฯลฯ)</span>
                        <div class="row ml-4">
                            <div class="col-sm-10 checkbox-radios">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="crane1" value="1" type="checkbox" {{$checkResultView[0] -> crane1 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        ไม่มีการใช้ ข้ามไปข้อ 6.4
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="crane2" value="1" type="checkbox" {{$checkResultView[0] -> crane2 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        มีการใช้โปรดตรวจสอบข้อ 6.3.1 และ 6.3.2
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios pl-5">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="crane2_1" value="1" type="checkbox" {{$checkResultView[0] -> crane2_1 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        6.3.1 มีใบอนุญาตขับขี่ และยังไม่หมดอายุ
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios pl-5">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="crane2_2" value="1" type="checkbox" {{$checkResultView[0] -> crane2_2 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        6.3.2 มีสำเนา ปจ.2 และยังไม่หมดอายุ
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">6.4 มีการใช้พื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน</span>
                        <div class="row ml-4">
                            <div class="col-sm-10 checkbox-radios">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="workshop1" value="1" type="checkbox" {{$checkResultView[0]  -> workshop1 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        ไม่มีการใช้ ข้ามไปข้อ 6.5
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="workshop2" value="1" type="checkbox" {{$checkResultView[0]  -> workshop2 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        มีการใช้โปรดตรวจสอบข้อ 6.4.1 และ 6.4.2
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios pl-5">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="workshop2_1" value="1" type="checkbox" {{$checkResultView[0]  -> workshop2_1 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        6.4.1 แนบแบบฟอร์มขอใช้พื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน (F-MA-ME-001)
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios pl-5">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="workshop2_2" value="1" type="checkbox" {{$checkResultView[0]  -> workshop2_2 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        6.4.2 แนบแบบฟอร์มการตรวจสอบพื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน (F-MA-ME-002)
                                    </label>
                                </div>
                            </div>
                        </div>
                </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">6.5 มีการใช้เครื่องมือหรืออุปกรณ์ไฟไฟ้าที่ใช้ไฟไฟ้าตั้งแต่ 220 V. ขึ้นไป</span>
                        <div class="row ml-4">
                            <div class="col-sm-10">
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="electric_check1" value="1" type="checkbox" {{$checkResultView[0] -> electric_check1 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        ไม่มีการใช้ ข้ามไปข้อ 6.6
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="electric_check2" value="1" type="checkbox" {{$checkResultView[0] -> electric_check2 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        มีการใช้ โปรดกรอกรายละเอียดลงในตาราง
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" width="5%">ลำดับ</th>
                                    <th class="text-center" width="50%">รายการเครื่องมือหรืออุปกรณ์</th>
                                    <th class="text-center" width="10%">จำนวน</th>
                                    <th class="text-center" width="10%">หน่วย</th>
                                    <th class="text-center" width="10%">ผ่าน / ไม่ผ่าน</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">1.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment1"
                                               name="equipment1"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$checkResultView[0] -> equipment1}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount1"
                                               name="amount1"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$checkResultView[0] -> amount1}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit1"
                                               name="unit1"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$checkResultView[0] -> unit1}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission1" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$checkResultView[0] -> permission1 == 1 ? 'selected' : ''}} >ผ่าน</option>
                                            <option value="2" {{$checkResultView[0] -> permission1 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">2.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment2"
                                               name="equipment2"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$checkResultView[0] -> equipment2}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount2"
                                               name="amount2"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$checkResultView[0] -> amount2}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit2"
                                               name="unit2"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$checkResultView[0] -> unit2}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission2" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$checkResultView[0] -> permission2 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$checkResultView[0] -> permission2 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">3.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment3"
                                               name="equipment3"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$checkResultView[0] -> equipment3}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount3"
                                               name="amount3"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$checkResultView[0] -> amount3}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit3"
                                               name="unit3"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$checkResultView[0] -> unit3}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission3" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$checkResultView[0] -> permission3 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$checkResultView[0] -> permission3 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">4.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment4"
                                               name="equipment4"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$checkResultView[0] -> equipment1}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount4"
                                               name="amount4"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$checkResultView[0] -> amount4}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit4"
                                               name="unit4"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$checkResultView[0] -> unit4}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission4" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$checkResultView[0] -> permission4 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$checkResultView[0] -> permission4 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">5.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment5"
                                               name="equipment5"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$checkResultView[0] -> equipment5}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount5"
                                               name="amount5"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$checkResultView[0] -> amount5}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit5"
                                               name="unit5"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$checkResultView[0] -> unit5}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission5" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$checkResultView[0] -> permission5 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$checkResultView[0] -> permission5 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">6.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment6"
                                               name="equipment6"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$checkResultView[0] -> equipment6}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount6"
                                               name="amount6"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$checkResultView[0] -> amount6}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit6"
                                               name="unit6"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$checkResultView[0] -> unit6}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission6" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$checkResultView[0] -> permission6 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$checkResultView[0] -> permission6 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">7.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment7"
                                               name="equipment7"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$checkResultView[0] -> equipment7}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount7"
                                               name="amount7"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$checkResultView[0] -> amount7}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit7"
                                               name="unit7"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$checkResultView[0] -> unit7}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission7" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$checkResultView[0] -> permission7 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$checkResultView[0] -> permission7 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">8.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment8"
                                               name="equipment8"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$checkResultView[0] -> equipment8}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount8"
                                               name="amount8"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$checkResultView[0] -> amount8}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit8"
                                               name="unit8"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$checkResultView[0] -> unit8}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission8" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$checkResultView[0] -> permission8 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$checkResultView[0] -> permission8 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">9.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment9"
                                               name="equipment9"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$checkResultView[0] -> equipment9}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount9"
                                               name="amount9"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$checkResultView[0] -> amount9}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit9"
                                               name="unit9"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$checkResultView[0] -> unit9}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission9" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$checkResultView[0] -> permission9 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$checkResultView[0] -> permission9 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">10.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment10"
                                               name="equipment10"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$checkResultView[0] -> equipment10}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount10"
                                               name="amount10"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$checkResultView[0] -> amount10}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit10"
                                               name="unit10"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$checkResultView[0] -> unit10}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission10" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$checkResultView[0] -> permission10 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$checkResultView[0] -> permission10 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">6.6 กรณีต้องการป้ายห้ามแตะ (Don't Touch) สำหรับ ผรม. โปรดระบุใบ</span>
                        <div class="row">
                            <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" width="10%">ลำดับ</th>
                                    <th class="text-center" width="10%">จำนวนใบ</th>
                                    <th class="text-center" width="10%">หน่วย</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">1.</td>
                                    <td>
                                        <input type="number"
                                               id="touch"
                                               name="touch"
                                               class="form-control"
                                               placeholder="จำนวนใบ"
                                               value="{{$checkResultView[0] -> touch}}" disabled>
                                    </td>
                                    <td class="text-center">
                                        หน่วย
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card-header ">
                        <h5 class="m-md-2">
                            ผู้ควบคุมและผู้รับเหมา
                        </h5>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">7.การตรวจสอบระหว่างปฏิบัติงาน</span>
                        <div class="row">
                            <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" width="10%">ครั้งที่ {{$checkResultView[0]  -> time." ( ".substr($checkResultView[0]  -> check_time,0,5)." น. )"}}</th>
                                    <th class="text-center" width="10%">ครั้งที่ {{$checkResultView[1]  -> time." ( ".substr($checkResultView[1]  -> check_time,0,5)." น. )"}}</th>
                                    <th class="text-center" width="50%">รายการ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="equipment_personal" value="1"
                                                       type="checkbox" {{$checkResultView[0] -> equipment_personal == 1 ? 'checked' : ''}}
                                                       disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="equipment_personal" value="1"
                                                       type="checkbox" {{$checkResultView[1] -> equipment_personal == 1 ? 'checked' : ''}}
                                                       disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">1.สวมอุปกรณ์ป้องกันอันตรายส่วนบุคคล</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety_glasses" value="1"
                                                       {{$checkResultView[0] -> safety_glasses == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety_glasses" value="1"
                                                       {{$checkResultView[1] -> safety_glasses == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">2.แว่นตานิรภัย (Safety glasses)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety_belt" value="1"
                                                       {{$checkResultView[0] -> safety_belt == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety_belt" value="1"
                                                       {{$checkResultView[1] -> safety_belt == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">3.เข็มขัดนิรภัย (Safety belt)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="scba" value="1"
                                                       {{$checkResultView[0] -> scba == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="scba" value="1"
                                                       {{$checkResultView[1] -> scba == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">4.อุปกรณ์ช่วยหายใจ (SCBA)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="oximeter" value="1"
                                                       {{$checkResultView[0] -> oximeter == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="oximeter" value="1"
                                                       {{$checkResultView[1] -> oximeter == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">5.เครื่องวัดออกซิเจนครบถ้วน</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fan" value="1"
                                                       {{$checkResultView[0] -> fan == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fan" value="1"
                                                       {{$checkResultView[1] -> fan == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">6.พัดลมระบายอากาศครบถ้วน</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="acid_mask"
                                                       {{$checkResultView[0] -> acid_mask == 1 ? 'checked' : ''}}
                                                       value="1" type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="acid_mask"
                                                       {{$checkResultView[1] -> acid_mask == 1 ? 'checked' : ''}}
                                                       value="1" type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">7.หน้ากากป้องกันไอกรด (Acid fume protecting mash)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="sling" value="1"
                                                       {{$checkResultView[0] -> sling == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="sling" value="1"
                                                       {{$checkResultView[1] -> sling == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">8.สลิงอยู่ในสภาพพรอ้มใช้งาน (เป็นไปตามมาตรฐาน NS-SUS)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1" value="1"
                                                       {{$checkResultView[0] -> electric1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1" value="1"
                                                       {{$checkResultView[1] -> electric1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">9.เครื่องเชื่อมไฟฟ้า</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1_1" value="1"
                                                       {{$checkResultView[0] -> electric1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1_1" value="1"
                                                       {{$checkResultView[1] -> electric1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">9.1.สภาพสายไฟ(ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด,จุดต่อต้องมั่นคงแข็งแรง)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1_2" value="1"
                                                       {{$checkResultView[0] -> electric1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1_2" value="1"
                                                       {{$checkResultView[1] -> electric1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">9.1.สภาพสาย Ground (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด,จุดต่อต้องมั่นคงแข็งแรง)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1" value="1"
                                                       {{$checkResultView[0] -> cut_gas1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1" value="1"
                                                       {{$checkResultView[1] -> cut_gas1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">10.เครื่องเชื่อม ตัด เผา Gas</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_1" value="1"
                                                       {{$checkResultView[0] -> cut_gas1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_1" value="1"
                                                       {{$checkResultView[1] -> cut_gas1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">10.1.Gauge ไม่แตก / สามารถอ่านค่าได ้</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_2" value="1"
                                                       {{$checkResultView[0] -> cut_gas1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_2" value="1"
                                                       {{$checkResultView[1] -> cut_gas1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">10.2. ต้องมีอุปกรณ์ป้องกันไฟย้อนกลับ (Flash back Arrestor) 4 ตำแหน่ง
                                        และมีสติกเกอร์แสดงการตรวจสอบติดอยู่</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_3" value="1"
                                                       {{$checkResultView[0] -> cut_gas1_3 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_3" value="1"
                                                       {{$checkResultView[1] -> cut_gas1_3 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">10.3.สาย GAS อยู่ในสภาพดีไม่แตกลายงา และมีการยึดสายกับข้อต่อที่ถัง GAS
                                        และ หัว GAS แน่นหนา ไม่รั่วซึม</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_4" value="1"
                                                       {{$checkResultView[0] -> cut_gas1_4 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_4" value="1"
                                                       {{$checkResultView[1] -> cut_gas1_4 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">10.4. จุดยึดถัง GAS ต้องมันคงแข็งแรงและมีโซ่หรือเชือกมัดถัง ไม่เสี่ยงต่อการล้ม
                                        ของถัง GAS</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1" value="1"
                                                       {{$checkResultView[0] -> cut_hand1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1" value="1"
                                                       {{$checkResultView[1] -> cut_hand1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">11.เครื่องตัด เจียรมือ</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_1" value="1"
                                                       {{$checkResultView[0] -> cut_hand1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_1" value="1"
                                                       {{$checkResultView[1] -> cut_hand1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">11.1.สภาพสายไฟ (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด, จุดต่อต้องมั่นคงแข็งแรง)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_2" value="1"
                                                       {{$checkResultView[0] -> cut_hand1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_2" value="1"
                                                       {{$checkResultView[1] -> cut_hand1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">11.2.สภาพสาย Ground (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด, จุดต่อต้องมั่นคงแข็งแรง)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_3" value="1"
                                                       {{$checkResultView[0] -> cut_hand1_3 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_3" value="1"
                                                       {{$checkResultView[1] -> cut_hand1_3 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">11.3. มี Guard ป้องกัน (Guard ต้องยึดอย่างมั่นคง ไม่หลวมคลอน)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_ready" value="1"
                                                       {{$checkResultView[0] -> fire_ready == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_ready" value="1"
                                                       {{$checkResultView[1] -> fire_ready == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">12.อุปกรณ์ดับเพลิงมีพร้อมและตู้น้ำดับเพลิงอยู่ในสภาพพร้อมใช้งาน</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_amount" value="1"
                                                       {{$checkResultView[0] -> fire_amount == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_amount" value="1"
                                                       {{$checkResultView[1] -> fire_amount == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">13.จำนวนถังดับเพลิงมีครบถ้วน และตรวจสอบล่าสุด</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_resis" value="1"
                                                       {{$checkResultView[0] -> fire_resis == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_resis" value="1"
                                                       {{$checkResultView[1] -> fire_resis == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">14.มีการจัดเตรียมแผ่นวัสดุทนไฟ สำหรับป้องกันสะเก็ดไฟกระเด็น
                                        (วัสดุป้องกันสะเก็ดไฟต้องไม่ติดไฟ)
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety" value="1"
                                                       {{$checkResultView[0] -> safety == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety" value="1"
                                                       {{$checkResultView[1] -> safety == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">15.มีสภาพการปฏิบัตงานที่ปลอดภัย

                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="contractor" value="1"
                                                       {{$checkResultView[0] -> contractor == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="contractor" value="1"
                                                       {{$checkResultView[1] -> contractor == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">หัวหน้างานผู้รับเหมายืนยัน</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="leader" value="1"
                                                       {{$checkResultView[0] -> leader == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="leader" value="1"
                                                       {{$checkResultView[1] -> leader == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">ผู้ควบคุมงานยืนยัน</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">8.ตรวจสอบหลังเสร็จงาน</span>
                        <div class="row">
                            <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" width="5%">เลือก</th>
                                    <th class="text-center" width="10%">รายการ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> clean == 1 ? 'checked' : ''}} disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        ความสะอาดของหน้างาน
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> clear == 1 ? 'checked' : ''}} disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        ความเรียบร้อยของงาน
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> safe == 1 ? 'checked' : ''}} disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        ความปลอดภัย
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> another == 1 ? 'checked' : ''}} disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        อื่น ๆ โปรดระบุ <input type="text" size="30" value="{{$checkResultView[0] -> another_word}}"  disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        สถานะ
                                    </td>
                                    <td>
                                        &nbsp;
                                        @if($checkResultView[0] -> status == 7)
                                            ปิดงานสำเร็จ
                                        @elseif($checkResultView[0] -> status == 8)
                                            ปิดงานไม่สำเร็จ
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="javascript:history.go(-1)" class="btn btn-secondary btn-lg"> < ย้อนกลับ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

