@extends('layouts.paper')
@section('title','งานที่ได้รับอนุมัติ')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">งานที่ได้รับอนุมัติ</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <div class="card mt-5">
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">

                            <div class="col-md-12">
                                {{--                                <div class="card-header">--}}
                                {{--                                    <a href="{{route('admin.mail.list.edit')}}"--}}
                                {{--                                       class="btn btn-primary">--}}
                                {{--                                        งานที่ได้รับอนุมัติ--}}
                                {{--                                    </a>--}}

                                {{--                                </div>--}}
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="10%">No.</th>
                                                <th class="text-center" width="10%">สถานะ</th>
                                                <th class="text-center" width="15%">วันที่สแกนเข้างาน</th>
                                                <th class="text-center" width="15%">ชื่องาน</th>
                                                <th class="text-center" width="15%">สถาที่ปฏิบัติงาน</th>
                                                <th class="text-center" width="15%">หัวหน้าผู้รับเหมา</th>
                                                <th class="text-center" width="20%">ผู้ควบคุมงาน</th>
                                                <th class="text-center" width="15%" class="disabled-sorting text-center">ใบ Work Permit</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($task_view_details as $task_view_detail)
                                                <tr>
                                                    <td class="text-center"> {{$task_view_detail -> task_detail_id}}</td>
                                                    <td class="text-center">
                                                        @if($task_view_detail -> task_detail_status == 1)
                                                            <h5>
                                                                <span class="badge badge-warning">รอเข้างาน</span>
                                                            </h5>
                                                        @elseif($task_view_detail -> task_detail_status == 2)
                                                            <h5>
                                                                <span class="badge badge-primary">อนุมัติ</span>
                                                            </h5>
                                                        @elseif($task_view_detail -> task_detail_status == 3)
                                                            <h5>
                                                                <span class="badge badge-primary">เริ่มทำงาน</span>
                                                            </h5>
                                                        @elseif($task_view_detail -> task_detail_status == 4)
                                                            <h5>
                                                                <span class="badge badge-warning">ตรวจสอบ 1</span>
                                                            </h5>
                                                        @elseif($task_view_detail -> task_detail_status == 5)
                                                            <h5>
                                                                <span class="badge badge-warning">ตรวจสอบ 2</span>
                                                            </h5>
                                                        @elseif($task_view_detail -> task_detail_status == 6)
                                                            <h5>
                                                                <span class="badge badge-warning">ตรวจสอบปิดงาน</span>
                                                            </h5>
                                                        @elseif($task_view_detail -> task_detail_status == 7)
                                                            <h5>
                                                                <span class="badge badge-danger">ปิดงานสำเร็จ</span>
                                                            </h5>
                                                        @elseif($task_view_detail -> task_detail_status == 8)
                                                            <h5>
                                                                <span class="badge badge-danger">ปิดงานไม่สำเร็จ</span>
                                                            </h5>
                                                        @endif
                                                    </td>
                                                    <td class="text-center"> {{ConvertThaiDate($task_view_detail -> task_detail_date)}}</td>
                                                    <td class="text-center"> {{$task_view_detail -> task_title}}</td>
                                                    <td class="text-center"> {{$task_view_detail -> task_location}}</td>
                                                    <td class="text-center"> {{$task_view_detail -> contractor_prefix}}{{$task_view_detail -> contractor_firstname}} {{$task_view_detail -> contractor_lastname}}</td>
                                                    <td class="text-center"> {{$task_view_detail -> leader_firstname}} {{$task_view_detail -> leader_lastname}}</td>
                                                    <td class="text-center">
                                                        <a href="{{route('admin.workPermit.pdf',['id'=> $task_view_detail -> task_detail_id])}}"
                                                           class="btn btn-info btn-icon btn-sm like"
                                                           id="hover"
                                                           target="_blank"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="ใบ WorkPermit">
                                                            <i class="nc-icon nc-cloud-download-93"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <script>
        $('#datatable').DataTable({
            responsive: true,
            "order": [[ 2, "desc" ]]
        });
    </script>
@stop

