@extends('layouts.paper')
@section('title','รายละเอียด QR CODE')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
            <li class="breadcrumb-item active" aria-current="page">รายละเอียด QR CODE ( {{$task -> task_title ? $task -> task_title : '-'}} )</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card mt-5">
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <img src="data:image/png;base64,
                                                    {!! base64_encode(QrCode::format('png')
                                                        ->size(450)
                                                        ->generate(route('contractor.scanQR.openTask',['id' => $task -> task_id]))
                                                        ) !!}
                                         ">
                                    </div>
                                    <p class="h5">ชื่องาน : {{$task -> task_title}}</p>
                                    <p class="h5">หัวหน้าผู้รับเหมา : {{$task -> contractor_prefix}}{{$task -> contractor_first_name}} {{$task -> contractor_last_name}}</p>
                                    <p class="h5">ผู้ควบคุมงาน : {{$task -> leader_prefix}}{{$task -> leader_first_name}} {{$task -> leader_last_name}}</p>
                                    <p class="h5">วันเข้างาน : {{$task -> task_working_date ? ConvertThaiDate($task -> task_working_date) : '-'}}</p>
                                    <div class="row">
                                    <div class="col-md-6">
                                    <a class="btn btn-primary" href="data:image/png;base64,
                                                    {!! base64_encode(QrCode::format('png')
                                                        ->size(450)
                                                        ->generate(route('contractor.scanQR.openTask',['id' => $task -> task_id]))
                                                        ) !!}" class="btn btn-default btn-link" download="QrCode(สแกนเข้างาน{{$task -> task_title}})">Download QR Code</a>
                                    </div>
                                    <div class="col-md-3">
                                    <a class="btn btn-secondary" href="javascript: history.back()">ย้อนกลับ</a>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
@stop
