@extends('layouts.paper')
@section('title','แบบตรวจสอบและขออนุญาตปฏิบัติงานของผู้รับเหมาชั่วคราว')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item" aria-current="page">แบบตรวจสอบและขออนุญาตปฏิบัติงานของผู้รับเหมาชั่วคราว</li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{$getTask -> task_title}}
                    (Work Controller : {{$getTask -> leader_prefix.$getTask -> leader_first_name." ".$getTask -> leader_last_name}})
                </li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form method="POST" action="" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                6.1 เลือกประเภทงาน อ้างอิงจากกฏการตัดสินใจสำหรับผู้ปฏิบัติงาน (เฉพาะตัว) ฯ
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row ml-4">
                                <div class="col-sm-6 col-lg-12">
                                    <div class="form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="type1" value="1" {{$check_task -> type1 == 1 ? 'checked' : ''}}> งานประเภท 1 : งานปกติทำประจำ (หากไม่มี WI ให้พิจารณาเป็นข้อ 2)
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="type1" value="2" {{$check_task -> type1 == 2 ? 'checked' : ''}}> งานประเภท 2 : งานปกติผู้ปฏิบัติงานไม่ได้ทำ > 1 เดือน
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="type1" value="3" {{$check_task -> type1 == 3 ? 'checked' : ''}}> งานประเภท 3 : งานไม่ปกติ เคยทำไม่เกิน 1 เดือน หรือซ้อมทบทวนทุกเดือน
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="type1" value="4" {{$check_task -> type1 == 4 ? 'checked' : ''}}> งานประเภท 4 : งานไม่ปกติ ไม่เคยทำหรือไม่ได้ซ้อมทบทวนทุกเดือน
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                    <div class="form-check-radio">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="type1" value="5" {{$check_task -> type1 == 5 ? 'checked' : ''}}> งานประเภท 5 : กิจกรรมหรืองานอื่นๆ ที่ไม่มี WI
                                            <span class="form-check-sign"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                6.2 แนบสำเนาใบตรวจสอบอุปกรณ์ป้องกันอันตรายส่วนบุคคล (PPE)
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row ml-4">
                                <div class="col-sm-10 checkbox-radios">
                                    <div class="form-check">
                                        <label class="form-check-label font-weight-bold">
                                            <input class="form-check-input" name="ppe" value="1" type="checkbox" {{$check_task -> ppe == 1 ? 'checked' : ''}}>
                                            <span class="form-check-sign"></span>
                                            แนบสำเนาใบตรวจสอบอุปกรณ์ป้องกันอันตรายส่วนบุคคล (PPE)
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                6.3 การใช้เครนประเภทเคลี่อนที่ (รถเฮี๊ยบ รถเครน ฯลฯ)
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row ml-4">
                                <div class="col-sm-10 checkbox-radios">
                                    <div class="form-check">
                                        <label class="form-check-label font-weight-bold">
                                            <input class="form-check-input" name="crane1" value="1" type="checkbox" {{$check_task -> crane1 == 1 ? 'checked' : ''}}>
                                            <span class="form-check-sign"></span>
                                            ไม่มีการใช้ ข้ามไปข้อ 6.4
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-10 checkbox-radios">
                                    <div class="form-check">
                                        <label class="form-check-label font-weight-bold">
                                            <input class="form-check-input" name="crane2" value="1" type="checkbox" {{$check_task -> crane2 == 1 ? 'checked' : ''}}>
                                            <span class="form-check-sign"></span>
                                            มีการใช้โปรดตรวจสอบข้อ 6.3.1 และ 6.3.2
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-10 checkbox-radios pl-5">
                                    <div class="form-check">
                                        <label class="form-check-label font-weight-bold">
                                            <input class="form-check-input" name="crane2_1" value="1" type="checkbox" {{$check_task -> crane2_1 == 1 ? 'checked' : ''}}>
                                            <span class="form-check-sign"></span>
                                            6.3.1 มีใบอนุญาตขับขี่ และยังไม่หมดอายุ
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-10 checkbox-radios pl-5">
                                    <div class="form-check">
                                        <label class="form-check-label font-weight-bold">
                                            <input class="form-check-input" name="crane2_2" value="1" type="checkbox" {{$check_task -> crane2_2 == 1 ? 'checked' : ''}}>
                                            <span class="form-check-sign"></span>
                                            6.3.2 มีสำเนา ปจ.2 และยังไม่หมดอายุ
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                6.4 มีการใช้พื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row ml-4">
                                <div class="col-sm-10 checkbox-radios">
                                    <div class="form-check">
                                        <label class="form-check-label font-weight-bold">
                                            <input class="form-check-input" name="workshop1" value="1" type="checkbox" {{$check_task -> workshop1 == 1 ? 'checked' : ''}}>
                                            <span class="form-check-sign"></span>
                                            ไม่มีการใช้ ข้ามไปข้อ 6.5
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-10 checkbox-radios">
                                    <div class="form-check">
                                        <label class="form-check-label font-weight-bold">
                                            <input class="form-check-input" name="workshop2" value="1" type="checkbox" {{$check_task -> workshop2 == 1 ? 'checked' : ''}}>
                                            <span class="form-check-sign"></span>
                                            มีการใช้โปรดตรวจสอบข้อ 6.4.1 และ 6.4.2
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-10 checkbox-radios pl-5">
                                    <div class="form-check">
                                        <label class="form-check-label font-weight-bold">
                                            <input class="form-check-input" name="workshop2_1" value="1" type="checkbox" {{$check_task -> workshop2_1 == 1 ? 'checked' : ''}}>
                                            <span class="form-check-sign"></span>
                                            6.4.1 แนบแบบฟอร์มขอใช้พื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน (F-MA-ME-001)
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-10 checkbox-radios pl-5">
                                    <div class="form-check">
                                        <label class="form-check-label font-weight-bold">
                                            <input class="form-check-input" name="workshop2_2" value="1" type="checkbox" {{$check_task -> workshop2_2 == 1 ? 'checked' : ''}}>
                                            <span class="form-check-sign"></span>
                                            6.4.2 แนบแบบฟอร์มการตรวจสอบพื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน (F-MA-ME-002)
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                6.5 มีการใช้เครื่องมือหรืออุปกรณ์ไฟไฟ้าที่ใช้ไฟไฟ้าตั้งแต่ 220 V. ขึ้นไป
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row ml-4">
                                <div class="col-sm-10">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label font-weight-bold">
                                            <input class="form-check-input" name="electric_check1" value="1" type="checkbox" {{$check_task -> electric_check1 == 1 ? 'checked' : ''}}>
                                            <span class="form-check-sign"></span>
                                            ไม่มีการใช้ ข้ามไปข้อ 6.6
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label font-weight-bold">
                                            <input class="form-check-input" name="electric_check2" value="1" type="checkbox" {{$check_task -> electric_check2 == 1 ? 'checked' : ''}}>
                                            <span class="form-check-sign"></span>
                                            มีการใช้ โปรดกรอกรายละเอียดลงในตาราง
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="5%">ลำดับ</th>
                                        <th class="text-center" width="50%">รายการเครื่องมือหรืออุปกรณ์</th>
                                        <th class="text-center" width="10%">จำนวน</th>
                                        <th class="text-center" width="10%">หน่วย</th>
                                        <th class="text-center" width="10%">ผ่าน / ไม่ผ่าน</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">1.</td>
                                        <td>
                                            <input type="text"
                                                   id="equipment1"
                                                   name="equipment1"
                                                   class="form-control"
                                                   placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                                   value="{{$check_task -> equipment1}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="amount1"
                                                   name="amount1"
                                                   class="form-control"
                                                   placeholder="จำนวน"
                                                   value="{{$check_task -> amount1}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="unit1"
                                                   name="unit1"
                                                   class="form-control"
                                                   placeholder="หน่วย"
                                                   value="{{$check_task -> unit1}}">
                                        </td>
                                        <td>
                                            <select  class="form-control select2"  name="permission1">
                                                <option value="">กรุณาเลือก</option>
                                                <option value="1" {{$check_task -> permission1 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                                <option value="2" {{$check_task -> permission1 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2.</td>
                                        <td>
                                            <input type="text"
                                                   id="equipment2"
                                                   name="equipment2"
                                                   class="form-control"
                                                   placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                                   value="{{$check_task -> equipment2}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="amount2"
                                                   name="amount2"
                                                   class="form-control"
                                                   placeholder="จำนวน"
                                                   value="{{$check_task -> amount2}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="unit2"
                                                   name="unit2"
                                                   class="form-control"
                                                   placeholder="หน่วย"
                                                   value="{{$check_task -> unit2}}">
                                        </td>
                                        <td>
                                            <select  class="form-control select2"  name="permission2">
                                                <option value="">กรุณาเลือก</option>
                                                <option value="1" {{$check_task -> permission2 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                                <option value="2" {{$check_task -> permission2 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3.</td>
                                        <td>
                                            <input type="text"
                                                   id="equipment3"
                                                   name="equipment3"
                                                   class="form-control"
                                                   placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                                   value="{{$check_task -> equipment3}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="amount3"
                                                   name="amount3"
                                                   class="form-control"
                                                   placeholder="จำนวน"
                                                   value="{{$check_task -> amount3}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="unit3"
                                                   name="unit3"
                                                   class="form-control"
                                                   placeholder="หน่วย"
                                                   value="{{$check_task -> unit3}}">
                                        </td>
                                        <td>
                                            <select  class="form-control select2"  name="permission3">
                                                <option value="">กรุณาเลือก</option>
                                                <option value="1" {{$check_task -> permission3 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                                <option value="2" {{$check_task -> permission3 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4.</td>
                                        <td>
                                            <input type="text"
                                                   id="equipment4"
                                                   name="equipment4"
                                                   class="form-control"
                                                   placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                                   value="{{$check_task -> equipment1}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="amount4"
                                                   name="amount4"
                                                   class="form-control"
                                                   placeholder="จำนวน"
                                                   value="{{$check_task -> amount4}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="unit4"
                                                   name="unit4"
                                                   class="form-control"
                                                   placeholder="หน่วย"
                                                   value="{{$check_task -> unit4}}">
                                        </td>
                                        <td>
                                            <select  class="form-control select2"  name="permission4">
                                                <option value="">กรุณาเลือก</option>
                                                <option value="1" {{$check_task -> permission4 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                                <option value="2" {{$check_task -> permission4 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">5.</td>
                                        <td>
                                            <input type="text"
                                                   id="equipment5"
                                                   name="equipment5"
                                                   class="form-control"
                                                   placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                                   value="{{$check_task -> equipment5}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="amount5"
                                                   name="amount5"
                                                   class="form-control"
                                                   placeholder="จำนวน"
                                                   value="{{$check_task -> amount5}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="unit5"
                                                   name="unit5"
                                                   class="form-control"
                                                   placeholder="หน่วย"
                                                   value="{{$check_task -> unit5}}">
                                        </td>
                                        <td>
                                            <select  class="form-control select2"  name="permission5">
                                                <option value="">กรุณาเลือก</option>
                                                <option value="1" {{$check_task -> permission5 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                                <option value="2" {{$check_task -> permission5 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">6.</td>
                                        <td>
                                            <input type="text"
                                                   id="equipment6"
                                                   name="equipment6"
                                                   class="form-control"
                                                   placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                                   value="{{$check_task -> equipment6}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="amount6"
                                                   name="amount6"
                                                   class="form-control"
                                                   placeholder="จำนวน"
                                                   value="{{$check_task -> amount6}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="unit6"
                                                   name="unit6"
                                                   class="form-control"
                                                   placeholder="หน่วย"
                                                   value="{{$check_task -> unit6}}">
                                        </td>
                                        <td>
                                            <select  class="form-control select2"  name="permission6">
                                                <option value="">กรุณาเลือก</option>
                                                <option value="1" {{$check_task -> permission6 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                                <option value="2" {{$check_task -> permission6 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">7.</td>
                                        <td>
                                            <input type="text"
                                                   id="equipment7"
                                                   name="equipment7"
                                                   class="form-control"
                                                   placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                                   value="{{$check_task -> equipment7}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="amount7"
                                                   name="amount7"
                                                   class="form-control"
                                                   placeholder="จำนวน"
                                                   value="{{$check_task -> amount7}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="unit7"
                                                   name="unit7"
                                                   class="form-control"
                                                   placeholder="หน่วย"
                                                   value="{{$check_task -> unit7}}">
                                        </td>
                                        <td>
                                            <select  class="form-control select2"  name="permission7">
                                                <option value="">กรุณาเลือก</option>
                                                <option value="1" {{$check_task -> permission7 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                                <option value="2" {{$check_task -> permission7 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">8.</td>
                                        <td>
                                            <input type="text"
                                                   id="equipment8"
                                                   name="equipment8"
                                                   class="form-control"
                                                   placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                                   value="{{$check_task -> equipment8}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="amount8"
                                                   name="amount8"
                                                   class="form-control"
                                                   placeholder="จำนวน"
                                                   value="{{$check_task -> amount8}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="unit8"
                                                   name="unit8"
                                                   class="form-control"
                                                   placeholder="หน่วย"
                                                   value="{{$check_task -> unit8}}">
                                        </td>
                                        <td>
                                            <select  class="form-control select2"  name="permission8">
                                                <option value="">กรุณาเลือก</option>
                                                <option value="1" {{$check_task -> permission8 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                                <option value="2" {{$check_task -> permission8 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">9.</td>
                                        <td>
                                            <input type="text"
                                                   id="equipment9"
                                                   name="equipment9"
                                                   class="form-control"
                                                   placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                                   value="{{$check_task -> equipment9}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="amount9"
                                                   name="amount9"
                                                   class="form-control"
                                                   placeholder="จำนวน"
                                                   value="{{$check_task -> amount9}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="unit9"
                                                   name="unit9"
                                                   class="form-control"
                                                   placeholder="หน่วย"
                                                   value="{{$check_task -> unit9}}">
                                        </td>
                                        <td>
                                            <select  class="form-control select2"  name="permission9">
                                                <option value="">กรุณาเลือก</option>
                                                <option value="1" {{$check_task -> permission9 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                                <option value="2" {{$check_task -> permission9 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">10.</td>
                                        <td>
                                            <input type="text"
                                                   id="equipment10"
                                                   name="equipment10"
                                                   class="form-control"
                                                   placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                                   value="{{$check_task -> equipment10}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="amount10"
                                                   name="amount10"
                                                   class="form-control"
                                                   placeholder="จำนวน"
                                                   value="{{$check_task -> amount10}}">
                                        </td>
                                        <td>
                                            <input type="text"
                                                   id="unit10"
                                                   name="unit10"
                                                   class="form-control"
                                                   placeholder="หน่วย"
                                                   value="{{$check_task -> unit10}}">
                                        </td>
                                        <td>
                                            <select  class="form-control select2"  name="permission10">
                                                <option value="">กรุณาเลือก</option>
                                                <option value="1" {{$check_task -> permission10 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                                <option value="2" {{$check_task -> permission10 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                6.6 กรณีต้องการป้ายห้ามแตะ (Don't Touch) สำหรับ ผรม. โปรดระบุใบ
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="10%">ลำดับ</th>
                                        <th class="text-center" width="10%">จำนวนใบ</th>
                                        <th class="text-center" width="10%">หน่วย</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">1.</td>
                                        <td>
                                            <input type="number"
                                                   id="touch"
                                                   name="touch"
                                                   class="form-control"
                                                   placeholder="จำนวนใบ"
                                                   value="{{$check_task -> touch}}">
                                        </td>
                                        <td class="text-center">
                                            หน่วย
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                            <br/>
                            <a href="javascript:history.go(-1)" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    <script>

    </script>
@stop
