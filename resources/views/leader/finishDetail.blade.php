@extends('layouts.paper')
@section('title','ตรวจสอบหลังเสร็จงาน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item" aria-current="page">ตรวจสอบหลังเสร็จงาน</li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{$getTask -> task_title}} - No.{{$contractorCheckTasks[0] -> task_detail_id}}
                    (Work Controller : {{$getTask -> leader_prefix.$getTask -> leader_first_name." ".$getTask -> leader_last_name}})
                </li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card-header ">
                        <h5 class="m-md-2">
                            ผู้ควบคุม(ตรวจสอบข้อมูล)
                        </h5>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5"> 6.1 เลือกประเภทงาน อ้างอิงจากกฏการตัดสินใจสำหรับผู้ปฏิบัติงาน (เฉพาะตัว) ฯ </span>
                        <div class="row ml-4">
                            <div class="col-sm-6 col-lg-12">
                                <div class="form-check-radio">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="type1" value="1" {{$leaderCheckTask -> type1 == 1 ? 'checked' : ''}} disabled> งานประเภท 1 : งานปกติทำประจำ (หากไม่มี WI ให้พิจารณาเป็นข้อ 2)
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <div class="form-check-radio">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="type1" value="2" {{$leaderCheckTask  -> type1 == 2 ? 'checked' : ''}} disabled> งานประเภท 2 : งานปกติผู้ปฏิบัติงานไม่ได้ทำ > 1 เดือน
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <div class="form-check-radio">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="type1" value="3" {{$leaderCheckTask  -> type1 == 3 ? 'checked' : ''}} disabled> งานประเภท 3 : งานไม่ปกติ เคยทำไม่เกิน 1 เดือน หรือซ้อมทบทวนทุกเดือน
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <div class="form-check-radio">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="type1" value="4" {{$leaderCheckTask -> type1 == 4 ? 'checked' : ''}} disabled> งานประเภท 4 : งานไม่ปกติ ไม่เคยทำหรือไม่ได้ซ้อมทบทวนทุกเดือน
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                                <div class="form-check-radio">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="type1" value="5" {{$leaderCheckTask  -> type1 == 5 ? 'checked' : ''}} disabled> งานประเภท 5 : กิจกรรมหรืองานอื่นๆ ที่ไม่มี WI
                                        <span class="form-check-sign"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">6.2 แนบสำเนาใบตรวจสอบอุปกรณ์ป้องกันอันตรายส่วนบุคคล (PPE)</span>
                        <div class="row ml-4">
                            <div class="col-sm-10 checkbox-radios">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="ppe" value="1" type="checkbox" {{$leaderCheckTask-> ppe == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        แนบสำเนาใบตรวจสอบอุปกรณ์ป้องกันอันตรายส่วนบุคคล (PPE)
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">6.3 การใช้เครนประเภทเคลี่อนที่ (รถเฮี๊ยบ รถเครน ฯลฯ)</span>
                        <div class="row ml-4">
                            <div class="col-sm-10 checkbox-radios">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="crane1" value="1" type="checkbox" {{$leaderCheckTask -> crane1 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        ไม่มีการใช้ ข้ามไปข้อ 6.4
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="crane2" value="1" type="checkbox" {{$leaderCheckTask -> crane2 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        มีการใช้โปรดตรวจสอบข้อ 6.3.1 และ 6.3.2
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios pl-5">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="crane2_1" value="1" type="checkbox" {{$leaderCheckTask -> crane2_1 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        6.3.1 มีใบอนุญาตขับขี่ และยังไม่หมดอายุ
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios pl-5">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="crane2_2" value="1" type="checkbox" {{$leaderCheckTask -> crane2_2 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        6.3.2 มีสำเนา ปจ.2 และยังไม่หมดอายุ
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">6.4 มีการใช้พื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน</span>
                        <div class="row ml-4">
                            <div class="col-sm-10 checkbox-radios">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="workshop1" value="1" type="checkbox" {{$leaderCheckTask  -> workshop1 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        ไม่มีการใช้ ข้ามไปข้อ 6.5
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="workshop2" value="1" type="checkbox" {{$leaderCheckTask  -> workshop2 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        มีการใช้โปรดตรวจสอบข้อ 6.4.1 และ 6.4.2
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios pl-5">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="workshop2_1" value="1" type="checkbox" {{$leaderCheckTask  -> workshop2_1 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        6.4.1 แนบแบบฟอร์มขอใช้พื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน (F-MA-ME-001)
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10 checkbox-radios pl-5">
                                <div class="form-check">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="workshop2_2" value="1" type="checkbox" {{$leaderCheckTask  -> workshop2_2 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        6.4.2 แนบแบบฟอร์มการตรวจสอบพื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน (F-MA-ME-002)
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">6.5 มีการใช้เครื่องมือหรืออุปกรณ์ไฟไฟ้าที่ใช้ไฟไฟ้าตั้งแต่ 220 V. ขึ้นไป</span>
                        <div class="row ml-4">
                            <div class="col-sm-10">
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="electric_check1" value="1" type="checkbox" {{$leaderCheckTask -> electric_check1 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        ไม่มีการใช้ ข้ามไปข้อ 6.6
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label font-weight-bold">
                                        <input class="form-check-input" name="electric_check2" value="1" type="checkbox" {{$leaderCheckTask -> electric_check2 == 1 ? 'checked' : ''}} disabled>
                                        <span class="form-check-sign"></span>
                                        มีการใช้ โปรดกรอกรายละเอียดลงในตาราง
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" width="5%">ลำดับ</th>
                                    <th class="text-center" width="50%">รายการเครื่องมือหรืออุปกรณ์</th>
                                    <th class="text-center" width="10%">จำนวน</th>
                                    <th class="text-center" width="10%">หน่วย</th>
                                    <th class="text-center" width="10%">ผ่าน / ไม่ผ่าน</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">1.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment1"
                                               name="equipment1"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$leaderCheckTask -> equipment1}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount1"
                                               name="amount1"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$leaderCheckTask -> amount1}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit1"
                                               name="unit1"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$leaderCheckTask -> unit1}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission1" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$leaderCheckTask -> permission1 == 1 ? 'selected' : ''}} >ผ่าน</option>
                                            <option value="2" {{$leaderCheckTask -> permission1 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">2.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment2"
                                               name="equipment2"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$leaderCheckTask -> equipment2}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount2"
                                               name="amount2"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$leaderCheckTask -> amount2}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit2"
                                               name="unit2"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$leaderCheckTask -> unit2}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission2" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$leaderCheckTask -> permission2 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$leaderCheckTask -> permission2 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">3.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment3"
                                               name="equipment3"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$leaderCheckTask -> equipment3}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount3"
                                               name="amount3"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$leaderCheckTask -> amount3}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit3"
                                               name="unit3"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$leaderCheckTask -> unit3}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission3" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$leaderCheckTask -> permission3 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$leaderCheckTask -> permission3 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">4.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment4"
                                               name="equipment4"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$leaderCheckTask -> equipment1}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount4"
                                               name="amount4"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$leaderCheckTask -> amount4}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit4"
                                               name="unit4"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$leaderCheckTask -> unit4}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission4" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$leaderCheckTask -> permission4 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$leaderCheckTask -> permission4 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">5.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment5"
                                               name="equipment5"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$leaderCheckTask -> equipment5}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount5"
                                               name="amount5"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$leaderCheckTask -> amount5}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit5"
                                               name="unit5"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$leaderCheckTask -> unit5}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission5" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$leaderCheckTask -> permission5 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$leaderCheckTask -> permission5 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">6.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment6"
                                               name="equipment6"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$leaderCheckTask -> equipment6}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount6"
                                               name="amount6"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$leaderCheckTask -> amount6}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit6"
                                               name="unit6"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$leaderCheckTask -> unit6}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission6" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$leaderCheckTask -> permission6 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$leaderCheckTask -> permission6 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">7.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment7"
                                               name="equipment7"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$leaderCheckTask -> equipment7}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount7"
                                               name="amount7"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$leaderCheckTask -> amount7}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit7"
                                               name="unit7"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$leaderCheckTask -> unit7}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission7" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$leaderCheckTask -> permission7 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$leaderCheckTask -> permission7 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">8.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment8"
                                               name="equipment8"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$leaderCheckTask -> equipment8}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount8"
                                               name="amount8"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$leaderCheckTask -> amount8}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit8"
                                               name="unit8"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$leaderCheckTask -> unit8}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission8" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$leaderCheckTask -> permission8 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$leaderCheckTask -> permission8 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">9.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment9"
                                               name="equipment9"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$leaderCheckTask -> equipment9}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount9"
                                               name="amount9"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$leaderCheckTask -> amount9}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit9"
                                               name="unit9"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$leaderCheckTask -> unit9}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission9" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$leaderCheckTask -> permission9 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$leaderCheckTask -> permission9 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">10.</td>
                                    <td>
                                        <input type="text"
                                               id="equipment10"
                                               name="equipment10"
                                               class="form-control"
                                               placeholder="รายการเครื่องมือหรืออุปกรณ์"
                                               value="{{$leaderCheckTask -> equipment10}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="amount10"
                                               name="amount10"
                                               class="form-control"
                                               placeholder="จำนวน"
                                               value="{{$leaderCheckTask -> amount10}}" disabled>
                                    </td>
                                    <td>
                                        <input type="text"
                                               id="unit10"
                                               name="unit10"
                                               class="form-control"
                                               placeholder="หน่วย"
                                               value="{{$leaderCheckTask -> unit10}}" disabled>
                                    </td>
                                    <td>
                                        <select  class="form-control select2"  name="permission10" disabled>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="1" {{$leaderCheckTask -> permission10 == 1 ? 'selected' : ''}}>ผ่าน</option>
                                            <option value="2" {{$leaderCheckTask -> permission10 == 2 ? 'selected' : ''}}>ไม่ผ่าน</option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-body">
                        <span class="font-weight-bold mb-5">6.6 กรณีต้องการป้ายห้ามแตะ (Don't Touch) สำหรับ ผรม. โปรดระบุใบ</span>
                        <div class="row">
                            <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" width="10%">ลำดับ</th>
                                    <th class="text-center" width="10%">จำนวนใบ</th>
                                    <th class="text-center" width="10%">หน่วย</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">1.</td>
                                    <td>
                                        <input type="number"
                                               id="touch"
                                               name="touch"
                                               class="form-control"
                                               placeholder="จำนวนใบ"
                                               value="{{$leaderCheckTask -> touch}}" disabled>
                                    </td>
                                    <td class="text-center">
                                        หน่วย
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card-header ">
                        <h5 class="m-md-2">
                            ผู้ควบคุมและผู้รับเหมา(ตรวจสอบข้อมูล)
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" width="10%">ครั้งที่ {{$contractorCheckTasks[0]  -> time." ( ".substr($contractorCheckTasks[0]  -> check_time,0,5)." น. )"}}</th>
                                    <th class="text-center" width="10%">ครั้งที่ {{$contractorCheckTasks[1]  -> time." ( ".substr($contractorCheckTasks[1]  -> check_time,0,5)." น. )"}}</th>
                                    <th class="text-center" width="50%">รายการ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="equipment_personal" value="1"
                                                       type="checkbox" {{$contractorCheckTasks[0] -> equipment_personal == 1 ? 'checked' : ''}}
                                                       disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="equipment_personal" value="1"
                                                       type="checkbox" {{$contractorCheckTasks[1] -> equipment_personal == 1 ? 'checked' : ''}}
                                                       disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">1.สวมอุปกรณ์ป้องกันอันตรายส่วนบุคคล</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety_glasses" value="1"
                                                       {{$contractorCheckTasks[0] -> safety_glasses == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety_glasses" value="1"
                                                       {{$contractorCheckTasks[1] -> safety_glasses == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">2.แว่นตานิรภัย (Safety glasses)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety_belt" value="1"
                                                       {{$contractorCheckTasks[0] -> safety_belt == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety_belt" value="1"
                                                       {{$contractorCheckTasks[1] -> safety_belt == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">3.เข็มขัดนิรภัย (Safety belt)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="scba" value="1"
                                                       {{$contractorCheckTasks[0] -> scba == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="scba" value="1"
                                                       {{$contractorCheckTasks[1] -> scba == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">4.อุปกรณ์ช่วยหายใจ (SCBA)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="oximeter" value="1"
                                                       {{$contractorCheckTasks[0] -> oximeter == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="oximeter" value="1"
                                                       {{$contractorCheckTasks[1] -> oximeter == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">5.เครื่องวัดออกซิเจนครบถ้วน</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fan" value="1"
                                                       {{$contractorCheckTasks[0] -> fan == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fan" value="1"
                                                       {{$contractorCheckTasks[1] -> fan == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">6.พัดลมระบายอากาศครบถ้วน</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="acid_mask"
                                                       {{$contractorCheckTasks[0] -> acid_mask == 1 ? 'checked' : ''}}
                                                       value="1" type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="acid_mask"
                                                       {{$contractorCheckTasks[1] -> acid_mask == 1 ? 'checked' : ''}}
                                                       value="1" type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">7.หน้ากากป้องกันไอกรด (Acid fume protecting mash)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="sling" value="1"
                                                       {{$contractorCheckTasks[0] -> sling == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="sling" value="1"
                                                       {{$contractorCheckTasks[1] -> sling == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">8.สลิงอยู่ในสภาพพรอ้มใช้งาน (เป็นไปตามมาตรฐาน NS-SUS)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1" value="1"
                                                       {{$contractorCheckTasks[0] -> electric1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1" value="1"
                                                       {{$contractorCheckTasks[1] -> electric1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">9.เครื่องเชื่อมไฟฟ้า</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1_1" value="1"
                                                       {{$contractorCheckTasks[0] -> electric1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1_1" value="1"
                                                       {{$contractorCheckTasks[1] -> electric1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">9.1.สภาพสายไฟ(ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด,จุดต่อต้องมั่นคงแข็งแรง)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1_2" value="1"
                                                       {{$contractorCheckTasks[0] -> electric1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="electric1_2" value="1"
                                                       {{$contractorCheckTasks[1] -> electric1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">9.1.สภาพสาย Ground (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด,จุดต่อต้องมั่นคงแข็งแรง)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1" value="1"
                                                       {{$contractorCheckTasks[0] -> cut_gas1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1" value="1"
                                                       {{$contractorCheckTasks[1] -> cut_gas1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">10.เครื่องเชื่อม ตัด เผา Gas</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_1" value="1"
                                                       {{$contractorCheckTasks[0] -> cut_gas1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_1" value="1"
                                                       {{$contractorCheckTasks[1] -> cut_gas1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">10.1.Gauge ไม่แตก / สามารถอ่านค่าได ้</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_2" value="1"
                                                       {{$contractorCheckTasks[0] -> cut_gas1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_2" value="1"
                                                       {{$contractorCheckTasks[1] -> cut_gas1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">10.2. ต้องมีอุปกรณ์ป้องกันไฟย้อนกลับ (Flash back Arrestor) 4 ตำแหน่ง
                                        และมีสติกเกอร์แสดงการตรวจสอบติดอยู่</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_3" value="1"
                                                       {{$contractorCheckTasks[0] -> cut_gas1_3 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_3" value="1"
                                                       {{$contractorCheckTasks[1] -> cut_gas1_3 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">10.3.สาย GAS อยู่ในสภาพดีไม่แตกลายงา และมีการยึดสายกับข้อต่อที่ถัง GAS
                                        และ หัว GAS แน่นหนา ไม่รั่วซึม</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_4" value="1"
                                                       {{$contractorCheckTasks[0] -> cut_gas1_4 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_gas1_4" value="1"
                                                       {{$contractorCheckTasks[1] -> cut_gas1_4 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">10.4. จุดยึดถัง GAS ต้องมันคงแข็งแรงและมีโซ่หรือเชือกมัดถัง ไม่เสี่ยงต่อการล้ม
                                        ของถัง GAS</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1" value="1"
                                                       {{$contractorCheckTasks[0] -> cut_hand1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1" value="1"
                                                       {{$contractorCheckTasks[1] -> cut_hand1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">11.เครื่องตัด เจียรมือ</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_1" value="1"
                                                       {{$contractorCheckTasks[0] -> cut_hand1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_1" value="1"
                                                       {{$contractorCheckTasks[1] -> cut_hand1_1 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">11.1.สภาพสายไฟ (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด, จุดต่อต้องมั่นคงแข็งแรง)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_2" value="1"
                                                       {{$contractorCheckTasks[0] -> cut_hand1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_2" value="1"
                                                       {{$contractorCheckTasks[1] -> cut_hand1_2 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">11.2.สภาพสาย Ground (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด, จุดต่อต้องมั่นคงแข็งแรง)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_3" value="1"
                                                       {{$contractorCheckTasks[0] -> cut_hand1_3 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="cut_hand1_3" value="1"
                                                       {{$contractorCheckTasks[1] -> cut_hand1_3 == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">11.3. มี Guard ป้องกัน (Guard ต้องยึดอย่างมั่นคง ไม่หลวมคลอน)</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_ready" value="1"
                                                       {{$contractorCheckTasks[0] -> fire_ready == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_ready" value="1"
                                                       {{$contractorCheckTasks[1] -> fire_ready == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">12.อุปกรณ์ดับเพลิงมีพร้อมและตู้น้ำดับเพลิงอยู่ในสภาพพร้อมใช้งาน</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_amount" value="1"
                                                       {{$contractorCheckTasks[0] -> fire_amount == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_amount" value="1"
                                                       {{$contractorCheckTasks[1] -> fire_amount == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">13.จำนวนถังดับเพลิงมีครบถ้วน และตรวจสอบล่าสุด</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_resis" value="1"
                                                       {{$contractorCheckTasks[0] -> fire_resis == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="fire_resis" value="1"
                                                       {{$contractorCheckTasks[1] -> fire_resis == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">14.มีการจัดเตรียมแผ่นวัสดุทนไฟ สำหรับป้องกันสะเก็ดไฟกระเด็น
                                        (วัสดุป้องกันสะเก็ดไฟต้องไม่ติดไฟ)
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety" value="1"
                                                       {{$contractorCheckTasks[0] -> safety == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="safety" value="1"
                                                       {{$contractorCheckTasks[1] -> safety == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">15.มีสภาพการปฏิบัตงานที่ปลอดภัย

                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="contractor" value="1"
                                                       {{$contractorCheckTasks[0] -> contractor == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="contractor" value="1"
                                                       {{$contractorCheckTasks[1] -> contractor == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">หัวหน้างานผู้รับเหมายืนยัน</td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="leader" value="1"
                                                       {{$contractorCheckTasks[0] -> leader == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label font-weight-bold">
                                                <input class="form-check-input"
                                                       name="leader" value="1"
                                                       {{$contractorCheckTasks[1] -> leader == 1 ? 'checked' : ''}}
                                                       type="checkbox" disabled>
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="font-weight-bold">ผู้ควบคุมงานยืนยัน</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                    <form method="POST" action="" id="check_form"  class="needs-validation" novalidate>
                        @csrf
                        <div class="card mt-5">
                            <div class="card-header ">
                                <h5 class="m-md-2">
                                    ตรวจสอบหลังเสร็จงาน
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="5%">เลือก</th>
                                            <th class="text-center" width="50%">รายการ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="table-warning">
                                            <td class="text-center">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label font-weight-bold">
                                                        <input class="form-check-input" id="clean" name="clean" value="1" type="checkbox">
                                                        <span class="form-check-sign"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="font-weight-bold">ความสะอาดของหน้างาน</td>
                                        </tr>
                                        <tr class="table-warning">
                                            <td class="text-center">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label font-weight-bold">
                                                        <input class="form-check-input" id="clear" name="clear" value="1" type="checkbox">
                                                        <span class="form-check-sign"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="font-weight-bold">ความเรียบร้อยของงาน</td>
                                        </tr>
                                        <tr class="table-warning">
                                            <td class="text-center">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label font-weight-bold">
                                                        <input class="form-check-input" id="safe" name="safe" value="1" type="checkbox">
                                                        <span class="form-check-sign"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="font-weight-bold">ความปลอดภัย</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label font-weight-bold">
                                                        <input class="form-check-input" name="another" value="1" type="checkbox">
                                                        <span class="form-check-sign"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="font-weight-bold">อื่น ๆ โปรดระบุ <input type="text" size="30" name="another_word"></td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" id="submit" class="btn btn-primary btn-lg">บันทึกปิดงาน</button>
                                        <br/>
                                        <a href="javascript:history.go(-1)" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function() {
        $("#check_form").on("submit", function(){
            let clean = document.getElementById("clean").checked;
            let clear = document.getElementById("clear").checked;
            let safe = document.getElementById("safe").checked;
            console.log(clean,clear,safe)
            if(!clean || !clear || !safe){
                Swal.fire({
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'ตกลง',
                    title: 'กรุณาเลือกยืนยันช่องสีเหลืองให้ครบ',
                })
                return false;
            }
            if(clean && clear && safe){
                Swal.fire({
                    title: 'กรุณารอบันทึกข้อมูลสักครู่',
                    onOpen: () => {
                        swal.showLoading();
                    }
                })
                document.getElementById('submit').className = "btn btn-primary btn-lg disabled"
            }
        });//submit

    });
</script>
@stop
