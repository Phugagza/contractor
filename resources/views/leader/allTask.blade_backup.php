@extends('layouts.paper')
@section('title','จัดการข้อมูลงาน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('flash-message')
                <div class="card mt-5">
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="10%">ชื่องาน</th>
                                                <th class="text-center" width="5%">สถานะล่าสุด</th>
                                                <th class="text-center" width="10%">สถาที่ปฏิบัติงาน</th>
                                                <th class="text-center" width="15%">ผู้ควบคุมงาน</th>
                                                <th class="text-center" width="15%">หัวหน้าผู้รับเหมา</th>
                                                <th class="text-center" width="5%">No.</th>
                                                <th class="text-center" width="5%">สถานะ QR Code</th>
                                                <th class="text-center" width="10%">เข้างานล่าสุด</th>
                                                <th class="text-center" width="10%">วันที่ทำการลงทะเบียน</th>
                                                <th class="text-center" width="5%">QR Code</th>
                                                <th class="text-center" class="disabled-sorting text-center" width="10%">Action</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($tasks as $index => $task)
                                                <tr>
                                                    <td class="text-center"> {{$task -> task_title}}</td>
                                                    <td class="text-center">
                                                        @if($task -> task_type == null)
                                                            <h5>
                                                                <span class="badge badge-info">ลงทะเบียน</span>
                                                            </h5>
                                                        @elseif($task -> task_type != null && $task -> task_detail_status == null)
                                                            <h5>
                                                                <span class="badge badge-info">5 Job Rule</span>
                                                            </h5>
                                                        @elseif($task -> task_type != null && $task -> task_detail_status != null)
                                                            @if($task -> task_detail_status == 1)
                                                                <h5>
                                                                    <span class="badge badge-warning">รอเข้างาน</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 2)
                                                                <h5>
                                                                    <span class="badge badge-primary">อนุมัติ</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 3)
                                                                <h5>
                                                                    <span class="badge badge-primary">เริ่มทำงาน</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 4)
                                                                <h5>
                                                                    <span class="badge badge-warning">ตรวจสอบ 1</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 5)
                                                                <h5>
                                                                    <span class="badge badge-warning">ตรวจสอบ 2</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 6)
                                                                <h5>
                                                                    <span class="badge badge-warning">ตรวจสอบปิดงาน</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 7)
                                                                <h5>
                                                                    <span class="badge badge-success">ปิดงานสำเร็จ</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 8)
                                                                <h5>
                                                                    <span class="badge badge-danger">ปิดงานไม่สำเร็จ</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 9)
                                                                <h5>
                                                                    <span class="badge badge-danger">ยกเลิก</span>
                                                                </h5>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td class="text-center"> {{$task -> task_location}}</td>
                                                    <td class="text-center"> {{$task -> leader_prefix.$task -> leader_first_name." ".$task -> leader_last_name}}</td>
                                                    <td class="text-center"> {{$task -> contractor_prefix.$task -> contractor_first_name." ".$task -> contractor_last_name.' ('.$task -> enterprise_name.')'}}</td>
                                                    <td class="text-center"> {{$task -> task_detail_id}}</td>
                                                    <td class="text-center">
                                                        @if($task -> task_status == 1)
                                                            <h5>
                                                                <span class="badge badge-success">QR OK</span>
                                                            </h5>
                                                        @elseif($task -> task_status == 2)
                                                            <h5>
                                                                <span class="badge badge-danger">QR Close</span>
                                                            </h5>
                                                        @endif
                                                    </td>
                                                    @if($task -> register_task_detail_date != null)
                                                        <td class="text-center" data-sort='{{$task -> register_task_detail_date}}'>
                                                            <h5>
                                                            <span class="badge badge-success">
                                                              {{$task -> register_task_detail_date ? ConvertThaiDate($task -> register_task_detail_date) : '-'}}
                                                            </span>
                                                            </h5>
                                                        </td>
                                                    @else
                                                        <td class="text-center" data-sort='{{$task -> task_working_date}}'>
                                                            <h5>
                                                                <span class="badge badge-danger">
                                                                  {{$task -> task_working_date ? ConvertThaiDate($task -> task_working_date) : '-'}}
                                                                </span>
                                                            </h5>
                                                        </td>
                                                    @endif

                                                    <td class="text-center"> {{ConvertThaiDate($task -> task_register_date)}}</td>
                                                    <td class="text-center">
                                                        {{-- <button class="btn btn-primary" data-toggle="modal" data-target="#myQR{{$index}}">
                                                            แสดง
                                                        </button> --}}
                                                        <a href="{{route('leader.task.allTask.qrcode',['id'=> $task -> task_id])}}"
                                                            class="btn btn-primary"
                                                            id="hover"
                                                            data-toggle="tooltip"
                                                            data-placement="top"
                                                            title="ดู">
                                                             <i class="nc-icon nc-zoom-split"></i>
                                                         </a>
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="{{route('leader.task.edit',['id'=> $task -> task_id])}}"
                                                           class="btn btn-warning btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="แก้ไข">
                                                            <i class="nc-icon nc-ruler-pencil"></i>
                                                        </a>
                                                        <a href="{{route('leader.task.detailGroup',['id'=> $task -> task_id])}}"
                                                           class="btn btn-secondary btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="ดูรายละเอียดงาน">
                                                            <i class="nc-icon nc-bullet-list-67"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <script>
        $('#datatable').DataTable({
            responsive: true,
            // "ordering": false
            // serverSide: true,
            "order": [[ 7, "desc" ]],
        });

        DeleteTask=(id)=>{
            Swal.fire({
                title: 'ต้องการลบข้อมูลใช่หรือไม่ ?',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#c8d2dd',
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if(result.dismiss == 'cancel'){
                    Swal.hideLoading();
                }else if(result.dismiss == 'overlay'){
                    Swal.hideLoading();
                } else{
                    Swal.fire({
                        title: 'กรุณารอสักครู่',
                        onOpen: () => {
                            swal.showLoading();
                        }
                    })
                }
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url: "{{ route('admin.task.delete') }}",
                        method: 'post',
                        data: {
                            id: id
                        },
                        success: function(response){
                            if (response.success == 'Yes') {
                                Swal.fire({
                                    type: 'success',
                                    title: 'บันทึกข้อมูลเรียบร้อย',
                                    showConfirmButton: true,
                                    confirmButtonText: "OK!"
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                            else if(response.success == 'No'){
                                Swal.fire({
                                    type: 'error',
                                    title: 'ล้มเหลว',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({
                                    type: 'error',
                                    title: 'เกิดข้อผิดพลาด',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    </script>
@stop
