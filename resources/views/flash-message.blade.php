@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('openWorkpermit'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <p>เริ่มทำงานได้</p>
        <p>- Tool Box Meeting</p>
        <p>- ผรม.สแกน QR Code บนใบ Work Permit เพื่อตรวจสอบความปลอดภัยร่วมครั้งที่ 1</p>
        <p>- ผรม.สแกน QR Code บนใบ Work Permit เพื่อตรวจสอบความปลอดภัยร่วมครั้งที่ 2</p>
        <p>- ผรม.สแกน QR Code ครั้งที่ 3 เพื่อแจ้งตรวจสอบปิดงานร่วมกัน</p>
    </div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('info'))
<div class="alert alert-info alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $message }}</strong>
</div>
@endif


@if ($errors->any())
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">×</button>
	Please check the form below for errors
</div>
@endif

@if ($message = Session::get('checkPermission'))
    <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <p>ต้องผ่าน safety training หรือ ต้องมีใบรับการรับรองความรู้ความสามารถตาม พ.ร.บ. <a href="{{route('searchSafetyTraining')}}"> (คลิ๊กเพื่อตรวจสอบ)</a></p>
        @if(count($contractor_N) != 0)
            หัวหน้าผู้รับเหมา
            <ul>
                @foreach($contractor_N as $contractor)
                    <li>{{$contractor -> prefix.$contractor ->first_name." ".$contractor ->last_name}}</li>
                @endforeach
            </ul>
        @endif
        @if(count($subcontractor_N) != 0)
            ทีมงานผู้รับเหมา(ช่าง)
            <ul>
                @foreach($subcontractor_N as $index => $subcontractor)
                    <li>{{$subcontractor -> prefix.$subcontractor ->first_name." ".$subcontractor ->last_name}}</li>
                @endforeach
            </ul>
        @endif
        @if(count($assistant_subcontractor_N) != 0)
            ทีมงานผู้รับเหมา(ผู้ช่วยช่าง)
            <ul>
                @foreach($assistant_subcontractor_N as $index => $assistant_subcontractor)
                    <li>{{$assistant_subcontractor -> prefix.$assistant_subcontractor ->first_name." ".$assistant_subcontractor ->last_name}}</li>
                @endforeach
            </ul>
        @endif
    </div>
@endif
