@extends('layouts.paper')
@section('title','รายละเอียดการทำงาน')
@section('content')
<div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">รายละเอียดการทำงาน</li>
            </ol>
        </nav>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card mt-5">
                <div class="card">
                    <div class="card-header">
                        <h5>รายละเอียดเกี่ยวกับ...</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-4 col-6">
                                <div class="nav-tabs-navigation verical-navs">
                                    <div class="nav-tabs-wrapper">
                                        <ul class="nav nav-tabs flex-column nav-stacked" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active show" href="#info" role="tab" data-toggle="tab" aria-selected="true">ผู้เกี่ยวข้อง</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#sumTask" role="tab" data-toggle="tab" aria-selected="true">รายละเอียดงานทั้งหมด</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-7 col-sm-8 col-6">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active show" id="info">
                                        <p>หัวหน้าโครงงาน : นายอิอิซ่า 55+</p>
                                        <p>ผู้รับผิดชอบ : นายภูริณัฐ บุณยะชัย</p>
                                    </div>
                                    <div class="tab-pane" id="sumTask">
                                        <p>เวลาทั้งหมดที่ใช้ : 18 ชม.</p>
                                        <p>จำนวนครั้งทั้งหมดที่เข้างาน : 20 ครั้ง</p>
                                        <p>เวลาเฉลี่ยที่ใช้ : 5 ชม</p>
                                        <p>จำนวนทั้งหมดที่ไม่สำเร็จ : 0 ครั้ง</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="card mt-5">
                <div class="card-body" id="detailTask" style="display: inline;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-body">
                                <div class="row">
                                    <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="20%">สถานะ</th>
                                            <th class="text-center" width="20%">วันที่</th>
                                            <th class="text-center" width="20%">เวลาที่เริ่มต้น</th>
                                            <th class="text-center" width="20%">เวลาที่สิ้นสุด</th>
                                            <th class="text-center" width="20%">รวมเวลาทั้งหมด</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">
                                                <h5>
                                                    <span class="badge badge-warning">กำลังดำเนินงาน</span>
                                                </h5>
                                            </td>
                                            <td class="text-center"> 18 ธ.ค 2563</td>
                                            <td class="text-center"> 13.00 น.</td>
                                            <td class="text-center"> 18.00 น.</td>
                                            <td class="text-center"> 8 ชม.</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
</div>

<script>
    $('#datatable').DataTable({
        responsive: true
    });
</script>
@stop
