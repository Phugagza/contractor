@extends('layouts.paper')
@section('title','เพิ่มคำขอ')
@section('content')
<div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">เพิ่มคำขอ</li>
            </ol>
        </nav>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <form method="POST" action="" class="needs-validation" novalidate>
                @csrf
                <div class="card mt-5">
                    <div class="card-header ">
                        <h5 class="m-md-2">
                            กรอกข้อมูลคำขอ
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>หัวข้อคำขอ</label>
                                    <input type="text"
                                           id="TaskTitle"
                                           name="TaskTitle"
                                           class="form-control"
                                           placeholder="หัวข้อคำขอ"
                                           value=""
                                           required>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        โปรดระบุ คำขอ
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ผู้รับผิดชอบ</label>
                                    <select class="form-control"
                                            name="contractor_id"
                                            id="contractor_id">
                                        <option value="0">test</option>
                                    </select>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        โปรดระบุ ผู้รับผิดชอบ
                                    </div>
                                </div>
                            </div>
                            </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>หัวหน้าโครงการ</label>
                                    <select class="form-control"
                                            name="Leader_id"
                                            id="Leader_id">
                                        <option value="0">test</option>
                                    </select>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        โปรดระบุ หัวหน้าโครงการ
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>รายละเอียด</label>
                                    <textarea class="form-control" rows="5" id="TaskDescription" name="TaskDescription">

                                    </textarea>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        ...
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                <br/>
                                <a href="javascript:history.go(-1)" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                            </div>
                        </div>
                    </div>


            </form>
        </div>
    </div>
</div>
@stop
