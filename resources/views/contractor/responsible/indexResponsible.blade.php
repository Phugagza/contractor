@extends('layouts.paper')
@section('title','จัดการผู้รับผิดชอบ')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <div class="card mt-5">
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-header">
                                    <a href="{{route('contractor.editResponsible')}}"
                                       class="btn btn-primary">
                                        เพิ่มผู้รับผิดชอบ
                                    </a>

                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="20%">ชื่อ - นามสกุล</th>
                                                <th class="text-center" width="20%">เข้าทำงานล่าสุด</th>
                                                <th class="text-center" width="20%" class="disabled-sorting text-center">Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="text-center">นายภูริณัฐ บุณยะชัย</td>
                                                <td class="text-center">18 มกราคม 2563</td>
                                                <td class="text-center">
                                                    <a href="{{route('contractor.detailResponsible',['id'=> 1])}}"
                                                       class="btn btn-secondary btn-icon btn-sm like"
                                                       id="hover"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title="ดูรายละเอียดผู้รับผิดชอบ">
                                                        <i class="nc-icon nc-bullet-list-67"></i>
                                                    </a>
                                                    <a href="#"
                                                       class="btn btn-warning btn-icon btn-sm like"
                                                       id="hover"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title="แก้ไขคำขอ">
                                                        <i class="nc-icon nc-ruler-pencil"></i>
                                                    </a>
                                                    <a href="#"
                                                       class="btn btn-danger btn-icon btn-sm like"
                                                       id="hover"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title="ลบคำขอ">
                                                        <i class="nc-icon nc-simple-remove"></i>
                                                    </a>
                                                </td>
                                                <div class="modal fade" id="qrCode" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                    <i class="nc-icon nc-simple-remove"></i>
                                                                </button>
                                                                <h5 class="modal-title" id="myModalLabel">หัวข้ออออออออออออ</h5>
                                                            </div>
                                                            <div class="modal-body text-center">
                                                                <img src="https://via.placeholder.com/250">
                                                                <p></p>
                                                                <h5>บันทึกเวลาของวันที่</h5>
                                                                <h5>16 ธันวาคม 2563</h5>
                                                            </div>
                                                            <div class="modal-footer justify-content-center">
                                                                <button type="button" class="btn btn-danger btn-round" data-dismiss="modal">ปิด</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <script>
        $('#datatable').DataTable({
            responsive: true
        });
    </script>
@stop
