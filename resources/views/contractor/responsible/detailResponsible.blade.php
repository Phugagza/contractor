@extends('layouts.paper')
@section('title','รายละเอียดผู้รับผิดชอบ')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">รายละเอียดผู้รับผิดชอบ</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card">
                        <div class="card-header">
                            <h5>นายภูริณัฐ บุณยะชัยยยยย ชั่ยๆๆๆ</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4 col-md-5 col-sm-4 col-6">
                                    <div class="nav-tabs-navigation verical-navs">
                                        <div class="nav-tabs-wrapper">
                                            <ul class="nav nav-tabs flex-column nav-stacked" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active show" href="#info" role="tab" data-toggle="tab" aria-selected="true">รายละเอียด</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-7 col-sm-8 col-6">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active show" id="info">
                                            <p>จำนวนโครงการทั้งหมด :  800 งาน</p>
                                            <p>จำนวนชั่วโมงทั้งหมด : นายภูริณัฐ บุณยะชัย</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card-body" id="detailTask" style="display: inline;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="20%">หัวข้องาน</th>
                                                <th class="text-center" width="20%">วันที่เริ่ม</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="text-center"> ไปทำท่อครับ</td>
                                                <td class="text-center"> 12 ธันวาคม 2563</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>

    <script>
        $('#datatable').DataTable({
            responsive: true
        });
    </script>
@stop
