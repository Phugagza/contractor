@extends('layouts.paper')
@section('title','จัดการข้อมูลผู้ควบคุม')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">{{$leader -> id > 0 ? 'แก้ไขข้อมูลผู้ควบคุม' : 'เพิ่มข้อมูลผู้ควบคุม' }}</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form method="POST" action="" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                กรอกข้อมูล
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>คำนำหน้า</label>
                                        <input type="text"
                                               id="prefix"
                                               name="prefix"
                                               class="form-control"
                                               placeholder="คำนำหน้า"
                                               value="{{$leader -> prefix}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ คำนำหน้า
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>ชื่อ</label>
                                        <input type="text"
                                               id="first_name"
                                               name="first_name"
                                               class="form-control"
                                               placeholder="ชื่อ"
                                               value="{{$leader -> first_name}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ ชื่อ
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>นามสกุล</label>
                                        <input type="text"
                                               id="last_name"
                                               name="last_name"
                                               class="form-control"
                                               placeholder="นามสกุล"
                                               value="{{$leader -> last_name}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ นามสกุล
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>อีเมล์</label>
                                        <input type="email"
                                               id="email"
                                               name="email"
                                               class="form-control"
                                               placeholder="อีเมล์"
                                               value="{{$leader -> email}}"
                                               onchange="checkDuplicate(this.value)"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ อีเมล์
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>รหัสผ่าน</label>
                                        <input type="password"
                                               id="password"
                                               name="password"
                                               class="form-control"
                                               placeholder="รหัสผ่าน"
                                               value=""
                                               >
                                        <input type="hidden"
                                               name="password_hidden"
                                               value="{{$leader -> password}}">

                                        <span class="form-text">
                                            <div class="form-check">
                                              <label class="form-check-label">
                                                <input class="form-check-input" id="password_check" type="checkbox" onchange="changeTypePassword()">
                                                <span class="form-check-sign"></span>
                                                แสดงรหัสผ่าน
                                              </label>
                                            </div>
                                        </span>

                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ รหัสผ่าน
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>กลุ่มไลน์</label>
                                        <select  class="form-control select2"  name="line_token">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="A7xbCdEAfBn1RTCEdv7pySy2g9uTZxpv3jfjpKhM4Ky" {{$leader -> line_token == "A7xbCdEAfBn1RTCEdv7pySy2g9uTZxpv3jfjpKhM4Ky" ? 'selected' : ''}}>ทดสอบ 1</option>
                                            <option value="fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf" {{$leader -> line_token == "fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf" ? 'selected' : ''}}>ME 1</option>
                                            <option value="rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm" {{$leader -> line_token == "rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm" ? 'selected' : ''}}>ME 2</option>
                                            <option value="fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl" {{$leader -> line_token == "fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl" ? 'selected' : ''}}>ME 3</option>
                                            <option value="gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk" {{$leader -> line_token == "gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk" ? 'selected' : ''}}>ME 4</option>
                                            <option value="BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ" {{$leader -> line_token == "BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ" ? 'selected' : ''}}>ME 5</option>
                                            <option value="EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq" {{$leader -> line_token == "EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq" ? 'selected' : ''}}>MRG</option>
                                            <option value="NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW" {{$leader -> line_token == "NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW" ? 'selected' : ''}}>MAG</option>
                                            <option value="6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri" {{$leader -> line_token == "6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri" ? 'selected' : ''}}>MUG</option>
                                            <option value="jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m" {{$leader -> line_token == "jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m" ? 'selected' : ''}}>EE 1</option>
                                            <option value="AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH" {{$leader -> line_token == "AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH" ? 'selected' : ''}}>EE 2</option>
                                            <option value="hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d" {{$leader -> line_token == "hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d" ? 'selected' : ''}}>EE 3</option>
                                            <option value="G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT" {{$leader -> line_token == "G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT" ? 'selected' : ''}}>PCSI</option>
                                            <option value="x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ" {{$leader -> line_token == "x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ" ? 'selected' : ''}}>EE 5</option>
                                            <option value="GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc" {{$leader -> line_token == "GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc" ? 'selected' : ''}}>ENERGY</option>
                                            <option value="FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM" {{$leader -> line_token == "FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM" ? 'selected' : ''}}>ENGINEERING</option>
                                            <option value="SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf" {{$leader -> line_token == "SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf" ? 'selected' : ''}}>PM</option>
                                            <option value="AMScd53BO35HJqmyWW7Y2C2MVZBgk5sSVucKxezwuWy" {{$leader -> line_token == "AMScd53BO35HJqmyWW7Y2C2MVZBgk5sSVucKxezwuWy" ? 'selected' : ''}}>PD 1</option>
                                            <option value="JQwvJaW0OdH8i6HuoxiWQsLiggMvvbW6zqNuryMAUL1" {{$leader -> line_token == "JQwvJaW0OdH8i6HuoxiWQsLiggMvvbW6zqNuryMAUL1" ? 'selected' : ''}}>PD 2</option>
                                            <option value="MNR2wcZsweShOHuQEMX2s1o0Hxc8Hrs8gOfWTvuevrC" {{$leader -> line_token == "MNR2wcZsweShOHuQEMX2s1o0Hxc8Hrs8gOfWTvuevrC" ? 'selected' : ''}}>PD 3</option>
                                            <option value="hUys8VH8x9NP0Bnrv0r31mWiAEdLMpTwvfhwgBiAPdp" {{$leader -> line_token == "hUys8VH8x9NP0Bnrv0r31mWiAEdLMpTwvfhwgBiAPdp" ? 'selected' : ''}}>PD 4</option>
                                            <option value="gmMv6fQe9j8u7CLdTNr5PWU2As0cQuSkxYyT0z4wsWu" {{$leader -> line_token == "gmMv6fQe9j8u7CLdTNr5PWU2As0cQuSkxYyT0z4wsWu" ? 'selected' : ''}}>RS</option>
                                            <option value="ARnNiMwv8EaQYc6PTRcEqVaitfuOFld3ZROOsfrwIlz" {{$leader -> line_token == "ARnNiMwv8EaQYc6PTRcEqVaitfuOFld3ZROOsfrwIlz" ? 'selected' : ''}}>EU</option>
                                            <option value="TPaOhHmy7JQsDFOw5dTTZ8Jwn5gy2V6jCNhF4aDEduJ" {{$leader -> line_token == "TPaOhHmy7JQsDFOw5dTTZ8Jwn5gy2V6jCNhF4aDEduJ" ? 'selected' : ''}}>Office 1</option>
                                            <option value="Ha6wlWxi2cDIRaT7p7ANVlrUEQYEB8z3IolzJRWjfJB" {{$leader -> line_token == "Ha6wlWxi2cDIRaT7p7ANVlrUEQYEB8z3IolzJRWjfJB" ? 'selected' : ''}}>Office 2</option>
                                            <option value="8Jp0bUQPYtOKYRUun8Q5gbppHvPPjCiuiJ1LONZxby5" {{$leader -> line_token == "8Jp0bUQPYtOKYRUun8Q5gbppHvPPjCiuiJ1LONZxby5" ? 'selected' : ''}}>Other</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดเลือกกลุ่มไลน์
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>เบอร์โทร</label>
                                        <input type="number"
                                               id="tel"
                                               name="tel"
                                               class="form-control"
                                               placeholder="เบอร์โทร"
                                               value="{{$leader -> tel}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ เบอร์โทร
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                    <br/>
                                    <a href="javascript:history.go(-1)" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>

    </div>

    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });

        changeTypePassword=()=>{
            var getChecked = document.getElementById("password_check");
            if(getChecked.checked == true){
                document.getElementById('password').setAttribute('type','text')
            }else{
                document.getElementById('password').setAttribute('type','password')
            }
        }

        checkDuplicate=(keyword)=>{
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            jQuery.ajax({
                url: "{{ route('admin.checkEmailLeader') }}",
                method: 'get',
                data: {
                    keyword: keyword
                },
                success: function(response){
                    if(response.email.length > 0){
                        Swal.fire({
                            type: 'error',
                            title: 'ข้อมูลซ้ำกรุณาลองใหม่',
                            showConfirmButton: true,
                            confirmButtonText: "OK!"
                        }).then((result) => {
                            if (result.value) {
                                $('#email').val('');
                            }
                        });
                    }


                }
            });
        }
    </script>
@stop
