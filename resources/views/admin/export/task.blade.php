@extends('layouts.paper')
@section('title','Export ข้อมูลงาน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">Export ข้อมูลงาน</li>
            </ol>
        </nav>
        @include('flash-message')
        <div class="row">
            <div class="col-md-3">
                <form method="POST" action="{{route('admin.export.task')}}" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                Export งานทั้งหมด
                            </h5>
                        </div>
                        <div class="card-body">
                            <button type="submit" class="btn btn-info btn-block">Download</button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-md-3">
                <form method="POST" action="{{route('admin.export.task')}}" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                Export งานเฉพาะวัน
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>วันที่</label>
                                        <input type="text"
                                               id="register_date_start"
                                               name="register_date_start"
                                               class="form-control datepicker"
                                               placeholder="วันที่"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันที่
                                        </div>
                                </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-block">Download</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-md-6">
                <form method="POST" action="{{route('admin.export.task')}}" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                Export งานแบบช่วงวัน
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>วันเริ่มต้น</label>
                                        <input type="text"
                                               id="register_date_start"
                                               name="register_date_start"
                                               class="form-control datepicker"
                                               placeholder="วันเริ่มต้น"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันเริ่มต้น
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>วันสิ้นสุด</label>
                                        <input type="text"
                                               id="register_date_end"
                                               name="register_date_end"
                                               class="form-control datepicker"
                                               placeholder="วันสิ้นสุด"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันสิ้นสุด
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-block">Download</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });

        });

    </script>

@stop
