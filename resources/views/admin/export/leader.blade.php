@extends('layouts.paper')
@section('title','Export ข้อมูลผู้ควบคุมงาน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">Export ข้อมูลผู้ควบคุมงาน</li>
            </ol>
        </nav>
        @include('flash-message')
        <div class="row">
            <div class="col-md-3">
                <form method="POST" action="{{route('admin.export.leader')}}" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                Export ข้อมูลผู้ควบคุมงานทั้งหมด
                            </h5>
                        </div>
                        <div class="card-body">
                            <button type="submit" class="btn btn-info btn-block">Download</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@stop
