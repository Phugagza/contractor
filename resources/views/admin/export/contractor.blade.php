@extends('layouts.paper')
@section('title','Export ข้อมูลผู้รับเหมา')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">Export ข้อมูลผู้รับเหมา</li>
            </ol>
        </nav>
        @include('flash-message')
        <div class="row">
            <div class="col-md-3">
                <form method="POST" action="{{route('admin.export.contractor')}}" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                Export ผู้รับเหมาทั้งหมด
                            </h5>
                        </div>
                        <div class="card-body">
                            <button type="submit" class="btn btn-info btn-block">Download</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <form method="POST" action="{{route('admin.export.contractor')}}" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                Export ผู้รับเหมาทั้งหมดเฉพาะบริษัท
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>บริษัท</label>
                                        <select  class="form-control select2"  name="enterprise_id">
                                            <option value="">กรุณาเลือก</option>
                                            @foreach($enterprises as $enterprise)
                                                <option value="{{$enterprise -> id}}"
                                                    >
                                                    {{$enterprise -> name}}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุบริษัท
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info btn-block">Download</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
@stop
