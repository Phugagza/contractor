@extends('layouts.paper')
@section('title','จัดการข้อมูลผู้ฝึกอบรม')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">{{$admin -> id > 0 ? 'แก้ไขข้อมูลผู้ฝึกอบรม' : 'เพิ่มข้อมูลผู้ฝึกอบรม'}}</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form method="POST" action="" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                กรอกข้อมูล
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>คำนำหน้า</label>
                                        <input type="text"
                                               id="prefix"
                                               name="prefix"
                                               class="form-control"
                                               placeholder="คำนำหน้า"
                                               value="{{$admin -> prefix}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ คำนำหน้า
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>ชื่อ</label>
                                        <input type="text"
                                               id="first_name"
                                               name="first_name"
                                               class="form-control"
                                               placeholder="ชื่อ"
                                               value="{{$admin -> first_name}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ ชื่อ
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>นามสกุล</label>
                                        <input type="text"
                                               id="last_name"
                                               name="last_name"
                                               class="form-control"
                                               placeholder="นามสกุล"
                                               value="{{$admin -> last_name}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ นามสกุล
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>อีเมล์</label>
                                        <input type="email"
                                               id="email"
                                               name="email"
                                               class="form-control"
                                               placeholder="อีเมล์"
                                               value="{{$admin -> email}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ อีเมล์
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>รหัสผ่าน</label>
                                        <input type="password"
                                               id="password"
                                               name="password"
                                               class="form-control"
                                               placeholder="รหัสผ่าน"
                                               value=""
                                        >
                                        <input type="hidden"
                                               name="password_hidden"
                                               value="{{$admin -> password}}">

                                        <span class="form-text">
                                            <div class="form-check">
                                              <label class="form-check-label">
                                                <input class="form-check-input" id="password_check" type="checkbox" onchange="changeTypePassword()">
                                                <span class="form-check-sign"></span>
                                                แสดงรหัสผ่าน
                                              </label>
                                            </div>
                                        </span>

                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ รหัสผ่าน
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                    <br/>
                                    <a href="javascript:history.go(-1)" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
        changeTypePassword=()=>{
            var getChecked = document.getElementById("password_check");
            if(getChecked.checked == true){
                document.getElementById('password').setAttribute('type','text')
            }else{
                document.getElementById('password').setAttribute('type','password')
            }
        }
    </script>
@stop
