@extends('layouts.paper')
@section('title','จัดการข้อมูล Safety Training')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">แก้ไขข้อมูล Safety Training</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form method="POST" action="" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                แก้ไข Safety Training ({{$contractor -> perfix.$contractor -> first_name." ".$contractor -> last_name}})
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>ผู้ฝึกอบรม</label>
                                    <select  class="form-control select2"  name="trainer_id">
                                        <option value="">กรุณาเลือก</option>
                                        @foreach($trainers as $trainer)
                                            <option value="{{$trainer -> id}}" {{$contractor -> trainer_id == $trainer -> id ? 'selected' : ''}}>
                                                {{$trainer -> prefix.
                                                  $trainer -> first_name." ".
                                                  $trainer-> last_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        โปรดระบุผู้ฝึกอบรม
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="training_date"
                                               name="training_date"
                                               class="form-control"
                                               value="{{$contractor -> safety_training_date}}"
                                               placeholder="วันอบรม"
                                        >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="expired_date" onchange="expired(this.value)">
                                            <option value="365">1 ปี</option>
                                            <option value="another">ระบุวัน</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 d-none" id="hiddenExpired">
                                    <div class="form-group">
                                        <label>ระบุ(วัน)</label>
                                        <input type="number" class="form-control" id="expired_another" name="expired_another" placeholder="ระบุ" min="1">
                                    </div>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        โปรดระบุ
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                    <br/>
                                    <a href="javascript:history.go(-1)" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>

    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $('#training_date').datetimepicker({
                // format: 'DD-MM-YYYY'
                format: 'YYYY-MM-DD'
            });
        });

        expired=(value)=>{
            if(value == "another"){
                document.getElementById("hiddenExpired").className = 'col-md-2';
                document.getElementById("expired_another").required = true;
            }else{
                document.getElementById("hiddenExpired").className = 'col-md-2 d-none';
                document.getElementById("expired_another").value = '';
                document.getElementById("expired_another").required = false;
            }
        }

        checkDuplicate=(keyword)=>{
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            jQuery.ajax({
                url: "{{ route('admin.checkCitizen') }}",
                method: 'get',
                data: {
                    keyword: keyword
                },
                success: function(response){
                    if(response.citizen.length > 0){
                        Swal.fire({
                            type: 'error',
                            title: 'ข้อมูลซ้ำกรุณาลองใหม่',
                            showConfirmButton: true,
                            confirmButtonText: "OK!"
                        }).then((result) => {
                            if (result.value) {
                                $('#citizen_id').val('');
                            }
                        });
                    }


                }
            });
        }
    </script>
@stop
