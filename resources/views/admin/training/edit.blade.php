@extends('layouts.paper')
@section('title','จัดการข้อมูลการอบรม')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">{{$training -> id > 0 ? 'แก้ไขข้อมูลการอบรม' : 'เพิ่มข้อมูลการอบรม' }}</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form method="POST" action="" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                กรอกข้อมูล
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>หัวข้อการอบรม</label>
                                        <input type="text"
                                               id="name"
                                               name="name"
                                               class="form-control"
                                               placeholder="หัวข้อการอบรม"
                                               value="{{$training -> name}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ หัวข้อการอบรม
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>รายละเอียด</label>
                                        <textarea
                                            id="description"
                                            name="description"
                                            class="form-control"
                                        >
                                        {{$training -> description}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                    <br/>
                                    <a href="javascript:history.go(-1)" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>

    </div>

    <script>

    </script>
@stop
