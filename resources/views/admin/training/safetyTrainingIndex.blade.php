@extends('layouts.paper')
@section('title','จัดการข้อมูลผู้รับเหมา')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <div class="card mt-5">
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card-header">
                                    <h3>ข้อมูลผู้รับเหมา</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="5%">Safety Training</th>
                                                <th class="text-center" width="20%">ชื่อ - นามสกุล</th>
                                                <th class="text-center" width="10%">บริษัท</th>
                                                <th class="text-center" width="20%" class="disabled-sorting text-center">Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($contractors as $contractor)
                                                <tr>
                                                    <td class="text-center">
                                                        @if($contractor -> permission == "Y")
                                                            <h6>
                                                                <span class="badge badge-success">Y</span>
                                                            </h6>
                                                        @elseif($contractor -> permission == "N")
                                                            <h6>
                                                                <span class="badge badge-danger">N</span>
                                                            </h6>
                                                        @endif
                                                    </td>
                                                    <td class="text-center"> {{$contractor -> prefix}}{{$contractor -> first_name}} {{$contractor -> last_name}}</td>
                                                    <td class="text-center"> {{$contractor -> enterprise_name ? $contractor -> enterprise_name : '-'}}</td>
                                                    <td class="text-center">
                                                        <a href="{{route('admin.trainer.safetyTraining.edit',['id'=> $contractor -> id])}}"
                                                           class="btn btn-warning btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="แก้ไขข้อมูล Safety Traning">
                                                            <i class="nc-icon nc-ruler-pencil"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <script>
        $('#datatable').DataTable({
            responsive: true,
            "order": [[ 1, "asc" ]]
        });

    </script>
@stop
