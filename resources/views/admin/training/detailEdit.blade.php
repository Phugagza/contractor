@extends('layouts.paper')
@section('title','จัดการข้อมูลการอบรม')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item" aria-current="page">{{$training -> name}}</li>
                <li class="breadcrumb-item active" aria-current="page">แก้ไขข้อมูล</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form method="POST" action="" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                แก้ไขข้อมูล-{{$contractor ->contractor_prefix.$contractor ->contractor_first_name." ".$contractor ->contractor_last_name."(".$contractor ->enterprise_name.")"}}
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันอบรม</label>
                                        <input type="text"
                                               id="training_date"
                                               name="training_date"
                                               class="form-control"
                                               placeholder="วันอบรมใหม่"
                                               value=""
                                               required>
                                        <span class="form-text">วันอบรมเดิม {{$training_detail ->training_date}}</span>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรมใหม่
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="expired_date" onchange="expired(this.value)" required>
                                            <option value="365">1 ปี</option>
                                            <option value="another">ระบุวัน</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 d-none" id="hiddenExpired">
                                    <div class="form-group">
                                        <label>ระบุ(วัน)</label>
                                        <input type="number" class="form-control" id="expired_another" name="expired_another" placeholder="ระบุ" min="1">
                                    </div>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        โปรดระบุ
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="permission" required>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$training_detail -> permission == "Y" ? "selected" : ""}}>Y</option>
                                            <option value="N" {{$training_detail -> permission == "N" ? "selected" : ""}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                    <br/>
                                    <a href="{{route('admin.training.detail' ,['training_id' => $training_detail -> training_id])}}" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#training_date').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });

        expired=(value)=>{
            if(value == "another"){
                document.getElementById("hiddenExpired").className = 'col-md-2';
                document.getElementById("expired_another").required = true;
            }else{
                document.getElementById("hiddenExpired").className = 'col-md-2 d-none';
                document.getElementById("expired_another").value = '';
                document.getElementById("expired_another").required = false;
            }
        }

    </script>
@stop


