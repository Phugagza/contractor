@extends('layouts.paper')
@section('title','จัดการข้อมูลการอบรม')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">{{$training -> name}}</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <form method="POST" action="" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                กรอกข้อมูล
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>ผู้รับเหมา</label>
                                    <select  class="form-control select2"  name="contractor_id" required>
                                        <option value="">กรุณาเลือก</option>
                                        @foreach($contractors as $contractor)
                                            <option value="{{$contractor -> contractor_id}}">
                                                {{$contractor -> contractor_prefix.$contractor -> contractor_name.
                                                  $contractor -> contractor_first_name." ".
                                                  $contractor -> contractor_last_name.
                                                  " (".($contractor -> enterprise_name ? $contractor -> enterprise_name : "-").")"}}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        โปรดระบุผู้รับเหมา
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>ผู้ฝึกอบรม</label>
                                    <select  class="form-control select2"  name="trainer_id" required>
                                        <option value="">กรุณาเลือก</option>
                                        @foreach($trainers as $trainer)
                                            <option value="{{$trainer -> id}}">
                                                {{$trainer -> prefix.
                                                  $trainer -> first_name." ".
                                                  $trainer-> last_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        โปรดระบุผู้ฝึกอบรม
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันอบรม</label>
                                        <input type="text"
                                               id="training_date"
                                               name="training_date"
                                               class="form-control"
                                               placeholder="วันอบรม"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="expired_date" onchange="expired(this.value)" required>
                                            <option value="365">1 ปี</option>
                                            <option value="another">ระบุวัน</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 d-none" id="hiddenExpired">
                                    <div class="form-group">
                                        <label>ระบุ(วัน)</label>
                                        <input type="number" class="form-control" id="expired_another" name="expired_another" placeholder="ระบุ" min="1">
                                    </div>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        โปรดระบุ
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="permission" required>
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y">Y</option>
                                            <option value="N">N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                    <br/>
                                    <a href="{{route('admin.training.index')}}" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">

                            <div class="col-md-12">
                                <h5 class="m-md-2">
                                    {{$training -> name}}
                                </h5>
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="10%">เลขบัตรประชาชน</th>
                                                <th class="text-center" width="20%">ชื่อ - นามสกุล</th>
                                                <th class="text-center" width="10%">บริษัท</th>
                                                <th class="text-center" width="20%">ผู้ฝึกอบรม</th>
                                                <th class="text-center" width="10%">วันอบรม</th>
                                                <th class="text-center" width="10%">วันหมดอายุ</th>
                                                <th class="text-center" width="10%">เวลาที่เหลือ</th>
                                                <th class="text-center" width="5%">Permission</th>
                                                <th class="text-center" width="10%" class="disabled-sorting text-center">Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($training_details as $training_detail)
                                                <tr>
                                                    <td class="text-center">{{$training_detail -> contractor_citizen_id}}</td>
                                                    <td class="text-center">
                                                        {{$training_detail -> contractor_prefix.$training_detail -> contractor_first_name." ".$training_detail -> contractor_last_name}}
                                                    </td>
                                                    <td class="text-center">{{$training_detail -> enterprise_name ? $training_detail -> enterprise_name : '-'}}</td>
                                                    <td class="text-center">{{$training_detail -> trainer_first_name." ".$training_detail -> trainer_last_name}}</td>
                                                    <td class="text-center">{{ConvertThaiDate($training_detail -> training_date)}}</td>
                                                    <td class="text-center">{{ConvertThaiDate($training_detail -> expired_date)}}</td>
                                                    <td class="text-center">
                                                        @if($training_detail -> count_date >= 0)
                                                            {{$training_detail -> count_date}}
                                                        @elseif($training_detail -> count_date < 0)
                                                            <h6>
                                                                <span class="badge badge-danger">หมดอายุ</span>
                                                            </h6>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        @if($training_detail -> permission == "Y")
                                                            <h6>
                                                                <span class="badge badge-success">Y</span>
                                                            </h6>
                                                        @elseif($training_detail -> permission == "N")
                                                            <h6>
                                                                <span class="badge badge-danger">N</span>
                                                            </h6>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="{{route('admin.training.detail.edit',['training_id' => $training_detail -> training_id ,'training_detail_id' => $training_detail -> training_detail_id])}}"
                                                           class="btn btn-warning btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="แก้ไขข้อมูลการอบรม">
                                                            <i class="nc-icon nc-ruler-pencil"></i>
                                                        </a>
                                                        <a href=""
                                                           onclick="DeleteTraining({{$training_detail -> training_detail_id}}); return false"
                                                           class="btn btn-danger btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="ลบข้อมูลการอบรม">
                                                            <i class="nc-icon nc-simple-remove"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $('#datatable').DataTable({
                responsive: true,
                "order": [[ 7, "desc" ]]
            });

            $('#training_date').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });

        expired=(value)=>{
            if(value == "another"){
                document.getElementById("hiddenExpired").className = 'col-md-2';
                document.getElementById("expired_another").required = true;
            }else{
                document.getElementById("hiddenExpired").className = 'col-md-2 d-none';
                document.getElementById("expired_another").value = '';
                document.getElementById("expired_another").required = false;
            }
        }

        DeleteTraining=(id)=>{
            Swal.fire({
                title: 'ต้องการลบข้อมูลใช่หรือไม่ ?',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#c8d2dd',
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if(result.dismiss == 'cancel'){
                    Swal.hideLoading();
                }else if(result.dismiss == 'overlay'){
                    Swal.hideLoading();
                } else{
                    Swal.fire({
                        title: 'กรุณารอสักครู่',
                        onOpen: () => {
                            swal.showLoading();
                        }
                    })
                }
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url: "{{ route('admin.training.detail.delete') }}" + "/" + id,
                        method: 'get',
                        success: function(response){
                            console.log(response.success);
                            if (response.success == 'Yes') {
                                Swal.fire({
                                    type: 'success',
                                    title: 'บันทึกข้อมูลเรียบร้อย',
                                    showConfirmButton: true,
                                    confirmButtonText: "OK!"
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                            else if(response.success == 'No'){
                                Swal.fire({
                                    type: 'error',
                                    title: 'ล้มเหลว',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({
                                    type: 'error',
                                    title: 'เกิดข้อผิดพลาด',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    </script>
@stop

