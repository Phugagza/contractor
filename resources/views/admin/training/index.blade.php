@extends('layouts.paper')
@section('title','การอบรม')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าแรก</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <div class="card mt-5">
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card-header">
                                    <a href="{{route('admin.training.edit')}}"
                                       class="btn btn-primary">
                                        เพิ่มหัวข้อการอบรม
                                    </a>

                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="20%">หัวข้อการอบรม</th>
                                                <th class="text-center" width="20%" class="disabled-sorting text-center">Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($trainings as $training)
                                                <tr>
                                                       <td class="text-center">{{$training -> name}}</td>
                                                       <td class="text-center">
                                                            <a href="{{route('admin.training.edit',['id'=> $training -> id])}}"
                                                               class="btn btn-warning btn-icon btn-sm like"
                                                               id="hover"
                                                               data-toggle="tooltip"
                                                               data-placement="top"
                                                               title="แก้ไขข้อมูลการอบรม">
                                                                <i class="nc-icon nc-ruler-pencil"></i>
                                                            </a>
                                                           <a href="{{route('admin.training.detail',['id'=> $training -> id])}}"
                                                              class="btn btn-success btn-icon btn-sm like"
                                                              id="hover"
                                                              data-toggle="tooltip"
                                                              data-placement="top"
                                                              title="เพิ่มผู้อบรม">
                                                               <i class="nc-icon nc-simple-add"></i>
                                                           </a>
                                                           <a href="#"
                                                              onclick="DeleteTraining({{$training -> id}}); return false"
                                                              class="btn btn-danger btn-icon btn-sm like"
                                                              id="hover"
                                                              data-toggle="tooltip"
                                                              data-placement="top"
                                                              title="ลบข้อมูลการอบรม">
                                                               <i class="nc-icon nc-simple-remove"></i>
                                                           </a>
                                                        </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <script>
        $('#datatable').DataTable({
            responsive: true
        });

        DeleteTraining=(id)=>{
            Swal.fire({
                title: 'ต้องการลบข้อมูลใช่หรือไม่ ?',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#c8d2dd',
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if(result.dismiss == 'cancel'){
                    Swal.hideLoading();
                }else if(result.dismiss == 'overlay'){
                    Swal.hideLoading();
                } else{
                    Swal.fire({
                        title: 'กรุณารอสักครู่',
                        onOpen: () => {
                            swal.showLoading();
                        }
                    })
                }
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url: "{{ route('admin.training.delete') }}",
                        method: 'post',
                        data: {
                            id: id
                        },
                        success: function(response){
                            if (response.success == 'Yes') {
                                Swal.fire({
                                    type: 'success',
                                    title: 'บันทึกข้อมูลเรียบร้อย',
                                    showConfirmButton: true,
                                    confirmButtonText: "OK!"
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                            else if(response.success == 'No'){
                                Swal.fire({
                                    type: 'error',
                                    title: 'ล้มเหลว',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({
                                    type: 'error',
                                    title: 'เกิดข้อผิดพลาด',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    </script>
@stop

