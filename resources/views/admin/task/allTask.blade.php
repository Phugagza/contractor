@extends('layouts.paper')
@section('title','จัดการข้อมูลงาน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('flash-message')
                <div class="card mt-5">
                    <div class="card-body" id="Task" >
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-header">
                                    <h3> ค้นหา </h3>
                                </div>
                                <div class="card-body">
                                    <form  method="POST" id="search_form">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>สถานะ</label>
                                                    <select  class="form-control select2"  name="task_detail_status">
                                                        <option value="">ทั้งหมด</option>
                                                        <option value="1" {{$task_detail_status == 1 ? 'selected' : ''}}>{{config('constants.task_detail.status.1.status_name')}}</option>
                                                        <option value="2" {{$task_detail_status == 2 ? 'selected' : ''}}>{{config('constants.task_detail.status.2.status_name')}}</option>
                                                        <option value="3" {{$task_detail_status == 3 ? 'selected' : ''}}>{{config('constants.task_detail.status.3.status_name')}}</option>
                                                        <option value="4" {{$task_detail_status == 4 ? 'selected' : ''}}>{{config('constants.task_detail.status.4.status_name')}}</option>
                                                        <option value="5" {{$task_detail_status == 5 ? 'selected' : ''}}>{{config('constants.task_detail.status.5.status_name')}}</option>
                                                        <option value="6" {{$task_detail_status == 6 ? 'selected' : ''}}>{{config('constants.task_detail.status.6.status_name')}}</option>
                                                        <option value="7" {{$task_detail_status == 7 ? 'selected' : ''}}>{{config('constants.task_detail.status.7.status_name')}}</option>
                                                        <option value="9" {{$task_detail_status == 9 ? 'selected' : ''}}>{{config('constants.task_detail.status.9.status_name')}}</option>
                                                        <option value="registered" {{$task_detail_status == "registered" ? 'selected' : ''}}>{{config('constants.task_detail.status.registered.status_name')}}</option>
                                                        <option value="5rule" {{$task_detail_status == "5rule" ? 'selected' : ''}}>{{config('constants.task_detail.status.5rule.status_name')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>วัน</label>
                                                    <input type="text"
                                                           id="date_start"
                                                           name="date_start"
                                                           class="form-control"
                                                           placeholder="วันที่เริ่ม"
                                                           value="{{$date_start}}"
                                                    >
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>ถึง</label>
                                                    <input type="text"
                                                           id="date_end"
                                                           name="date_end"
                                                           class="form-control"
                                                           placeholder="วันที่สิ้นสุด"
                                                           value="{{$date_end}}"
                                                    >
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>จำนวนที่แสดง</label>
                                                    <select  class="form-control select2"  name="perPageDefault">
                                                        <option value="10" {{$perPageDefault == 15 ? 'selected':''}}>15</option>
                                                        <option value="30" {{$perPageDefault == 30 ? 'selected':''}}>30</option>
                                                        <option value="45" {{$perPageDefault == 45 ? 'selected':''}}>45</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>ค้นหาโดย ชื่องาน,ชื่อสถานที่,ชื่อหัวหน้าผู้รับเหมา,ชื่อผู้ควบคุม</label>
                                                    <input type="text"
                                                           id="keyword"
                                                           name="keyword"
                                                           class="form-control"
                                                           placeholder="ชื่องาน,ชื่อสถานที่,ชื่อหัวหน้าผู้รับเหมา,ชื่อผู้ควบคุม"
                                                           value="{{$keyword}}"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <input type="submit" id="search" value="ค้นหา"  class="btn btn-info">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="10%">ชื่องาน</th>
                                                <th class="text-center" width="5%">สถานะล่าสุด</th>
                                                <th class="text-center" width="10%">สถาที่ปฏิบัติงาน</th>
                                                <th class="text-center" width="15%">ผู้ควบคุมงาน</th>
                                                <th class="text-center" width="15%">หัวหน้าผู้รับเหมา</th>
                                                <th class="text-center" width="5%">No.</th>
                                                <th class="text-center" width="5%">สถานะ QR Code</th>
                                                <th class="text-center" width="10%">เข้างานล่าสุด</th>
                                                <th class="text-center" width="10%">วันที่ทำการลงทะเบียน</th>
                                                <th class="text-center" width="5%">QR Code</th>
                                                <th class="text-center" class="disabled-sorting text-center" width="10%">Action</th>
                                                <th class="text-center" class="disabled-sorting text-center"width="5%">ลบ</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($tasks as $index => $task)
                                                <tr>
                                                    <td class="text-center"> {{$task -> task_title}}</td>
                                                    <td class="text-center">
                                                        @if($task -> task_detail_status == 9)
                                                            <h5>
                                                                <span class="badge badge-{{config("constants.task_detail.status.9.color")}}">{{config("constants.task_detail.status.9.status_name")}}</span>
                                                            </h5>
                                                        @elseif($task -> task_type == null)
                                                            <h5>
                                                                <span class="badge badge-info">ลงทะเบียน</span>
                                                            </h5>
                                                        @elseif($task -> task_type != null && $task -> task_detail_status == null)
                                                            <h5>
                                                                <span class="badge badge-info">5 Job Rule</span>
                                                            </h5>
                                                        @elseif($task -> task_type != null && $task -> task_detail_status != null)
                                                            @if($task -> task_detail_status == 1)
                                                                <h5>
                                                                    <span class="badge badge-warning">รอเข้างาน</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 2)
                                                                <h5>
                                                                    <span class="badge badge-primary">อนุมัติ</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 3)
                                                                <h5>
                                                                    <span class="badge badge-primary">เริ่มทำงาน</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 4)
                                                                <h5>
                                                                    <span class="badge badge-warning">ตรวจสอบ 1</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 5)
                                                                <h5>
                                                                    <span class="badge badge-warning">ตรวจสอบ 2</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 6)
                                                                <h5>
                                                                    <span class="badge badge-warning">ตรวจสอบปิดงาน</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 7)
                                                                <h5>
                                                                    <span class="badge badge-success">ปิดงานสำเร็จ</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 8)
                                                                <h5>
                                                                    <span class="badge badge-danger">ปิดงานไม่สำเร็จ</span>
                                                                </h5>
                                                            @elseif($task -> task_detail_status == 9)
                                                                <h5>
                                                                    <span class="badge badge-danger">ยกเลิก</span>
                                                                </h5>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td class="text-center"> {{$task -> task_location}}</td>
                                                    <td class="text-center"> {{$task -> leader_prefix.$task -> leader_first_name." ".$task -> leader_last_name}}</td>
                                                    <td class="text-center"> {{$task -> contractor_prefix.$task -> contractor_first_name." ".$task -> contractor_last_name.' ('.$task -> enterprise_name.')'}}</td>
                                                    <td class="text-center"> {{$task -> task_detail_id}}</td>
                                                    <td class="text-center">
                                                        @if($task -> task_status == 1)
                                                            <h5>
                                                                <span class="badge badge-success">QR OK</span>
                                                            </h5>
                                                        @elseif($task -> task_status == 2)
                                                            <h5>
                                                                <span class="badge badge-danger">QR Close</span>
                                                            </h5>
                                                        @endif
                                                    </td>
                                                    @if($task -> register_task_detail_date != null)
                                                        <td class="text-center">
                                                            <h5>
                                                            <span class="badge badge-success">
                                                              {{$task -> register_task_detail_date ? ConvertThaiDate($task -> register_task_detail_date) : '-'}}
                                                            </span>
                                                            </h5>
                                                        </td>
                                                    @else
                                                        <td class="text-center">
                                                            <h5>
                                                                <span class="badge badge-danger">
                                                                  {{$task -> task_working_date ? ConvertThaiDate($task -> task_working_date) : '-'}}
                                                                </span>
                                                            </h5>
                                                        </td>
                                                    @endif
                                                    <td class="text-center"> {{ConvertThaiDate($task -> task_register_date)}}</td>
                                                    <td class="text-center">
                                                        <a href="{{route('admin.task.allTask.qrcode',['id'=> $task -> task_id])}}"
                                                           class="btn btn-primary"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="ดู">
                                                            <i class="nc-icon nc-zoom-split"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="{{route('admin.task.edit',['id'=> $task -> task_id])}}"
                                                           class="btn btn-warning btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="แก้ไข">
                                                            <i class="nc-icon nc-ruler-pencil"></i>
                                                        </a>
                                                        <a href="{{route('admin.task.detail',['id'=> $task -> task_id])}}"
                                                           class="btn btn-secondary btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="ดูรายละเอียดงาน">
                                                            <i class="nc-icon nc-bullet-list-67"></i>
                                                        </a>
                                                        <a href="{{route('admin.task.change.status',['task_id'=> $task -> task_id])}}"
                                                           class="btn {{$task -> task_status == 2 ? 'btn-success' : 'btn-danger'}} btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="{{$task -> task_status == 2 ? 'ยกเลิกระงับ' : 'ระงับงาน'}}">
                                                            <i class="nc-icon {{$task -> task_status == 2 ? 'nc-simple-add' : 'nc-simple-delete'}}"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#"
                                                           class="btn btn-danger btn-icon btn-sm like"
                                                           id="hover"
                                                           onclick="DeleteTask({{$task -> task_id}}); return false"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="ลบข้อมูลลงทะเบียน">
                                                            <i class="nc-icon nc-simple-remove"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            {{ $tasks->appends($allRequest)->links() }}
                                        </div>
                                        <div class="col-md-12">
                                            {{
                                              ($tasks->perPage() * $tasks->currentPage()) - ($tasks->perPage() - $tasks->count())
                                               ." ทั้งหมด ".
                                               $tasks -> total()
                                            }}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <script>
        $('#datatable').DataTable({
            responsive: true,
            "paging": false,
            "bInfo" : false,
            "searching": false,
            order:false
        });
        $(document).ready(function() {
            $('#date_start').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('#date_end').datetimepicker({
                format: 'YYYY-MM-DD'
            });

        })

        DeleteTask=(id)=>{
            Swal.fire({
                title: 'ต้องการลบข้อมูลใช่หรือไม่ ?',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#c8d2dd',
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if(result.dismiss == 'cancel'){
                    Swal.hideLoading();
                }else if(result.dismiss == 'overlay'){
                    Swal.hideLoading();
                } else{
                    Swal.fire({
                        title: 'กรุณารอสักครู่',
                        onOpen: () => {
                            swal.showLoading();
                        }
                    })
                }
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url: "{{ route('admin.task.delete') }}",
                        method: 'post',
                        data: {
                            id: id
                        },
                        success: function(response){
                            if (response.success == 'Yes') {
                                Swal.fire({
                                    type: 'success',
                                    title: 'บันทึกข้อมูลเรียบร้อย',
                                    showConfirmButton: true,
                                    confirmButtonText: "OK!"
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                            else if(response.success == 'No'){
                                Swal.fire({
                                    type: 'error',
                                    title: 'ล้มเหลว',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({
                                    type: 'error',
                                    title: 'เกิดข้อผิดพลาด',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    </script>
@stop
