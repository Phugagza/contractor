@extends('layouts.paper')
@section('title','จัดการข้อมูลงาน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('flash-message')
                <div class="card mt-5">
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="10%">ชื่องาน</th>
                                                <th class="text-center" width="5%">สถานะล่าสุด</th>
                                                <th class="text-center" width="10%">สถาที่ปฏิบัติงาน</th>
                                                <th class="text-center" width="15%">ผู้ควบคุมงาน</th>
                                                <th class="text-center" width="15%">หัวหน้าผู้รับเหมา</th>
                                                <th class="text-center" width="5%">No.</th>
                                                <th class="text-center" width="5%">สถานะ QR Code</th>
                                                <th class="text-center" width="10%">เข้างานล่าสุด</th>
                                                <th class="text-center" width="10%">วันที่ทำการลงทะเบียน</th>
                                                <th class="text-center" width="5%">QR Code</th>
                                                <th class="text-center" class="disabled-sorting text-center" width="10%">Action</th>
                                                <th class="text-center" class="disabled-sorting text-center"width="5%">ลบ</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr id="loading"><td colspan='12'><h5 class='text-center'>รอสักครู่..</h5></td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <script>
        $( document ).ready(function() {

            $.ajax({
                type: 'GET',
                dataType: "json",
                url: '{{ route('admin.getAllTask') }}',
                "xhrFields": {
                    "withCredentials": true
                },
                success: function (response) {
                    let newRowContent;
                    for (let i = 0; i < response.data.length; i++) {

                        const statusTask = getTaskStatus(response.data[i].task_type,response.data[i].task_detail_status)
                        const dateTask = getTaskDate(response.data[i].register_task_detail_date,response.data[i].task_working_date)

                        newRowContent =
                            "<tr><td class='text-center'>" + response.data[i].task_title + "</td>" +
                            "<td td class='text-center'><h5><span class='badge badge-"+statusTask[0]+"'>" + statusTask[1] + "</span></h5></td>" +
                            "<td td class='text-center'>" + (response.data[i].task_location ? response.data[i].task_location : '-') + "</td>" +
                            "<td td class='text-center'>" + (response.data[i].leader_prefix + response.data[i].leader_first_name +" "+ response.data[i].leader_last_name ) + "</td>" +
                            "<td td class='text-center'>" + (response.data[i].contractor_prefix + response.data[i].contractor_first_name +" "+ response.data[i].contractor_last_name ) + "</td>" +
                            "<td td class='text-center'>" + (response.data[i].task_detail_id ? response.data[i].task_detail_id : '-') + "</td>" +
                            "<td td class='text-center'><h5><span class='badge badge-"+ (response.data[i].task_status == 1 ? "success" : "danger") +"'>"+ (response.data[i].task_status == 1 ? "QR OK" : "QR Close") + "</span></h5></td>" +
                            "<td td class='text-center' data-sort='"+ dateTask[2] +"'><h5><span class='badge badge-"+dateTask[0]+"'>"+ dateTask[1] +"</h5></span></td>" +
                            "<td td class='text-center'>" + ConvertThaiDate(response.data[i].task_register_date) + "</td>" +
                            "<td td class='text-center'>" +  "<a href='{{route('admin.task.allTask.qrcode')}}/"+response.data[i].task_id+"' class='btn btn-primary' id='hover' data-toggle='tooltip' data-placement='top'" +
                            "title='ดู'> <i class='nc-icon nc-zoom-split'></i></a></td>"+
                            "<td td class='text-center'>" +
                            "<a href='{{route('admin.task.edit')}}/"+response.data[i].task_id+"' class='btn btn-warning btn-icon btn-sm like id='hover' data-toggle='tooltip' data-placement='top'" +
                            "title='แก้ไข'> <i class='nc-icon nc-ruler-pencil'></i></a>"+
                            "<a href='{{route('admin.task.detail')}}/"+response.data[i].task_id+"' class='btn btn-secondary btn-icon btn-sm like' id='hover' data-toggle='tooltip' data-placement='top'" +
                            "title='ดูรายละเอียดงาน'> <i class='nc-icon nc-zoom-split'></i></a>"+
                            "<a href='{{route('admin.task.change.status')}}"+response.data[i].task_id+"' class='btn btn-"+(response.data[i].task_status  == 2 ? "success" : "danger")+" btn-icon btn-sm like' id='hover' data-toggle='tooltip' data-placement='top'" +
                            "title='" + (response.data[i].task_status  == 2 ? "ยกเลิกระงับ" : "ระงับงาน") + "'> <i class='nc-icon "+(response.data[i].task_status  == 2 ? "nc-simple-add" : "nc-simple-delete")+"'></i></a>"+
                            "</td>"+
                            "<td td class='text-center'>" +
                            "<a href='#'"+ 'onclick="DeleteTask('+response.data[i].task_id+');"  return false' + " class='btn btn-danger btn-icon btn-sm like id='hover' data-toggle='tooltip' data-placement='top'" +
                            "title='ลบข้อมูลลงทะเบียน'> <i class='nc-icon nc-simple-remove'></i></a>"+
                            "</td></tr>";

                        $("#datatable tbody").append(newRowContent);

                    }
                }
            }).done(function() {
                $('#loading').remove();
                runDataTable();
            });

        });

        runDataTable=()=>{
            $('#datatable').DataTable({
                responsive: true,
                "order": [[ 7, "desc" ]],
                "paging": true,

            });
        }

        ConvertThaiDate=(date)=>{
            const sub_date = date.split('-');
            const strMonthCut = {
                "0":"",
                "01":"มกราคม",
                "02":"กุมภาพันธ์",
                "03":"มีนาคม",
                "04":"เมษายน",
                "05":"พฤษภาคม",
                "06":"มิถุนายน",
                "07":"กรกฎาคม",
                "08":"สิงหาคม",
                "09":"กันยายน",
                "10":"ตุลาคม",
                "11":"พฤศจิกายน",
                "12":"ธันวาคม"
            }
            let strYear =parseInt(sub_date[0])+543;
            let $strDay= sub_date[2];
            let strMonthThai=strMonthCut[sub_date[1]];
            return $strDay+" "+ strMonthThai+" "+strYear;
        }

        getTaskStatus = (task_type,task_detail_status) => {
            let status = [];
            if(task_type == null){
                status=['info','ลงทะเบียน']
            }
            else if( task_type != null && task_detail_status == null){
                status=['info','5 Job Rule']
            }
            else if(task_type != null && task_detail_status != null){
                if(task_detail_status == 1){ status=['warning','รอเข้างาน'] }
                else if(task_detail_status == 2){ status=['primary','อนุมัติ'] }
                else if(task_detail_status == 3){ status=['primary','เริ่มทำงาน'] }
                else if(task_detail_status == 4){ status=['warning','ตรวจสอบ 1'] }
                else if(task_detail_status == 5){ status=['warning','ตรวจสอบ 2'] }
                else if(task_detail_status == 6){ status=['warning','ตรวจสอบปิดงาน'] }
                else if(task_detail_status == 7){ status=['success','ปิดงานสำเร็จ'] }
                else if(task_detail_status == 8){ status=['danger','ปิดงานไม่สำเร็จ'] }
                else if(task_detail_status == 9){ status=['danger','ยกเลิก'] }
                else {status=['primary','-']}

            }
            else {
                status=['primary','-']
            }
            return status;
        }

        getTaskDate=(register_task_detail_date,task_working_date)=>{
            let date;
            if(register_task_detail_date != null){ date = ['success',ConvertThaiDate(register_task_detail_date),register_task_detail_date] }
            else{ date = ['danger',ConvertThaiDate(task_working_date),task_working_date] }


            return date;
        }

        DeleteTask=(id)=>{
            Swal.fire({
                title: 'ต้องการลบข้อมูลใช่หรือไม่ ?',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#c8d2dd',
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if(result.dismiss == 'cancel'){
                    Swal.hideLoading();
                }else if(result.dismiss == 'overlay'){
                    Swal.hideLoading();
                } else{
                    Swal.fire({
                        title: 'กรุณารอสักครู่',
                        onOpen: () => {
                            swal.showLoading();
                        }
                    })
                }
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url: "{{ route('admin.task.delete') }}",
                        method: 'post',
                        data: {
                            id: id
                        },
                        success: function(response){
                            if (response.success == 'Yes') {
                                Swal.fire({
                                    type: 'success',
                                    title: 'บันทึกข้อมูลเรียบร้อย',
                                    showConfirmButton: true,
                                    confirmButtonText: "OK!"
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                            else if(response.success == 'No'){
                                Swal.fire({
                                    type: 'error',
                                    title: 'ล้มเหลว',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({
                                    type: 'error',
                                    title: 'เกิดข้อผิดพลาด',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    </script>
@stop
