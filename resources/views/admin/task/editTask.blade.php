@extends('layouts.paper')
@section('title','แก้ไขการลงทะเบียน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">แก้ไขการลงทะเบียน</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <form method="POST" action="" id="register_form" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                แก้ไขการลงทะเบียน
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>ชื่องาน (ไม่เกิน 40 ตัวอักษร)</label>
                                        <input type="text"
                                               id="title"
                                               name="title"
                                               class="form-control"
                                               placeholder="ชื่องาน"
                                               value="{{ $task -> title }}"
                                               maxlength="40"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุชื่องาน
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>วันที่เข้าทำงาน(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="working_date"
                                               name="working_date"
                                               class="form-control"
                                               placeholder="วันที่เข้าทำงาน"
                                               value="{{ $task -> working_date }}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันที่เข้าทำงาน
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>สถานที่ปฏิบัติงาน</label>
                                        <input type="text"
                                               id="location"
                                               name="location"
                                               class="form-control"
                                               placeholder="สถานที่ปฏิบัติงาน"
                                               maxlength="255"
                                               value="{{ $task -> location }}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุสถานที่ปฏิบัติงาน
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>รายละเอียดงาน</label>
                                        <textarea
                                            id="description"
                                            name="description"
                                            class="form-control"

                                        >
                                           {{ $task -> description }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>หัวหน้าผู้รับเหมา</label>
                                        <select  class="form-control select2"  name="contractor_id" required>
                                            <option value="">กรุณาเลือก</option>
                                            @foreach($contractors as $contractor)
                                                <option value="{{$contractor -> id}}" {{ $task -> contractor_id == $contractor -> id ? 'selected' : '' }}>{{$contractor -> prefix.$contractor -> first_name." ".$contractor -> last_name." (".$contractor -> enterprise_name.")"}}</option>
                                            @endforeach
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดเลือกผู้รับเหมา
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>ผู้ควบคุมงาน</label>
                                        <select  class="form-control select2"  name="leader_id" required>
                                            <option value="">กรุณาเลือก</option>
                                            @foreach($leaders as $leader)
                                                <option value="{{$leader -> id}}" {{ $leader -> id == $task -> leader_id ? 'selected' : '' }}>{{$leader -> prefix.$leader -> first_name." ".$leader -> last_name}} </option>
                                            @endforeach
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดเลือกผู้ควบคุมงาน
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ทีมงานผู้รับเหมา</label>
                                        <select  class="form-control select2Multi"  name="subcontractor_id[]" multiple="multiple" >
                                            @foreach($contractors as $index => $contractor)
                                                <option value="{{$contractor -> id}}" >{{$contractor -> prefix.$contractor -> first_name." ".$contractor -> last_name." (".$contractor -> enterprise_name.")"}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                    <br/>
                                    <a href="javascript:history.go(-1)" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    </div>

    <script>
        $(document).ready(function() {
             $('.select2').select2();
            var $selectMulti = $('.select2Multi').select2();
            $selectMulti.val([{{$task -> subcontractor_id}}]).trigger("change");
            $('#working_date').datetimepicker({
                // format: 'DD-MM-YYYY'
                format: 'YYYY-MM-DD'
            });

            $("#register_form").on("submit", function(){
                Swal.fire({
                    title: 'กรุณารอสักครู่',
                    onOpen: () => {
                        swal.showLoading();
                    }
                })
            });//submit
        });

    </script>
@stop

