<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>WorkPermit</title>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ asset('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }
        html,body{
            margin: 0;
            padding: 0;
            height: 100%;
            font-family: "THSarabunNew";
            background: #FFFFFF;
        }

        .bg{
            position: absolute;
            opacity: 0.05;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            z-index: -1;
            transform: rotate(-45deg);

        }

        .container{
            margin: 20px;
            text-align: center;
        }

        .item1{
            margin-top: -60px;
        }

         .title {
             font-size: 60px;
         }

        .item2{
            margin-top: 0px;
        }

        p{
            font-size: 35px;
            font-weight: bold;
        }

        table{
            margin-top: 50px;
        }

        .status{
            font-weight: bold;
            font-size: 55px;
            color: black;
        }

        .sub-status{
            font-weight: 900;
            font-size: 30px;
            color: black;
        }
    </style>
</head>
<body>
        <div class="bg">
{{--            <img src="http://iiu.isit.or.th/uploads/1312201906.jpg" alt="">--}}
        </div>
        <div class="container">
            <div class="item1">
                <table width="100%">
                    <tr>
                        <td width = '80%' align="center">
                            <h1 class="title">ใบอนุญาตปฏิบัติงานของผู้รับเหมาชั่วคราว </h1>
                            <h1 class="sub-status"> Work Permission for Temporary Contractors  </h1>
                        </td>
                        <td>
                            <img src="data:image/png;base64,
                                                    {{
                                                        base64_encode(QrCode::format('png')
                                                        ->size(200)
                                                        ->color(40,40,40)
                                                        ->generate(route('contractor.scanQR.workPermit',['id' => $task_detail -> task_detail_id])))
                                                    }}
                                ">
                        </td>
                    </tr>
                </table>
            </div>

            <div class="item2">
                <h1 class="status"> {{$task_detail -> task_title}} </h1>
                <h1 class="sub-status">No. {{$task_detail -> task_detail_id}}  </h1>
                <p class="date">ได้รับอนุมัติเมื่อวันที่ : {{ConvertThaiDate($task_detail -> task_detail_date)}}</p>
                <p class="date">หัวหน้าผู้รับเหมา : {{$task_detail -> contractor_firstname}} {{$task_detail -> contractor_lastname}}</p>
                <p class="date">ผู้ควบคุมงาน : {{$task_detail -> leader_firstname}} {{$task_detail -> leader_lastname}}</p>
            </div>
        </div>
</body>
</html>
