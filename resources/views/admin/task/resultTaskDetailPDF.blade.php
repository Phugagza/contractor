<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>รายงานสรุป</title>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ asset('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body,html{
            padding: 0;
            margin: 10px;
            font-family: "THSarabunNew";

        }
        .page-number:before {
            content: "Page " counter(page);
        }
        .page-break {
            page-break-after: always;
        }

        table{
            width: 100%;
            border: 1px solid black;
            padding: 10px;
            margin-bottom: 30px;
        }
        tr,td{
           padding: 0;
        }

        .font-weight-bold{
            font-weight: bold;
        }

        .text-center{
            text-align: center;
        }

        .header{
            margin: 0;
        }
        .footer{
            text-align: right;
        }
        table.border-none{
            border: 0;
            margin: 0
        }
    </style>
</head>
<body>
<table class="border-none">
    <tr>
        <td style="text-align: left" width="70%">
        <img src="{{asset('img/bg/ns-logo.png')}}">
        </td>
        <td style="text-align: right" width="30%">
            <h2>No.{{$task_detail_id}}</h2>
        </td>
    </tr>

</table>
<h1>แบบตรวจสอบและขออนุญาตปฏิบัติงานของผู้รับเหมาชั่วคราว</h1>

<table>
        <tr>
            <td colspan="6" > <span style="font-size: 32px; font-weight: bold;">ผู้รับเหมา </span> (ผู้ควบคุมงาน: {{$leader->prefix.$leader->first_name." ".$leader->last_name}}) </td>
        </tr>
        <tr>
            <td class="text-center">1.</td>
            <td>
                ผู้ขออนุญาต(หัวหน้าผู้รับเหมา):&nbsp; <span class="font-weight-bold">{{$checkResultView[0] -> contractor_prefix.$checkResultView[0] -> contractor_first_name." ".$checkResultView[0] -> contractor_last_name}}</span>
                เบอร์โทร:&nbsp; <span class="font-weight-bold"> {{$checkResultView[0] -> contractor_tel ? $checkResultView[0] -> contractor_tel : '-'}}</span>
                บริษัท:&nbsp; <span class="font-weight-bold">{{$checkResultView[0] -> enterprise_name ? $checkResultView[0] -> enterprise_name : '-'}}</span>
            </td>
        </tr>
        <tr>
            <td class="text-center">2.</td>
            <td>
                งานที่ปฏิบัติ:&nbsp;<span class="font-weight-bold"> {{$checkResultView[0] -> task_title}} </span>
            </td>
        </tr>
        <tr>
            <td class="text-center">3.</td>
            <td>
                สถานที่ปฏิบัติงาน:&nbsp;<span class="font-weight-bold"> {{$checkResultView[0] -> task_location}} </span>
            </td>
        </tr>
        <tr>
            <td class="text-center">4.</td>
            <td>รายละเอียดงาน:&nbsp;
                <span class="font-weight-bold">
                                                {{$checkResultView[0] -> task_description ? $checkResultView[0] -> task_description : "-"}}
                                            </span>
            </td>
        </tr>
        <tr>
            <td class="text-center">5.</td>
            <td>วันที่ปฏิบัติ:&nbsp;
                <span class="font-weight-bold">
                                                {{ConvertThaiDate($checkResultView[0] -> task_detail_date)}}
                                            </span>
                ช่วงเวลาที่ปฏิบัติงาน:&nbsp;
                <span class="font-weight-bold">
                                                {{ConvertThaiDateHour($checkResultView[0] -> task_detail_start)}}
                                            </span>
                &nbsp;ถึง&nbsp;
                <span class="font-weight-bold">
                                                {{ConvertThaiDateHour($checkResultView[0] -> task_detail_end)}}
                                            </span>
            </td>
        </tr>
        <tr>
            <td class="text-center">6.</td>
            <td>ประเภทงานตาม License:&nbsp;
                <span class="font-weight-bold">
                    {{$checkResultView[0] -> task_type ? config("constants.task_type.{$checkResultView[0] -> task_type}")  : "อื่นๆ"}}
                </span>
            </td>
        </tr>
        <tr>
            <td class="text-center">7.</td>
            <td>จำนวนผู้ปฏิบัติงาน:&nbsp;
                <span class="font-weight-bold">{{count($subcontractors) + count($contractors) + count($assistant_subcontractors)}}</span>&nbsp;คน
            </td>
        </tr>
        <tr>
            <td class="text-center"></td>
            <td>ทีมงานผู้รับเหมา</td>
        </tr>
        <tr>
            <td class="text-center"></td>
            <td>
                @if(count($subcontractors) > 0 || count($contractors) > 0)
                    <ul style="margin: 0; padding-left: 21px">
                        @foreach($contractors as $contractor)
                            <li>{{$contractor -> prefix.$contractor -> first_name." ".$contractor -> last_name}} (หัวหน้าผู้รับเหมา)</li>
                        @endforeach
                            ----------------
                        @foreach($subcontractors as $subcontractor)
                            <li>{{$subcontractor -> prefix.$subcontractor -> first_name." ".$subcontractor -> last_name}} (ช่าง)</li>
                        @endforeach
                            ----------------
                        @if(count($assistant_subcontractors) > 0 )
                            @foreach($assistant_subcontractors as $assistant_subcontractor)
                                <li>{{$assistant_subcontractor -> prefix.$assistant_subcontractor -> first_name." ".$assistant_subcontractor -> last_name}} (ผู้ช่วยช่าง)</li>
                            @endforeach
                        @endif
                    </ul>
                @endif
            </td>
        </tr>

</table>
<table>
    <tr>
        <td colspan="5"><h1 class="header">ผู้ควบคุม</h1></td>
    </tr>
    <tr>
        <td width="350">6.1 เลือกประเภทงาน อ้างอิงจากกฏการตัดสินใจสำหรับผู้ปฏิบัติงาน (เฉพาะตัว) ฯ</td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px "><input class="form-check-input" type="radio" {{$checkResultView[0]  -> type1 == 1 ? 'checked' : ''}}> งานประเภท 1 : งานปกติทำประจำ (หากไม่มี WI ให้พิจารณาเป็นข้อ 2)</td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px "><input class="form-check-input" type="radio" {{$checkResultView[0]  -> type1 == 2 ? 'checked' : ''}}> งานประเภท 2 : งานปกติผู้ปฏิบัติงานไม่ได้ทำ > 1 เดือน</td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px "><input class="form-check-input" type="radio" {{$checkResultView[0]  -> type1 == 3 ? 'checked' : ''}}> งานประเภท 3 : งานไม่ปกติ เคยทำไม่เกิน 1 เดือน หรือซ้อมทบทวนทุกเดือน</td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px "><input class="form-check-input" type="radio" {{$checkResultView[0]  -> type1 == 4 ? 'checked' : ''}}> งานประเภท 4 : งานไม่ปกติ ไม่เคยทำหรือไม่ได้ซ้อมทบทวนทุกเดือน</td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px "><input class="form-check-input" type="radio" {{$checkResultView[0]  -> type1 == 5 ? 'checked' : ''}}> งานประเภท 5 : กิจกรรมหรืองานอื่นๆ ที่ไม่มี WI</td>
    </tr>
   {{---------}}
    <tr>
        <td width="350">6.2 แนบสำเนาใบตรวจสอบอุปกรณ์ป้องกันอันตรายส่วนบุคคล (PPE)</td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px "><input class="form-check-input" type="checkbox" {{$checkResultView[0] -> ppe == 1 ? 'checked' : ''}}> แนบสำเนาใบตรวจสอบอุปกรณ์ป้องกันอันตรายส่วนบุคคล (PPE)</td>
    </tr>
    {{---------}}
    <tr>
        <td width="350">6.3 การใช้เครนประเภทเคลี่อนที่ (รถเฮี๊ยบ รถเครน ฯลฯ)</td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px ">
            <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> crane1 == 1 ? 'checked' : ''}}>
            ไม่มีการใช้ ข้ามไปข้อ 6.4
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px ">
            <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> crane2 == 1 ? 'checked' : ''}}>
            มีการใช้โปรดตรวจสอบข้อ 6.3.1 และ 6.3.2
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 40px ">
            <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> crane2_1 == 1 ? 'checked' : ''}}>
            6.3.1 มีใบอนุญาตขับขี่ และยังไม่หมดอายุ
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 40px ">
            <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> crane2_2 == 1 ? 'checked' : ''}}>
            6.3.2 มีสำเนา ปจ.2 และยังไม่หมดอายุ
        </td>
    </tr>
</table>
@if(count($subcontractors) + count($contractors) + count($assistant_subcontractors) < 4)
    <div class="page-break"></div>
@endif
<table>
    <tr>
        <td width="350">6.4 มีการใช้พื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน</td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px ">
            <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> workshop1 == 1 ? 'checked' : ''}}>
            ไม่มีการใช้ ข้ามไปข้อ 6.5
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px ">
            <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> workshop2 == 1 ? 'checked' : ''}}>
            มีการใช้โปรดตรวจสอบข้อ 6.4.1 และ 6.4.2
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 40px ">
            <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> workshop2_1 == 1 ? 'checked' : ''}}>
            6.4.1 แนบแบบฟอร์มขอใช้พื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน (F-MA-ME-001)
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 40px ">
            <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> workshop2_2 == 1 ? 'checked' : ''}}>
            6.4.2 แนบแบบฟอร์มการตรวจสอบพื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน (F-MA-ME-002)
        </td>
    </tr>
    {{---------}}
    <tr>
        <td width="350">6.5 มีการใช้เครื่องมือหรืออุปกรณ์ไฟไฟ้าที่ใช้ไฟไฟ้าตั้งแต่ 220 V. ขึ้นไป</td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left: 20px; width: 50px ">
            <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> electric_check1 == 1 ? 'checked' : ''}}>
            ไม่มีการใช้ ข้ามไปข้อ 6.6 &nbsp;&nbsp;&nbsp;&nbsp;
            <input class="form-check-input" type="checkbox" {{$checkResultView[0] -> electric_check2 == 1 ? 'checked' : ''}}>
            มีการใช้ โปรดกรอกรายละเอียดลงในตาราง
        </td>
    </tr>
    <tr>
        <td>
            <table style="border-collapse: collapse; margin: 0; width: 730px">
                <tr>
                    <td class="text-center" style="border: 1px solid black">ลำดับ</td>
                    <td class="text-center" style="border: 1px solid black">รายการเครื่องมือหรืออุปกรณ์</td>
                    <td class="text-center" style="border: 1px solid black">จำนวน</td>
                    <td class="text-center" style="border: 1px solid black">หน่วย</td>
                    <td class="text-center" style="border: 1px solid black">ผ่าน / ไม่ผ่าน</td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black">1.</td>
                    <td style="border: 1px solid black"> &nbsp;{{$checkResultView[0] -> equipment1}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> amount1}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> unit1}}</td>
                    <td class="text-center" style="border: 1px solid black">
                        @if($checkResultView[0] -> permission1 == 1)
                            ผ่าน
                        @elseif($checkResultView[0] -> permission1 == 2)
                            ไม่ผ่าน
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black">2.</td>
                    <td style="border: 1px solid black"> &nbsp;{{$checkResultView[0] -> equipment2}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> amount2}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> unit2}}</td>
                    <td class="text-center" style="border: 1px solid black">
                        @if($checkResultView[0] -> permission2 == 1)
                            ผ่าน
                        @elseif($checkResultView[0] -> permission2 == 2)
                            ไม่ผ่าน
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black">3.</td>
                    <td style="border: 1px solid black"> &nbsp;{{$checkResultView[0] -> equipment3}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> amount3}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> unit3}}</td>
                    <td class="text-center" style="border: 1px solid black">
                        @if($checkResultView[0] -> permission3 == 1)
                            ผ่าน
                        @elseif($checkResultView[0] -> permission3 == 2)
                            ไม่ผ่าน
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black">4.</td>
                    <td style="border: 1px solid black"> &nbsp;{{$checkResultView[0] -> equipment4}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> amount4}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> unit4}}</td>
                    <td class="text-center" style="border: 1px solid black">
                        @if($checkResultView[0] -> permission4 == 1)
                            ผ่าน
                        @elseif($checkResultView[0] -> permission4 == 2)
                            ไม่ผ่าน
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black">5.</td>
                    <td style="border: 1px solid black"> &nbsp;{{$checkResultView[0] -> equipment5}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> amount5}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> unit5}}</td>
                    <td class="text-center" style="border: 1px solid black">
                        @if($checkResultView[0] -> permission5 == 1)
                            ผ่าน
                        @elseif($checkResultView[0] -> permission5 == 2)
                            ไม่ผ่าน
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black">6.</td>
                    <td style="border: 1px solid black"> &nbsp;{{$checkResultView[0] -> equipment6}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> amount6}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> unit6}}</td>
                    <td class="text-center" style="border: 1px solid black">
                        @if($checkResultView[0] -> permission6 == 1)
                            ผ่าน
                        @elseif($checkResultView[0] -> permission6 == 2)
                            ไม่ผ่าน
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black">7.</td>
                    <td style="border: 1px solid black"> &nbsp;{{$checkResultView[0] -> equipment7}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> amount7}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> unit7}}</td>
                    <td class="text-center" style="border: 1px solid black">
                        @if($checkResultView[0] -> permission7 == 1)
                            ผ่าน
                        @elseif($checkResultView[0] -> permission7 == 2)
                            ไม่ผ่าน
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black">8.</td>
                    <td style="border: 1px solid black"> &nbsp;{{$checkResultView[0] -> equipment8}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> amount8}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> unit8}}</td>
                    <td class="text-center" style="border: 1px solid black">
                        @if($checkResultView[0] -> permission8 == 1)
                            ผ่าน
                        @elseif($checkResultView[0] -> permission8 == 2)
                            ไม่ผ่าน
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black">9.</td>
                    <td style="border: 1px solid black"> &nbsp;{{$checkResultView[0] -> equipment9}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> amount9}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> unit9}}</td>
                    <td class="text-center" style="border: 1px solid black">
                        @if($checkResultView[0] -> permission9 == 1)
                            ผ่าน
                        @elseif($checkResultView[0] -> permission9 == 2)
                            ไม่ผ่าน
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black">10.</td>
                    <td style="border: 1px solid black"> &nbsp;{{$checkResultView[0] -> equipment10}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> amount10}}</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> unit10}}</td>
                    <td class="text-center" style="border: 1px solid black">
                        @if($checkResultView[0] -> permission10 == 1)
                            ผ่าน
                        @elseif($checkResultView[0] -> permission10 == 2)
                            ไม่ผ่าน
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="350">6.6 กรณีต้องการป้ายห้ามแตะ (Don't Touch) สำหรับ ผรม. โปรดระบุใบ</td>
    </tr>
    <tr>
        <td>
            <table style="border-collapse: collapse; margin: 0; width: 730px">
                <tr>
                    <td class="text-center" style="border: 1px solid black">ลำดับ</td>
                    <td class="text-center" style="border: 1px solid black">จำนวนใบ</td>
                    <td class="text-center" style="border: 1px solid black">หน่วย</td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black">1.</td>
                    <td class="text-center" style="border: 1px solid black">{{$checkResultView[0] -> touch}}</td>
                    <td class="text-center" style="border: 1px solid black">ใบ</td>

                </tr>
            </table>
        </td>
    </tr>
</table>
{{--@if(count($subcontractors) + count($contractors) >= 4)--}}
{{--    <div class="page-break"></div>--}}
{{--@endif--}}
<table>
    <tr>
        <td colspan="5"><h1 class="header">ผู้ควบคุมและผู้รับเหมา</h1></td>
    </tr>
    <tr>
        <td width="350">7.การตรวจสอบระหว่างปฏิบัติงาน</td>
    </tr>
    <tr>
        <td>
            <table style="border-collapse: collapse; margin: 0; width: 730px">
                <tr>
                    <td class="text-center" style="border: 1px solid black">ครั้งที่ {{$checkResultView[0]  -> time." ( ".substr($checkResultView[0]  -> check_time,0,5)." น. )"}}</td>
                    <td class="text-center" style="border: 1px solid black">ครั้งที่ {{$checkResultView[1]  -> time." ( ".substr($checkResultView[1]  -> check_time,0,5)." น. )"}}</td>
                    <td class="text-center" style="border: 1px solid black">รายการ</td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> equipment_personal == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> equipment_personal == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;1.สวมอุปกรณ์ป้องกันอันตรายส่วนบุคคล
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> safety_glasses == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> safety_glasses == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;2.แว่นตานิรภัย (Safety glasses)
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> safety_belt == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> safety_belt == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;3.เข็มขัดนิรภัย (Safety belt)
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> scba == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> scba == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;4.อุปกรณ์ช่วยหายใจ (SCBA)
                    </td>
                </tr>
                 @if(count($subcontractors) + count($contractors) < 4)
{{--                    <div class="page-break"></div>--}}
{{--                @endif--}}
            </table>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <table style="border-collapse: collapse; margin: 0; width: 730px">
                @endif

                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> oximeter == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> oximeter == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;5.เครื่องวัดออกซิเจนครบถ้วน
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> fan == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> fan == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;6.พัดลมระบายอากาศครบถ้วน
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> acid_mask == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> acid_mask == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;7.หน้ากากป้องกันไอกรด (Acid fume protecting mash)
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> sling == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> sling == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;8.สลิงอยู่ในสภาพพรอ้มใช้งาน (เป็นไปตามมาตรฐาน NS-SUS)
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> electric1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> electric1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;9.เครื่องเชื่อมไฟฟ้า
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> electric1_1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> electric1_1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;9.1.สภาพสายไฟ(ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด,จุดต่อต้องมั่นคงแข็งแรง)
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> electric1_2 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> electric1_2 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;9.2.สภาพสาย Ground (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด,จุดต่อต้องมั่นคงแข็งแรง)
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> cut_gas1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> cut_gas1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;10.เครื่องเชื่อม ตัด เผา Gas
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> cut_gas1_1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> cut_gas1_1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;10.1.Gauge ไม่แตก / สามารถอ่านค่าได
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> cut_gas1_2 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> cut_gas1_2 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;10.2. ต้องมีอุปกรณ์ป้องกันไฟย้อนกลับ (Flash back Arrestor) 4 ตำแหน่งและมีสติกเกอร์แสดงการตรวจสอบติดอยู่
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> cut_gas1_3 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> cut_gas1_3 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;10.3.สาย GAS อยู่ในสภาพดีไม่แตกลายงา และมีการยึดสายกับข้อต่อที่ถัง GASและ หัว GAS แน่นหนา ไม่รั่วซึม
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> cut_gas1_4 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> cut_gas1_4 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;10.4. จุดยึดถัง GAS ต้องมันคงแข็งแรงและมีโซ่หรือเชือกมัดถัง ไม่เสี่ยงต่อการล้มของถัง GAS
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> cut_hand1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> cut_hand1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;11.เครื่องตัด เจียรมือ
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> cut_hand1_1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> cut_hand1_1 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;11.1.สภาพสายไฟ (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด, จุดต่อต้องมั่นคงแข็งแรง)
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> cut_hand1_2 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> cut_hand1_2 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;11.2.สภาพสาย Ground (ฉนวนหุ้มสายไฟต้องไม่ฉีกขาด, จุดต่อต้องมั่นคงแข็งแรง)
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> cut_hand1_3 == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> cut_hand1_3 == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;11.3. มี Guard ป้องกัน (Guard ต้องยึดอย่างมั่นคง ไม่หลวมคลอน)
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> fire_ready == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> fire_ready == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;12.อุปกรณ์ดับเพลิงมีพร้อมและตู้น้ำดับเพลิงอยู่ในสภาพพร้อมใช้งาน
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> fire_amount == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> fire_amount == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;13.จำนวนถังดับเพลิงมีครบถ้วน และตรวจสอบล่าสุด
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> fire_resis == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> fire_resis == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;14.มีการจัดเตรียมแผ่นวัสดุทนไฟ สำหรับป้องกันสะเก็ดไฟกระเด็น(วัสดุป้องกันสะเก็ดไฟต้องไม่ติดไฟ)
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> safety == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> safety == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;15.มีสภาพการปฏิบัตงานที่ปลอดภัย
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> contractor == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> contractor == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;หัวหน้างานผู้รับเหมายืนยัน
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> leader == 1 ? 'checked' : ''}}>
                    </td>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[1] -> leader == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;ผู้ควบคุมงานยืนยัน
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="350">8.ตรวจสอบหลังเสร็จงาน</td>
    </tr>
    <tr>
        <td>
            <table style="border-collapse: collapse; margin: 0; width: 730px">
                <tr>
                    <td class="text-center" style="border: 1px solid black">เลือก</td>
                    <td class="text-center" style="border: 1px solid black">รายการ</td>
                </tr>

                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> clean == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;ความสะอาดของหน้างาน
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> clean == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;ความเรียบร้อยของงาน
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> safe == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;ความปลอดภัย
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input class="form-check-input" type="checkbox" {{$checkResultView[0] -> another == 1 ? 'checked' : ''}}>
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;อื่นๆ โปรดระบุ: &nbsp; <span class="font-weight-bold">{{$checkResultView[0] -> another_word}}</span>
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black; width: 100px">
                       สถานะ
                    </td>
                    <td style="border: 1px solid black;">
                        &nbsp;
                        @if($checkResultView[0] -> status == 7)
                            ปิดงานสำเร็จ
                        @elseif($checkResultView[0] -> status == 8)
                            ปิดงานไม่สำเร็จ
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
{{--    <div class="footer">3/3</div>--}}
</table>
<script type="text/php">

    if (isset($pdf)) {
        $text = "หน้า {PAGE_NUM} / {PAGE_COUNT}";
        $size = 12;
        $font = $fontMetrics->getFont("THSarabunNew");
        $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
        $x = ($pdf->get_width() - $width) / 2;
        $y = $pdf->get_height() - 35;
        $pdf->page_text(550, $y, $text, $font, $size);
    }
</script>
</body>
</html>
