@extends('layouts.paper')
@section('title','รายละเอียดการทำงาน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item" aria-current="page">รายละเอียดการทำงาน</li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{$getTask -> task_title}}
                    (Work Controller : {{$getTask -> leader_prefix.$getTask -> leader_first_name." ".$getTask -> leader_last_name}})
                </li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <div class="card mt-5">
                    <div class="card">
                        <div class="card-header">
                            <h5>รายละเอียดเกี่ยวกับ{{$contractorDetails -> title}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4 col-md-5 col-sm-4 col-6">
                                    <div class="nav-tabs-navigation verical-navs">
                                        <div class="nav-tabs-wrapper">
                                            <ul class="nav nav-tabs flex-column nav-stacked" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active show" href="#info" role="tab" data-toggle="tab" aria-selected="true">รายละเอียดผู้รับเหมา</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#sumTask" role="tab" data-toggle="tab" aria-selected="true">รายละเอียดงานทั้งหมด</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-7 col-sm-8 col-6">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active show" id="info">
                                            <p>สถานที่ปฏิบัติงาน : {{$contractorDetails ->  location ? $contractorDetails ->  location : '-'}}</p>
                                            <p>หัวหน้าผู้รับเหมา : {{$contractorDetails ->  prefix}}{{$contractorDetails ->  first_name}} {{$contractorDetails ->  last_name}}</p>
                                            <p>รายละเอียด : {{$contractorDetails -> description ? $contractorDetails -> description : "-"}}</p>
                                            <p>ทีมงานผู้รับเหมา</p>
                                            <ul>
                                                @if(count($subcontractors) > 0)
                                                    @foreach($subcontractors as $subcontractor)
                                                        <li>{{$subcontractor -> prefix.$subcontractor -> first_name." ".$subcontractor -> last_name}}</li>
                                                    @endforeach
                                                @else
                                                    <li>ไม่มีทีมงาน</li>
                                                @endif

                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="sumTask">
                                            <p>เวลาทั้งหมดที่ใช้ : {{$taskDetails_total -> sum_total_day}} วัน {{$taskDetails_total -> sum_total_hour}} ชม. {{$taskDetails_total -> sum_total_minute}} นาที.</p>
                                            <p>จำนวนครั้งทั้งหมดที่เข้างาน : {{$taskDetails_total -> sum_of_work}} ครั้ง</p>
                                            <p>จำนวนทั้งหมดที่สำเร็จ : {{$taskDetails_total -> sum_success}} ครั้ง</p>
                                            <p>จำนวนทั้งหมดที่ไม่สำเร็จ : {{$taskDetails_total -> sum_fail}} ครั้ง</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-left">
                                    <a href="javascript:history.go(-1)" class="btn btn-secondary"> < ย้อนกลับ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card-body" id="detailTask" style="display: inline;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-header ">
                                    <h5 class="m-md-2">
                                         การเข้างาน
                                    </h5>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="5%">No.</th>
                                                <th class="text-center" width="15%">สถานะ</th>
                                                <th class="text-center" width="15%">วันที่เข้างาน</th>
                                                <th class="text-center" width="15%">เริ่ม</th>
                                                <th class="text-center" width="15%">สิ้นสุด</th>
                                                <th class="text-center" width="10%">รวมเวลาทั้งหมด</th>
                                                <th class="text-center" width="5%">รายงานสรุป</th>
                                                <th class="text-center" width="5%">Export</th>
                                                <th class="text-center" width="10%">ปิดงาน</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($taskDetails as $taskDetail)
                                            <tr>
                                                <td class="text-center">{{$taskDetail -> task_detail_id}}</td>
                                                <td class="text-center">
                                                    @if($taskDetail -> task_detail_status == 1)
                                                        <h5>
                                                            <span class="badge badge-warning">รอเข้างาน</span>
                                                        </h5>
                                                    @elseif($taskDetail -> task_detail_status == 2)
                                                        <h5>
                                                            <span class="badge badge-primary">อนุมัติ</span>
                                                        </h5>
                                                    @elseif($taskDetail -> task_detail_status == 3)
                                                        <h5>
                                                            <span class="badge badge-primary">เริ่มทำงาน</span>
                                                        </h5>
                                                    @elseif($taskDetail -> task_detail_status == 4)
                                                        <h5>
                                                            <span class="badge badge-warning">ตรวจสอบ 1</span>
                                                        </h5>
                                                    @elseif($taskDetail -> task_detail_status == 5)
                                                        <h5>
                                                            <span class="badge badge-warning">ตรวจสอบ 2</span>
                                                        </h5>
                                                    @elseif($taskDetail -> task_detail_status == 6)
                                                        <h5>
                                                            <span class="badge badge-warning">ตรวจสอบปิดงาน</span>
                                                        </h5>
                                                    @elseif($taskDetail -> task_detail_status == 7)
                                                        <h5>
                                                            <span class="badge badge-success">ปิดงานสำเร็จ</span>
                                                        </h5>
                                                    @elseif($taskDetail -> task_detail_status == 8)
                                                        <h5>
                                                            <span class="badge badge-danger">ปิดงานไม่สำเร็จ</span>
                                                        </h5>
                                                    @elseif($taskDetail -> task_detail_status == 9)
                                                        <h5>
                                                            <span class="badge badge-danger">ยกเลิก</span>
                                                        </h5>
                                                    @endif
                                                </td>
                                                <td class="text-center"> {{ConvertThaiDate($taskDetail ->  task_detail_date)}}</td>
                                                <td class="text-center"> {{$taskDetail -> task_detail_start_time ? ConvertThaiDateHour($taskDetail -> task_detail_start_time) : '-'}}</td>
                                                <td class="text-center"> {{$taskDetail -> task_detail_end_time ? ConvertThaiDateHour($taskDetail -> task_detail_end_time) : '-'}}</td>
                                                <td class="text-center"> {{$taskDetail -> task_detail_sum_day ? $taskDetail -> task_detail_sum_day : '-'}}</td>
                                                <td class="text-center">
                                                    @if($taskDetail -> task_detail_status == 7 || $taskDetail -> task_detail_status == 8)
                                                        <a href="{{route('admin.task.resultTaskDetail',['task_id' => $taskDetail -> task_detail_task_id ,'task_detail_id' => $taskDetail -> task_detail_id])}}"
                                                           class="btn btn-success btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="รายงานสรุป">
                                                            <i class="nc-icon nc-single-copy-04"></i>
                                                        </a>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($taskDetail -> task_detail_status == 7 || $taskDetail -> task_detail_status == 8)
                                                        <a href="{{route('admin.task.resultTaskDetailPDF',['task_id' => $taskDetail -> task_detail_task_id ,'task_detail_id' => $taskDetail -> task_detail_id])}}"
                                                           class="btn btn-info btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="Export PDF">
                                                            <i class="nc-icon nc-cloud-download-93"></i>
                                                        </a>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($taskDetail -> task_detail_status != 7 && $taskDetail -> task_detail_status != 8)
                                                        <a href="#"
                                                           onclick="finishTask({{$taskDetail -> task_detail_id}}); return false"
                                                           class="btn btn-warning btn-icon btn-sm like"
                                                           id="hover"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="ปิดงานโดยแอดมิน">
                                                            <i class="nc-icon nc-tap-01"></i>
                                                        </a>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>

    <script>
        $('#datatable').DataTable({
            responsive: true,
            "order": [[ 2, "desc" ]]
        });

        finishTask=(task_detail_id)=>{
            Swal.fire({
                title: 'ปิดงานโดยแอดมินใช่หรือไม่ ?',
                type: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#c8d2dd',
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if(result.dismiss == 'cancel'){
                    Swal.hideLoading();
                }else if(result.dismiss == 'overlay'){
                    Swal.hideLoading();
                } else{
                    Swal.fire({
                        title: 'กรุณารอสักครู่',
                        onOpen: () => {
                            swal.showLoading();
                        }
                    })
                }
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url: "{{ route('admin.finish.task') }}",
                        method: 'post',
                        data: {
                            task_detail_id: task_detail_id
                        },
                        success: function(response){
                            if (response.success == 'Yes') {
                                Swal.fire({
                                    type: 'success',
                                    title: 'ปิดงานโดยแอดมินสำเร็จ',
                                    showConfirmButton: true,
                                    confirmButtonText: "OK!"
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                            else if(response.success == 'No'){
                                Swal.fire({
                                    type: 'error',
                                    title: 'ล้มเหลว',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({
                                    type: 'error',
                                    title: 'เกิดข้อผิดพลาด',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    </script>
@stop

