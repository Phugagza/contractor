@extends('layouts.paper')
@section('title','จัดการข้อมูลบริษัทผู้รับเหมา')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">{{$enterprise -> id > 0 ? 'แก้ไขข้อมูลบริษัทผู้รับเหมา' : 'เพิ่มข้อมูลบริษัทผู้รับเหมา' }}</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form method="POST" action="" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                กรอกข้อมูล
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ชื่อบริษัท</label>
                                        <input type="text"
                                               id="name"
                                               name="name"
                                               class="form-control"
                                               placeholder="ชื่อบริษัท"
                                               value="{{$enterprise -> name}}"
                                               onchange="checkDuplicate(this.value)"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ ชื่อบริษัท
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ที่อยู่บริษัท</label>
                                        <textarea
                                            id="address"
                                            name="address"
                                            class="form-control"
                                        >
                                        {{$enterprise -> address}}
                                        </textarea>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ ที่อยู่บริษัท
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                    <br/>
                                    <a href="javascript:history.go(-1)" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>

    </div>

    <script>
        checkDuplicate=(keyword)=>{
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            jQuery.ajax({
                url: "{{ route('admin.checkEnterprise') }}",
                method: 'get',
                data: {
                    keyword: keyword
                },
                success: function(response){
                    if(response.enterprise.length > 0){
                        Swal.fire({
                            type: 'error',
                            title: 'ข้อมูลซ้ำกรุณาลองใหม่',
                            showConfirmButton: true,
                            confirmButtonText: "OK!"
                        }).then((result) => {
                            if (result.value) {
                                $('#name').val('');
                            }
                        });
                    }


                }
            });
        }
    </script>
@stop
