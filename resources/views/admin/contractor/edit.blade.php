@extends('layouts.paper')
@section('title','จัดการข้อมูลผู้รับเหมา')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">{{$contractor -> id > 0 ? 'แก้ไขข้อมูลผู้รับเหมา' : 'เพิ่มข้อมูลผู้รับเหมา' }}</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <form method="POST" action="" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                กรอกข้อมูลส่วนตัว
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>คำนำหน้า</label>
                                        <input type="text"
                                               id="prefix"
                                               name="prefix"
                                               class="form-control"
                                               placeholder="คำนำหน้า"
                                               value="{{$contractor -> prefix}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ คำนำหน้า
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>ชื่อ</label>
                                        <input type="text"
                                               id="first_name"
                                               name="first_name"
                                               class="form-control"
                                               placeholder="ชื่อ"
                                               value="{{$contractor -> first_name}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ ชื่อ
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>นามสกุล</label>
                                        <input type="text"
                                               id="last_name"
                                               name="last_name"
                                               class="form-control"
                                               placeholder="นามสกุล"
                                               value="{{$contractor -> last_name}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ นามสกุล
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>เลขบัตรประชาชน</label>
                                        <input type="text"
                                               id="citizen_id"
                                               name="citizen_id"
                                               class="form-control"
                                               placeholder="เลขบัตรประชาชน"
                                               maxlength="13"
                                               value="{{$contractor -> citizen_id}}"
                                               onchange="checkDuplicate(this.value)"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ เลขบัตรประชาชน
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>บริษัท</label>
                                        <select  class="form-control select2"  name="enterprise_id" required>
                                            <option value="">กรุณาเลือก</option>
                                            @foreach($enterprises as $enterprise)
                                                <option value="{{$enterprise -> id}}"
                                                    {{$contractor -> enterprise_id == $enterprise -> id ? 'selected' : '' }}>
                                                    {{$enterprise -> name}}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ บริษัท
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>อีเมล์ (<span class="text-danger"> ใช้สำหรับการส่ง QR Code </span>)</label>
                                        <input type="email"
                                               id="email"
                                               name="email"
                                               class="form-control"
                                               placeholder="อีเมล์"
                                               value="{{$contractor -> email}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ อีเมล์
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>เบอร์โทร</label>
                                        <input type="number"
                                               id="tel"
                                               name="tel"
                                               class="form-control"
                                               placeholder="เบอร์โทร"
                                               value="{{$contractor -> tel}}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ เบอร์โทร
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                               ข้อมูล Safety Training
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>ผู้ฝึกอบรม</label>
                                    <select  class="form-control select2"  name="trainer_id">
                                        <option value="">กรุณาเลือก</option>
                                        @foreach($trainers as $trainer)
                                            <option value="{{$trainer -> id}}" {{$contractor -> trainer_id == $trainer -> id ? 'selected' : ''}}>
                                                {{$trainer -> prefix.
                                                  $trainer -> first_name." ".
                                                  $trainer-> last_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        โปรดระบุผู้ฝึกอบรม
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="training_date"
                                               name="training_date"
                                               class="form-control"
                                               value="{{$contractor -> safety_training_date}}"
                                               placeholder="วันอบรม"
                                               >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="expired_date" onchange="expired(this.value)">
                                            <option value="365" {{$contractor -> safety_range_date == 365 ? 'selected' : ''}}>1 ปี</option>
                                            <option value="730" {{$contractor -> safety_range_date == 730 ? 'selected' : ''}}>2 ปี</option>
                                            @if($contractor -> id > 0)
                                            <option value="another" {{$contractor -> safety_range_date != 365 && $contractor -> safety_range_date != 730 ? 'selected' : ''}}>ระบุวัน</option>
                                            @else
                                            <option value="another">ระบุวัน</option>
                                            @endif
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                @if($contractor -> id > 0)
                                    <div class="col-md-1 {{$contractor -> safety_range_date != 365 && $contractor -> safety_range_date != 730 ? '' : 'd-none'}}" id="hiddenExpired">
                                @else
                                    <div class="col-md-1 d-none" id="hiddenExpired">
                                @endif
                                    <div class="form-group">
                                        <label>ระบุ(วัน)</label>
                                        <input type="number" class="form-control" id="expired_another" name="expired_another" value="{{$contractor -> safety_range_date != 365 && $contractor -> safety_range_date != 730 ? $contractor -> safety_range_date : ''}}" placeholder="ระบุ">
                                    </div>
                                    <div class="valid-feedback">
                                        ผ่าน
                                    </div>
                                    <div class="invalid-feedback">
                                        โปรดระบุ
                                    </div>
                                @if($contractor -> id > 0)
                                    </div>
                                @else
                                    </div>
                                @endif
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                Certificate
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3" style="padding-top: 30px"><span >ช่างไฟไฟ้าภายในอาคาร</span></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="indoor_electricity_training_date"
                                               name="indoor_electricity_training_date"
                                               class="form-control"
                                               value="{{$contractor -> indoor_electricity_training_date}}"
                                               placeholder="วันอบรม"
                                        >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="indoor_electricity_expired_date">
                                            <option value="1825" {{$contractor -> indoor_electricity_range_date == 1825 ? 'selected' : ''}}>5 ปี</option>
                                            <option value="365" {{$contractor -> indoor_electricity_range_date == 365 ? 'selected' : ''}}>1 ปี</option>
                                            <option value="730" {{$contractor -> indoor_electricity_range_date == 730 ? 'selected' : ''}}>2 ปี</option>
                                            <option value="1095" {{$contractor -> indoor_electricity_range_date == 1095 ? 'selected' : ''}}>3 ปี</option>
                                            <option value="3650" {{$contractor -> indoor_electricity_range_date == 3650 ? 'selected' : ''}}>10 ปี</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="indoor_electricity_permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> indoor_electricity_permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> indoor_electricity_permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="padding-top: 30px"><span >ช่างเครื่องปรับอากาศภายในบ้านและการพาณิชย์ขนาดเล็ก</span></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="indoor_air_conditioner_training_date"
                                               name="indoor_air_conditioner_training_date"
                                               class="form-control"
                                               value="{{$contractor -> indoor_air_conditioner_training_date}}"
                                               placeholder="วันอบรม"
                                        >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="indoor_air_conditioner_expired_date" >
                                            <option value="1825" {{$contractor -> indoor_air_conditioner_range_date == 1825 ? 'selected' : ''}}>5 ปี</option>
                                            <option value="365" {{$contractor -> indoor_air_conditioner_range_date == 365 ? 'selected' : ''}}>1 ปี</option>
                                            <option value="730" {{$contractor -> indoor_air_conditioner_range_date == 730 ? 'selected' : ''}}>2 ปี</option>
                                            <option value="1095" {{$contractor -> indoor_air_conditioner_range_date == 1095 ? 'selected' : ''}}>3 ปี</option>
                                            <option value="3650" {{$contractor -> indoor_air_conditioner_range_date == 3650 ? 'selected' : ''}}>10 ปี</option>

                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="indoor_air_conditioner_permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> indoor_air_conditioner_permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> indoor_air_conditioner_permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="padding-top: 30px"><span >ช่างเชื่อมอาร์กโลหะด้วยมือ</span></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="metal_arc_training_date"
                                               name="metal_arc_training_date"
                                               class="form-control"
                                               value="{{$contractor -> metal_arc_training_date}}"
                                               placeholder="วันอบรม"
                                        >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="metal_arc_expired_date" >
                                            <option value="1825" {{$contractor -> metal_arc_range_date == 1825 ? 'selected' : ''}}>5 ปี</option>
                                            <option value="365" {{$contractor -> metal_arc_range_date == 365 ? 'selected' : ''}}>1 ปี</option>
                                            <option value="730" {{$contractor -> metal_arc_range_date == 730 ? 'selected' : ''}}>2 ปี</option>
                                            <option value="1095" {{$contractor -> metal_arc_range_date == 1095 ? 'selected' : ''}}>3 ปี</option>
                                            <option value="3650" {{$contractor -> metal_arc_range_date == 3650 ? 'selected' : ''}}>10 ปี</option>

                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="metal_arc_permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> metal_arc_permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> metal_arc_permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="padding-top: 30px"><span >ช่างเชื่อมทิก</span></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="tig_welding_training_date"
                                               name="tig_welding_training_date"
                                               class="form-control"
                                               value="{{$contractor -> tig_welding_training_date}}"
                                               placeholder="วันอบรม"
                                        >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="tig_welding_expired_date" >
                                            <option value="1825" {{$contractor -> tig_welding_range_date == 1825 ? 'selected' : ''}}>5 ปี</option>
                                            <option value="365" {{$contractor -> tig_welding_range_date == 365 ? 'selected' : ''}}>1 ปี</option>
                                            <option value="730" {{$contractor -> tig_welding_range_date == 730 ? 'selected' : ''}}>2 ปี</option>
                                            <option value="1095" {{$contractor -> tig_welding_range_date == 1095 ? 'selected' : ''}}>3 ปี</option>
                                            <option value="3650" {{$contractor -> tig_welding_range_date == 3650 ? 'selected' : ''}}>10 ปี</option>

                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="tig_welding_permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> tig_welding_permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> tig_welding_permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="padding-top: 30px"><span >ช่างเชื่อมแม็ก</span></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="alloy_welding_training_date"
                                               name="alloy_welding_training_date"
                                               class="form-control"
                                               value="{{$contractor -> alloy_welding_training_date}}"
                                               placeholder="วันอบรม"
                                        >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="alloy_welding_expired_date" >
                                            <option value="1825" {{$contractor -> alloy_welding_range_date == 1825 ? 'selected' : ''}}>5 ปี</option>
                                            <option value="365" {{$contractor -> alloy_welding_range_date == 365 ? 'selected' : ''}}>1 ปี</option>
                                            <option value="730" {{$contractor -> alloy_welding_range_date == 730 ? 'selected' : ''}}>2 ปี</option>
                                            <option value="1095" {{$contractor -> alloy_welding_range_date == 1095 ? 'selected' : ''}}>3 ปี</option>
                                            <option value="3650" {{$contractor -> alloy_welding_range_date == 3650 ? 'selected' : ''}}>10 ปี</option>

                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="alloy_welding_permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> alloy_welding_permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> alloy_welding_permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="padding-top: 30px"><span >ช่างติดตั้งนั่งร้าน</span></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="scaffolding_training_date"
                                               name="scaffolding_training_date"
                                               class="form-control"
                                               value="{{$contractor -> scaffolding_training_date}}"
                                               placeholder="วันอบรม"
                                        >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="scaffolding_expired_date" >
                                            <option value="1825" {{$contractor -> scaffolding_range_date == 1825 ? 'selected' : ''}}>5 ปี</option>
                                            <option value="365" {{$contractor -> scaffolding_range_date == 365 ? 'selected' : ''}}>1 ปี</option>
                                            <option value="730" {{$contractor -> scaffolding_range_date == 730 ? 'selected' : ''}}>2 ปี</option>
                                            <option value="1095" {{$contractor -> scaffolding_range_date == 1095 ? 'selected' : ''}}>3 ปี</option>
                                            <option value="3650" {{$contractor -> scaffolding_range_date == 3650 ? 'selected' : ''}}>10 ปี</option>

                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="scaffolding_permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> scaffolding_permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> scaffolding_permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="padding-top: 30px"><span >ผู้ปฏิบัติงาน ในงานอับอากาศ</span></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="worker_confine_training_date"
                                               name="worker_confine_training_date"
                                               class="form-control"
                                               value="{{$contractor -> worker_confine_training_date}}"
                                               placeholder="วันอบรม"
                                        >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="worker_confine_expired_date" >
                                            <option value="1825" {{$contractor -> worker_confine_range_date == 1825 ? 'selected' : ''}}>5 ปี</option>
                                            <option value="365" {{$contractor -> worker_confine_range_date == 365 ? 'selected' : ''}}>1 ปี</option>
                                            <option value="730" {{$contractor -> worker_confine_range_date == 730 ? 'selected' : ''}}>2 ปี</option>
                                            <option value="1095" {{$contractor -> worker_confine_range_date == 1095 ? 'selected' : ''}}>3 ปี</option>
                                            <option value="3650" {{$contractor -> worker_confine_range_date == 3650 ? 'selected' : ''}}>10 ปี</option>

                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="worker_confine_permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> worker_confine_permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> worker_confine_permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="padding-top: 30px"><span >ผู้ช่วยเหลือ ในงานอับอากาศ</span></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="assistant_confine_training_date"
                                               name="assistant_confine_training_date"
                                               class="form-control"
                                               value="{{$contractor -> assistant_confine_training_date}}"
                                               placeholder="วันอบรม"
                                        >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="assistant_confine_expired_date" >
                                            <option value="1825" {{$contractor -> assistant_confine_range_date == 1825 ? 'selected' : ''}}>5 ปี</option>
                                            <option value="365" {{$contractor -> assistant_confine_range_date == 365 ? 'selected' : ''}}>1 ปี</option>
                                            <option value="730" {{$contractor -> assistant_confine_range_date == 730 ? 'selected' : ''}}>2 ปี</option>
                                            <option value="1095" {{$contractor -> assistant_confine_range_date == 1095 ? 'selected' : ''}}>3 ปี</option>
                                            <option value="3650" {{$contractor -> assistant_confine_range_date == 3650 ? 'selected' : ''}}>10 ปี</option>

                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="assistant_confine_permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> assistant_confine_permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> assistant_confine_permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="padding-top: 30px"><span >ผู้ควบคุม ในงานอับอากาศ</span></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="controller_confine_training_date"
                                               name="controller_confine_training_date"
                                               class="form-control"
                                               value="{{$contractor -> controller_confine_training_date}}"
                                               placeholder="วันอบรม"
                                        >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="controller_confine_expired_date" >
                                            <option value="1825" {{$contractor -> controller_confine_range_date == 1825 ? 'selected' : ''}}>5 ปี</option>
                                            <option value="365" {{$contractor -> controller_confine_range_date == 365 ? 'selected' : ''}}>1 ปี</option>
                                            <option value="730" {{$contractor -> controller_confine_range_date == 730 ? 'selected' : ''}}>2 ปี</option>
                                            <option value="1095" {{$contractor -> controller_confine_range_date == 1095 ? 'selected' : ''}}>3 ปี</option>
                                            <option value="3650" {{$contractor -> controller_confine_range_date == 3650 ? 'selected' : ''}}>10 ปี</option>

                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="controller_confine_permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> controller_confine_permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> controller_confine_permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="padding-top: 30px"><span >ผู้อนุญาต ในงานอับอากาศ</span></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>วันอบรม(ปี / เดือน / วัน)</label>
                                        <input type="text"
                                               id="warrantor_confine_training_date"
                                               name="warrantor_confine_training_date"
                                               class="form-control"
                                               value="{{$contractor -> warrantor_confine_training_date}}"
                                               placeholder="วันอบรม"
                                        >
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>วันหมดอายุ</label>
                                        <select  class="form-control"  name="warrantor_confine_expired_date" >
                                            <option value="1825" {{$contractor -> warrantor_confine_range_date == 1825 ? 'selected' : ''}}>5 ปี</option>
                                            <option value="365" {{$contractor -> warrantor_confine_range_date == 365 ? 'selected' : ''}}>1 ปี</option>
                                            <option value="730" {{$contractor -> warrantor_confine_range_date == 730 ? 'selected' : ''}}>2 ปี</option>
                                            <option value="1095" {{$contractor -> warrantor_confine_range_date == 1095 ? 'selected' : ''}}>3 ปี</option>
                                            <option value="3650" {{$contractor -> warrantor_confine_range_date == 3650 ? 'selected' : ''}}>10 ปี</option>

                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุวันอบรม
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Permission</label>
                                        <select  class="form-control"  name="warrantor_confine_permission">
                                            <option value="">กรุณาเลือก</option>
                                            <option value="Y" {{$contractor -> warrantor_confine_permission == "Y" ? 'selected' : ''}}>Y</option>
                                            <option value="N" {{$contractor -> warrantor_confine_permission == "N" ? 'selected' : ''}}>N</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุ Permission
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                            <br/>
                            <a href="{{route('admin.contractor.index')}}" style="color: gray;  text-decoration: underline;">ยกเลิก</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $('#training_date').datetimepicker({format: 'YYYY-MM-DD'});
            $('#indoor_electricity_training_date').datetimepicker({format: 'YYYY-MM-DD'});
            $('#indoor_air_conditioner_training_date').datetimepicker({format: 'YYYY-MM-DD'});
            $('#metal_arc_training_date').datetimepicker({format: 'YYYY-MM-DD'});
            $('#tig_welding_training_date').datetimepicker({format: 'YYYY-MM-DD'});
            $('#alloy_welding_training_date').datetimepicker({format: 'YYYY-MM-DD'});
            $('#scaffolding_training_date').datetimepicker({format: 'YYYY-MM-DD'});
            $('#worker_confine_training_date').datetimepicker({format: 'YYYY-MM-DD'});
            $('#assistant_confine_training_date').datetimepicker({format: 'YYYY-MM-DD'});
            $('#controller_confine_training_date').datetimepicker({format: 'YYYY-MM-DD'});
            $('#warrantor_confine_training_date').datetimepicker({format: 'YYYY-MM-DD'});
        });

        expired=(value)=>{
            if(value == "another"){
                document.getElementById("hiddenExpired").className = 'col-md-2';
                document.getElementById("expired_another").required = true;
                document.getElementById("expired_another").min = 1;
            }else{
                document.getElementById("hiddenExpired").className = 'col-md-2 d-none';
                document.getElementById("expired_another").value = '';
                document.getElementById("expired_another").required = false;
                document.getElementById("expired_another").removeAttribute('min');
            }
        }

        checkDuplicate=(keyword)=>{
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            jQuery.ajax({
                url: "{{ route('admin.checkCitizen') }}",
                method: 'get',
                data: {
                    keyword: keyword
                },
                success: function(response){
                    if(response.citizen.length > 0){
                        Swal.fire({
                            type: 'error',
                            title: 'ข้อมูลซ้ำกรุณาลองใหม่',
                            showConfirmButton: true,
                            confirmButtonText: "OK!"
                        }).then((result) => {
                            if (result.value) {
                                $('#citizen_id').val('');
                            }
                        });
                    }


                }
            });
        }
    </script>
@stop
