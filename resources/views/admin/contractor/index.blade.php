@extends('layouts.paper')
@section('title','จัดการข้อมูลผู้รับเหมา')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <div class="card mt-5">
                    <div class="card-body" id="Task" >
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-header">
                                    <h3> ค้นหา </h3>
                                </div>
                                <div class="card-body">
                                    <form  method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>บริษัท</label>
                                                    <select  class="form-control select2"  name="enterprise_id">
                                                        <option value="">ทั้งหมด</option>
                                                        <option value="enterprise_exit" {{$enterprise_id == 'enterprise_exit' ?'selected':''}}>ไม่มีบริษัท</option>
                                                        @foreach($enterprises as $enterprise)
                                                            <option value="{{$enterprise -> id}}" {{$enterprise -> id == $enterprise_id ? 'selected':''}}>{{$enterprise -> name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>จำนวนที่แสดง</label>
                                                    <select  class="form-control select2"  name="perPageDefault">
                                                        <option value="10" {{$perPageDefault == 10 ? 'selected':''}}>10</option>
                                                        <option value="20" {{$perPageDefault == 20 ? 'selected':''}}>20</option>
                                                        <option value="30" {{$perPageDefault == 30 ? 'selected':''}}>30</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>ค้นหาโดย ชื่อ - นามสกุล</label>
                                                    <input type="text"
                                                           id="keyword"
                                                           name="keyword"
                                                           class="form-control"
                                                           placeholder="ชื่อ - นามสกุล"
                                                           value="{{$keyword}}"
                                                           >
                                                </div>
                                            </div>
                                        </div>
                                        <input type="submit" value="ค้นหา"  class="btn btn-info">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-body" id="Task" >
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-header">
                                    <a href="{{route('admin.contractor.edit')}}"
                                       class="btn btn-primary">
                                        เพิ่มผู้รับเหมา
                                    </a>

                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="1%">Safety Training</th>
                                                <th class="text-center" width="20%">ชื่อ - นามสกุล</th>
                                                <th class="text-center" width="20%">บริษัท</th>
                                                <th class="text-center" width="1%">ไฟไฟ้าภายในอาคาร</th>
                                                <th class="text-center" width="1%">เครื่องปรับอากาศภายในบ้าน</th>
                                                <th class="text-center" width="1%">เชื่อมอาร์กโลหะ</th>
                                                <th class="text-center" width="1%">เชื่อมทิก</th>
                                                <th class="text-center" width="1%">เชื่อมแม็ก</th>
                                                <th class="text-center" width="1%">ติดตั้งนั่งร้าน</th>
                                                <th class="text-center" width="1%">ผู้ปฏิบัติงานอับอากาศ</th>
                                                <th class="text-center" width="1%">ผู้ช่วยเหลืออับอากาศ</th>
                                                <th class="text-center" width="1%">ผู้ควบคุมอับอากาศ</th>
                                                <th class="text-center" width="1%">ผู้อนุญาตอับอากาศ</th>
                                                <th class="text-center" width="5%" class="disabled-sorting text-center">แก้ไขข้อมูล</th>
                                                <th class="text-center" width="3%" class="disabled-sorting text-center">ลบ</th>

                                            </tr>
                                            </thead>
                                            <tbody class="text-center">
                                            @foreach($contractors as $contractor)
                                                <tr>
                                                    <td class="text-center">
                                                        {!! $contractor -> permission_with_style  !!}
                                                    </td>
                                                    <td class="text-center">{{$contractor -> full_name}}</td>
                                                    <td class="text-center">{{$contractor -> enterprise ? $contractor -> enterprise -> name : '-'}}</td>
                                                    <td class="text-center">
                                                        {!! $contractor -> indoor_electricity_permission_with_style  !!}
                                                    </td>
                                                    <td class="text-center">
                                                        {!! $contractor -> indoor_air_conditioner_permission_with_style  !!}
                                                    </td>
                                                    <td class="text-center">
                                                        {!! $contractor -> metal_arc_permission_with_style  !!}
                                                    </td>
                                                    <td class="text-center">
                                                        {!! $contractor -> tig_welding_permission_with_style  !!}
                                                    </td>
                                                    <td class="text-center">
                                                        {!! $contractor -> alloy_welding_permission_with_style  !!}
                                                    </td>
                                                    <td class="text-center">
                                                        {!! $contractor -> scaffolding_permission_with_style  !!}
                                                    </td>
                                                    <td class="text-center">
                                                        {!! $contractor -> worker_confine_permission_with_style  !!}
                                                    </td>
                                                    <td class="text-center">
                                                        {!! $contractor -> assistant_confine_permission_with_style  !!}
                                                    </td>
                                                    <td class="text-center">
                                                        {!! $contractor -> controller_confine_permission_with_style  !!}
                                                    </td>
                                                    <td class="text-center">
                                                        {!! $contractor -> warrantor_confine_permission_with_style  !!}
                                                    </td>
                                                    <td class="text-center" class="disabled-sorting text-center">
                                                        <a href="{{route('admin.contractor.edit',['id'=>$contractor->id])}}"
                                                           class="btn btn-warning btn-icon btn-sm like" id="hover" data-toggle="tooltip" title="แก้ไขข้อมูลผู้รับเหมา">
                                                            <i class="nc-icon nc-ruler-pencil"></i>
                                                        </a>
                                                    </td>
                                                    <td class="text-center" class="disabled-sorting text-center">
                                                        <a href="#" onclick="DeleteContractor({{$contractor->id}}); return false"
                                                           class="btn btn-danger btn-icon btn-sm like" id="hover" data-toggle="tooltip" title="ลบ">
                                                            <i class="nc-icon nc-simple-remove"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            {{ $contractors->appends($allRequest)->links() }}
                                        </div>
                                        <div class="col-md-12">
                                            {{
                                              ($contractors->perPage() * $contractors->currentPage()) - ($contractors->perPage() - $contractors->count())
                                               ." ทั้งหมด ".
                                               $contractors -> total()
                                            }}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <script>
        $('#datatable').DataTable({
            responsive: true,
            "paging": false,
            "bInfo" : false,
            "searching": false,
            "order": [[ 1, "asc" ]]
        })

        $('.select2').select2();

        DeleteContractor=(id)=>{
            Swal.fire({
                title: 'ต้องการลบข้อมูลใช่หรือไม่ ?',
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#c8d2dd',
                confirmButtonText: 'ใช่',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if(result.dismiss == 'cancel'){
                    Swal.hideLoading();
                }else if(result.dismiss == 'overlay'){
                    Swal.hideLoading();
                } else{
                    Swal.fire({
                        title: 'กรุณารอสักครู่',
                        onOpen: () => {
                            swal.showLoading();
                        }
                    })
                }
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    jQuery.ajax({
                        url: "{{ route('admin.contractor.delete') }}",
                        method: 'post',
                        data: {
                            id: id
                        },
                        success: function(response){
                            if (response.success == 'Yes') {
                                Swal.fire({
                                    type: 'success',
                                    title: 'บันทึกข้อมูลเรียบร้อย',
                                    showConfirmButton: true,
                                    confirmButtonText: "OK!"
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                            else if(response.success == 'No'){
                                Swal.fire({
                                    type: 'error',
                                    title: 'ล้มเหลว',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }else{
                                Swal.fire({
                                    type: 'error',
                                    title: 'เกิดข้อผิดพลาด',
                                    text: 'กรุณาตรวจสอบข้อมูลอีกครั้ง',
                                    timer: 2000,
                                    showConfirmButton: false
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    </script>
@stop
