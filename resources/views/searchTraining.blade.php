@extends('layouts.paper')
@section('title','ตรวจสอบการอบรม')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">ตรวจสอบการอบรม</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form method="POST" action="" id="register_form" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                ค้นหา
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  class="form-control select2"  name="contractor_id" required>
                                            <option value="">กรุณาเลือกชื่อท่าน</option>
                                            @foreach($contractors as $contractor)
                                                <option value="{{$contractor -> id}}">{{$contractor -> prefix.$contractor -> first_name." ".$contractor -> last_name." (".$contractor -> enterprise_name.")"}}</option>
                                            @endforeach
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดเลือกผู้รับเหมา
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block">ค้นหา</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card-header ">
                        <h5 class="m-md-2">
                            ผลการค้นหา
                        </h5>
                    </div>
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="20%">การอบรม</th>
                                                <th class="text-center" width="20%">ชื่อ - นามสกุล</th>
                                                <th class="text-center" width="10%">บริษัท</th>
{{--                                                <th class="text-center" width="20%">ผู้ฝึกอบรม</th>--}}
                                                <th class="text-center" width="10%">วันอบรม</th>
                                                <th class="text-center" width="10%">วันหมดอายุ</th>
                                                <th class="text-center" width="10%">เวลาที่เหลือ</th>
                                                <th class="text-center" width="5%">Permission</th>
                                            </tr>
                                            </thead>
                                            @if($training_details != null)
                                                <tbody>
                                                @foreach($training_details as $training_detail)
                                                        <td class="text-center"> {{$training_detail -> training_name}}</td>
                                                        <td class="text-center">
                                                            {{$training_detail -> contractor_prefix.$training_detail -> contractor_first_name." ".$training_detail -> contractor_last_name}}
                                                        </td>
                                                        <td class="text-center">{{$training_detail -> enterprise_name ? $training_detail -> enterprise_name : '-'}}</td>
{{--                                                        <td class="text-center">{{$training_detail -> trainer_first_name." ".$training_detail -> trainer_last_name}}</td>--}}
                                                        <td class="text-center">{{$training_detail -> training_date ? ConvertThaiDate($training_detail -> training_date) : '-'}}</td>
                                                        <td class="text-center">{{$training_detail -> expired_date ? ConvertThaiDate($training_detail -> expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($training_detail -> count_date >= 0)
                                                                {{$training_detail -> count_date}}
                                                            @elseif($training_detail -> count_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($training_detail -> permission == "Y")
                                                                <h6>
                                                                    <span class="badge badge-success">Y</span>
                                                                </h6>
                                                            @elseif($training_detail -> permission == "N")
                                                                <h6>
                                                                    <span class="badge badge-danger">N</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            @elseif($training_details == null)
                                                <tbody>
                                                    <tr>
                                                        <td colspan="7" class="text-center">ไม่พบข้อมูล</td>
                                                    </tr>
                                                </tbody>
                                            @endif
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });

    </script>
@stop
