@extends('layouts.paper')
@section('title','ลงทะเบียน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">ลงทะเบียน</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <form method="POST" action="{{route('register')}}" id="register_form" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                กรอกข้อมูลลงทะเบียน
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><span class="text-danger">*</span> ชื่องาน (ไม่เกิน 40 ตัวอักษร)</label>
                                        <input type="text"
                                               id="title"
                                               name="title"
                                               class="form-control"
                                               placeholder="ชื่องาน"
                                               value="{{ old('title') }}"
                                               maxlength="40"
                                               onkeypress="return preventDot(event);"
                                               onpaste="return false;"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback" id="title_check">
                                            โปรดระบุชื่องาน
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><span class="text-danger">*</span> วันที่เข้าทำงาน</label>
                                        <input type="text"
                                               id="working_date"
                                               name="working_date"
                                               class="form-control"
                                               placeholder="วันที่เข้าทำงาน"
                                               value="{{ old('working_date') }}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุรายละเอียด
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>สถานที่ปฏิบัติงาน</label>
                                        <input type="text"
                                               id="location"
                                               name="location"
                                               class="form-control"
                                               placeholder="สถานที่ปฏิบัติงาน"
                                               maxlength="255"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุสถานที่ปฏิบัติงาน
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><span class="text-danger">*</span> รายละเอียดงาน (ไม่เกิน 255 ตัวอักษร)</label>
                                        <textarea
                                          id="description"
                                          name="description"
                                          class="form-control"
                                          maxlength="255"
                                        >
                                            {{ old('description') }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label><span class="text-danger">*</span> หัวหน้าผู้รับเหมา</label>
{{--                                        <select  class="form-control select2" id="contractor_id"  name="contractor_id" required>--}}
{{--                                            <option value="">กรุณาเลือก</option>--}}
{{--                                            @foreach($contractors as $contractor)--}}
{{--                                                <option value="{{$contractor -> id}}" {{ old('contractor_id') ? 'selected' : '' }}>{{$contractor -> prefix.$contractor -> first_name." ".$contractor -> last_name." (".$contractor -> enterprise_name.")"}}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
                                        <select  class="form-control" id="contractor_id" name="contractor_id" required>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดเลือกผู้รับเหมา
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><span class="text-danger">*</span> ผู้ควบคุมงาน</label>
{{--                                        <select  class="form-control select2"  id="leader_id" name="leader_id" required>--}}
{{--                                            <option value="">กรุณาเลือก</option>--}}
{{--                                            @foreach($leaders as $leader)--}}
{{--                                                <option value="{{$leader -> id}}" {{ old('leader_id') ? 'selected' : '' }}>{{$leader -> prefix.$leader -> first_name." ".$leader -> last_name}} </option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
                                        <select  class="form-control"  id="leader_id" name="leader_id" required></select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดเลือกผู้ควบคุมงาน
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ทีมงานผู้รับเหมา</label>
{{--                                        <select  class="form-control select2"  name="subcontractor_id[]" multiple="multiple">--}}
{{--                                            @foreach($contractors as $contractor)--}}
{{--                                                <option value="{{$contractor -> id}}">{{$contractor -> prefix.$contractor -> first_name." ".$contractor -> last_name." (".$contractor -> enterprise_name.")"}}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
                                        <select  class="form-control" id="subcontractor_id" name="subcontractor_id[]" multiple="multiple"> </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" id="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>


    <script>
        $(document).ready(function() {
            document.getElementById('submit').className = "btn btn-primary btn-lg disabled"
            $('.select2').select2();

            $("#contractor_id").select2({
                ajax: {
                    url: "{{route('getContractor')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'กรุณาเลือก',
                minimumInputLength: 1,
                language: {
                    inputTooShort: function () {
                        return 'พิมพ์อย่างน้อย 1 ตัวอักษร';
                    }
                },
                templateResult: formatContractor,
                templateSelection: formatContractorSelection
            });

            $("#subcontractor_id").select2({
                ajax: {
                    url: "{{route('getContractor')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'กรุณาเลือก',
                minimumInputLength: 1,
                language: {
                    inputTooShort: function () {
                        return 'พิมพ์อย่างน้อย 1 ตัวอักษร';
                    }
                },
                templateResult: formatContractor,
                templateSelection: formatContractorSelection
            });

            function formatContractor(repo) {

                if (repo.loading) {
                    return repo.text;
                }

                var $container = $(
                    "<div class='select2-result-repository clearfix'>" +
                    "<div class='select2-result-repository__avatar'></div>" +
                    "<div class='select2-result-repository__meta'>" +
                    "<div class='select2-result-repository__title'></div>" +
                    "</div>" +
                    "</div>"
                );

                $container.find(".select2-result-repository__title").text(repo.first_name + " " + repo.last_name + "(" + repo.enterprise_name + ")");


                return $container;
            }

            function formatContractorSelection(repo) {
                // console.log(repo.prefix);
                if (repo.prefix != null) {
                    return repo.prefix + repo.first_name + " " + repo.last_name + '(' + repo.enterprise_name + ')';
                }

                if (repo.prefix != 'undefined') {
                    return repo.text
                }
            }

            $("#leader_id").select2({
                ajax: {
                    url: "{{route('getLeader')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'กรุณาเลือก',
                minimumInputLength: 1,
                language: {
                    inputTooShort: function () {
                        return 'พิมพ์อย่างน้อย 1 ตัวอักษร';
                    }
                },
                templateResult: formatLeader,
                templateSelection: formatLeaderSelection
            });

            function formatLeader(repo) {
                if (repo.loading) {
                    return repo.text;
                }

                var $container = $(
                    "<div class='select2-result-repository clearfix'>" +
                    "<div class='select2-result-repository__avatar'></div>" +
                    "<div class='select2-result-repository__meta'>" +
                    "<div class='select2-result-repository__title'></div>" +
                    "</div>" +
                    "</div>"
                );

                $container.find(".select2-result-repository__title").text(repo.first_name + " " + repo.last_name);


                return $container;
            }

            function formatLeaderSelection(repo) {

                if (repo.prefix != null) {
                    return repo.prefix + repo.first_name + " " + repo.last_name;
                }

                if (repo.prefix == null) {
                    return repo.text;
                }

            }

            $('#working_date').datetimepicker({
                format: 'DD-MM-YYYY'
            });

            $("#register_form").on("submit", function () {
                Swal.fire({
                    title: 'กรุณารอสักครู่',
                    onOpen: () => {
                        swal.showLoading();
                    }
                })
                document.getElementById('submit').className = "btn btn-primary btn-lg disabled"
            });//submit

            $("#register_form").change(function () {
                var title = document.getElementById('title').value;
                var working_date = document.getElementById('working_date').value;
                var contractor_id = document.getElementById('contractor_id').value;
                var leader_id = document.getElementById('leader_id').value;

                if (title == "" || working_date == "" || contractor_id == "" || leader_id == "") {
                    Swal.hideLoading();
                    document.getElementById('submit').className = "btn btn-primary btn-lg disabled"
                } else {
                    document.getElementById('submit').className = "btn btn-primary btn-lg"
                }

            });//submit

        });

        preventDot =(e) =>
        {
            var key = e.charCode ? e.charCode : e.keyCode;
            if (key == 46)
            {
                return false;
            }
        }


    </script>
@stop
