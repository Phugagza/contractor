<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <!-- <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png"> -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/apple-icon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        {{ env('APP_NAME') }}
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport' />
    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <!-- CSS Files -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/paper-dashboard.css')}}" rel="stylesheet" />
</head>
<body class="login-page">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
    <div class="container">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#ns">
                <img src="{{asset('img/bg/ns-logo.png')}}">
            </a>
        </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="wrapper wrapper-full-page ">
    <div class="full-page section-image" filter-color="black" data-image="{{asset('img/bg/fabio-mangione.jpg')}}">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content">
            <div class="container">
                <div class="col-lg-8 col-md-8 ml-auto mr-auto">

                    <div class="card card-login">
                        <div class="card-header ">
                            <div class="card-header ">
                                <h1 class="header text-danger text-center">{{$status}}</h1>
                            </div>
                        </div>
                        <div class="card-body ">
                            <h3 class="text-center">{{$message}}</h3>
                            <a href="javascript:history.go(-1)" class="btn btn-secondary btn-lg btn-block">กลับ</a>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="full-page-background" style="background-image: url('{{asset('img/bg/ns-bg.png')}}')"></div>
    </div>
</div>

</body>
</html>
