@extends('layouts.paper')
@section('title','ลงทะเบียน')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">ลงทะเบียน</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('flash-message')
                <form method="POST" action="{{route('register')}}" id="register_form" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                กรอกข้อมูลลงทะเบียน
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><span class="text-danger">* </span>ประเภทงาน</label>
                                        <select  class="form-control" id="task_type" name="task_type" {{--onchange="showTaskTypeInfo(this.value)"--}} required>
                                                <option value="">กรุณาเลือก</option>
                                                <option value="1">งานไฟฟ้าในอาคาร</option>
                                                <option value="2">งานเครื่องปรับอากาศ</option>
                                                <option value="3">งานเชื่อม</option>
                                                <option value="4">งานติดตั้งนั่งร้าน</option>
                                                <option value="5">งานอับอากาศ</option>
                                                <option value="6">อื่นๆ</option>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดเลือกผู้รับเหมา
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><span class="text-danger">*</span> ชื่องาน (ไม่เกิน 40 ตัวอักษร)</label>
                                        <input type="text"
                                               id="title"
                                               name="title"
                                               class="form-control"
                                               placeholder="ชื่องาน"
                                               value="{{ old('title') }}"
                                               maxlength="40"
                                               onkeypress="return preventDot(event);"
                                               onpaste="return false;"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback" id="title_check">
                                            โปรดระบุชื่องาน
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><span class="text-danger">*</span> วันที่เข้าทำงาน</label>
                                        <input type="text"
                                               id="working_date"
                                               name="working_date"
                                               class="form-control"
                                               placeholder="วันที่เข้าทำงาน"
                                               value="{{ old('working_date') }}"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุรายละเอียด
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>สถานที่ปฏิบัติงาน</label>
                                        <input type="text"
                                               id="location"
                                               name="location"
                                               class="form-control"
                                               placeholder="สถานที่ปฏิบัติงาน"
                                               maxlength="255"
                                               required>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดระบุสถานที่ปฏิบัติงาน
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><span class="text-danger">*</span> รายละเอียดงาน (ไม่เกิน 255 ตัวอักษร)</label>
                                        <textarea
                                          id="description"
                                          name="description"
                                          class="form-control"
                                          maxlength="255"
                                        >
                                            {{ old('description') }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label><span class="text-danger">*</span> หัวหน้าผู้รับเหมา</label>

                                        <select  class="form-control" id="contractor_id" name="contractor_id" required>
                                        </select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดเลือกผู้รับเหมา
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><span class="text-danger">*</span> ผู้ควบคุมงาน</label>
                                        <select  class="form-control"  id="leader_id" name="leader_id" required></select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดเลือกผู้ควบคุมงาน
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ทีมงานผู้รับเหมา(ช่าง)</label>
                                        <select  class="form-control" id="subcontractor_id" name="subcontractor_id[]" multiple="multiple"> </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ทีมงานผู้ช่วยช่าง</label>
                                        <select  class="form-control" id="assistant_subcontractor_id" name="assistant_subcontractor_id[]" multiple="multiple"> </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mt-5">
                            <div class="card-header ">
                                <h5 class="m-md-2">
                                    <span class="text-danger">*</span> การใช้เครื่องมือหรืออุปกรณ์ไฟไฟ้าที่ใช้ไฟไฟ้าตั้งแต่ 220 V. ขึ้นไป
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="row ml-4">
                                    <div class="col-sm-10">
                                        <div class="form-check-radio ">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="electric_check" id="electric_check1" value="1"> ไม่มีการใช้
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                        <div class="form-check-radio">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="electric_check" id="electric_check2" value="2"> มีการใช้ โปรดกรอกรายละเอียดลงในตาราง
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <table id="datatable" class="table table-striped table-bordered m-3" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="2%">ลำดับ</th>
                                            <th class="text-center" width="50%">รายการเครื่องมือหรืออุปกรณ์</th>
                                            <th class="text-center" width="10%">จำนวน</th>
                                            <th class="text-center" width="10%">หน่วย</th>
                                            <th class="text-center" width="10%">ผ่าน / ไม่ผ่าน</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td>
                                                <input type="text"
                                                       id="equipment1"
                                                       name="equipment1"
                                                       class="form-control"
                                                       placeholder="รายการเครื่องมือหรืออุปกรณ์" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="amount1"
                                                       name="amount1"
                                                       class="form-control"
                                                       placeholder="จำนวน" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="unit1"
                                                       name="unit1"
                                                       class="form-control"
                                                       placeholder="หน่วย" disabled>
                                            </td>
                                            <td>
                                                <select  class="form-control"  name="permission1" id="permission1" disabled>
                                                    <option value="">กรุณาเลือก</option>
                                                    <option value="1">ผ่าน</option>
                                                    <option value="2">ไม่ผ่าน</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">2.</td>
                                            <td>
                                                <input type="text"
                                                       id="equipment2"
                                                       name="equipment2"
                                                       class="form-control"
                                                       placeholder="รายการเครื่องมือหรืออุปกรณ์" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="amount2"
                                                       name="amount2"
                                                       class="form-control"
                                                       placeholder="จำนวน" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="unit2"
                                                       name="unit2"
                                                       class="form-control"
                                                       placeholder="หน่วย" disabled>
                                            </td>
                                            <td>
                                                <select  class="form-control"  name="permission2" id="permission2" disabled>
                                                    <option value="">กรุณาเลือก</option>
                                                    <option value="1">ผ่าน</option>
                                                    <option value="2">ไม่ผ่าน</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">3.</td>
                                            <td>
                                                <input type="text"
                                                       id="equipment3"
                                                       name="equipment3"
                                                       class="form-control"
                                                       placeholder="รายการเครื่องมือหรืออุปกรณ์" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="amount3"
                                                       name="amount3"
                                                       class="form-control"
                                                       placeholder="จำนวน" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="unit3"
                                                       name="unit3"
                                                       class="form-control"
                                                       placeholder="หน่วย" disabled>
                                            </td>
                                            <td>
                                                <select  class="form-control"  name="permission3" id="permission3" disabled>
                                                    <option value="">กรุณาเลือก</option>
                                                    <option value="1">ผ่าน</option>
                                                    <option value="2">ไม่ผ่าน</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">4.</td>
                                            <td>
                                                <input type="text"
                                                       id="equipment4"
                                                       name="equipment4"
                                                       class="form-control"
                                                       placeholder="รายการเครื่องมือหรืออุปกรณ์" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="amount4"
                                                       name="amount4"
                                                       class="form-control"
                                                       placeholder="จำนวน" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="unit4"
                                                       name="unit4"
                                                       class="form-control"
                                                       placeholder="หน่วย" disabled>
                                            </td>
                                            <td>
                                                <select  class="form-control"  name="permission4" id="permission4" disabled>
                                                    <option value="">กรุณาเลือก</option>
                                                    <option value="1">ผ่าน</option>
                                                    <option value="2">ไม่ผ่าน</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">5.</td>
                                            <td>
                                                <input type="text"
                                                       id="equipment5"
                                                       name="equipment5"
                                                       class="form-control"
                                                       placeholder="รายการเครื่องมือหรืออุปกรณ์" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="amount5"
                                                       name="amount5"
                                                       class="form-control"
                                                       placeholder="จำนวน" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="unit5"
                                                       name="unit5"
                                                       class="form-control"
                                                       placeholder="หน่วย" disabled>
                                            </td>
                                            <td>
                                                <select  class="form-control"  name="permission5" id="permission5" disabled>
                                                    <option value="">กรุณาเลือก</option>
                                                    <option value="1">ผ่าน</option>
                                                    <option value="2">ไม่ผ่าน</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">6.</td>
                                            <td>
                                                <input type="text"
                                                       id="equipment6"
                                                       name="equipment6"
                                                       class="form-control"
                                                       placeholder="รายการเครื่องมือหรืออุปกรณ์" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="amount6"
                                                       name="amount6"
                                                       class="form-control"
                                                       placeholder="จำนวน" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="unit6"
                                                       name="unit6"
                                                       class="form-control"
                                                       placeholder="หน่วย" disabled>
                                            </td>
                                            <td>
                                                <select  class="form-control"  name="permission6" id="permission6" disabled>
                                                    <option value="">กรุณาเลือก</option>
                                                    <option value="1">ผ่าน</option>
                                                    <option value="2">ไม่ผ่าน</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">7.</td>
                                            <td>
                                                <input type="text"
                                                       id="equipment7"
                                                       name="equipment7"
                                                       class="form-control"
                                                       placeholder="รายการเครื่องมือหรืออุปกรณ์" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="amount7"
                                                       name="amount7"
                                                       class="form-control"
                                                       placeholder="จำนวน" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="unit7"
                                                       name="unit7"
                                                       class="form-control"
                                                       placeholder="หน่วย" disabled>
                                            </td>
                                            <td>
                                                <select  class="form-control"  name="permission7" id="permission7" disabled>
                                                    <option value="">กรุณาเลือก</option>
                                                    <option value="1">ผ่าน</option>
                                                    <option value="2">ไม่ผ่าน</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">8.</td>
                                            <td>
                                                <input type="text"
                                                       id="equipment8"
                                                       name="equipment8"
                                                       class="form-control"
                                                       placeholder="รายการเครื่องมือหรืออุปกรณ์" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="amount8"
                                                       name="amount8"
                                                       class="form-control"
                                                       placeholder="จำนวน" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="unit8"
                                                       name="unit8"
                                                       class="form-control"
                                                       placeholder="หน่วย" disabled>
                                            </td>
                                            <td>
                                                <select  class="form-control"  name="permission8" id="permission8" disabled>
                                                    <option value="">กรุณาเลือก</option>
                                                    <option value="1">ผ่าน</option>
                                                    <option value="2">ไม่ผ่าน</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">9.</td>
                                            <td>
                                                <input type="text"
                                                       id="equipment9"
                                                       name="equipment9"
                                                       class="form-control"
                                                       placeholder="รายการเครื่องมือหรืออุปกรณ์" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="amount9"
                                                       name="amount9"
                                                       class="form-control"
                                                       placeholder="จำนวน" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="unit9"
                                                       name="unit9"
                                                       class="form-control"
                                                       placeholder="หน่วย" disabled>
                                            </td>
                                            <td>
                                                <select  class="form-control"  name="permission9" id="permission9" disabled>
                                                    <option value="">กรุณาเลือก</option>
                                                    <option value="1">ผ่าน</option>
                                                    <option value="2">ไม่ผ่าน</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">10.</td>
                                            <td>
                                                <input type="text"
                                                       id="equipment10"
                                                       name="equipment10"
                                                       class="form-control"
                                                       placeholder="รายการเครื่องมือหรืออุปกรณ์" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="amount10"
                                                       name="amount10"
                                                       class="form-control"
                                                       placeholder="จำนวน" disabled>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       id="unit10"
                                                       name="unit10"
                                                       class="form-control"
                                                       placeholder="หน่วย" disabled>
                                            </td>
                                            <td>
                                                <select  class="form-control"  name="permission10" id="permission10" disabled>
                                                    <option value="">กรุณาเลือก</option>
                                                    <option value="1">ผ่าน</option>
                                                    <option value="2">ไม่ผ่าน</option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" id="submit" class="btn btn-primary btn-lg">บันทึกข้อมูล</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>


    <script>
        $(document).ready(function() {
            document.getElementById('submit').className = "btn btn-primary btn-lg disabled"
            $('.select2').select2();

            $("#contractor_id").select2({
                ajax: {
                    url: "{{route('getContractor')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'กรุณาเลือก',
                minimumInputLength: 1,
                language: {
                    inputTooShort: function () {
                        return 'พิมพ์อย่างน้อย 1 ตัวอักษร';
                    }
                },
                templateResult: formatContractor,
                templateSelection: formatContractorSelection
            });

            $("#subcontractor_id").select2({
                ajax: {
                    url: "{{route('getContractor')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'กรุณาเลือก',
                minimumInputLength: 1,
                language: {
                    inputTooShort: function () {
                        return 'พิมพ์อย่างน้อย 1 ตัวอักษร';
                    }
                },
                templateResult: formatContractor,
                templateSelection: formatContractorSelection
            });

            $("#assistant_subcontractor_id").select2({
                ajax: {
                    url: "{{route('getContractor')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'กรุณาเลือก',
                minimumInputLength: 1,
                language: {
                    inputTooShort: function () {
                        return 'พิมพ์อย่างน้อย 1 ตัวอักษร';
                    }
                },
                templateResult: formatContractor,
                templateSelection: formatContractorSelection
            });

            function formatContractor(repo) {

                if (repo.loading) {
                    return repo.text;
                }

                var $container = $(
                    "<div class='select2-result-repository clearfix'>" +
                    "<div class='select2-result-repository__avatar'></div>" +
                    "<div class='select2-result-repository__meta'>" +
                    "<div class='select2-result-repository__title'></div>" +
                    "</div>" +
                    "</div>"
                );

                $container.find(".select2-result-repository__title").text(repo.first_name + " " + repo.last_name + "(" + repo.enterprise_name + ")");


                return $container;
            }

            function formatContractorSelection(repo) {
                // console.log(repo.prefix);
                if (repo.prefix != null) {
                    return repo.prefix + repo.first_name + " " + repo.last_name + '(' + repo.enterprise_name + ')';
                }

                if (repo.prefix != 'undefined') {
                    return repo.text
                }
            }

            $("#leader_id").select2({
                ajax: {
                    url: "{{route('getLeader')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'กรุณาเลือก',
                minimumInputLength: 1,
                language: {
                    inputTooShort: function () {
                        return 'พิมพ์อย่างน้อย 1 ตัวอักษร';
                    }
                },
                templateResult: formatLeader,
                templateSelection: formatLeaderSelection
            });

            function formatLeader(repo) {
                if (repo.loading) {
                    return repo.text;
                }

                var $container = $(
                    "<div class='select2-result-repository clearfix'>" +
                    "<div class='select2-result-repository__avatar'></div>" +
                    "<div class='select2-result-repository__meta'>" +
                    "<div class='select2-result-repository__title'></div>" +
                    "</div>" +
                    "</div>"
                );

                $container.find(".select2-result-repository__title").text(repo.first_name + " " + repo.last_name);


                return $container;
            }

            function formatLeaderSelection(repo) {

                if (repo.prefix != null) {
                    return repo.prefix + repo.first_name + " " + repo.last_name;
                }

                if (repo.prefix == null) {
                    return repo.text;
                }

            }


            $('#working_date').datetimepicker({
                format: 'DD-MM-YYYY'
            });

            $("#register_form").on("submit", function () {
                let subcontractor_id = $('#subcontractor_id').val();
                let task_type = document.getElementById('task_type').value;
                if((task_type == 1 || task_type == 2 || ask_type == 3 || task_type == 4 || task_type == 5) && subcontractor_id ==''){
                    document.getElementById('subcontractor_id').required  = true;
                }else{
                    document.getElementById('subcontractor_id').required  = false;
                }
                let electric_check2 = document.getElementById('electric_check2').checked;
                let electric_check = 0;
                if(electric_check2){electric_check = document.getElementById('electric_check2').value}
                if(electric_check == 2) {
                    let equipment1 = document.getElementById('equipment1');
                    let equipment2 = document.getElementById('equipment2');
                    let equipment3 = document.getElementById('equipment3');
                    let equipment4 = document.getElementById('equipment4');
                    let equipment5 = document.getElementById('equipment5');
                    let equipment6 = document.getElementById('equipment6');
                    let equipment7 = document.getElementById('equipment7');
                    let equipment8 = document.getElementById('equipment8');
                    let equipment9 = document.getElementById('equipment9');
                    let equipment10 = document.getElementById('equipment10');
                    let permission1 = document.getElementById('permission1');
                    let permission2 = document.getElementById('permission2');
                    let permission3 = document.getElementById('permission3');
                    let permission4 = document.getElementById('permission4');
                    let permission5 = document.getElementById('permission5');
                    let permission6 = document.getElementById('permission6');
                    let permission7 = document.getElementById('permission7');
                    let permission8 = document.getElementById('permission8');
                    let permission9 = document.getElementById('permission9');
                    let permission10 = document.getElementById('permission10');
                    if((equipment1.required && equipment1.value == "") && (permission1.required && permission1.value == "")){
                        Swal.hideLoading();
                    }
                    else if((equipment1.required && equipment1.value != "") && (permission1.required && permission1.value == "")){
                        Swal.hideLoading();
                    }
                    else if((equipment1.required && equipment1.value == "") && (permission1.required && permission1.value != "")){
                        Swal.hideLoading();
                    }
                   else if((equipment2.value != "") && (permission2.required && permission2.value == "")){
                        Swal.hideLoading();
                    }
                    else if((equipment3.value != "") && (permission3.required && permission3.value == "")){
                        Swal.hideLoading();
                    }
                    else if((equipment4.value != "") && (permission4.required && permission4.value == "")){
                        Swal.hideLoading();
                    }
                    else if((equipment5.value != "") && (permission5.required && permission5.value == "")){
                        Swal.hideLoading();
                    }
                    else if((equipment6.value != "") && (permission6.required && permission6.value == "")){
                        Swal.hideLoading();
                    }
                    else if((equipment7.value != "") && (permission7.required && permission7.value == "")){
                        Swal.hideLoading();
                    }
                    else if((equipment8.value != "") && (permission8.required && permission8.value == "")){
                        Swal.hideLoading();
                    }
                    else if((equipment9.value != "") && (permission9.required && permission9.value == "")){
                        Swal.hideLoading();
                    }
                    else if((equipment10.value != "") && (permission10.required && permission10.value == "")){
                        Swal.hideLoading();
                    }
                    else if((equipment10.required && equipment10.value == "") && (permission10.required && permission10.value == "")){
                        Swal.hideLoading();
                    }else{
                        Swal.fire({
                            title: 'กรุณารอสักครู่',
                            onOpen: () => {
                                swal.showLoading();
                            }
                        })
                        document.getElementById('submit').className = "btn btn-primary btn-lg disabled"
                    }

                }

            });//submit


            $("#register_form").change(function () {
                let title = document.getElementById('title').value;
                let working_date = document.getElementById('working_date').value;
                let contractor_id = document.getElementById('contractor_id').value;
                let subcontractor_id = $('#subcontractor_id').val();
                let leader_id = document.getElementById('leader_id').value;
                let task_type = document.getElementById('task_type').value;
                if((task_type == 1 || task_type == 2 || task_type == 3 || task_type == 4 || task_type == 5) && subcontractor_id ==''){
                    document.getElementById('subcontractor_id').required  = true;
                }else{
                    document.getElementById('subcontractor_id').required  = false;
                }
                let electric_check1 = document.getElementById('electric_check1').checked;
                let electric_check2 = document.getElementById('electric_check2').checked;
                let electric_check = 0;
                if(electric_check1){ electric_check = document.getElementById('electric_check1').value}
                if(electric_check2){ electric_check = document.getElementById('electric_check2').value}
                if(electric_check == 1){
                    let i = 1;
                    while (i <= 10) {
                        document.getElementById(`equipment${i}`).disabled  = true;
                        document.getElementById(`amount${i}`).disabled  = true;
                        document.getElementById(`unit${i}`).disabled = true;
                        document.getElementById(`permission${i}`).disabled  = true;

                        document.getElementById(`equipment${i}`).required  = false;
                        document.getElementById(`permission${i}`).required  = false;

                        document.getElementById(`equipment${i}`).value  = "";
                        document.getElementById(`amount${i}`).value  = "";
                        document.getElementById(`unit${i}`).value = "";
                        document.getElementById(`permission${i}`).value  = "";
                        i++
                    }


                }
                if(electric_check == 2){
                    let i = 1;
                    while (i <= 10) {
                        document.getElementById(`equipment${i}`).disabled  = false;
                        document.getElementById(`amount${i}`).disabled  = false;
                        document.getElementById(`unit${i}`).disabled = false;
                        document.getElementById(`permission${i}`).disabled  = false;
                        document.getElementById(`equipment${i}`).value ? document.getElementById(`permission${i}`).required = true : document.getElementById(`permission${i}`).required = false;
                        if(document.getElementById(`equipment${i}`).value != "" && document.getElementById(`permission${i}`) == ""){
                            Swal.hideLoading();
                        }
                        i++
                    }
                    document.getElementById('equipment1').required  = true;
                    document.getElementById('permission1').required  = true;

                }

                if (title == "" || working_date == "" || contractor_id == "" || leader_id == "" || electric_check == 0
                    || task_type == "" || (task_type == 1 && subcontractor_id =='') || (task_type == 2 && subcontractor_id =='')
                    || (task_type == 3 && subcontractor_id =='') || (task_type == 4 && subcontractor_id =='') || (task_type == 5 && subcontractor_id =='')
                   ) {
                    Swal.hideLoading();
                    document.getElementById('submit').className = "btn btn-primary btn-lg disabled"
                } else {
                    document.getElementById('submit').className = "btn btn-primary btn-lg"
                }
            });//submit

        });

        preventDot =(e) =>
        {
            var key = e.charCode ? e.charCode : e.keyCode;
            if (key == 46)
            {
                return false;
            }
        }


    </script>
@stop
