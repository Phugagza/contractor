@extends('layouts.paper')
@section('title','ตรวจสอบการอบรม Safety Training')
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item" aria-current="page">หน้าหลัก</li>
                <li class="breadcrumb-item active" aria-current="page">ตรวจสอบการอบรม</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form method="POST" action="" id="register_form" class="needs-validation" novalidate>
                    @csrf
                    <div class="card mt-5">
                        <div class="card-header ">
                            <h5 class="m-md-2">
                                ค้นหา
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
{{--                                        <select  class="form-control select2"  name="contractor_id" required>--}}
{{--                                            <option value="">กรุณาเลือกชื่อท่าน</option>--}}
{{--                                            @foreach($contractors as $contractor)--}}
{{--                                                <option value="{{$contractor -> id}}">{{$contractor -> prefix.$contractor -> first_name." ".$contractor -> last_name." (".$contractor -> enterprise_name.")"}}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
                                        <select  class="form-control" id="contractor_id" name="contractor_id" required></select>
                                        <div class="valid-feedback">
                                            ผ่าน
                                        </div>
                                        <div class="invalid-feedback">
                                            โปรดเลือกผู้รับเหมา
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block">ค้นหา</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card mt-5">
                    <div class="card-header ">
                        <h5 class="m-md-2">
                            ผลการค้นหา
                        </h5>
                    </div>
                    <div class="card-body" id="Task" style="display: inline;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="20%">การอบรม</th>
                                                <th class="text-center" width="20%">ชื่อ - นามสกุล</th>
                                                <th class="text-center" width="10%">บริษัท</th>
{{--                                                <th class="text-center" width="20%">ผู้ฝึกอบรม</th>--}}
                                                <th class="text-center" width="10%">วันอบรม</th>
                                                <th class="text-center" width="10%">วันหมดอายุ</th>
                                                <th class="text-center" width="10%">เวลาที่เหลือ</th>
                                                <th class="text-center" width="5%">Permission</th>
                                            </tr>
                                            </thead>
                                                <tbody>
                                                @if($safety_training != null)
                                                    <tr>
                                                        <td class="text-center">{{config('constants.training.SafetyTraining')}}</td>
                                                        <td class="text-center">
                                                            {{$safety_training -> prefix.$safety_training -> first_name." ".$safety_training -> last_name}}
                                                        </td>
                                                        <td class="text-center">{{$safety_training -> enterprise_name ? $safety_training -> enterprise_name : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> safety_training_date ? ConvertThaiDate($safety_training -> safety_training_date) : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> safety_expired_date ? ConvertThaiDate($safety_training -> safety_expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($safety_training -> count_safety_date >= 0)
                                                                {{$safety_training -> count_safety_date}}
                                                            @elseif($safety_training -> count_safety_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                           {!! $safety_training -> permission_with_style !!}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">{{config('constants.training.IndoorElectricity')}}</td>
                                                        <td class="text-center">
                                                            {{$safety_training -> prefix.$safety_training -> first_name." ".$safety_training -> last_name}}
                                                        </td>
                                                        <td class="text-center">{{$safety_training -> enterprise_name ? $safety_training -> enterprise_name : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> indoor_electricity_training_date ? ConvertThaiDate($safety_training -> indoor_electricity_training_date) : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> indoor_electricity_expired_date ? ConvertThaiDate($safety_training -> indoor_electricity_expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($safety_training -> count_indoor_electricity_date >= 0)
                                                                {{$safety_training -> count_indoor_electricity_date}}
                                                            @elseif($safety_training -> count_indoor_electricity_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($safety_training -> indoor_electricity_permission == "Y")
                                                                <h6>
                                                                    <span class="badge badge-success">Y</span>
                                                                </h6>
                                                            @elseif($safety_training -> indoor_electricity_permission == "N")
                                                                <h6>
                                                                    <span class="badge badge-danger">N</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">{{config('constants.training.IndoorAirConditioner')}}</td>
                                                        <td class="text-center">
                                                            {{$safety_training -> prefix.$safety_training -> first_name." ".$safety_training -> last_name}}
                                                        </td>
                                                        <td class="text-center">{{$safety_training -> enterprise_name ? $safety_training -> enterprise_name : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> indoor_air_conditioner_training_date ? ConvertThaiDate($safety_training -> indoor_air_conditioner_training_date) : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> indoor_air_conditioner_expired_date ? ConvertThaiDate($safety_training -> indoor_air_conditioner_expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($safety_training -> count_indoor_air_conditioner_date >= 0)
                                                                {{$safety_training -> count_indoor_air_conditioner_date}}
                                                            @elseif($safety_training -> count_indoor_air_conditioner_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($safety_training -> indoor_air_conditioner_permission == "Y")
                                                                <h6>
                                                                    <span class="badge badge-success">Y</span>
                                                                </h6>
                                                            @elseif($safety_training -> indoor_air_conditioner_permission == "N")
                                                                <h6>
                                                                    <span class="badge badge-danger">N</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">{{config('constants.training.MetalArc')}}</td>
                                                        <td class="text-center">
                                                            {{$safety_training -> prefix.$safety_training -> first_name." ".$safety_training -> last_name}}
                                                        </td>
                                                        <td class="text-center">{{$safety_training -> enterprise_name ? $safety_training -> enterprise_name : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> metal_arc_training_date ? ConvertThaiDate($safety_training -> metal_arc_training_date) : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> metal_arc_expired_date ? ConvertThaiDate($safety_training -> metal_arc_expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($safety_training -> count_metal_arc_date >= 0)
                                                                {{$safety_training -> count_metal_arc_date}}
                                                            @elseif($safety_training -> count_metal_arc_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($safety_training -> metal_arc_permission == "Y")
                                                                <h6>
                                                                    <span class="badge badge-success">Y</span>
                                                                </h6>
                                                            @elseif($safety_training -> metal_arc_permission == "N")
                                                                <h6>
                                                                    <span class="badge badge-danger">N</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">{{config('constants.training.TigWelding')}}</td>
                                                        <td class="text-center">
                                                            {{$safety_training -> prefix.$safety_training -> first_name." ".$safety_training -> last_name}}
                                                        </td>
                                                        <td class="text-center">{{$safety_training -> enterprise_name ? $safety_training -> enterprise_name : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> tig_welding_training_date ? ConvertThaiDate($safety_training -> tig_welding_training_date) : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> tig_welding_expired_date ? ConvertThaiDate($safety_training -> tig_welding_expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($safety_training -> count_tig_welding_date >= 0)
                                                                {{$safety_training -> count_tig_welding_date}}
                                                            @elseif($safety_training -> count_tig_welding_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($safety_training -> tig_welding_permission == "Y")
                                                                <h6>
                                                                    <span class="badge badge-success">Y</span>
                                                                </h6>
                                                            @elseif($safety_training -> tig_welding_permission == "N")
                                                                <h6>
                                                                    <span class="badge badge-danger">N</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">{{config('constants.training.AlloyWelding')}}</td>
                                                        <td class="text-center">
                                                            {{$safety_training -> prefix.$safety_training -> first_name." ".$safety_training -> last_name}}
                                                        </td>
                                                        <td class="text-center">{{$safety_training -> enterprise_name ? $safety_training -> enterprise_name : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> alloy_welding_training_date ? ConvertThaiDate($safety_training -> alloy_welding_training_date) : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> alloy_welding_expired_date ? ConvertThaiDate($safety_training -> alloy_welding_expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($safety_training -> count_alloy_welding_date >= 0)
                                                                {{$safety_training -> count_alloy_welding_date}}
                                                            @elseif($safety_training -> count_alloy_welding_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($safety_training -> alloy_welding_permission == "Y")
                                                                <h6>
                                                                    <span class="badge badge-success">Y</span>
                                                                </h6>
                                                            @elseif($safety_training -> alloy_welding_permission == "N")
                                                                <h6>
                                                                    <span class="badge badge-danger">N</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">{{config('constants.training.Scaffolding')}}</td>
                                                        <td class="text-center">
                                                            {{$safety_training -> prefix.$safety_training -> first_name." ".$safety_training -> last_name}}
                                                        </td>
                                                        <td class="text-center">{{$safety_training -> enterprise_name ? $safety_training -> enterprise_name : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> scaffolding_training_date ? ConvertThaiDate($safety_training -> scaffolding_training_date) : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> scaffolding_expired_date ? ConvertThaiDate($safety_training -> scaffolding_expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($safety_training -> count_scaffolding_date >= 0)
                                                                {{$safety_training -> count_scaffolding_date}}
                                                            @elseif($safety_training -> count_scaffolding_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($safety_training -> scaffolding_permission == "Y")
                                                                <h6>
                                                                    <span class="badge badge-success">Y</span>
                                                                </h6>
                                                            @elseif($safety_training -> scaffolding_permission == "N")
                                                                <h6>
                                                                    <span class="badge badge-danger">N</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">{{config('constants.training.WorkerConfine')}}</td>
                                                        <td class="text-center">
                                                            {{$safety_training -> prefix.$safety_training -> first_name." ".$safety_training -> last_name}}
                                                        </td>
                                                        <td class="text-center">{{$safety_training -> enterprise_name ? $safety_training -> enterprise_name : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> worker_confine_training_date ? ConvertThaiDate($safety_training -> worker_confine_training_date) : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> worker_confine_expired_date ? ConvertThaiDate($safety_training -> worker_confine_expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($safety_training -> count_worker_confine_date >= 0)
                                                                {{$safety_training -> count_worker_confine_date}}
                                                            @elseif($safety_training -> count_worker_confine_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($safety_training -> worker_confine_permission == "Y")
                                                                <h6>
                                                                    <span class="badge badge-success">Y</span>
                                                                </h6>
                                                            @elseif($safety_training -> worker_confine_permission == "N")
                                                                <h6>
                                                                    <span class="badge badge-danger">N</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">{{config('constants.training.AssistantConfine')}}</td>
                                                        <td class="text-center">
                                                            {{$safety_training -> prefix.$safety_training -> first_name." ".$safety_training -> last_name}}
                                                        </td>
                                                        <td class="text-center">{{$safety_training -> enterprise_name ? $safety_training -> enterprise_name : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> assistant_confine_training_date ? ConvertThaiDate($safety_training -> assistant_confine_training_date) : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> assistant_confine_expired_date ? ConvertThaiDate($safety_training -> assistant_confine_expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($safety_training -> count_assistant_confine_date >= 0)
                                                                {{$safety_training -> count_assistant_confine_date}}
                                                            @elseif($safety_training -> count_assistant_confine_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($safety_training -> assistant_confine_permission == "Y")
                                                                <h6>
                                                                    <span class="badge badge-success">Y</span>
                                                                </h6>
                                                            @elseif($safety_training -> assistant_confine_permission == "N")
                                                                <h6>
                                                                    <span class="badge badge-danger">N</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">{{config('constants.training.ControllerConfine')}}</td>
                                                        <td class="text-center">
                                                            {{$safety_training -> prefix.$safety_training -> first_name." ".$safety_training -> last_name}}
                                                        </td>
                                                        <td class="text-center">{{$safety_training -> enterprise_name ? $safety_training -> enterprise_name : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> controller_confine_training_date ? ConvertThaiDate($safety_training -> controller_confine_training_date) : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> controller_confine_expired_date ? ConvertThaiDate($safety_training -> controller_confine_expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($safety_training -> count_controller_confine_date >= 0)
                                                                {{$safety_training -> count_controller_confine_date}}
                                                            @elseif($safety_training -> count_controller_confine_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($safety_training -> controller_confine_permission == "Y")
                                                                <h6>
                                                                    <span class="badge badge-success">Y</span>
                                                                </h6>
                                                            @elseif($safety_training -> controller_confine_permission == "N")
                                                                <h6>
                                                                    <span class="badge badge-danger">N</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">{{config('constants.training.WarrantorConfine')}}</td>
                                                        <td class="text-center">
                                                            {{$safety_training -> prefix.$safety_training -> first_name." ".$safety_training -> last_name}}
                                                        </td>
                                                        <td class="text-center">{{$safety_training -> enterprise_name ? $safety_training -> enterprise_name : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> warrantor_confine_training_date ? ConvertThaiDate($safety_training -> warrantor_confine_training_date) : '-'}}</td>
                                                        <td class="text-center">{{$safety_training -> warrantor_confine_expired_date ? ConvertThaiDate($safety_training -> warrantor_confine_expired_date) : '-'}}</td>
                                                        <td class="text-center">
                                                            @if($safety_training -> count_warrantor_confine_date >= 0)
                                                                {{$safety_training -> count_warrantor_confine_date}}
                                                            @elseif($safety_training -> count_warrantor_confine_date < 0)
                                                                <h6>
                                                                    <span class="badge badge-danger">หมดอายุ</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            @if($safety_training -> warrantor_confine_permission == "Y")
                                                                <h6>
                                                                    <span class="badge badge-success">Y</span>
                                                                </h6>
                                                            @elseif($safety_training -> warrantor_confine_permission == "N")
                                                                <h6>
                                                                    <span class="badge badge-danger">N</span>
                                                                </h6>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                @elseif($safety_training == null)
                                                    <tbody>
                                                    <tr>
                                                        <td colspan="7" class="text-center">ไม่พบข้อมูล</td>
                                                    </tr>
                                                    </tbody>
                                                @endif
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#contractor_id").select2({
                ajax: {
                    url: "{{route('getContractor')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'กรุณาเลือก',
                minimumInputLength: 1,
                language: {
                    inputTooShort: function () {
                        return 'พิมพ์อย่างน้อย 1 ตัวอักษร';
                    }
                },
                templateResult: formatContractor,
                templateSelection: formatContractorSelection
            });


            function formatContractor(repo) {

                if (repo.loading) {
                    return repo.text;
                }

                var $container = $(
                    "<div class='select2-result-repository clearfix'>" +
                    "<div class='select2-result-repository__avatar'></div>" +
                    "<div class='select2-result-repository__meta'>" +
                    "<div class='select2-result-repository__title'></div>" +
                    "</div>" +
                    "</div>"
                );

                $container.find(".select2-result-repository__title").text(repo.first_name + " " + repo.last_name + "(" + repo.enterprise_name + ")");


                return $container;
            }

            function formatContractorSelection(repo) {
                // console.log(repo.prefix);
                if (repo.prefix != null) {
                    return repo.prefix + repo.first_name + " " + repo.last_name + '(' + repo.enterprise_name + ')';
                }

                if (repo.prefix != 'undefined') {
                    return repo.text
                }
            }
        });

    </script>
@stop
