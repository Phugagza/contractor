<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaderCheckTaskDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leader_check_task_details', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('task_detail_id');
            $table->smallInteger('clean')->nullable();
            $table->smallInteger('clear')->nullable();
            $table->smallInteger('safe')->nullable();
            $table->smallInteger('another')->nullable();
            $table->string('another_word')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leader_check_task_details');
    }
}
