<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_details', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('task_id');
            $table->date('date');
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->string('description',255)->nullable();
            $table->smallInteger('status')->comment('1 = wait for approve ,2 = approved , 3 = start work,
                                                                      4 = check_task 1, 5 = check_task 2, 6 = check_work_done,
                                                                       7 = work done, 8 = work not done, 9 = work cancel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_details');
    }
}
