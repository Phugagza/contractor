<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingDetailsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
          CREATE VIEW contractor.training_details_view AS 
          SELECT
                training_details.contractor_id,
                training_details.trainer_id AS trainer_id,
                training_details.expired_date AS expired_date,
                training_details.training_date AS training_date,
                training_details.permission AS permission,
                contractors.citizen_id AS contractor_citizen_id,
                contractors.prefix AS contractor_prefix,
                contractors.first_name AS contractor_first_name,
                contractors.last_name AS contractor_last_name,
                enterprises.`name` AS enterprise_name,
                admins.first_name AS trainer_first_name,
                admins.last_name AS trainer_last_name,
                training_details.id AS training_detail_id,
                training_details.training_id AS training_id,
                (DATEDIFF(training_details.expired_date,training_details.training_date) - DATEDIFF(DATE(now()),training_details.training_date)) AS count_date,
                trainings.`name` as training_name
            FROM
                training_details
            INNER JOIN trainings ON trainings.id = training_details.training_id
            INNER JOIN contractors ON contractors.id = training_details.contractor_id
            INNER JOIN admins ON admins.id = training_details.trainer_id
            LEFT JOIN enterprises ON enterprises.id = contractors.enterprise_id 
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS training_details_view");
    }
}
