<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractorCheckTaskDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractor_check_task_details', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('time')->nullable();
            $table->integer('task_detail_id')->nullable();
            $table->integer('equipment_personal')->nullable();
            $table->integer('safety_glasses')->nullable();
            $table->integer('safety_belt')->nullable();
            $table->integer('scba')->nullable();
            $table->integer('oximeter')->nullable();
            $table->integer('fan')->nullable();
            $table->integer('acid_mask')->nullable();
            $table->integer('sling')->nullable();
            $table->integer('electric1')->nullable();
            $table->integer('electric1_1')->nullable();
            $table->integer('electric1_2')->nullable();
            $table->integer('cut_gas1')->nullable();
            $table->integer('cut_gas1_1')->nullable();
            $table->integer('cut_gas1_2')->nullable();
            $table->integer('cut_gas1_3')->nullable();
            $table->integer('cut_gas1_4')->nullable();
            $table->integer('cut_hand1')->nullable();
            $table->integer('cut_hand1_1')->nullable();
            $table->integer('cut_hand1_2')->nullable();
            $table->integer('cut_hand1_3')->nullable();
            $table->integer('fire_ready')->nullable();
            $table->integer('fire_amount')->nullable();
            $table->integer('fire_resis')->nullable();
            $table->integer('safety')->nullable();
            $table->integer('contractor')->nullable();
            $table->integer('leader')->nullable();
            $table->time('check_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractor_check_task_details');
    }
}
