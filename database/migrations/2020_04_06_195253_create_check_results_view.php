<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckResultsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
          CREATE VIEW contractor.check_results_view AS
         SELECT
                tasks.leader_id AS leader_id,
                tasks.contractor_id AS contractor_id,
                tasks.subcontractor_id AS subcontractor_id,
                tasks.title AS task_title,
                tasks.description AS task_description,
                leaders.prefix AS leader_prefix,
                leaders.first_name AS leader_first_name,
                leaders.last_name AS leader_last_name,
                contractors.prefix AS contractor_prefix,
                contractors.first_name AS contractor_first_name,
                contractors.last_name AS contractor_last_name,
                enterprises.`name` AS enterprise_name,
                tasks.id AS task_id,
                task_details.id AS task_detail_id,
                leader_check_tasks.type1 AS type1,
                leader_check_tasks.type2 AS type2,
                leader_check_tasks.type3 AS type3,
                leader_check_tasks.type4 AS type4,
                leader_check_tasks.type5 AS type5,
                leader_check_tasks.ppe AS ppe,
                leader_check_tasks.crane1 AS crane1,
                leader_check_tasks.crane2 AS crane2,
                leader_check_tasks.crane2_1 AS crane2_1,
                leader_check_tasks.crane2_2 AS crane2_2,
                leader_check_tasks.workshop1 AS workshop1,
                leader_check_tasks.workshop2 AS workshop2,
                leader_check_tasks.workshop2_1 AS workshop2_1,
                leader_check_tasks.workshop2_2 AS workshop2_2,
                leader_check_tasks.electric_check1 AS electric_check1,
                leader_check_tasks.electric_check2 AS electric_check2,
                leader_check_tasks.equipment1 AS equipment1,
                leader_check_tasks.equipment2 AS equipment2,
                leader_check_tasks.equipment3 AS equipment3,
                leader_check_tasks.equipment4 AS equipment4,
                leader_check_tasks.equipment5 AS equipment5,
                leader_check_tasks.equipment6 AS equipment6,
                leader_check_tasks.equipment7 AS equipment7,
                leader_check_tasks.equipment8 AS equipment8,
                leader_check_tasks.equipment9 AS equipment9,
                leader_check_tasks.equipment10 AS equipment10,
                leader_check_tasks.amount1 AS amount1,
                leader_check_tasks.amount2 AS amount2,
                leader_check_tasks.amount3 AS amount3,
                leader_check_tasks.amount4 AS amount4,
                leader_check_tasks.amount5 AS amount5,
                leader_check_tasks.amount6 AS amount6,
                leader_check_tasks.amount7 AS amount7,
                leader_check_tasks.amount8 AS amount8,
                leader_check_tasks.amount9 AS amount9,
                leader_check_tasks.amount10 AS amount10,
                leader_check_tasks.unit1 AS unit1,
                leader_check_tasks.unit2 AS unit2,
                leader_check_tasks.unit3 AS unit3,
                leader_check_tasks.unit4 AS unit4,
                leader_check_tasks.unit5 AS unit5,
                leader_check_tasks.unit6 AS unit6,
                leader_check_tasks.unit7 AS unit7,
                leader_check_tasks.unit8 AS unit8,
                leader_check_tasks.unit9 AS unit9,
                leader_check_tasks.unit10 AS unit10,
                leader_check_tasks.permission1 AS permission1,
                leader_check_tasks.permission2 AS permission2,
                leader_check_tasks.permission3 AS permission3,
                leader_check_tasks.permission4 AS permission4,
                leader_check_tasks.permission5 AS permission5,
                leader_check_tasks.permission6 AS permission6,
                leader_check_tasks.permission7 AS permission7,
                leader_check_tasks.permission8 AS permission8,
                leader_check_tasks.permission9 AS permission9,
                leader_check_tasks.permission10 AS permission10,
                leader_check_tasks.touch AS touch,
                contractor_check_task_details.time AS time,
                contractor_check_task_details.equipment_personal AS equipment_personal,
                contractor_check_task_details.safety_glasses AS safety_glasses,
                contractor_check_task_details.safety_belt AS safety_belt,
                contractor_check_task_details.scba AS scba,
                contractor_check_task_details.oximeter AS oximeter,
                contractor_check_task_details.fan AS fan,
                contractor_check_task_details.acid_mask AS acid_mask,
                contractor_check_task_details.sling AS sling,
                contractor_check_task_details.electric1 AS electric1,
                contractor_check_task_details.electric1_1 AS electric1_1,
                contractor_check_task_details.electric1_2 AS electric1_2,
                contractor_check_task_details.cut_gas1 AS cut_gas1,
                contractor_check_task_details.cut_gas1_1 AS cut_gas1_1,
                contractor_check_task_details.cut_gas1_2 AS cut_gas1_2,
                contractor_check_task_details.cut_gas1_3 AS cut_gas1_3,
                contractor_check_task_details.cut_gas1_4 AS cut_gas1_4,
                contractor_check_task_details.cut_hand1 AS cut_hand1,
                contractor_check_task_details.cut_hand1_1 AS cut_hand1_1,
                contractor_check_task_details.cut_hand1_2 AS cut_hand1_2,
                contractor_check_task_details.cut_hand1_3 AS cut_hand1_3,
                contractor_check_task_details.fire_ready AS fire_ready,
                contractor_check_task_details.fire_amount AS fire_amount,
                contractor_check_task_details.fire_resis AS fire_resis,
                contractor_check_task_details.safety AS safety,
                contractor_check_task_details.contractor AS contractor,
                contractor_check_task_details.check_time AS check_time,
                contractor_check_task_details.leader AS leader,
                leader_check_task_details.clean AS clean,
                leader_check_task_details.clear AS clear,
                leader_check_task_details.safe AS safe,
                leader_check_task_details.another AS another,
                leader_check_task_details.another_word AS another_word,
                contractors.tel AS contractor_tel,
                task_details.start_time AS task_detail_start,
                task_details.end_time AS task_detail_end,
                task_details.date AS task_detail_date,
                task_details.`status` AS `status`,
                tasks.location as task_location
        FROM
                (((((((tasks
        JOIN contractors ON ((contractors.id = tasks.contractor_id)))
        JOIN leaders ON ((leaders.id = tasks.leader_id)))
        LEFT JOIN enterprises ON ((contractors.enterprise_id = enterprises.id)))
        JOIN task_details ON ((tasks.id = task_details.task_id)))
        JOIN leader_check_tasks ON ((tasks.id = leader_check_tasks.task_id)))
        JOIN contractor_check_task_details ON ((task_details.id = contractor_check_task_details.task_detail_id)))
        JOIN leader_check_task_details ON ((task_details.id = leader_check_task_details.task_detail_id)))
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS check_results_view");
    }
}
