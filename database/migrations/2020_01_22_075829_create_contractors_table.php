<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractors', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('enterprise_id')->nullable();
            $table->integer('trainer_id')->nullable();
            $table->string('citizen_id',13);
            $table->string('prefix',50);
            $table->string('first_name',50);
            $table->string('last_name',50);
            $table->string('email',50);
            $table->string('password',255);
            $table->string('tel',50);
            $table->smallInteger('status')->comment('0 = inactive , 1 = active');
            $table->date('safety_training_date')->nullable();
            $table->date('safety_expired_date')->nullable();
            $table->string('permission',2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractors');
    }
}
