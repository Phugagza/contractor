<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
          CREATE VIEW contractor.tasks_view AS
          SELECT
                tasks.leader_id AS leader_id,
                tasks.register_date AS task_register_date,
                tasks.title AS task_title,
                tasks.`status` AS task_status,
                contractors.prefix AS contractor_prefix,
                contractors.first_name AS contractor_first_name,
                contractors.last_name AS contractor_last_name,
                tasks.id AS task_id,
                contractors.email AS contractor_email,
                enterprises.`name` AS enterprise_name,
                Max(task_details.date) AS register_task_detail_date,
                leaders.prefix AS leader_perfix,
                leaders.first_name AS leader_first_name,
                leaders.last_name AS leader_last_name,
                task_details.`status` AS task_detail_status,
                leader_check_tasks.type1 AS task_type,
                tasks.location AS task_location,
                task_details.id AS task_detail_id,
                tasks.working_date as task_working_date
           FROM
                ((((tasks
           JOIN leaders ON ((leaders.id = tasks.leader_id)))
           JOIN contractors ON ((contractors.id = tasks.contractor_id)))
           LEFT JOIN enterprises ON ((enterprises.id = contractors.enterprise_id)))
           LEFT JOIN task_details ON ((tasks.id = task_details.task_id)))
           INNER JOIN leader_check_tasks ON leader_check_tasks.task_id = tasks.id
           GROUP BY
                tasks.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS tasks_view");
    }
}
