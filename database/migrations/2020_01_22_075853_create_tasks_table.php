<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('leader_id');
            $table->integer('contractor_id');
            $table->string('subcontractor_id',255)->nullable();
            $table->string('title',255);
            $table->string('description',255)->nullable();
            $table->string('location',255)->nullable();
            $table->date('register_date');
            $table->date('working_date');
            $table->smallInteger('status')->comment(' 1 = allow , 9 = not allow');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
