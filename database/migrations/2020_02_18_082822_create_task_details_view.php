<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskDetailsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
          CREATE VIEW contractor.task_details_view AS
          SELECT
                contractors.prefix AS contractor_prefix,
                contractors.first_name AS contractor_firstname,
                contractors.last_name AS contractor_lastname,
                contractors.email AS contractor_email,
                tasks.leader_id AS leader_id,
                tasks.contractor_id AS contractor_id,
                tasks.title AS task_title,
                tasks.description AS description,
                task_details.`status` AS task_detail_status,
                task_details.date AS task_detail_date,
                task_details.id AS task_detail_id,
                leaders.first_name AS leader_firstname,
                leaders.last_name AS leader_lastname,
                enterprises.`name` AS `name`,
                task_details.task_id AS task_detail_task_id,
                leaders.id AS task_leader_id,
                task_details.start_time AS task_detail_start_time,
                task_details.end_time AS task_detail_end_time,
                leaders.line_token AS leader_line_token,
                concat(
                        floor((
                                HOUR (
                                timediff( `task_details`.`end_time`, `task_details`.`start_time` )) / 24
                            )),
                        ' วัน ',(
                            HOUR (
                            timediff( `task_details`.`end_time`, `task_details`.`start_time` )) % 24
                        ),
                        ' ชม. ',
                        MINUTE (
                        timediff(DATE_FORMAT(`task_details`.`end_time`, \"%H:%i\"), DATE_FORMAT(`task_details`.`start_time`, \"%H:%i\") )) % 60,
                        ' นาที'
                    ) AS task_detail_sum_day,
               tasks.location as task_location
        FROM
        ((((contractors
        JOIN tasks ON ((tasks.contractor_id = contractors.id)))
        JOIN task_details ON ((tasks.id = task_details.task_id)))
        LEFT JOIN enterprises ON (( (enterprises.id = contractors.enterprise_id))))
        JOIN leaders ON ((tasks.leader_id = leaders.id)))
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS task_details_view");
    }
}
