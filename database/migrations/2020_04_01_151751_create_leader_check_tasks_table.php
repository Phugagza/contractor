<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaderCheckTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leader_check_tasks', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('task_id');
            $table->smallInteger('type1')->nullable();
            $table->smallInteger('type2')->nullable();
            $table->smallInteger('type3')->nullable();
            $table->smallInteger('type4')->nullable();
            $table->smallInteger('type5')->nullable();
            $table->smallInteger('ppe')->nullable();
            $table->smallInteger('crane1')->nullable();
            $table->smallInteger('crane2')->nullable();
            $table->smallInteger('crane2_1')->nullable();
            $table->smallInteger('crane2_2')->nullable();
            $table->smallInteger('workshop1')->nullable();
            $table->smallInteger('workshop2')->nullable();
            $table->smallInteger('workshop2_1')->nullable();
            $table->smallInteger('workshop2_2')->nullable();
            $table->smallInteger('electric_check1')->nullable();
            $table->smallInteger('electric_check2')->nullable();
            $table->string('equipment1',255)->nullable();
            $table->string('equipment2',255)->nullable();
            $table->string('equipment3',255)->nullable();
            $table->string('equipment4',255)->nullable();
            $table->string('equipment5',255)->nullable();
            $table->string('equipment6',255)->nullable();
            $table->string('equipment7',255)->nullable();
            $table->string('equipment8',255)->nullable();
            $table->string('equipment9',255)->nullable();
            $table->string('equipment10',255)->nullable();
            $table->string('amount1',255)->nullable();
            $table->string('amount2',255)->nullable();
            $table->string('amount3',255)->nullable();
            $table->string('amount4',255)->nullable();
            $table->string('amount5',255)->nullable();
            $table->string('amount6',255)->nullable();
            $table->string('amount7',255)->nullable();
            $table->string('amount8',255)->nullable();
            $table->string('amount9',255)->nullable();
            $table->string('amount10',255)->nullable();
            $table->string('unit1',255)->nullable();
            $table->string('unit2',255)->nullable();
            $table->string('unit3',255)->nullable();
            $table->string('unit4',255)->nullable();
            $table->string('unit5',255)->nullable();
            $table->string('unit6',255)->nullable();
            $table->string('unit7',255)->nullable();
            $table->string('unit8',255)->nullable();
            $table->string('unit9',255)->nullable();
            $table->string('unit10',255)->nullable();
            $table->smallInteger('permission1')->nullable();
            $table->smallInteger('permission2')->nullable();
            $table->smallInteger('permission3')->nullable();
            $table->smallInteger('permission4')->nullable();
            $table->smallInteger('permission5')->nullable();
            $table->smallInteger('permission6')->nullable();
            $table->smallInteger('permission7')->nullable();
            $table->smallInteger('permission8')->nullable();
            $table->smallInteger('permission9')->nullable();
            $table->smallInteger('permission10')->nullable();
            $table->string('touch',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leader_check_tasks');
    }
}
