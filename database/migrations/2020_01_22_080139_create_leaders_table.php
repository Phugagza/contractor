<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaders', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('prefix',50);
            $table->string('first_name',50);
            $table->string('last_name',50);
            $table->string('password',255);
            $table->string('line_token',255)->nullable();
            $table->string('tel',50)->nullable();
            $table->string('email',50);
            $table->smallInteger('status')->comment('0 = inactive , 1 = active');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaders');
    }
}
