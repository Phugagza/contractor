<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskDetailsConfirmView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement("
          CREATE VIEW contractor.task_details_confirm_view AS 
            SELECT
                task_details.date AS task_scan_date,
                task_details.`status` AS task_status,
                tasks.title AS task_title,
                contractors.prefix AS contractor_prefix,
                contractors.first_name AS contractor_first_name,
                contractors.last_name AS contractor_last_name,
                tasks.leader_id AS task_leader_id,
                tasks.contractor_id AS task_contractor_id,
                task_details.id as taskDetail_id
            FROM
                contractors
            INNER JOIN tasks ON contractors.id = tasks.contractor_id
            INNER JOIN task_details ON tasks.id = task_details.task_id
            INNER JOIN leaders ON leaders.id = tasks.leader_id 
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS task_details_confirm_view");
    }
}
