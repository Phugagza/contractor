<?php

use Ixudra\Curl\Facades\Curl;

function ConvertThaiDate($date){
    $sub_date = explode('-',$date);
    $strMonthCut=array(
        "0"=>"",
        "01"=>"มกราคม",
        "02"=>"กุมภาพันธ์",
        "03"=>"มีนาคม",
        "04"=>"เมษายน",
        "05"=>"พฤษภาคม",
        "06"=>"มิถุนายน",
        "07"=>"กรกฎาคม",
        "08"=>"สิงหาคม",
        "09"=>"กันยายน",
        "10"=>"ตุลาคม",
        "11"=>"พฤศจิกายน",
        "12"=>"ธันวาคม"
    );
    $strYear = $sub_date[0]+543;
    $strDay= $sub_date[2];
    $strMonthThai=$strMonthCut[$sub_date[1]];
    return $strDay." ". $strMonthThai." ".$strYear;
}

function ConvertThaiDateLine($date){
    $strMonthCut=array(
        "0"=>"",
        "1"=>"มกราคม",
        "2"=>"กุมภาพันธ์",
        "3"=>"มีนาคม",
        "4"=>"เมษายน",
        "5"=>"พฤษภาคม",
        "6"=>"มิถุนายน",
        "7"=>"กรกฎาคม",
        "8"=>"สิงหาคม",
        "9"=>"กันยายน",
        "10"=>"ตุลาคม",
        "11"=>"พฤศจิกายน",
        "12"=>"ธันวาคม"
    );
    $strYear = date("Y",strtotime($date))+543;
    $strMonth= date("n",strtotime($date));
    $strDay= date("j",strtotime($date));
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}

//function ConvertThaiMonth($date){
//    $sub_year = substr($date,0,4);
//    $sub_month = substr($date,4,2);
//    $strMonthCut=array(
//        "0"=>"",
//        "01"=>"มกราคม",
//        "02"=>"กุมภาพันธ์",
//        "03"=>"มีนาคม",
//        "04"=>"เมษายน",
//        "05"=>"พฤษภาคม",
//        "06"=>"มิถุนายน",
//        "07"=>"กรกฎาคม",
//        "08"=>"สิงหาคม",
//        "09"=>"กันยายน",
//        "10"=>"ตุลาคม",
//        "11"=>"พฤศจิกายน",
//        "12"=>"ธันวาคม"
//    );
//    $strYear = $sub_year+543;
//    $strMonthThai=$strMonthCut[$sub_month];
//    return "$strMonthThai $strYear";
//}

function ConvertThaiDateHour($date){
    $sub_date = explode('-',$date);
    $strMonthCut=array(
        "0"=>"",
        "1"=>"ม.ค",
        "2"=>"ก.พ",
        "3"=>"มี.ค",
        "4"=>"เม.ย",
        "5"=>"พ.ค",
        "6"=>"มิ.ย",
        "7"=>"ก.ค",
        "8"=>"ส.ค",
        "9"=>"ก.ย",
        "10"=>"ต.ค",
        "11"=>"พ.ย",
        "12"=>"ธ.ค"
    );
    $strYear = substr((date("Y",strtotime($date))+543),-2);
    $strMonth= date("n",strtotime($date));
    $strDay= date("j",strtotime($date));
    $strMonthThai=$strMonthCut[$strMonth];
    $strHour= date("H",strtotime($date));
    $strMinute= date("i",strtotime($date));
    return "$strDay $strMonthThai $strYear ($strHour.$strMinute น.)";
}

function lineNotify($data){

    $response = Curl::to('https://notify-api.line.me/api/notify')
        ->withHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Authorization' => "Bearer $data[0]"
        ))
        ->withData(array(
            'message' => $data[1],
        ))
        ->post();

    $getStatus = json_decode($response);
    return $getStatus -> status;
}



