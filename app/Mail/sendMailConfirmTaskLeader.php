<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\mailLists;

class sendMailConfirmTaskLeader extends Mailable
{
    use Queueable, SerializesModels;
    public $task;
    public $status;
    public $taskDetail_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($task,$status,$taskDetail_id)
    {
        $this->task = $task;
        $this->status = $status;
        $this->taskDetail_id = $taskDetail_id;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $getMails = mailLists::select('email_name')->get();
        $mails = collect($getMails)->pluck('email_name');
        $task = $this -> task;
        $status = $this -> status == 2 ? 'ผ่านการอนุมัติ' : 'ไม่ผ่านอนุมัติ' ;
        $taskDetail_id = $this -> taskDetail_id;
        $subject = "{$status} - {$this->task -> task_title}";

        if($this -> status == 2) {
            return $this->from('nssiamnoreply@gmail.com', 'NS-SUS WPO')
                ->subject($subject)
                ->view('emails.leader.confirmTask', compact('task', 'status', 'taskDetail_id'));
        }else{
            return $this->from('nssiamnoreply@gmail.com', 'NS-SUS WPO')
                ->subject($subject)
                ->view('emails.leader.confirmTask', compact('task', 'status', 'taskDetail_id'));
        }
    }
}
