<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendCertificate extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contractor,$title)
    {
        $this->contractor = $contractor;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $contractor=$this->contractor;
        $title=$this->title;
        $subject = "แจ้งเตือนวันหมดอายุ{$title}";
        return $this->from('nssiamnoreply@gmail.com', 'NS-SUS WPO')
                ->subject($subject)
                ->view('emails.contractor.sendCertificate',compact('contractor','title'));
    }
}
