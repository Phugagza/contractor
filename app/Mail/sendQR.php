<?php

namespace App\Mail;

use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendQR extends Mailable
{
    use Queueable, SerializesModels;
    public $taskDetail;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($taskDetail)
    {
        $this->taskDetail = $taskDetail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $taskDetail = $this -> taskDetail;
        $subject = "QRCode สำหรับสแกน - {$this->taskDetail -> task_title}";
        return $this->from('nssiamnoreply@gmail.com','NS-SUS WPO')
                    ->subject($subject)
                    ->view('emails.contractor.sendQR' ,compact('taskDetail'));
    }
}
