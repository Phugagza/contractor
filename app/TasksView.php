<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TasksView extends Model
{
    protected $table = 'tasks_view';
}
