<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($this->isHttpException($exception)) {
            switch ($exception->getStatusCode()) {

                // not authorized
                case '403':
                    return response()->view('errors.error', ['status' => '403','message' => 'ระบุตัวตนแล้วแต่ไม่มีสิทธิ์เข้าถึงส่วนนี้'], 403);
                    break;

                // not found
                case '404':
                    return response()->view('errors.error', ['status' => '404','message' => 'ไม่พบหน้าที่ท่านต้องการ'], 404);
                    break;

                // internal error
                case '500':
                    return response()->view('errors.error', ['status' => '500','message' => 'มีข้อผิดพลาดบางอย่างภายใน'], 500);
                    break;

                // internal error
                case '400':
                    return response()->view('errors.error', ['status' => '400','message' => 'มีข้อผิดพลาดบางอย่างภายใน'], 400);
                    break;

                default:
                    return $this->renderHttpException($exception);
                    break;
            }
        } else {

//            return response()->view('errors.error', ['status' => '400','message' => 'เกิดข้อผิดพลาด'], 400);
              return parent::render($request, $exception);
        }

    }
}
