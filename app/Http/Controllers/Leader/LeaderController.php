<?php

namespace App\Http\Controllers\Leader;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class LeaderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:leader');
    }


}
