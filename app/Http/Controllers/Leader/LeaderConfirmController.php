<?php

namespace App\Http\Controllers\Leader;

use App\Contractor;
use App\contractorCheckTaskDetail;
use App\Leader;
use App\leaderCheckTaskDetail;
use App\Mail\sendMailConfirmTask;
use App\Mail\sendMailConfirmTaskLeader;
use App\mailLists;
use App\Task;
use App\taskDetail;
use App\taskDetailsConfirmView;
use App\taskDetailsView;
use App\TasksView;
use Carbon\Carbon;
use DemeterChain\C;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\leaderCheckTask;

class LeaderConfirmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:leader');
    }

    public function confirmTask(Request $request){
        $taskDetail_id = $request -> taskDetail_id;
        $status = $request -> status;

        $taskDetail = taskDetail::where('id',$taskDetail_id)->first();
        $taskDetail -> status = $status;

        if($taskDetail -> save()){
            $Task = tasksView::where('task_id',$taskDetail -> task_id)->first();
            $leader = Leader::where('id',$Task -> leader_id)->first();
            $checkType = leaderCheckTask::where('task_id',$Task -> task_id)->first();
            if($leader -> line_token =="fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf"){
                $group_name = 'ME 1';
            }elseif($leader -> line_token =="rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm"){
                $group_name = 'ME 2';
            }elseif($leader -> line_token =="fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl"){
                $group_name = 'ME 3';
            }elseif($leader -> line_token =="gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk"){
                $group_name = 'ME 4';
            }elseif($leader -> line_token =="BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ"){
                $group_name = 'ME 5';
            }elseif($leader -> line_token =="EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq"){
                $group_name = 'MRG';
            }elseif($leader -> line_token =="NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW"){
                $group_name = 'MAG';
            }elseif($leader -> line_token =="6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri"){
                $group_name = 'MUG';
            }elseif($leader -> line_token =="jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m"){
                $group_name = 'EE 1';
            }elseif($leader -> line_token =="AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH"){
                $group_name = 'EE 2';
            }elseif($leader -> line_token =="hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d"){
                $group_name = 'EE 3';
            }elseif($leader -> line_token =="G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT"){
                $group_name = 'PCSI';
            }elseif($leader -> line_token =="x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ"){
                $group_name = 'EE 5';
            }elseif($leader -> line_token =="GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc"){
                $group_name = 'ENERGY';
            }elseif($leader -> line_token =="FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM"){
                $group_name = 'ENGINEERING';
            }elseif($leader -> line_token =="SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf"){
                $group_name = 'PM';
            }elseif($leader -> line_token =="AMScd53BO35HJqmyWW7Y2C2MVZBgk5sSVucKxezwuWy"){
                $group_name = 'PD 1';
            }elseif($leader -> line_token =="JQwvJaW0OdH8i6HuoxiWQsLiggMvvbW6zqNuryMAUL1"){
                $group_name = 'PD 2';
            }elseif($leader -> line_token =="MNR2wcZsweShOHuQEMX2s1o0Hxc8Hrs8gOfWTvuevrC"){
                $group_name = 'PD 3';
            }elseif($leader -> line_token =="hUys8VH8x9NP0Bnrv0r31mWiAEdLMpTwvfhwgBiAPdp"){
                $group_name = 'PD 4';
            }elseif($leader -> line_token =="gmMv6fQe9j8u7CLdTNr5PWU2As0cQuSkxYyT0z4wsWu"){
                $group_name = 'RS';
            }elseif($leader -> line_token =="ARnNiMwv8EaQYc6PTRcEqVaitfuOFld3ZROOsfrwIlz"){
                $group_name = 'EU';
            }elseif($leader -> line_token =="TPaOhHmy7JQsDFOw5dTTZ8Jwn5gy2V6jCNhF4aDEduJ"){
                $group_name = 'Office 1';
            }elseif($leader -> line_token =="Ha6wlWxi2cDIRaT7p7ANVlrUEQYEB8z3IolzJRWjfJB"){
                $group_name = 'Office 2';
            }elseif($leader -> line_token =="8Jp0bUQPYtOKYRUun8Q5gbppHvPPjCiuiJ1LONZxby5"){
                $group_name = 'Other';
            }else{
                $group_name = '-';
            }
            if ($checkType -> type1 == 2 || $checkType -> type1 == 3 || $checkType -> type1 == 4 || $checkType -> type1 == 5) {
                if ($leader -> line_token =="AMScd53BO35HJqmyWW7Y2C2MVZBgk5sSVucKxezwuWy"
                    || $leader -> line_token =="JQwvJaW0OdH8i6HuoxiWQsLiggMvvbW6zqNuryMAUL1"
                    || $leader -> line_token =="MNR2wcZsweShOHuQEMX2s1o0Hxc8Hrs8gOfWTvuevrC"
                    || $leader -> line_token =="gmMv6fQe9j8u7CLdTNr5PWU2As0cQuSkxYyT0z4wsWu"){
                //noti to PD plant 1 - PD1 PD2 PD3 RS
                    $data_risk_plant1 = ["fACtHtEzQi5qjiCmKCPlTVFniHbCGzzq8frWuiijBcz", "\n                  งานที่ต้องประเมินความเสี่ยง(งานประเภทที่ {$checkType -> type1})\nหัวข้อ : {$Task -> task_title} ({$Task -> task_location})\nผู้ควบคุมงาน : {$leader -> prefix}{$leader -> first_name} {$leader -> last_name} ($group_name)\nหัวหน้าผู้รับเหมา : {$Task -> contractor_prefix}{$Task -> contractor_first_name} {$Task -> contractor_last_name} ({$Task -> enterprise_name})"];
                    $getStatus_risk = lineNotify($data_risk_plant1);

                }else if($leader -> line_token == "hUys8VH8x9NP0Bnrv0r31mWiAEdLMpTwvfhwgBiAPdp"){
                //noti to PD plant 2 - PD4
                    $data_risk_plant2 = ["ceIyRSk8RdBlRXF4hSg4aFP0d2DWprBepVJm7FFAOR6", "\n                  งานที่ต้องประเมินความเสี่ยง(งานประเภทที่ {$checkType -> type1})\nหัวข้อ : {$Task -> task_title} ({$Task -> task_location})\nผู้ควบคุมงาน : {$leader -> prefix}{$leader -> first_name} {$leader -> last_name} ($group_name)\nหัวหน้าผู้รับเหมา : {$Task -> contractor_prefix}{$Task -> contractor_first_name} {$Task -> contractor_last_name} ({$Task -> enterprise_name})"];
                    $getStatus_risk = lineNotify($data_risk_plant2);

                }else if($leader -> line_token =="TPaOhHmy7JQsDFOw5dTTZ8Jwn5gy2V6jCNhF4aDEduJ"
                    ||$leader -> line_token =="ARnNiMwv8EaQYc6PTRcEqVaitfuOFld3ZROOsfrwIlz"){
                //noti to Office 1 - Office 1 EU
                    $data_risk_office1 = ["RewjOyGYf0gvyZpDD13u8WLjBjvGJcrEKQJKtMS2SeO", "\n                  งานที่ต้องประเมินความเสี่ยง(งานประเภทที่ {$checkType -> type1})\nหัวข้อ : {$Task -> task_title} ({$Task -> task_location})\nผู้ควบคุมงาน : {$leader -> prefix}{$leader -> first_name} {$leader -> last_name} ($group_name)\nหัวหน้าผู้รับเหมา : {$Task -> contractor_prefix}{$Task -> contractor_first_name} {$Task -> contractor_last_name} ({$Task -> enterprise_name})"];
                    $getStatus_risk = lineNotify($data_risk_office1);

                }else if($leader -> line_token =="Ha6wlWxi2cDIRaT7p7ANVlrUEQYEB8z3IolzJRWjfJB"){
                //noti to Office 2 - Office 2
                    $data_risk_office2 = ["aCnlFlK8lcwQo4GSN0UhzcZo1GaOeJznUyBouqBwieY", "\n                  งานที่ต้องประเมินความเสี่ยง(งานประเภทที่ {$checkType -> type1})\nหัวข้อ : {$Task -> task_title} ({$Task -> task_location})\nผู้ควบคุมงาน : {$leader -> prefix}{$leader -> first_name} {$leader -> last_name} ($group_name)\nหัวหน้าผู้รับเหมา : {$Task -> contractor_prefix}{$Task -> contractor_first_name} {$Task -> contractor_last_name} ({$Task -> enterprise_name})"];
                    $getStatus_risk = lineNotify($data_risk_office2);

                } else{
                //noti to Office ED - ME1,ME2,ME3,ME4,ME5,MRG,MAG,MUG,EE1,EE2,EE3,PCSI,EE5,ENGINEERING,ENERGY,PM,Other
                    $data_risk_ED = ["j0PskSYRubSDDSIgsnsW9PESiLOzoEl3jlOTgGBLe3f", "\n                  งานที่ต้องประเมินความเสี่ยง(งานประเภทที่ {$checkType -> type1})\nหัวข้อ : {$Task -> task_title} ({$Task -> task_location})\nผู้ควบคุมงาน : {$leader -> prefix}{$leader -> first_name} {$leader -> last_name} ($group_name)\nหัวหน้าผู้รับเหมา : {$Task -> contractor_prefix}{$Task -> contractor_first_name} {$Task -> contractor_last_name} ({$Task -> enterprise_name})"];
                    $getStatus_risk = lineNotify($data_risk_ED);
                }
            }
            $this -> sendMailConfirm($status,$taskDetail ->task_id,$taskDetail -> id);
            $request->session()->flash('success','บันทึกสำเร็จ');
            $result = 'Yes';
        }else{
            $request->session()->flash('error','บันทึกไม่สำเร็จ');
            $result = 'No';
        }
        return response()->json(['success'=> $result]);
    }

    public function confirmIndex(){
        $leader_id = Auth::guard('leader')->user()->id;
        $taskDetails = taskDetailsConfirmView::where([
            ['task_status',1],
            ['task_leader_id',$leader_id]
        ])
            ->get();

        return view('leader.confirmIndex',compact('taskDetails'));
    }

    public function sendMailConfirm($status,$task_id,$taskDetail_id){

        $task= tasksView::where('task_id',$task_id)->first();
        $leader = Leader::where('id',$task -> leader_id)->first();
        if($status == 2){

            Mail::to($task -> contractor_email)->send(
                new sendMailConfirmTask($task,$status,$taskDetail_id)
            );

            Mail::to($leader -> email)->send(
                new sendMailConfirmTaskLeader($task,$status,$taskDetail_id)
            );
            $workpermitRoute = route('admin.task.index');
            $data = ["lZ0rqpZw7YEojFFP2ftTDIIVApC7O3oAWptp3cyk0bn","\n                  ผู้รับเหมาผ่านการอนุมัติ\nหัวข้อ : {$task -> task_title}\nหัวหน้าผู้รับเหมา : {$task -> contractor_prefix}{$task -> contractor_first_name} {$task -> contractor_last_name} ({$task -> enterprise_name})\nผู้ควบคุมงาน : {$leader -> prefix}{$leader -> first_name} {$leader -> last_name}\nปริ้นใบ Workpermit : {$workpermitRoute}"];
            lineNotify($data);
        }elseif ($status == 9){
            Mail::to($task -> contractor_email)->send(
                new sendMailConfirmTask($task,$status,$taskDetail_id)
            );
        }else{
            return "error";
        }

    }

    public function finishIndex(){
        $leader_id = Auth::guard('leader')->user()->id;
        $taskDetails = taskDetailsConfirmView::where([
            ['task_status',6],
            ['task_leader_id',$leader_id]
        ])
            ->get();

        return view('leader.finishIndex',compact('taskDetails'));
    }

    public function finishTask(Request $request,$id=null){
        $contractorCheckTasks = contractorCheckTaskDetail::where('task_detail_id',$id)->get();
        $getTaskID = taskDetail::find($id);
        $leaderCheckTask = leaderCheckTask::where('task_id',$getTaskID -> task_id)->first();
        $leaderCheckTaskDetail = leaderCheckTaskDetail::where('task_detail_id',$id)->first();
        $getTask = TasksView::where('task_id',$getTaskID -> task_id)->first();
        if ($request->isMethod('post')) {
            $leaderCheckTaskDetail -> clean = $request -> clean;
            $leaderCheckTaskDetail -> clear = $request -> clear;
            $leaderCheckTaskDetail -> safe = $request -> safe;
            $leaderCheckTaskDetail -> another = $request -> another;
            $leaderCheckTaskDetail -> another_word = $request -> another_word;
            if($leaderCheckTaskDetail -> save()){
                $taskDetail = taskDetail::where('id',$leaderCheckTaskDetail -> task_detail_id)->first();
                $taskDetail -> end_time = Carbon::now();
                $taskDetail -> status = 7;
                if($taskDetail -> save()){
                    $taskDetailView = taskDetailsView::where('task_detail_id',$taskDetail -> id)->first();
                    $task_finish_date = ConvertThaiDateLine($taskDetail -> end_time);
                    $task_cancel_date = ConvertThaiDateLine(Carbon::now());
                    $detailRoute = route('leader.resultTaskDetail',['task_id' => $taskDetail -> task_id ,'task_detail_id' => $taskDetail -> id]);
                    if ($taskDetail -> status == 7){
                        $data = [$taskDetailView -> leader_line_token,"\nหัวข้อ : {$taskDetailView -> task_title}(ปิดงานสำเร็จ)\nเมื่อวันที่ : {$task_finish_date}\nของหัวหน้าผู้รับเหมา : {$taskDetailView -> contractor_firstname} {$taskDetailView -> contractor_lastname} ({$taskDetailView -> name})\nรายงานสรุป : {$detailRoute}"];
                        $getStatus = lineNotify($data);
                    }else if ($taskDetail -> status == 8){
                        $data = [$taskDetailView -> leader_line_token,"\nหัวข้อ : {$taskDetailView -> task_title}(ปิดงานไม่สำเร็จ)\nเมื่อวันที่ : {$task_cancel_date}\nของหัวหน้าผู้รับเหมา : {$taskDetailView -> contractor_firstname} {$taskDetailView -> contractor_lastname} ({$taskDetailView -> name})\nรายงานสรุป : {$detailRoute} "];
                        $getStatus = lineNotify($data);
                    }
                    if($getStatus == 200){
                        session()->flash('success','ปิดงานสำเร็จ');
                        return redirect($detailRoute);
                    }else{
                        session()->flash('error',"การส่ง Line แจ้งเตือนไม่สำเร็จ ({$getStatus})");
                        return redirect($detailRoute);

                    }
                }

            }
        }

        return view('leader.finishDetail',compact('contractorCheckTasks','leaderCheckTask','getTask'));
    }


}
