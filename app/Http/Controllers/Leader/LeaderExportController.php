<?php

namespace App\Http\Controllers\Leader;

use App\Exports\TaskGroupExport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class LeaderExportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:leader');
    }

    public function exportTaskIndex(){
        return view('leader.export.task');
    }

    public function exportTask(Request $request){
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 600);
        $register_date_start = $request -> register_date_start ? new Carbon($request -> register_date_start) : null;
        $register_date_end = $request -> register_date_end ? new Carbon($request -> register_date_end) : null;

        $getDate = Carbon::today();

        if ($register_date_start == null && $register_date_end == null){
            $fileName = 'All_Data_Task_'.$getDate -> day.'_'.$getDate -> localeMonth.'_'.$getDate -> year.''.'.csv';
        }

        if ($register_date_start != null && $register_date_end == null){
            $fileName = 'Data_Task_'.$register_date_start -> day.'_'.$register_date_start -> localeMonth.'_'.$register_date_start -> year.''.'.csv';
        }

        if ($register_date_start != null && $register_date_end != null){
            $fileName = 'Data_Task_'.$register_date_start -> day.'_'.$register_date_start -> localeMonth.'_'.$register_date_start -> year.' - '.$register_date_end -> day.'_'.$register_date_end -> localeMonth.'_'.$register_date_end -> year.'.csv';
        }

        $excel =  Excel::download(new TaskGroupExport($register_date_start,$register_date_end),$fileName);
        if ($excel -> getStatusCode() == 200){
            return $excel;
        }else{
            $request->session()->flash('error','Download ไม่สำเสร็จ');
            return redirect(route('leader.export.task.index'));
        }
    }
}
