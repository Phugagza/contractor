<?php

namespace App\Http\Controllers\Leader\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class LoginController extends Controller
{
    use ThrottlesLogins;

    /**
     * Show the login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login',[
            'title' => 'ผู้ควบคุมงาน',
            'loginRoute' => 'leader.login',
            'forgotPasswordRoute' => 'leader.password.request',
        ]);
    }

    /**
     * Login the leader.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
        $this->validator($request);

        if(Auth::guard('leader')->attempt($request->only('email','password'),$request->filled('remember'))){

            //Authentication passed...

            if( Auth::guard('admin')->check()){
                Auth::guard('admin')->logout();
            }

            return redirect()
                ->intended(route('leader.task.index'))
                ->with('success','You are Logged in as work controller!');
        }

        //Authentication failed...
        return $this->loginFailed();
    }

    /**
     * Logout the leader.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        Auth::guard('leader')->logout();
        return redirect()
            ->route('leader.login')
            ->with('success','work controller has been logged out!');
    }

    /**
     * Validate the form data.
     *
     * @param \Illuminate\Http\Request $request
     * @return
     */
    private function validator(Request $request)
    {
        //validation rules.
        $rules = [
            'email'    => 'required|email|exists:leaders|min:5|max:191',
            'password' => 'required|string|max:255',
        ];

        //custom validation error messages.
        $messages = [
            'email.exists' => 'ไม่พบอีเมล์นี้ในระบบ'
        ];

        //validate the request.
        $request->validate($rules,$messages);
    }

    /**
     * Redirect back after a failed login.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    private function loginFailed()
    {
        return redirect()
            ->back()
            ->withInput()
            ->with('error','กรุณาตรวจสอบอีเมล์ รหัสอีกครั้ง');
    }

    public function username(){
        return 'email';
    }

    public function __construct()
    {
        $this->middleware('guest:leader')->except('logout');
    }
}
