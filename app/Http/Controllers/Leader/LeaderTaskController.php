<?php

namespace App\Http\Controllers\Leader;

use App\Contractor;
use App\contractorCheckTaskDetail;
use App\Leader;
use App\leaderCheckTask;
use App\leaderCheckTaskDetail;
use App\Task;
use App\taskDetail;
use App\taskDetailsView;
use App\TasksView;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;
use App\checkResultsView;
use PDF;

class LeaderTaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:leader');
    }

    public function index(){
        $leader_id = Auth::guard('leader')->user()->id;
        $tasks = TasksView::where('leader_id',$leader_id)->get();

        return view('leader.index',compact('tasks'));
    }

    public function taskDetail($id){
        $leader_id = Auth::guard('leader')->user()->id;
        $getTask = TasksView::where('task_id',$id)->first();
        $taskDetails = taskDetailsView::where([
                                                ['task_detail_task_id',$id],
                                                ['task_leader_id',$leader_id],
                                            ])
                                       ->get();

        $contractorDetails = Task::join('contractors','contractors.id','=','tasks.contractor_id')
                                  ->where('tasks.id',$id)
                                  ->first();

        $subcontractors = Contractor::wherein('id',explode(",",$contractorDetails -> subcontractor_id))->get();

        $taskDetails_total = taskDetailsView::select(DB::raw('count(*) as sum_of_work'),
            DB::raw('sum(if(task_detail_status = 7 or task_detail_status = 8 ,floor(timestampdiff(hour,task_detail_start_time,task_detail_end_time) /24),0)) as sum_total_day'),
            DB::raw("sum(if(task_detail_status = 7 or task_detail_status  = 8 ,hour(timediff(date_format(task_detail_start_time,'%H:%i'),date_format(task_detail_end_time,'%H:%i'))) %24 ,0)) as sum_total_hour"),
            DB::raw("sum(if(task_detail_status = 7 or task_detail_status  = 8 ,minute(timediff(date_format(task_detail_start_time,'%H:%i'),date_format(task_detail_end_time,'%H:%i'))),0) %60) as sum_total_minute"),
            DB::raw('sum(if(task_detail_status = 7,1,0)) as sum_success'),
            DB::raw('sum(if(task_detail_status = 8,1,0)) as sum_fail')
        )
            ->where([
                ['task_detail_task_id',$id],
                ['task_leader_id',$leader_id],
            ])
            ->first();

        return view('leader.detailTask',compact('taskDetails','taskDetails_total','contractorDetails','subcontractors','getTask'));
    }

    public function changeStatusTask($task_id){
        $task = Task::find($task_id);
        $task -> status = $task -> status == 2 ? 1 : 2;
        if($task -> save()){
            session()->flash('success','บันทึกสำเร็จ');
            return redirect(route('leader.task.index'));
        }else{
            session()->flash('error','บันทึกไม่สำเร็จ');
            return redirect(route('leader.task.index'));
        }
    }

    public function checkTask(Request $request,$id = null){

        $check_task = leaderCheckTask::where('task_id',$id)->first();
        $getTask = TasksView::where('task_id',$id)->first();

        if ($request->isMethod('post')) {
            $check_task -> type1 = $request->type1;
            $check_task -> type2 = $request->type2;
            $check_task -> type3 = $request->type3;
            $check_task -> type4 = $request->type4;
            $check_task -> type5 = $request->type5;
            $check_task -> ppe = $request->ppe;
            $check_task -> crane1 = $request->crane1;
            $check_task -> crane2 = $request->crane2;
            $check_task -> crane2_1 = $request->crane2_1;
            $check_task -> crane2_2 = $request->crane2_2;
            $check_task -> workshop1 = $request->workshop1;
            $check_task -> workshop2 = $request->workshop2;
            $check_task -> workshop2_1 = $request->workshop2_1;
            $check_task -> workshop2_2 = $request->workshop2_2;
            $check_task -> workshop2_2 = $request->workshop2_2;
            $check_task -> electric_check1 = $request->electric_check1;
            $check_task -> electric_check2 = $request->electric_check2;
            $check_task -> equipment1 = $request->equipment1;
            $check_task -> equipment2 = $request->equipment2;
            $check_task -> equipment3 = $request->equipment3;
            $check_task -> equipment4 = $request->equipment4;
            $check_task -> equipment5 = $request->equipment5;
            $check_task -> equipment6 = $request->equipment6;
            $check_task -> equipment7 = $request->equipment7;
            $check_task -> equipment8 = $request->equipment8;
            $check_task -> equipment9 = $request->equipment9;
            $check_task -> equipment10 = $request->equipment10;
            $check_task -> amount1 = $request->amount1;
            $check_task -> amount2 = $request->amount2;
            $check_task -> amount3 = $request->amount3;
            $check_task -> amount4 = $request->amount4;
            $check_task -> amount5 = $request->amount5;
            $check_task -> amount6 = $request->amount6;
            $check_task -> amount7 = $request->amount7;
            $check_task -> amount8 = $request->amount8;
            $check_task -> amount9 = $request->amount9;
            $check_task -> amount10 = $request->amount10;
            $check_task -> unit1 = $request->unit1;
            $check_task -> unit2 = $request->unit2;
            $check_task -> unit3 = $request->unit3;
            $check_task -> unit4 = $request->unit4;
            $check_task -> unit5 = $request->unit5;
            $check_task -> unit6 = $request->unit6;
            $check_task -> unit7 = $request->unit7;
            $check_task -> unit8 = $request->unit8;
            $check_task -> unit9 = $request->unit9;
            $check_task -> unit10 = $request->unit10;
            $check_task -> permission1 = $request->permission1;
            $check_task -> permission2 = $request->permission2;
            $check_task -> permission3 = $request->permission3;
            $check_task -> permission4 = $request->permission4;
            $check_task -> permission5 = $request->permission5;
            $check_task -> permission6 = $request->permission6;
            $check_task -> permission7 = $request->permission7;
            $check_task -> permission8 = $request->permission8;
            $check_task -> permission9 = $request->permission9;
            $check_task -> permission10 = $request->permission10;
            $check_task -> touch = $request->touch;
            if($check_task -> save()){
                session()->flash('success','บันทึกสำเร็จ รอผู้รับเหมาสแกนเข้างาน');
                return redirect(route('leader.task.index'));
            }else{
                session()->flash('error','บันทึกไม่สำเร็จ');
                return redirect(route('leader.task.index'));
            }
        }

        return view('leader.check_task',compact('check_task','getTask'));
    }

    public function checkTaskDetail(Request $request,$id,$time){
        $contractorCheckTask = contractorCheckTaskDetail::where([
                                                                  ['task_detail_id',$id],
                                                                  ['time',$time],
                                                                ])->first();
        if ($request->isMethod('post')) {
            $contractorCheckTask -> leader = $request -> leader;
            if($contractorCheckTask -> save()){
                session()->flash('success','บันทึกสำเร็จ');
                return redirect(route('leader.task.index'));
            }else{
                session()->flash('error','บันทึกไม่สำเร็จ');
                return redirect(route('leader.task.index'));
            }
        }
        return view('leader.confirmTaskDetail',compact('contractorCheckTask'));
    }

    public function resultTaskDetail($task_id,$task_detail_id){
        $checkResultView = checkResultsView::where([['task_detail_id',$task_detail_id],['task_id',$task_id]])->get();
        $subcontractors = Contractor::wherein('id',explode(",",$checkResultView[0] -> subcontractor_id))->get();
        $assistant_subcontractors = Contractor::wherein('id',explode(",",$checkResultView[0] -> assistant_subcontractor_id))->get();
        $getTask = TasksView::where('task_id',$task_id)->first();
        $contractors = Contractor::where('id',$checkResultView[0] ->contractor_id)->get();

        return view('leader.resultTaskDetail',compact('checkResultView','subcontractors','getTask','contractors','assistant_subcontractors'));
    }

    public function taskDelete(Request $request){
        $task = Task::find($request -> id);
        if($task -> delete()){
            $leaderCheckTasks = leaderCheckTask::where('task_id',$task-> id)->get();
            if(count($leaderCheckTasks) >= 1){
                foreach ($leaderCheckTasks as $leaderCheckTask){
                    $leaderCheckTask -> delete();
                }
            }
            $taskDetails = taskDetail::where('task_id',$request -> id)->get();
            if(count($taskDetails) >= 1){
                foreach ($taskDetails as $taskDetail){

                    $contractorCheckTaskDetails =contractorCheckTaskDetail::where('task_detail_id',$taskDetail->id)->get();
                    if(count($contractorCheckTaskDetails) >= 1){
                        foreach ($contractorCheckTaskDetails as $contractorCheckTaskDetail){
                            $contractorCheckTaskDetail -> delete();
                        }
                    }

                    $leaderCheckTaskDetails =leaderCheckTaskDetail::where('task_detail_id',$taskDetail->id)->get();
                    if (count($leaderCheckTaskDetails) >= 1){
                        foreach ($leaderCheckTaskDetails as $leaderCheckTaskDetail){
                            $leaderCheckTaskDetail -> delete();
                        }
                    }
                    $taskDetail -> delete();
                }
            }


            $request->session()->flash('success','บันทึกสำเร็จ');
            $result = 'Yes';
        }else{
            $request->session()->flash('error','บันทึกไม่สำเร็จ');
            $result = 'No';
        }
        return response()->json(['success'=> $result]);
    }

    public function taskCancel(Request $request){
        $task = Task::find($request -> id);
        $exitTaskDetail = taskDetail::where('task_id',$task -> id)->first();

        if ($exitTaskDetail!= null){
            $exitTaskDetail -> status = 9;
            $exitTaskDetail -> description = $request -> cause;
            if($exitTaskDetail -> save()){
                $request->session()->flash('success','บันทึกสำเร็จ');
                $result =  'Yes';
            }else{
                $request->session()->flash('error','บันทึกไม่สำเร็จ');
                $result =  'No';
            }
        }else{
            $taskDetail = new taskDetail();
            $taskDetail -> task_id = $task -> id;
            $taskDetail -> description = $request -> cause;
            $taskDetail -> date = Carbon::now();
            $taskDetail -> start_time = Carbon::now();
            $taskDetail -> end_time = Carbon::now();
            $taskDetail -> status = 9;
            if($taskDetail -> save()){
                $request->session()->flash('success','บันทึกสำเร็จ');
                $result =  'Yes';
            }else{
                $request->session()->flash('error','บันทึกไม่สำเร็จ');
                $result =  'No';
            }
        }
        return response()->json(['success'=> $result]);
    }

    public function taskAll(Request $request){
        $getLeader = Auth::guard('leader')->user();
        $allRequest = $request->except('_method', '_token');
        $keyword = $request->keyword ? $request->keyword : '';
        $task_detail_status = $request->task_detail_status ? $request->task_detail_status :'';
        $date_start = $request->date_start ? Carbon::create($request->date_start) -> format('Y-m-d') :'';
        $date_end = $request->date_end ? Carbon::create($request->date_end) -> format('Y-m-d') :'';
        $perPageDefault = $request-> perPageDefault ? $request-> perPageDefault : 15;

        if ($getLeader -> prefix == "ED_" && $getLeader -> line_token == ''){
            if($keyword != '' && $date_start == '' && $task_detail_status == '') {
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ED_group'))
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);

            }
            if($date_start != '' && $task_detail_status == '' || $date_end){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ED_group'))
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->orwhereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ED_group'))
                    ->where('task_detail_status' ,$task_detail_status)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ED_group'))
                    ->where('task_detail_status' ,$task_detail_status)
                    ->whereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }

            if ($task_detail_status == 'registered' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ED_group'))
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->orderBy('task_id','desc')
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == 'registered' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ED_group'))
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ED_group'))
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ED_group'))
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if($task_detail_status == '' && $date_start =='' && $date_end =='' && $keyword == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ED_group'))
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
        }else if($getLeader -> prefix == "EE_" && $getLeader -> line_token == ''){
            if($keyword != '' && $date_start == '' && $task_detail_status == '') {
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EE_group'))
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);

            }
            if($date_start != '' && $task_detail_status == '' || $date_end){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EE_group'))
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->orwhereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EE_group'))
                    ->where('task_detail_status' ,$task_detail_status)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EE_group'))
                    ->where('task_detail_status' ,$task_detail_status)
                    ->whereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == 'registered' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EE_group'))
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->orderBy('task_id','desc')
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == 'registered' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EE_group'))
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EE_group'))
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EE_group'))
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if($task_detail_status == '' && $date_start =='' && $date_end =='' && $keyword == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EE_group'))
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
        }else if($getLeader -> prefix == "ME_" && $getLeader -> line_token == ''){
            if($keyword != '' && $date_start == '' && $task_detail_status == '') {
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ME_group'))
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);

            }
            if($date_start != '' && $task_detail_status == '' || $date_end){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ME_group'))
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->orwhereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ME_group'))
                    ->where('task_detail_status' ,$task_detail_status)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ME_group'))
                    ->where('task_detail_status' ,$task_detail_status)
                    ->whereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == 'registered' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ME_group'))
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->orderBy('task_id','desc')
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == 'registered' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ME_group'))
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ME_group'))
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ME_group'))
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if($task_detail_status == '' && $date_start =='' && $date_end =='' && $keyword == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_ME_group'))
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
        }else if($getLeader -> prefix == "EET_" && $getLeader -> line_token == ''){
            if($keyword != '' && $date_start == '' && $task_detail_status == '') {
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EET_group'))
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);

            }
            if($date_start != '' && $task_detail_status == '' || $date_end){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EET_group'))
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->orwhereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EET_group'))
                    ->where('task_detail_status' ,$task_detail_status)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EET_group'))
                    ->where('task_detail_status' ,$task_detail_status)
                    ->whereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == 'registered' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EET_group'))
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->orderBy('task_id','desc')
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == 'registered' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EET_group'))
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EET_group'))
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EET_group'))
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if($task_detail_status == '' && $date_start =='' && $date_end =='' && $keyword == ''){
                $tasks = DB::table('group_all_task_view')
                    ->whereIn('leader_line_token', config('constants.leader_EET_group'))
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
        }else if($getLeader -> line_token != ''){
            if($keyword != '' && $date_start == '' && $task_detail_status == '') {
                $tasks = DB::table('group_all_task_view')
                    ->where('leader_line_token',$getLeader -> line_token)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);

            }
            if($date_start != '' && $task_detail_status == '' || $date_end){
                $tasks = DB::table('group_all_task_view')
                    ->where('leader_line_token',$getLeader -> line_token)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->orwhereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->where('leader_line_token',$getLeader -> line_token)
                    ->where('task_detail_status' ,$task_detail_status)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->where('leader_line_token',$getLeader -> line_token)
                    ->where('task_detail_status' ,$task_detail_status)
                    ->whereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == 'registered' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->where('leader_line_token',$getLeader -> line_token)
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->orderBy('task_id','desc')
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == 'registered' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->where('leader_line_token',$getLeader -> line_token)
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->where('leader_line_token',$getLeader -> line_token)
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->where('leader_line_token',$getLeader -> line_token)
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if($task_detail_status == '' && $date_start =='' && $date_end =='' && $keyword == ''){
                $tasks = DB::table('group_all_task_view')
                    ->where('leader_line_token',$getLeader -> line_token)
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
        }else{
            if($keyword != '' && $date_start == '' && $task_detail_status == ''){
                $tasks = DB::table('group_all_task_view')
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if($date_start != '' && $task_detail_status == '' || $date_end){
                $tasks = DB::table('group_all_task_view')
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->orwhereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->where('task_detail_status' ,$task_detail_status)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->where('task_detail_status' ,$task_detail_status)
                    ->whereBetween('register_task_detail_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);

            }

            if ($task_detail_status == 'registered' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->orderBy('task_id','desc')
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == 'registered' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    ->where('task_type' ,null)
                    ->where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start == ''){
                $tasks = DB::table('group_all_task_view')
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if ($task_detail_status == '5rule' && $date_start != ''){
                $tasks = DB::table('group_all_task_view')
                    -> where('task_type' ,'<>',null)
                    -> where('task_detail_status' ,null)
                    ->whereBetween('task_working_date' ,[$date_start,$date_end])
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('task_title','like',"%".$keyword."%")
                            ->orWhere('task_location','like',"%".$keyword."%")
                            ->orWhere('contractor_first_name','like',"%".$keyword."%")
                            ->orWhere('leader_first_name','like',"%".$keyword."%");
                    })
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
            if($task_detail_status == '' && $date_start =='' && $date_end =='' && $keyword == ''){
                $tasks = DB::table('group_all_task_view')
                    ->orderBy('task_detail_id','desc')
                    ->paginate($perPageDefault);
            }
        }


        return view('leader.allTask',compact('tasks','allRequest','perPageDefault','keyword','task_detail_status','date_start','date_end'));

    }

    public function  allTaskJson(){
        $getLeader = Auth::guard('leader')->user();
        if ($getLeader -> prefix == "ED_" && $getLeader -> line_token == ''){
            //ED_ - EE_ + ME_ + EET_
            $tasks = DB::table('group_all_task_view')->whereIn('leader_line_token',[
                'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
                'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
                'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
                'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
                'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT',
                'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
                'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
                'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
                'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
                'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
                'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
                'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
                '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri',
                'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
                'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
                'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
            ])->get();
        }else if($getLeader -> prefix == "EE_" && $getLeader -> line_token == ''){
            //EE_ - EE1 EE2 EE3 EE5 PCSI
            $tasks = DB::table('group_all_task_view')->whereIn('leader_line_token',[
                'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
                'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
                'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
                'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
                'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT'
            ])->get();
        }else if($getLeader -> prefix == "ME_" && $getLeader -> line_token == ''){
            //ME_ - ME1 ME2 ME3 ME4 ME5 MAG MRG MUG
            $tasks = DB::table('group_all_task_view')->whereIn('leader_line_token',[
                'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
                'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
                'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
                'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
                'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
                'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
                'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
                '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri'
            ])->get();
        }else if($getLeader -> prefix == "EET_" && $getLeader -> line_token == ''){
            //EET_ - ENGINEERING PM ENERGY
            $tasks = DB::table('group_all_task_view')->whereIn('leader_line_token',[
                'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
                'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
                'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
            ])->get();
        }else if($getLeader -> line_token != ''){
            //workcontroller
            $tasks = DB::table('group_all_task_view')->where('leader_line_token',$getLeader -> line_token)->get();
        }else{
            $tasks = DB::table('group_all_task_view')->get();
        }
        return response()->json(["data"=>$tasks]);
    }

    public function taskWorkPermit(){
        $getLeader = Auth::guard('leader')->user();
        if ($getLeader -> prefix == "ED_" && $getLeader -> line_token == '') {
            //ED_ - EE_ + ME_ + EET_
            $task_view_details = taskDetailsView::whereIn('task_detail_status', [2, 3, 4, 5, 6])
                ->whereIn('leader_line_token', [
                    'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
                    'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
                    'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
                    'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
                    'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT',
                    'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
                    'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
                    'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
                    'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
                    'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
                    'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
                    'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
                    '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri',
                    'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
                    'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
                    'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
                ])
                ->get();
        }else if($getLeader -> prefix == "EE_" && $getLeader -> line_token == '') {
            //EE_ - EE1 EE2 EE3 EE5 PCSI
            $task_view_details = taskDetailsView::whereIn('task_detail_status', [2, 3, 4, 5, 6])
                ->whereIn('leader_line_token', [
                    'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
                    'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
                    'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
                    'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
                    'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT'
                ])
                ->get();
        }else if($getLeader -> prefix == "ME_" && $getLeader -> line_token == '') {
            //ME_ - ME1 ME2 ME3 ME4 ME5 MAG MRG MUG
            $task_view_details = taskDetailsView::whereIn('task_detail_status', [2, 3, 4, 5, 6])
                ->whereIn('leader_line_token', [
                    'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
                    'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
                    'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
                    'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
                    'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
                    'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
                    'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
                    '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri'
                ])
                ->get();

        }else if($getLeader -> prefix == "EET_" && $getLeader -> line_token == '') {
            //EET_ - ENGINEERING PM ENERGY
            $task_view_details = taskDetailsView::whereIn('task_detail_status', [2, 3, 4, 5, 6])
                ->whereIn('leader_line_token', [
                    'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
                    'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
                    'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
                ])
                ->get();
        }else if($getLeader -> line_token != '') {
            //workcontroller
            $task_view_details = taskDetailsView::whereIn('task_detail_status', [2, 3, 4, 5, 6])
                ->whereIn('leader_line_token', [
                    $getLeader -> line_token
                ])
                ->get();
        }else{
            $task_view_details = taskDetailsView::whereIn('task_detail_status', [2, 3, 4, 5, 6])->get();
        }
        return view('leader.workpermit',compact('task_view_details'));
    }

    public function taskEdit(Request $request,$id){
        $leaders = Leader::all();
        $contractors = Contractor::leftJoin('enterprises','enterprises.id','=','contractors.enterprise_id')
            ->select('contractors.id as id',
                'contractors.prefix as prefix','contractors.first_name as first_name',
                'contractors.last_name as last_name','enterprises.name as enterprise_name'
            )
            ->get();
        $task = Task::where('id',$id)->first();
        $multiSelect = explode(',',$task -> subcontractor_id);

        if ($request->isMethod('post')) {
            $task -> leader_id = $request -> leader_id;
            $task -> contractor_id = $request -> contractor_id;
            $task -> subcontractor_id = $request -> subcontractor_id ? implode(",",$request -> subcontractor_id) : null;
            $task -> title = $request -> title;
            $task -> location = $request -> location;
            $task -> description = $request -> description;
            $task -> working_date =  new Carbon($request -> working_date);
            $task -> status = 1;

            if($task -> save()){
                $request->session()->flash("success","บันทึกข้อมูลสำเร็จ");
                return redirect(route('leader.allTask'));
            }else{
                $request->session()->flash("error","บันทึกข้อมูลไม่สำเร็จ");
                return redirect(route('leader.allTask'));
            }


        }

        return view('leader.editTask',compact('task','contractors','leaders','multiSelect'));
    }

    public function taskDetailGroup($id){
        $taskDetails = taskDetailsView::where([
            ['task_detail_task_id',$id],
        ])
            ->get();
        $getTask = TasksView::where('task_id',$id)->first();
        $contractorDetails = Task::join('contractors','contractors.id','=','tasks.contractor_id')
            ->where('tasks.id',$id)
            ->first();

        $subcontractors = Contractor::wherein('id',explode(",",$contractorDetails -> subcontractor_id))->get();

        $taskDetails_total = taskDetailsView::select(DB::raw('count(*) as sum_of_work'),
            DB::raw('sum(if(task_detail_status = 7 or task_detail_status  = 8 ,floor(timestampdiff(hour,task_detail_start_time,task_detail_end_time) /24),0)) as sum_total_day'),
            DB::raw("sum(if(task_detail_status = 7 or task_detail_status  = 8 ,hour(timediff(date_format(task_detail_start_time,'%H:%i'),date_format(task_detail_end_time,'%H:%i'))) %24 ,0)) as sum_total_hour"),
            DB::raw("sum(if(task_detail_status = 7 or task_detail_status  = 8 ,minute(timediff(date_format(task_detail_start_time,'%H:%i'),date_format(task_detail_end_time,'%H:%i'))),0) %60) as sum_total_minute"),
            DB::raw('sum(if(task_detail_status = 7,1,0)) as sum_success'),
            DB::raw('sum(if(task_detail_status = 8,1,0)) as sum_fail')
        )
            ->where([
                ['task_detail_task_id',$id],
            ])
            ->first();
//        dd($taskDetails_total);
        return view('leader.detailTaskGroup',compact('taskDetails','taskDetails_total','contractorDetails','subcontractors','getTask'));
    }

    public function resultTaskDetailGroupPDF($task_id,$task_detail_id){
        $checkResultView = checkResultsView::where([['task_detail_id',$task_detail_id],['task_id',$task_id]])->get();
        $subcontractors = Contractor::wherein('id',explode(",",$checkResultView[0] -> subcontractor_id))->get();
        $assistant_subcontractors = Contractor::wherein('id',explode(",",$checkResultView[0] -> assistant_subcontractor_id))->get();
        $leader = Leader::where('id',$checkResultView[0]->leader_id)->first();
        $contractors = Contractor::where('id',$checkResultView[0] ->contractor_id)->get();
        $pdf = PDF::loadView('admin.task.resultTaskDetailPDF',['checkResultView' =>  $checkResultView,'subcontractors' => $subcontractors,'contractors' => $contractors,'task_detail_id'=>$task_detail_id,'leader'=>$leader,'assistant_subcontractors'=>$assistant_subcontractors]);
        $pdf->getDomPDF()->set_option("enable_php", true);
        return @$pdf->download("{$checkResultView[0] -> task_title}(รายงานสรุป NO.$task_detail_id).pdf");
//        return view('admin.task.resultTaskDetailPDF',compact('checkResultView','subcontractors','contractors','task_detail_id','leader'));
    }

    public function resultTaskDetailGroup($task_id,$task_detail_id){
        $checkResultView = checkResultsView::where([['task_detail_id',$task_detail_id],['task_id',$task_id]])->get();
        $subcontractors = Contractor::wherein('id',explode(",",$checkResultView[0] -> subcontractor_id))->get();
        $assistant_subcontractors = Contractor::wherein('id',explode(",",$checkResultView[0] -> assistant_subcontractor_id))->get();
        $getTask = TasksView::where('task_id',$task_id)->first();
        $contractors = Contractor::where('id',$checkResultView[0] ->contractor_id)->get();

        return view('leader.resultTaskDetailGroup',compact('checkResultView','subcontractors','getTask','contractors','assistant_subcontractors'));
    }

    public function taskAllQRcode($task_id){
        $task = DB::table('group_all_task_view')->where('task_id',$task_id)->first();

        if(empty($task)){
            session()->flash("warning","ไม่พบข้อมูล");
            return redirect(route('leader.task.allTask'));
        }

        return view('leader.taskQRCode',compact('task'));

    }

}
