<?php

namespace App\Http\Controllers\Contractor;

use App\Mail\testMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class TaskController extends Controller
{
    public function index(){
//        $dataController = 'dataController';
//        $sendMail = Mail::to('phugagza@gmail.com')
//              ->send(
//                  new testMail($dataController)
//              );
//       if(Mail::failures($sendMail)){
//          session()->flash("error", "ผิดพลาด");
//       }else{
//           session()->flash("success", "บันทึกข้อมูลเรียบร้อย");
//       }
        return view('contractor.task.indexTask');
    }

    public function task(){

        return view('contractor.task.editTask');
    }

    public function taskDetail(){

        return view('contractor.task.detailTask');
    }

}
