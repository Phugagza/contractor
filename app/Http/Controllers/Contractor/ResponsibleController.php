<?php

namespace App\Http\Controllers\Contractor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResponsibleController extends Controller
{
    public function index(){

        return view('contractor.responsible.indexResponsible');
    }

    public function responsible(){

        return view('contractor.responsible.editResponsible');
    }

    public function responsibleDetail(){

        return view('contractor.responsible.detailResponsible');
    }
}
