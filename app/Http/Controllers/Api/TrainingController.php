<?php

namespace App\Http\Controllers\Api;

use App\Contractor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\trainingDetailView;
use App\trainingDetail;
use Illuminate\Support\Facades\DB;
use App\Mail\sendCertificate;
use Illuminate\Support\Facades\Mail;

class TrainingController extends Controller
{
    public function checkTraining(){

        $trainingDetailViews = trainingDetailView::select('training_detail_id','count_date')->where('count_date','<',0)->get();
       foreach ($trainingDetailViews as $trainingDetailView){
           $trainingDetail = trainingDetail::where('id',$trainingDetailView ->training_detail_id)->first();
           $trainingDetail -> permission = "N";
           $trainingDetail -> save();
       }

        return "true";
    }

    public function checkSafetyTraining(){

//        $safety_trainings = Contractor::where(DB::raw('DATEDIFF(contractors.safety_expired_date,contractors.safety_training_date) - DATEDIFF(DATE(now()),contractors.safety_training_date)'),'<=',0)
//            ->get();
//        foreach ($safety_trainings as $safety_training){
//            $contractor = Contractor::where('id',$safety_training ->id)->first();
//            $contractor -> permission = "N";
//            $contractor -> save();
//        }

//        $contractors = Contractor::where('id',1)->get();
        $contractors = Contractor::all();
            foreach ($contractors as $contractor){

               if ($contractor -> count_safety_date <= 0){
                   $contractor -> permission = "N";
               }

               if ($contractor -> count_indoor_electricity_date <= 0){
                   $contractor -> indoor_electricity_permission = "N";
               }

               if ($contractor -> count_indoor_air_conditioner_date <= 0){
                   $contractor -> indoor_air_conditioner_permission = "N";
               }

               if ($contractor -> count_metal_arc_date <= 0){
                   $contractor -> metal_arc_permission = "N";
               }

               if ($contractor -> count_tig_welding_date <= 0){
                   $contractor -> tig_welding_permission = "N";
               }

               if ($contractor -> count_alloy_welding_date <= 0){
                   $contractor -> alloy_welding_permission = "N";
               }

               if ($contractor -> count_scaffolding_date <= 0){
                   $contractor -> scaffolding_permission = "N";
               }

               if ($contractor -> count_worker_confine_date <= 0){
                   $contractor -> worker_confine_permission = "N";
               }

               if ($contractor -> count_assistant_confine_date <= 0){
                   $contractor -> assistant_confine_permission = "N";
               }

               if ($contractor -> count_controller_confine_date <= 0){
                   $contractor -> controller_confine_permission = "N";
               }

               if ($contractor -> count_warrantor_confine_date <= 0){
                   $contractor -> warrantor_confine_permission = "N";
               }
               $contractor -> save();
           }

        return "true";
    }

    public function checkCertificate(){
        $contractors = Contractor::all();
        foreach($contractors as $contractor){

            if($contractor -> count_safety_date == 30 && $contractor->permission == "Y"){
                $safety_title = 'อบรมความปลอดภัย';
                Mail::to($contractor -> email)->send(
                  new sendCertificate($contractor,$safety_title)
                );
            }

            if($contractor -> count_indoor_electricity_date == 30 && $contractor->indoor_electricity_permission == "Y"){
                $indoor_electricity_title = 'ช่างไฟไฟ้าภายในอาคาร';
                Mail::to($contractor -> email)->send(
                    new sendCertificate($contractor,$indoor_electricity_title)
                );
            }

            if ($contractor -> count_indoor_air_conditioner_date  == 30 && $contractor->indoor_air_conditioner_permission == "Y"){
                $indoor_air_conditioner_title = 'ช่างเครื่องปรับอากาศภายในบ้านและการพาณิชย์ขนาดเล็ก';
                Mail::to($contractor -> email)->send(
                    new sendCertificate($contractor,$indoor_air_conditioner_title)
                );
            }

            if ($contractor -> count_metal_arc_date == 30 && $contractor->metal_arc_permission == "Y"){
                $metal_arc_title = 'ช่างเชื่อมอาร์กโลหะด้วยมือ';
                Mail::to($contractor -> email)->send(
                    new sendCertificate($contractor,$metal_arc_title)
                );
            }

            if ($contractor -> count_tig_welding_date == 30 && $contractor->tig_welding_permission == "Y"){
                $tig_welding_title = 'ช่างเชื่อมทิก';
                Mail::to($contractor -> email)->send(
                    new sendCertificate($contractor,$tig_welding_title)
                );
            }

            if ($contractor -> count_alloy_welding_date == 30 && $contractor->alloy_welding_permission == "Y"){
                $alloy_welding_title = 'ช่างเชื่อมแม็ก';
                Mail::to($contractor -> email)->send(
                    new sendCertificate($contractor,$alloy_welding_title)
                );
            }

            if ($contractor -> count_scaffolding_date == 30 && $contractor->scaffolding_permission == "Y"){
                $scaffolding_title = 'ติดตั้งนั่งร้าน';
                Mail::to($contractor -> email)->send(
                    new sendCertificate($contractor,$scaffolding_title)
                );
            }

            if ($contractor -> count_worker_confine_date == 30 && $contractor->worker_confine_permission == "Y"){
                $worker_confine_title = 'ผู้ปฏิบัติงาน ในงานอับอากาศ';
                Mail::to($contractor -> email)->send(
                    new sendCertificate($contractor,$worker_confine_title)
                );
            }

            if ($contractor -> count_assistant_confine_date == 30 && $contractor->assistant_confine_permission == "Y"){
                $assistant_confine_title = 'ผู้ช่วยเหลือ ในงานอับอากาศ';
                Mail::to($contractor -> email)->send(
                    new sendCertificate($contractor,$assistant_confine_title)
                );
            }

            if ($contractor -> count_controller_confine_date == 30 && $contractor->controller_confine_permission == "Y"){
                $controller_confine_title = 'ผู้ควบคุม ในงานอับอากาศ';
               Mail::to($contractor -> email)->send(
                    new sendCertificate($contractor,$controller_confine_title)
                );
            }

            if ($contractor -> count_warrantor_confine_date == 30 && $contractor->warrantor_confine_permission == "Y"){
                $warrantor_confine_title = 'ผู้อนุญาต ในงานอับอากาศ';
                Mail::to($contractor -> email)->send(
                    new sendCertificate($contractor,$warrantor_confine_title)
                );
            }
        }

    }
}
