<?php

namespace App\Http\Controllers\Admin;

use App\mailLists;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminMailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function mailIndex(){
        $mail_lists = mailLists::all();

        return view('admin.mail_list.index',compact('mail_lists'));
    }

    public function mailEdit(Request $request,$id=null){

        if ($id != null){
            $mail = mailLists::find($id);
        }else{
            $mail = new mailLists();
        }


        if ($request->isMethod('post')) {
            $mail -> email_name = $request -> email_name;
            if ($mail -> save()){
                $request->session()->flash("success","บันทึกข้อมูลสำเร็จ");
            }else{
                $request->session()->flash("error","บันทึกข้อมูลไม่สำเร็จ");
            }

            return redirect(route('admin.mail.list.index'));
        }

        return view('admin.mail_list.edit',compact('mail'));
    }

    public function mailDelete(Request $request){
        $mailList = mailLists::find($request -> id);
        if($mailList -> delete()){
            $request->session()->flash('success','บันทึกสำเร็จ');
            $result = 'Yes';
        }else{
            $request->session()->flash('error','บันทึกไม่สำเร็จ');
            $result = 'No';
        }
        return response()->json(['success'=> $result]);
    }
}
