<?php

namespace App\Http\Controllers\Admin;

use App\Contractor;
use App\Enterprise;
use App\Exports\ContractorExport;
use App\Exports\LeaderExport;
use App\Exports\TaskExport;
use App\Task;
use App\TasksView;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class AdminExportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function exportTaskIndex(){
        return view('admin.export.task');
    }

    public function exportTask(Request $request){
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 600);
        $register_date_start = $request -> register_date_start ? new Carbon($request -> register_date_start) : null;
        $register_date_end = $request -> register_date_end ? new Carbon($request -> register_date_end) : null;

        $getDate = Carbon::today();

        if ($register_date_start == null && $register_date_end == null){
            $fileName = 'All_Data_Task_'.$getDate -> day.'_'.$getDate -> localeMonth.'_'.$getDate -> year.''.'.csv';
        }

        if ($register_date_start != null && $register_date_end == null){
            $fileName = 'Data_Task_'.$register_date_start -> day.'_'.$register_date_start -> localeMonth.'_'.$register_date_start -> year.''.'.csv';
        }

        if ($register_date_start != null && $register_date_end != null){
            $fileName = 'Data_Task_'.$register_date_start -> day.'_'.$register_date_start -> localeMonth.'_'.$register_date_start -> year.' - '.$register_date_end -> day.'_'.$register_date_end -> localeMonth.'_'.$register_date_end -> year.'.csv';
        }

        $excel =  Excel::download(new TaskExport($register_date_start,$register_date_end),$fileName);
       if ($excel -> getStatusCode() == 200){
           return $excel;
       }else{
           $request->session()->flash('error','Download ไม่สำเสร็จ');
           return redirect(route('admin.export.task.index'));
       }
    }

    public function exportContractorIndex(){
        $enterprises = Enterprise::all();
        return view('admin.export.contractor' ,compact('enterprises'));
    }

    public function exportContractor(Request $request){
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 600);
        $enterprise_id = $request -> enterprise_id;
        $getEnterprise = Enterprise::where('id',$enterprise_id)->pluck('name');
        $getDate = Carbon::today();
        if($enterprise_id != null){
            $fileName = 'All_Data_Contractor_'.$getDate -> day.'_'.$getDate -> localeMonth.'_'.$getDate -> year."($getEnterprise[0])".''.'.csv';
        }
        if($enterprise_id == null){
            $fileName = 'All_Data_Contractor_'.$getDate -> day.'_'.$getDate -> localeMonth.'_'.$getDate -> year.''.'.csv';
        }
        $excel =  Excel::download(new ContractorExport($enterprise_id),$fileName);
        if ($excel -> getStatusCode() == 200){
            return $excel;
        }else{
            session()->flash('error','Download ไม่สำเสร็จ');
            return redirect(route('admin.export.task.index'));
        }
    }

    public function exportLeaderIndex(){
        return view('admin.export.leader');
    }

    public function exportLeader(){
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 600);
        $getDate = Carbon::today();
        $fileName = 'All_Data_WorkController_'.$getDate -> day.'_'.$getDate -> localeMonth.'_'.$getDate -> year.''.'.csv';

        $excel =  Excel::download(new LeaderExport(),$fileName);
        if ($excel -> getStatusCode() == 200){
            return $excel;
        }else{
            session()->flash('error','Download ไม่สำเสร็จ');
            return redirect(route('admin.export.task.index'));
        }
    }
}
