<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\taskDetailsView;
use App\TasksView;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index(){
        return view('admin.index');
    }

    public function adminIndex(){
        $admins = Admin::where('level',3)->get();

        return view('admin.admin.index',compact('admins'));
    }

    public function adminEdit(Request $request,$id=null){

        if ($id != null){
            $admin = Admin::find($id);
        }else{
            $admin = new Admin();
        }

        if ($request->isMethod('post')) {
            $admin -> prefix = $request -> prefix;
            $admin -> first_name = $request -> first_name;
            $admin -> last_name = $request -> last_name;
            $admin -> email = $request -> email;
            $admin -> password = $request -> password != '' ? Hash::make($request -> password) : $request -> password_hidden;
            $admin -> line_token = $request -> line_token;
            $admin -> level = 3;
            $admin -> status = 1;

            if($admin -> save()){
                $request->session()->flash("success","บันทึกข้อมูลสำเร็จ");
                return redirect(route('admin.admin.index'));
            }else{
                $request->session()->flash("error","บันทึกข้อมูลไม่สำเร็จ");
                return redirect(route('admin.admin.index'));
            }
        }

        return view('admin.admin.edit',compact('admin'));
    }

    public function adminDelete(Request $request){
        $admin = Admin::find($request -> id);
        if($admin -> delete()){
            $request->session()->flash('success','บันทึกสำเร็จ');
            $result = 'Yes';
        }else{
            $request->session()->flash('error','บันทึกไม่สำเร็จ');
            $result = 'No';
        }
        return response()->json(['success'=> $result]);
    }

}
