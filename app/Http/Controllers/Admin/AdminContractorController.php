<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Contractor;
use App\Enterprise;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminContractorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function contractorIndex(Request $request){
        try{

            $allRequest = $request->except('_method', '_token');
//        dd($allRequest);
            $enterprise_id = $request->enterprise_id;
            $keyword = $request->keyword ? $request->keyword : '';
            $perPageDefault = $request-> perPageDefault ? $request-> perPageDefault : 10;
            if($enterprise_id != ""){
                $contractors = Contractor::where('enterprise_id',$enterprise_id)->paginate($perPageDefault);
            }
            if($enterprise_id == ""){
                $contractors = Contractor::with('enterprise')->orderBy('first_name','asc')->paginate($perPageDefault);
            }
            if($enterprise_id == "enterprise_exit"){
                $contractors = Contractor::whereDoesnthave('enterprise')->orderBy('first_name','asc')->paginate($perPageDefault);
            }

            if($enterprise_id == "" && $keyword != ''){
                $contractors = Contractor::with('enterprise')
                                ->orwhere('first_name','like',"%".$keyword."%")
                                ->orWhere('last_name','like',"%".$keyword."%")
                                ->orderBy('first_name','asc')
                                ->paginate($perPageDefault);
            }

            if($enterprise_id != "" && $keyword != ''){
                $contractors = Contractor::with('enterprise')
                    //group parameter = where (enterprise_id = ?) and (first_name like %?% or last_name like %?% )
                    ->where('enterprise_id',$enterprise_id)
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('first_name','like',"%".$keyword."%")
                              ->orWhere('last_name','like',"%".$keyword."%");
                    })
                    ->orderBy('first_name','asc')
                    ->paginate($perPageDefault);
            }
            if($enterprise_id == "enterprise_exit" && $keyword != ''){
                $contractors = Contractor::whereDoesnthave('enterprise')
                    ->where(function ($query) use($keyword) {
                        $query->orwhere('first_name','like',"%".$keyword."%")
                            ->orWhere('last_name','like',"%".$keyword."%");
                    })
                    ->orderBy('first_name','asc')
                    ->paginate($perPageDefault);
            }

            $enterprises = Enterprise::all();
        }catch (Throwable $err){
            dd($err -> getMessage());
        }

        return view('admin.contractor.index',compact('contractors','allRequest','enterprises','enterprise_id','perPageDefault','keyword')) ;
    }

    public function contractorEdit(Request $request , $id=null){
        $trainers = Admin::where('level',2)->get();
        if ($id != null){
            $contractor = Contractor::where('id',$id)->first();
        }else{
            $contractor = new Contractor();
        }
        $enterprises = Enterprise::all();
        if ($request->isMethod('post')) {
            $contractor -> prefix = $request -> prefix;
            $contractor -> first_name = $request -> first_name;
            $contractor -> last_name = $request -> last_name;
            $contractor -> email = $request -> email;
            $contractor -> citizen_id = $request -> citizen_id;
            $contractor -> tel = $request -> tel;
            $contractor -> enterprise_id = $request -> enterprise_id;
            $contractor -> status = 1;
            $contractor -> password = Hash::make($request -> citizen_id);

            //safety training
            $expired_date = $request -> expired_date;
            $safety_training_date = new Carbon($request -> training_date);
            $contractor -> safety_training_date = new Carbon($request -> training_date);
            $contractor -> trainer_id = $request -> trainer_id;
            if ($expired_date == "365"){$cal_expired = $safety_training_date -> addDays($expired_date);}
            if ($expired_date == "730"){$cal_expired = $safety_training_date -> addDays($expired_date);}
            if ($expired_date == "another"){$cal_expired = $safety_training_date -> addDays($expired_date);}
            $contractor -> safety_expired_date = $cal_expired;
            $contractor -> permission = $request -> permission ? $request -> permission : "N";

            //indoor electricity
            $indoor_electricity_expired_date = $request -> indoor_electricity_expired_date;
            $indoor_electricity_training_date = new Carbon($request -> indoor_electricity_training_date);
            if ($indoor_electricity_expired_date == "365"){$indoor_electricity_cal_expired = $indoor_electricity_training_date -> addDays($indoor_electricity_expired_date);}
            if ($indoor_electricity_expired_date == "730"){$indoor_electricity_cal_expired = $indoor_electricity_training_date -> addDays($indoor_electricity_expired_date);}
            if ($indoor_electricity_expired_date == "1095"){$indoor_electricity_cal_expired = $indoor_electricity_training_date -> addDays($indoor_electricity_expired_date);}
            if ($indoor_electricity_expired_date == "1825"){$indoor_electricity_cal_expired = $indoor_electricity_training_date -> addDays($indoor_electricity_expired_date);}
            if ($indoor_electricity_expired_date == "3650"){$indoor_electricity_cal_expired = $indoor_electricity_training_date -> addDays($indoor_electricity_expired_date);}
            $contractor -> indoor_electricity_expired_date = $indoor_electricity_cal_expired;
            $contractor -> indoor_electricity_permission = $request -> indoor_electricity_permission ? $request -> indoor_electricity_permission : "N";
            $contractor -> indoor_electricity_training_date = new Carbon($request -> indoor_electricity_training_date);


            //indoor air conditioner
            $indoor_air_conditioner_expired_date = $request -> indoor_air_conditioner_expired_date;
            $indoor_air_conditioner_training_date = new Carbon($request -> indoor_air_conditioner_training_date);
            if ($indoor_air_conditioner_expired_date == "365"){$indoor_air_conditioner_cal_expired = $indoor_air_conditioner_training_date -> addDays($indoor_air_conditioner_expired_date);}
            if ($indoor_air_conditioner_expired_date == "730"){$indoor_air_conditioner_cal_expired = $indoor_air_conditioner_training_date -> addDays($indoor_air_conditioner_expired_date);}
            if ($indoor_air_conditioner_expired_date == "1095"){$indoor_air_conditioner_cal_expired = $indoor_air_conditioner_training_date -> addDays($indoor_air_conditioner_expired_date);}
            if ($indoor_air_conditioner_expired_date == "1825"){$indoor_air_conditioner_cal_expired = $indoor_air_conditioner_training_date -> addDays($indoor_air_conditioner_expired_date);}
            if ($indoor_air_conditioner_expired_date == "3650"){$indoor_air_conditioner_cal_expired = $indoor_air_conditioner_training_date -> addDays($indoor_air_conditioner_expired_date);}
            $contractor -> indoor_air_conditioner_expired_date = $indoor_air_conditioner_cal_expired;
            $contractor -> indoor_air_conditioner_permission = $request -> indoor_air_conditioner_permission ? $request -> indoor_air_conditioner_permission : "N";
            $contractor -> indoor_air_conditioner_training_date = new Carbon($request -> indoor_air_conditioner_training_date);


            //metal arc
            $metal_arc_expired_date = $request -> metal_arc_expired_date;
            $metal_arc_training_date = new Carbon($request -> metal_arc_training_date);
            if ($metal_arc_expired_date == "365"){$metal_arc_cal_expired = $metal_arc_training_date -> addDays($metal_arc_expired_date);}
            if ($metal_arc_expired_date == "730"){$metal_arc_cal_expired = $metal_arc_training_date -> addDays($metal_arc_expired_date);}
            if ($metal_arc_expired_date == "1095"){$metal_arc_cal_expired = $metal_arc_training_date -> addDays($metal_arc_expired_date);}
            if ($metal_arc_expired_date == "1825"){$metal_arc_cal_expired = $metal_arc_training_date -> addDays($metal_arc_expired_date);}
            if ($metal_arc_expired_date == "3650"){$metal_arc_cal_expired = $metal_arc_training_date -> addDays($metal_arc_expired_date);}
            $contractor -> metal_arc_expired_date = $metal_arc_cal_expired;
            $contractor -> metal_arc_permission = $request -> metal_arc_permission ? $request -> metal_arc_permission : "N";
            $contractor -> metal_arc_training_date = new Carbon($request -> metal_arc_training_date);


            //tig welding
            $tig_welding_expired_date = $request -> tig_welding_expired_date;
            $tig_welding_training_date = new Carbon($request -> tig_welding_training_date);
            if ($tig_welding_expired_date == "365"){$tig_welding_cal_expired = $tig_welding_training_date -> addDays($tig_welding_expired_date);}
            if ($tig_welding_expired_date == "730"){$tig_welding_cal_expired = $tig_welding_training_date -> addDays($tig_welding_expired_date);}
            if ($tig_welding_expired_date == "1095"){$tig_welding_cal_expired = $tig_welding_training_date -> addDays($tig_welding_expired_date);}
            if ($tig_welding_expired_date == "1825"){$tig_welding_cal_expired = $tig_welding_training_date -> addDays($tig_welding_expired_date);}
            if ($tig_welding_expired_date == "3650"){$tig_welding_cal_expired = $tig_welding_training_date -> addDays($tig_welding_expired_date);}
            $contractor -> tig_welding_expired_date = $tig_welding_cal_expired;
            $contractor -> tig_welding_permission = $request -> tig_welding_permission ? $request -> tig_welding_permission : "N";
            $contractor -> tig_welding_training_date = new Carbon($request -> tig_welding_training_date);


            //alloy welding
            $alloy_welding_expired_date = $request -> alloy_welding_expired_date;
            $alloy_welding_training_date = new Carbon($request -> alloy_welding_training_date);
            if ($alloy_welding_expired_date == "365"){$alloy_welding_cal_expired = $alloy_welding_training_date -> addDays($alloy_welding_expired_date);}
            if ($alloy_welding_expired_date == "730"){$alloy_welding_cal_expired = $alloy_welding_training_date -> addDays($alloy_welding_expired_date);}
            if ($alloy_welding_expired_date == "1095"){$alloy_welding_cal_expired = $alloy_welding_training_date -> addDays($alloy_welding_expired_date);}
            if ($alloy_welding_expired_date == "1825"){$alloy_welding_cal_expired = $alloy_welding_training_date -> addDays($alloy_welding_expired_date);}
            if ($alloy_welding_expired_date == "3650"){$alloy_welding_cal_expired = $alloy_welding_training_date -> addDays($alloy_welding_expired_date);}
            $contractor -> alloy_welding_expired_date = $alloy_welding_cal_expired;
            $contractor -> alloy_welding_permission = $request -> alloy_welding_permission ? $request -> alloy_welding_permission : "N";
            $contractor -> alloy_welding_training_date = new Carbon($request -> alloy_welding_training_date);


            //scaffolding
            $scaffolding_expired_date = $request -> scaffolding_expired_date;
            $scaffolding_training_date = new Carbon($request -> scaffolding_training_date);
            if ($scaffolding_expired_date == "365"){$scaffolding_cal_expired = $scaffolding_training_date -> addDays($scaffolding_expired_date);}
            if ($scaffolding_expired_date == "730"){$scaffolding_cal_expired = $scaffolding_training_date -> addDays($scaffolding_expired_date);}
            if ($scaffolding_expired_date == "1095"){$scaffolding_cal_expired = $scaffolding_training_date -> addDays($scaffolding_expired_date);}
            if ($scaffolding_expired_date == "1825"){$scaffolding_cal_expired = $scaffolding_training_date -> addDays($scaffolding_expired_date);}
            if ($scaffolding_expired_date == "3650"){$scaffolding_cal_expired = $scaffolding_training_date -> addDays($scaffolding_expired_date);}
            $contractor -> scaffolding_expired_date = $scaffolding_cal_expired;
            $contractor -> scaffolding_permission = $request -> scaffolding_permission ? $request -> scaffolding_permission : "N";
            $contractor -> scaffolding_training_date = new Carbon($request -> scaffolding_training_date);


            //worker confine
            $worker_confine_expired_date= $request -> worker_confine_expired_date;
            $worker_confine_training_date = new Carbon($request -> worker_confine_training_date);
            if ($worker_confine_expired_date == "365"){$worker_confine_cal_expired = $worker_confine_training_date -> addDays($worker_confine_expired_date);}
            if ($worker_confine_expired_date == "730"){$worker_confine_cal_expired = $worker_confine_training_date -> addDays($worker_confine_expired_date);}
            if ($worker_confine_expired_date == "1095"){$worker_confine_cal_expired = $worker_confine_training_date -> addDays($worker_confine_expired_date);}
            if ($worker_confine_expired_date == "1825"){$worker_confine_cal_expired = $worker_confine_training_date -> addDays($worker_confine_expired_date);}
            if ($worker_confine_expired_date == "3650"){$worker_confine_cal_expired = $worker_confine_training_date -> addDays($worker_confine_expired_date);}
            $contractor -> worker_confine_expired_date = $worker_confine_cal_expired;
            $contractor -> worker_confine_permission = $request -> worker_confine_permission ? $request -> worker_confine_permission : "N";
            $contractor -> worker_confine_training_date = new Carbon($request -> worker_confine_training_date);


            //assistant confine
            $assistant_confine_expired_date= $request -> assistant_confine_expired_date;
            $assistant_confine_training_date = new Carbon($request -> assistant_confine_training_date);
            if ($assistant_confine_expired_date == "365"){$assistant_confine_cal_expired = $assistant_confine_training_date -> addDays($assistant_confine_expired_date);}
            if ($assistant_confine_expired_date == "730"){$assistant_confine_cal_expired = $assistant_confine_training_date -> addDays($assistant_confine_expired_date);}
            if ($assistant_confine_expired_date == "1095"){$assistant_confine_cal_expired = $assistant_confine_training_date -> addDays($assistant_confine_expired_date);}
            if ($assistant_confine_expired_date == "1825"){$assistant_confine_cal_expired = $assistant_confine_training_date -> addDays($assistant_confine_expired_date);}
            if ($assistant_confine_expired_date == "3650"){$assistant_confine_cal_expired = $assistant_confine_training_date -> addDays($assistant_confine_expired_date);}
            $contractor -> assistant_confine_expired_date = $assistant_confine_cal_expired;
            $contractor -> assistant_confine_permission = $request -> assistant_confine_permission ? $request -> assistant_confine_permission : "N";
            $contractor -> assistant_confine_training_date = new Carbon($request -> assistant_confine_training_date);


            //controller confine
            $controller_confine_expired_date= $request -> controller_confine_expired_date;
            $controller_confine_training_date = new Carbon($request -> controller_confine_training_date);
            if ($controller_confine_expired_date == "365"){$controller_confine_cal_expired = $controller_confine_training_date -> addDays($controller_confine_expired_date);}
            if ($controller_confine_expired_date == "730"){$controller_confine_cal_expired = $controller_confine_training_date -> addDays($controller_confine_expired_date);}
            if ($controller_confine_expired_date == "1095"){$controller_confine_cal_expired = $controller_confine_training_date -> addDays($controller_confine_expired_date);}
            if ($controller_confine_expired_date == "1825"){$controller_confine_cal_expired = $controller_confine_training_date -> addDays($controller_confine_expired_date);}
            if ($controller_confine_expired_date == "3650"){$controller_confine_cal_expired = $controller_confine_training_date -> addDays($controller_confine_expired_date);}
            $contractor -> controller_confine_expired_date = $controller_confine_cal_expired;
            $contractor -> controller_confine_permission = $request -> controller_confine_permission ? $request -> controller_confine_permission : "N";
            $contractor -> controller_confine_training_date = new Carbon($request -> controller_confine_training_date);


            //warrantor confine
            $warrantor_confine_expired_date= $request -> warrantor_confine_expired_date;
            $warrantor_confine_training_date = new Carbon($request -> warrantor_confine_training_date);
            if ($warrantor_confine_expired_date == "365"){$warrantor_confine_cal_expired = $warrantor_confine_training_date -> addDays($warrantor_confine_expired_date);}
            if ($warrantor_confine_expired_date == "730"){$warrantor_confine_cal_expired = $warrantor_confine_training_date -> addDays($warrantor_confine_expired_date);}
            if ($warrantor_confine_expired_date == "1095"){$warrantor_confine_cal_expired = $warrantor_confine_training_date -> addDays($warrantor_confine_expired_date);}
            if ($warrantor_confine_expired_date == "1825"){$warrantor_confine_cal_expired = $warrantor_confine_training_date -> addDays($warrantor_confine_expired_date);}
            if ($warrantor_confine_expired_date == "3650"){$warrantor_confine_cal_expired = $warrantor_confine_training_date -> addDays($warrantor_confine_expired_date);}
            $contractor -> warrantor_confine_expired_date = $warrantor_confine_cal_expired;
            $contractor -> warrantor_confine_permission = $request -> warrantor_confine_permission ? $request -> warrantor_confine_permission : "N";
            $contractor -> warrantor_confine_training_date = new Carbon($request -> warrantor_confine_training_date);


            if($contractor -> save()){
                $request->session()->flash("success","บันทึกข้อมูล {$contractor -> first_name} สำเร็จ");
                return redirect(route('admin.contractor.edit'));
            }else{
                $request->session()->flash("error","บันทึกข้อมูล {$contractor -> first_name} ไม่สำเร็จ");
                return redirect(route('admin.contractor.edit'));
            }

        }

        return view('admin.contractor.edit',compact('contractor','enterprises','trainers'));
    }

    public function contractorDelete(Request $request){
        $contractor = Contractor::find($request -> id);
        if($contractor -> delete()){
            $request->session()->flash('success','บันทึกสำเร็จ');
            $result = 'Yes';
        }else{
            $request->session()->flash('error','บันทึกไม่สำเร็จ');
            $result = 'No';
        }
        return response()->json(['success'=> $result]);
    }

    public function contractorEnterpriseIndex(){
        $enterprises = Enterprise::all();

        return view('admin.contractor.enterprise.index',compact('enterprises'));
    }

    public function contractorEnterpriseEdit(Request $request,$id=null){
        if ($id != null){
            $enterprise = Enterprise::find($id);
        }else{
            $enterprise = new Enterprise();
        }

        if ($request->isMethod('post')) {
            $enterprise -> name = $request -> name;
            $enterprise -> address = $request -> address;
            if($enterprise -> save()){
                $request->session()->flash("success","บันทึกข้อมูลสำเร็จ");
                return redirect(route('admin.contractor.enterprise.index'));
            }else{
                $request->session()->flash("error","บันทึกข้อมูลไม่สำเร็จ");
                return redirect(route('admin.contractor.enterprise.index'));
            }
        }

        return view('admin.contractor.enterprise.edit' ,compact('enterprise'));
    }

    public function enterpriseDelete(Request $request){
        $enterprise = Enterprise::find($request -> id);
        $contractors = Contractor::where('enterprise_id',$request -> id)->get();
        foreach ($contractors as $contractor){
            $contractor -> delete();
        }
        if($enterprise -> delete()){
            $request->session()->flash('success','บันทึกสำเร็จ');
            $result = 'Yes';
        }else{
            $request->session()->flash('error','บันทึกไม่สำเร็จ');
            $result = 'No';
        }
        return response()->json(['success'=> $result]);
    }


    public function checkCitizen(Request $request){
        $keyword = $request->keyword;
        $citizen_id = Contractor::where('citizen_id',$keyword)->get();
        return response()->json(['success'=> true,
            'citizen' => $citizen_id,
            ]);
    }

    public function checkEnterprise(Request $request){
        $keyword = $request->keyword;
        $enterprise= Enterprise::where('name',$keyword)->get();
        return response()->json(['success'=> true,
            'enterprise' => $enterprise,
        ]);
    }

    public function getContractorAdmin(){

        $contractors = Contractor::select(DB::raw('DATEDIFF(contractors.safety_expired_date,contractors.safety_training_date) - DATEDIFF(DATE(now()),contractors.safety_training_date) as count_date'),
            'contractors.*','enterprises.name as enterprise_name')
            ->leftjoin('enterprises','enterprises.id' ,'=' ,'contractors.enterprise_id')
            ->get();

        return response()->json(["data"=>$contractors]);
    }

}
