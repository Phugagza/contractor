<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Contractor;
use App\Enterprise;
use App\Trainer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Training;
use App\trainingDetail;
use App\trainingDetailView;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminTrainingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function trainingIndex(){
        $trainings = Training::all();
        return view('admin.training.index',compact('trainings'));
    }

    public function trainingEdit(Request $request,$id=null){

        if ($id != null){
            $training = Training::find($id);
        }else{
            $training = new Training();
        }


        if ($request->isMethod('post')) {
            $training -> name = $request -> name;
            $training -> description = $request -> description;
            if ($training -> save()){
                $request->session()->flash("success","บันทึกข้อมูลสำเร็จ");
            }else{
                $request->session()->flash("error","บันทึกข้อมูลไม่สำเร็จ");
            }

            return redirect(route('admin.training.index'));
        }

        return view('admin.training.edit',compact('training'));
    }

    public function trainingDetail(Request $request,$id){
        $training_details = trainingDetailView::where('training_id',$id)->get();
        $trainers = Admin::where('level',2)->get();
        $training = Training::find($id);
        $contractors = Contractor::leftJoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                                   ->select('contractors.id as contractor_id',
                                            'contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                                            'contractors.last_name as contractor_last_name','enterprises.name as enterprise_name'
                                           )
                                   ->get();

        if ($request->isMethod('post')) {
            $checkTraining = trainingDetailView::where([['training_id',$id],['contractor_id',$request -> contractor_id]])->get();
           if (count($checkTraining) == 0 ){
                $expired_date = $request -> expired_date;
                $training_date = new Carbon($request -> training_date);
                $training_detail = new trainingDetail();
                $training_detail -> contractor_id = $request -> contractor_id;
                $training_detail -> trainer_id = $request -> trainer_id;
                $training_detail -> training_id = $id;
                $training_detail -> training_date = new Carbon($request -> training_date);

                if ($expired_date == "365"){
                    $cal_expired = $training_date -> addDays($request ->expired_date);
                }

                if ($expired_date == "another"){
                    $cal_expired = $training_date -> addDays($request ->expired_another);
                }

                $training_detail -> expired_date = $cal_expired;
                $training_detail -> permission = $request -> permission;
                if($training_detail -> save()){
                    $request->session()->flash("success","บันทึกข้อมูลสำเร็จ");
                }else{
                    $request->session()->flash("error","บันทึกข้อมูลไม่สำเร็จ");
                }
           }else{
               $request->session()->flash("warning","ผู้รับเหมาได้รับการอบรมเรียบร้อยแล้ว");
           }
            return redirect(route('admin.training.detail',['id'=> $id]));
        }

        return view('admin.training.detail',compact('contractors','training_details','training','trainers'));
    }

    public function trainerIndex(){
        $admins = Admin::where('level',2)->get();

        return view('admin.trainer.index',compact('admins'));
    }

    public function trainerEdit(Request $request,$id=null){

        if ($id != null){
            $admin= Admin::find($id);
        }else{
            $admin = new Admin();
        }

        if ($request->isMethod('post')) {
            $admin -> prefix = $request -> prefix;
            $admin -> first_name = $request -> first_name;
            $admin -> last_name = $request -> last_name;
            $admin -> email = $request -> email;
            $admin -> password = $request -> password != '' ? Hash::make($request -> password) : $request -> password_hidden;
            $admin -> level = 2;
            $admin -> status = 1;

            if($admin -> save()){
                $request->session()->flash("success","บันทึกข้อมูลสำเร็จ");
                return redirect(route('admin.trainer.index'));
            }else{
                $request->session()->flash("error","บันทึกข้อมูลไม่สำเร็จ");
                return redirect(route('admin.trainer.index'));
            }
        }

        return view('admin.trainer.edit',compact('admin'));
    }

    public function trainerDelete($id){
        $admin = Admin::find($id);
        if($admin -> delete()){
            session()->flash('success','บันทึกสำเร็จ');
            $result = 'Yes';
        }else{
            session()->flash('error','บันทึกไม่สำเร็จ');
            $result = 'No';
        }
        return response()->json(['success'=> $result]);
    }

    public function trainingDetailDelete(Request $request){
        $trainingDetail = trainingDetail::find($request -> id);
        if($trainingDetail -> delete()){
            $request->session()->flash('success','บันทึกสำเร็จ');
            $result = 'Yes';
        }else{
            $request->session()->flash('error','บันทึกไม่สำเร็จ');
            $result = 'No';
        }
        return response()->json(['success'=> $result]);
    }

    public function trainingDelete(Request $request){
        $training = Training::find($request -> id);
        if($training -> delete()){
            $trainingDetails = trainingDetail::where('training_id',$request -> id)->get();
            foreach ($trainingDetails as $trainingDetail){
                $trainingDetail -> delete();
            }
            $request->session()->flash('success','บันทึกสำเร็จ');
            $result = 'Yes';
        }else{
            $request->session()->flash('error','บันทึกไม่สำเร็จ');
            $result = 'No';
        }
        return response()->json(['success'=> $result]);
    }



    public function trainingDetailEdit(Request $request,$training_id,$training_detail_id){
        $training_detail = trainingDetail::where([['training_id',$training_id],['id',$training_detail_id]])->first();
        $training = Training::find($training_id);
        $contractor = Contractor::leftJoin('enterprises','enterprises.id','=','contractors.enterprise_id')
            ->where('contractors.id',$training_detail->contractor_id)
            ->select('contractors.id as contractor_id',
                'contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                'contractors.last_name as contractor_last_name','enterprises.name as enterprise_name'
            )
            ->first();
        if ($request->isMethod('post')) {
            $expired_date = $request -> expired_date;
            $training_date = new Carbon($request -> training_date);
            $training_detail -> training_date = new Carbon($request -> training_date);

            if ($expired_date == "365"){
                $cal_expired = $training_date -> addDays($request ->expired_date);
            }

            if ($expired_date == "another"){
                $cal_expired = $training_date -> addDays($request ->expired_another);
            }

            $training_detail -> expired_date = $cal_expired;
            $training_detail -> permission = $request -> permission;
            if($training_detail -> save()){
                $request->session()->flash("success","บันทึกข้อมูลสำเร็จ");
            }else{
                $request->session()->flash("error","บันทึกข้อมูลไม่สำเร็จ");
            }
            return redirect(route('admin.training.detail' ,['training_id' => $training_id]));
        }
        return view('admin.training.detailEdit',compact('training_detail','training','contractor'));
    }

    public function safetyTrainingIndex(){

        $contractors = Contractor::select(DB::raw('DATEDIFF(contractors.safety_expired_date,contractors.safety_training_date) - DATEDIFF(DATE(now()),contractors.safety_training_date) as count_date'),
            'contractors.*','enterprises.name as enterprise_name')
            ->leftjoin('enterprises','enterprises.id' ,'=' ,'contractors.enterprise_id')
            ->get();
        return view('admin.training.safetyTrainingIndex' ,compact('contractors'));
    }

    public function safetyTrainingEdit(Request $request ,$id=null){
        $trainers = Admin::where('level',2)->get();
        $contractor = Contractor::find($id);
        $enterprises = Enterprise::all();
        if ($request->isMethod('post')) {
            //safety training
            $expired_date = $request -> expired_date;
            $contractor -> permission = $request -> permission ? $request -> permission : "N";
            $safety_training_date = new Carbon($request -> training_date);
            $contractor -> safety_training_date = new Carbon($request -> training_date);
            $contractor -> trainer_id = $request -> trainer_id;
            if ($expired_date == "365"){
                $cal_expired = $safety_training_date -> addDays($request ->expired_date);
            }

            if ($expired_date == "another"){
                $cal_expired = $safety_training_date -> addDays($request ->expired_another);
            }

            $contractor -> safety_expired_date = $cal_expired;

            if($contractor -> save()){
                $request->session()->flash("success","บันทึกข้อมูลสำเร็จ");
                return redirect(route('admin.trainer.safetyTraining.index'));
            }else{
                $request->session()->flash("error","บันทึกข้อมูลไม่สำเร็จ");
                return redirect(route('admin.trainer.safetyTraining.index'));
            }

        }

        return view('admin.training.safetyTrainingEdit',compact('contractor','enterprises','trainers'));
    }
}
