<?php

namespace App\Http\Controllers\Admin;

use App\checkResultsView;
use App\Contractor;
use App\contractorCheckTaskDetail;
use App\Exports\TaskExport;
use App\Leader;
use App\leaderCheckTask;
use App\leaderCheckTaskDetail;
use App\Task;
use App\taskDetailsView;
use App\TasksView;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use PDF;
use App\taskDetail;
use Dompdf\Dompdf;

class AdminTaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function taskIndex(){
        $task_view_details = taskDetailsView::whereIn('task_detail_status',[2,3,4,5,6])->get();

        return view('admin.task.index',compact('task_view_details'));
    }

    public function taskAll(Request $request){

        $allRequest = $request->except('_method', '_token');
        $keyword = $request->keyword ? $request->keyword : '';
        $task_detail_status = $request->task_detail_status ? $request->task_detail_status :'';
        $date_start = $request->date_start ? Carbon::create($request->date_start) -> format('Y-m-d') :'';
        $date_end = $request->date_end ? Carbon::create($request->date_end) -> format('Y-m-d') :'';
        $perPageDefault = $request-> perPageDefault ? $request-> perPageDefault : 15;
        if($keyword != '' && $date_start == '' && $task_detail_status == ''){
            $tasks = DB::table('group_all_task_view')
                ->where(function ($query) use($keyword) {
                    $query->orwhere('task_title','like',"%".$keyword."%")
                        ->orWhere('task_location','like',"%".$keyword."%")
                        ->orWhere('contractor_first_name','like',"%".$keyword."%")
                        ->orWhere('leader_first_name','like',"%".$keyword."%");
                })
                ->orderBy('task_detail_id','desc')
                ->paginate($perPageDefault);
        }
        if($date_start != '' && $task_detail_status == '' || $date_end){
            $tasks = DB::table('group_all_task_view')
                ->whereBetween('task_working_date' ,[$date_start,$date_end])
                ->orwhereBetween('register_task_detail_date' ,[$date_start,$date_end])
                ->where(function ($query) use($keyword) {
                    $query->orwhere('task_title','like',"%".$keyword."%")
                        ->orWhere('task_location','like',"%".$keyword."%")
                        ->orWhere('contractor_first_name','like',"%".$keyword."%")
                        ->orWhere('leader_first_name','like',"%".$keyword."%");
                })
                ->orderBy('task_detail_id','desc')
                ->paginate($perPageDefault);
        }
        if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start == ''){
            $tasks = DB::table('group_all_task_view')
                ->where('task_detail_status' ,$task_detail_status)
                ->where(function ($query) use($keyword) {
                    $query->orwhere('task_title','like',"%".$keyword."%")
                          ->orWhere('task_location','like',"%".$keyword."%")
                          ->orWhere('contractor_first_name','like',"%".$keyword."%")
                          ->orWhere('leader_first_name','like',"%".$keyword."%");
                })
                ->orderBy('task_detail_id','desc')
                ->paginate($perPageDefault);
        }
        if ($task_detail_status != '' && ($task_detail_status != 'registered' && $task_detail_status != '5rule') && $date_start != ''){
            $tasks = DB::table('group_all_task_view')
                ->where('task_detail_status' ,$task_detail_status)
                ->whereBetween('register_task_detail_date' ,[$date_start,$date_end])
                ->where(function ($query) use($keyword) {
                    $query->orwhere('task_title','like',"%".$keyword."%")
                        ->orWhere('task_location','like',"%".$keyword."%")
                        ->orWhere('contractor_first_name','like',"%".$keyword."%")
                        ->orWhere('leader_first_name','like',"%".$keyword."%");
                })
                ->orderBy('task_detail_id','desc')
                ->paginate($perPageDefault);

        }

        if ($task_detail_status == 'registered' && $date_start == ''){
            $tasks = DB::table('group_all_task_view')
                ->where('task_type' ,null)
                ->where('task_detail_status' ,null)
                ->orderBy('task_id','desc')
                ->where(function ($query) use($keyword) {
                    $query->orwhere('task_title','like',"%".$keyword."%")
                        ->orWhere('task_location','like',"%".$keyword."%")
                        ->orWhere('contractor_first_name','like',"%".$keyword."%")
                        ->orWhere('leader_first_name','like',"%".$keyword."%");
                })
                ->paginate($perPageDefault);
        }
        if ($task_detail_status == 'registered' && $date_start != ''){
            $tasks = DB::table('group_all_task_view')
                ->where('task_type' ,null)
                ->where('task_detail_status' ,null)
                ->whereBetween('task_working_date' ,[$date_start,$date_end])
                ->where(function ($query) use($keyword) {
                    $query->orwhere('task_title','like',"%".$keyword."%")
                        ->orWhere('task_location','like',"%".$keyword."%")
                        ->orWhere('contractor_first_name','like',"%".$keyword."%")
                        ->orWhere('leader_first_name','like',"%".$keyword."%");
                })
                ->orderBy('task_detail_id','desc')
                ->paginate($perPageDefault);
        }
        if ($task_detail_status == '5rule' && $date_start == ''){
            $tasks = DB::table('group_all_task_view')
                -> where('task_type' ,'<>',null)
                -> where('task_detail_status' ,null)
                ->where(function ($query) use($keyword) {
                    $query->orwhere('task_title','like',"%".$keyword."%")
                        ->orWhere('task_location','like',"%".$keyword."%")
                        ->orWhere('contractor_first_name','like',"%".$keyword."%")
                        ->orWhere('leader_first_name','like',"%".$keyword."%");
                })
                ->orderBy('task_detail_id','desc')
                ->paginate($perPageDefault);
        }
        if ($task_detail_status == '5rule' && $date_start != ''){
            $tasks = DB::table('group_all_task_view')
                -> where('task_type' ,'<>',null)
                -> where('task_detail_status' ,null)
                ->whereBetween('task_working_date' ,[$date_start,$date_end])
                ->where(function ($query) use($keyword) {
                    $query->orwhere('task_title','like',"%".$keyword."%")
                        ->orWhere('task_location','like',"%".$keyword."%")
                        ->orWhere('contractor_first_name','like',"%".$keyword."%")
                        ->orWhere('leader_first_name','like',"%".$keyword."%");
                })
                ->orderBy('task_detail_id','desc')
                ->paginate($perPageDefault);
        }
        if($task_detail_status == '' && $date_start =='' && $date_end =='' && $keyword == ''){
            $tasks = DB::table('group_all_task_view')
                ->orderBy('task_detail_id','desc')
                ->paginate($perPageDefault);
        }
        return view('admin.task.allTask',compact('tasks','allRequest','perPageDefault','keyword','task_detail_status','date_start','date_end'));

    }

    public function taskDetail($id){
        $taskDetails = taskDetailsView::where([
            ['task_detail_task_id',$id],
        ])
            ->get();
        $getTask = TasksView::where('task_id',$id)->first();
        $contractorDetails = Task::join('contractors','contractors.id','=','tasks.contractor_id')
            ->where('tasks.id',$id)
            ->first();

        $subcontractors = Contractor::wherein('id',explode(",",$contractorDetails -> subcontractor_id))->get();

        $taskDetails_total = taskDetailsView::select(DB::raw('count(*) as sum_of_work'),
            DB::raw('sum(if(task_detail_status = 7 or task_detail_status  = 8 ,floor(timestampdiff(hour,task_detail_start_time,task_detail_end_time) /24),0)) as sum_total_day'),
            DB::raw("sum(if(task_detail_status = 7 or task_detail_status  = 8 ,hour(timediff(date_format(task_detail_start_time,'%H:%i'),date_format(task_detail_end_time,'%H:%i'))) %24 ,0)) as sum_total_hour"),
            DB::raw("sum(if(task_detail_status = 7 or task_detail_status  = 8 ,minute(timediff(date_format(task_detail_start_time,'%H:%i'),date_format(task_detail_end_time,'%H:%i'))),0) %60) as sum_total_minute"),
            DB::raw('sum(if(task_detail_status = 7,1,0)) as sum_success'),
            DB::raw('sum(if(task_detail_status = 8,1,0)) as sum_fail')
        )
            ->where([
                ['task_detail_task_id',$id],
            ])
            ->first();
//        dd($taskDetails_total);
        return view('admin.task.detailTask',compact('taskDetails','taskDetails_total','contractorDetails','subcontractors','getTask'));
    }

    public function resultTaskDetail($task_id,$task_detail_id){
        $checkResultView = checkResultsView::where([['task_detail_id',$task_detail_id],['task_id',$task_id]])->get();
        $subcontractors = Contractor::wherein('id',explode(",",$checkResultView[0] -> subcontractor_id))->get();
        $assistant_subcontractors = Contractor::wherein('id',explode(",",$checkResultView[0] -> assistant_subcontractor_id))->get();
        $getTask = TasksView::where('task_id',$task_id)->first();
        $contractors = Contractor::where('id',$checkResultView[0] ->contractor_id)->get();
        return view('admin.task.resultTaskDetail',compact('checkResultView','subcontractors','getTask','contractors','assistant_subcontractors'));
    }

    public function resultTaskDetailPDF($task_id,$task_detail_id){
        $checkResultView = checkResultsView::where([['task_detail_id',$task_detail_id],['task_id',$task_id]])->get();
        $leader = Leader::where('id',$checkResultView[0]->leader_id)->first();
        $subcontractors = Contractor::wherein('id',explode(",",$checkResultView[0] -> subcontractor_id))->get();
        $assistant_subcontractors = Contractor::wherein('id',explode(",",$checkResultView[0] -> assistant_subcontractor_id))->get();
        $contractors = Contractor::where('id',$checkResultView[0] ->contractor_id)->get();
        $pdf = PDF::loadView('admin.task.resultTaskDetailPDF',
                    ['checkResultView' =>  $checkResultView,'subcontractors' => $subcontractors,'contractors' => $contractors,
                     'task_detail_id'=>$task_detail_id,'leader'=>$leader,'assistant_subcontractors'=>$assistant_subcontractors]);
        $pdf->getDomPDF()->set_option("enable_php", true);
        return @$pdf->download("{$checkResultView[0] -> task_title}(รายงานสรุป NO.$task_detail_id).pdf");

//        return view('admin.task.resultTaskDetailPDF',compact('checkResultView','subcontractors','contractors','task_detail_id','leader'));
    }

    public function taskDelete(Request $request){
        $task = Task::find($request -> id);
        if($task -> delete()){
        $leaderCheckTasks = leaderCheckTask::where('task_id',$task-> id)->get();
            if(count($leaderCheckTasks) >= 1){
                foreach ($leaderCheckTasks as $leaderCheckTask){
                    $leaderCheckTask -> delete();
                }
            }
        $taskDetails = taskDetail::where('task_id',$request -> id)->get();
                if(count($taskDetails) >= 1){
                    foreach ($taskDetails as $taskDetail){

                        $contractorCheckTaskDetails =contractorCheckTaskDetail::where('task_detail_id',$taskDetail->id)->get();
                        if(count($contractorCheckTaskDetails) >= 1){
                            foreach ($contractorCheckTaskDetails as $contractorCheckTaskDetail){
                                $contractorCheckTaskDetail -> delete();
                            }
                        }

                        $leaderCheckTaskDetails =leaderCheckTaskDetail::where('task_detail_id',$taskDetail->id)->get();
                        if (count($leaderCheckTaskDetails) >= 1){
                            foreach ($leaderCheckTaskDetails as $leaderCheckTaskDetail){
                                $leaderCheckTaskDetail -> delete();
                            }
                        }
                        $taskDetail -> delete();
                    }
                 }


            $request->session()->flash('success','บันทึกสำเร็จ');
            $result = 'Yes';
        }else{
            $request->session()->flash('error','บันทึกไม่สำเร็จ');
            $result = 'No';
        }
        return response()->json(['success'=> $result]);
    }

    public function taskEdit(Request $request,$id){
        $leaders = Leader::all();
        $contractors = Contractor::leftJoin('enterprises','enterprises.id','=','contractors.enterprise_id')
            ->select('contractors.id as id',
                'contractors.prefix as prefix','contractors.first_name as first_name',
                'contractors.last_name as last_name','enterprises.name as enterprise_name'
            )
            ->get();
        $task = Task::where('id',$id)->first();
        $multiSelect = explode(',',$task -> subcontractor_id);

        if ($request->isMethod('post')) {
            $task -> leader_id = $request -> leader_id;
            $task -> contractor_id = $request -> contractor_id;
            $task -> subcontractor_id = $request -> subcontractor_id ? implode(",",$request -> subcontractor_id) : null;
            $task -> title = $request -> title;
            $task -> location = $request -> location;
            $task -> description = $request -> description;
            $task -> working_date =  new Carbon($request -> working_date);
            $task -> status = 1;

            if($task -> save()){
                $request->session()->flash("success","บันทึกข้อมูลสำเร็จ");
                return redirect(route('admin.task.allTask'));
            }else{
                $request->session()->flash("error","บันทึกข้อมูลไม่สำเร็จ");
                return redirect(route('admin.task.allTask'));
            }


        }

        return view('admin.task.editTask',compact('task','contractors','leaders','multiSelect'));
    }

    public function changeStatusTask($task_id){
        $task = Task::find($task_id);
        $task -> status = $task -> status == 2 ? 1 : 2;
        if($task -> save()){
            session()->flash('success','บันทึกสำเร็จ');
            return redirect(route('admin.task.allTask'));
        }else{
            session()->flash('error','บันทึกไม่สำเร็จ');
            return redirect(route('admin.task.allTask'));
        }
    }

    public function taskFinish(Request $request){
        $task_detail_id = $request -> task_detail_id;

        $checkContractorCheck1 = contractorCheckTaskDetail::where([
            ['task_detail_id',$task_detail_id],
            ['time',1]
        ])
            ->first();
        if ($checkContractorCheck1 == null){
            $ContractorCheck1 = new contractorCheckTaskDetail();
            $ContractorCheck1 -> task_detail_id = $task_detail_id;
            $ContractorCheck1 -> time = 1;
            $ContractorCheck1 -> check_time = Carbon::now();
            $ContractorCheck1 -> save();
        }
        $checkContractorCheck2 = contractorCheckTaskDetail::where([
            ['task_detail_id',$task_detail_id],
            ['time',2]
        ])
            ->first();
        if ($checkContractorCheck2 == null){
            $ContractorCheck2 = new contractorCheckTaskDetail();
            $ContractorCheck2 -> task_detail_id = $task_detail_id;
            $ContractorCheck2 -> time = 2;
            $ContractorCheck2 -> check_time = Carbon::now();
            $ContractorCheck2 -> save();
        }
        $checkLeaderCheck = leaderCheckTaskDetail::where('task_detail_id',$task_detail_id)->first();

        if ($checkLeaderCheck == null){
            $checkLeaderCheck = new leaderCheckTaskDetail();
            $checkLeaderCheck -> task_detail_id = $task_detail_id;;
            $checkLeaderCheck ->save();
        }

        $task_detail = taskDetail::find($task_detail_id);
        $task_detail -> start_time = Carbon::now();
        $task_detail -> end_time = Carbon::now();
        $task_detail -> status = 7;
        if($task_detail -> save()){
            $request->session()->flash("success","ปิดงานโดยแอดมินสำเร็จ");
            $result = 'Yes';
        }else{
            $request->session()->flash("error","ปิดงานโดยแอดมินไม่สำเร็จ");
            $result = 'No';
        }

        return response()->json(['success'=> $result]);
    }

    public function taskAllQRcode($task_id){
        $task = DB::table('group_all_task_view')->where('task_id',$task_id)->first();

        if(empty($task)){
            session()->flash("warning","ไม่พบข้อมูล");
            return redirect(route('admin.task.allTask'));
        }

        return view('admin.task.taskQRCode',compact('task'));

    }

    public function allTaskJson(){
     $tasks = DB::table('group_all_task_view')->get();
     return response()->json(["data"=>$tasks]);
    }

}
