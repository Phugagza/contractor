<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Leader;
use Illuminate\Support\Facades\Hash;

class AdminLeaderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function leaderIndex(){
        $leaders = Leader::all();
        return view('admin.leader.index' ,compact('leaders'));
    }

    public function leaderEdit(Request $request , $id=null){

        if ($id != null){
            $leader = Leader::find($id);
        }else{
            $leader = new Leader();
        }


        if ($request->isMethod('post')) {
            $leader -> prefix = $request -> prefix;
            $leader -> first_name = $request -> first_name;
            $leader -> last_name = $request -> last_name;
            $leader -> email = $request -> email;
            $leader -> password = $request -> password != '' ? Hash::make($request -> password) : $request -> password_hidden;
            $leader -> line_token = $request -> line_token;
            $leader -> tel = $request -> tel;
            $leader -> status = 1;

            if($leader -> save()){
                $request->session()->flash("success","บันทึกข้อมูลสำเร็จ");
                return redirect(route('admin.leader.index'));
            }else{
                $request->session()->flash("error","บันทึกข้อมูลไม่สำเร็จ");
                return redirect(route('admin.leader.index'));
            }

        }
        return view('admin.leader.edit' ,compact('leader'));
    }

    public function leaderDelete(Request $request){
        $leader = Leader::find($request -> id);
        if($leader -> delete()){
            $request->session()->flash('success','บันทึกสำเร็จ');
            $result = 'Yes';
        }else{
            $request->session()->flash('error','บันทึกไม่สำเร็จ');
            $result = 'No';
        }
        return response()->json(['success'=> $result]);
    }

    public function checkEmailLeader(Request $request){
        $keyword = $request->keyword;
        $email= Leader::where('email',$keyword)->get();
        return response()->json(['success'=> true,
            'email' => $email,
        ]);
    }
}
