<?php

namespace App\Http\Controllers;

use App\taskDetail;
use App\taskDetailsView;
use App\trainingDetail;
use App\trainingDetailView;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Leader;
use App\Contractor;
use App\Task;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendQR;
use App\TasksView;
use PDF;
use App\leaderCheckTask;
use App\contractorCheckTaskDetail;
use App\leaderCheckTaskDetail;
use App\Training;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $subcontractor_N = array();
        $contractor_N = array();
        $assistant_subcontractor_N = array();
        $task_type = 0;
        return view('home',compact('subcontractor_N','contractor_N','assistant_subcontractor_N','task_type'));
    }

    public function index_task_type()
    {
        $subcontractor_N = array();
        $contractor_N = array();
        $assistant_subcontractor_N = array();
        $task_type = 0;
        return view('home_v2',compact('subcontractor_N','contractor_N','assistant_subcontractor_N','task_type'));
    }

    public function register(Request $request){
        if ($request->isMethod('post')){
            $subcontractor_N = array();
            $contractor_N = array();
            if ($request -> subcontractor_id != null){
                $subcontractor_N = Contractor::whereIn('id',$request -> subcontractor_id)
                    ->where('permission',"N")
                    ->get();
            }
            $contractor_N = Contractor::where([['id',$request -> contractor_id],['permission',"N"]])->get();

            if(count($subcontractor_N) == 0 && count($contractor_N) == 0){
                $register_date = Carbon::now();
                $working_date = new Carbon($request -> working_date);
                //new task
                $task = new Task();
                $task -> leader_id = $request -> leader_id;
                $task -> contractor_id = $request -> contractor_id;
                $task -> subcontractor_id = $request -> subcontractor_id ? implode(",",$request -> subcontractor_id) : null;
                $task -> title = $request -> title;
                $task -> description = $request -> description;
                $task -> location = $request -> location;
                $task -> register_date = $register_date;
                $task -> working_date =  $working_date;
                $task -> status = 1;

                if($task -> save()){
                    //new check form on this task
                    $leader_check_task = new leaderCheckTask();
                    $leader_check_task -> task_id = $task -> id;
                    if($leader_check_task -> save()){
                        $taskDetail = tasksView::where('task_id',$task -> id)->first();
                        $checkRoute = route('leader.task.check',['task_id' => $task -> id]);
                        $task_working_date = ConvertThaiDateLine($task -> working_date);
                        $leader = Leader::where('id',$task -> leader_id)->first();
                        $data =[$leader -> line_token,"\n                  ผู้รับเหมาลงทะเบียน\nหัวข้อ : {$taskDetail -> task_title}\nหัวหน้าผู้รับเหมา : {$taskDetail -> contractor_prefix}{$taskDetail -> contractor_first_name} {$taskDetail -> contractor_last_name} ({$taskDetail -> enterprise_name})\nผู้ควบคุมงาน : {$leader -> prefix}{$leader -> first_name} {$leader -> last_name}\nวันที่จะเข้าทำงาน : {$task_working_date}\nกรุณาตรวจสอบ Check List : {$checkRoute}"];
                        lineNotify($data);
                        Mail::to($taskDetail -> contractor_email)->send(
                            new sendQR($taskDetail)
                        );
                        $request->session()->flash('success','ลงทะเบียนสำเร็จ กรุณาตรวจสอบ QR Code ในอีเมล์ของท่าน เพื่อนำไปสแกนในวันเข้างาน');
                        return redirect(route('resultPage'));
                    }
                }else{
                    $request->session()->flash('error','ลงทะเบียนไม่สำเร็จ');
                    return redirect(route('resultPage'));
                }
            }
            else{
                $request->session()->flash('checkPermission',$contractor_N,$subcontractor_N);
                $leaders = Leader::all();
                $contractors = Contractor::leftJoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('contractors.id as id',
                        'contractors.prefix as prefix','contractors.first_name as first_name',
                        'contractors.last_name as last_name','enterprises.name as enterprise_name'
                    )
                    ->get();
                return view('home',compact('leaders','contractors','contractor_N','subcontractor_N'));
            }

        }

    }

    public function register_task_type(Request $request){
        if ($request->isMethod('post')){
            $task_type =  $request->task_type;
            $assistant_subcontractor_N = array();
            $subcontractor_N = array();
            $contractor_N = array();
            if($task_type == 1){
                $contractor_N = Contractor::where("id",$request -> contractor_id)
                    ->where('permission','N')
                    ->get();

                if ($request -> subcontractor_id != null){
                    $subcontractor_N = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where(function ($query) {
                            $query->where('permission','N')
                                ->orWhere('indoor_electricity_permission','N');
                        })
                        ->get();
                }
                if ($request -> assistant_subcontractor_id != null){
                    $assistant_subcontractor_N = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where('permission',"N")
                        ->get();
                }
            }

            if($task_type == 2){
                $contractor_N = Contractor::where("id",$request -> contractor_id)
                    ->where('permission','N')
                    ->get();

                if ($request -> subcontractor_id != null){
                    $subcontractor_N = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where(function ($query) {
                            $query->where('permission','N')
                                ->orWhere('indoor_air_conditioner_permission','N');
                        })
                        ->get();
                }
                if ($request -> assistant_subcontractor_id != null){
                    $assistant_subcontractor_N = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where('permission',"N")
                        ->get();
                }
            }

            if($task_type == 3){
                $contractor_N = Contractor::where("id",$request -> contractor_id)
                    ->where('permission','N')
                    ->get();

                if ($request -> subcontractor_id != null){
                    $subcontractor_check_N1 = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where('permission','N')
                        ->get();
                    $subcontractor_check_N2 = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where('permission','Y')
                        ->where(function ($query) {
                            $query->Where('metal_arc_permission','N')
                                ->Where('tig_welding_permission','N')
                                ->Where('alloy_welding_permission','N');
                        })
                        ->get();

                    if(count($subcontractor_check_N1) >= 1 || count($subcontractor_check_N2) >= 1){
                        $subcontractor_N = $subcontractor_check_N1->merge($subcontractor_check_N2);
                    }

                }
                if ($request -> assistant_subcontractor_id != null){
                    $assistant_subcontractor_N = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where('permission',"N")
                        ->get();
                }
            }

            if($task_type == 4){
                $contractor_N = Contractor::where("id",$request -> contractor_id)
                    ->where('permission','N')
                    ->get();

                if ($request -> subcontractor_id != null){
                    $subcontractor_N = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where(function ($query) {
                            $query->where('permission','N')
                                ->orWhere('scaffolding_permission','N');
                        })
                        ->get();
                }
                if ($request -> assistant_subcontractor_id != null){
                    $assistant_subcontractor_N = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where('permission',"N")
                        ->get();
                }
            }

            if($task_type == 5){
                $contractor_N = Contractor::where("id",$request -> contractor_id)
                    ->where('permission','N')
                    ->get();

                if ($request -> subcontractor_id != null){
                    $subcontractor_check_N1 = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where('permission','N')
                        ->get();
                    $subcontractor_check_N2 = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where('permission','Y')
                        ->where(function ($query) {
                            $query->Where('worker_confine_permission','N')
                                ->Where('assistant_confine_permission','N')
                                ->Where('controller_confine_permission','N');
                        })
                        ->get();

                    if(count($subcontractor_check_N1) >= 1 || count($subcontractor_check_N2) >= 1){
                        $subcontractor_N = $subcontractor_check_N1->merge($subcontractor_check_N2);
                    }

                }
                if ($request -> assistant_subcontractor_id != null){
                    $assistant_subcontractor_N = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where('permission',"N")
                        ->get();
                }

            }

            if($task_type == 6){
                $contractor_N = Contractor::where("id",$request -> contractor_id)
                    ->where('permission','N')
                    ->get();

                if ($request -> subcontractor_id != null){
                    $subcontractor_N = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where('permission',"N")
                        ->get();
                }
                if ($request -> assistant_subcontractor_id != null){
                    $assistant_subcontractor_N = Contractor::whereIn('id',$request -> subcontractor_id)
                        ->where('permission',"N")
                        ->get();
                }
            }

            if(count($subcontractor_N) == 0 && count($contractor_N) == 0 && count($assistant_subcontractor_N) == 0){
                $register_date = Carbon::now();
                $working_date = new Carbon($request -> working_date);
                //new task
                $task = new Task();
                $task -> leader_id = $request -> leader_id;
                $task -> contractor_id = $request -> contractor_id;
                $task -> subcontractor_id = $request -> subcontractor_id ? implode(",",$request -> subcontractor_id) : null;
                $task -> assistant_subcontractor_id = $request -> assistant_subcontractor_id ? implode(",",$request -> assistant_subcontractor_id) : null;
                $task -> title = $request -> title;
                $task -> description = $request -> description;
                $task -> location = $request -> location;
                $task -> type = $request -> task_type;
                $task -> register_date = $register_date;
                $task -> working_date =  $working_date;
                $task -> status = 1;

                if($task -> save()){
                    //new check form on this task
                    $leader_check_task = new leaderCheckTask();
                    $leader_check_task -> task_id = $task -> id;
                    $leader_check_task -> electric_check1 = $request->electric_check == 1 ? 1 : null;
                    $leader_check_task -> electric_check2 =  $request->electric_check == 2 ? 1 : null;
                    $leader_check_task -> equipment1 = $request->equipment1;
                    $leader_check_task -> equipment2 = $request->equipment2;
                    $leader_check_task -> equipment3 = $request->equipment3;
                    $leader_check_task -> equipment4 = $request->equipment4;
                    $leader_check_task -> equipment5 = $request->equipment5;
                    $leader_check_task -> equipment6 = $request->equipment6;
                    $leader_check_task -> equipment7 = $request->equipment7;
                    $leader_check_task -> equipment8 = $request->equipment8;
                    $leader_check_task -> equipment9 = $request->equipment9;
                    $leader_check_task -> equipment10 = $request->equipment10;
                    $leader_check_task -> amount1 = $request->amount1;
                    $leader_check_task -> amount2 = $request->amount2;
                    $leader_check_task -> amount3 = $request->amount3;
                    $leader_check_task -> amount4 = $request->amount4;
                    $leader_check_task -> amount5 = $request->amount5;
                    $leader_check_task -> amount6 = $request->amount6;
                    $leader_check_task -> amount7 = $request->amount7;
                    $leader_check_task -> amount8 = $request->amount8;
                    $leader_check_task -> amount9 = $request->amount9;
                    $leader_check_task -> amount10 = $request->amount10;
                    $leader_check_task -> unit1 = $request->unit1;
                    $leader_check_task -> unit2 = $request->unit2;
                    $leader_check_task -> unit3 = $request->unit3;
                    $leader_check_task -> unit4 = $request->unit4;
                    $leader_check_task -> unit5 = $request->unit5;
                    $leader_check_task -> unit6 = $request->unit6;
                    $leader_check_task -> unit7 = $request->unit7;
                    $leader_check_task -> unit8 = $request->unit8;
                    $leader_check_task -> unit9 = $request->unit9;
                    $leader_check_task -> unit10 = $request->unit10;
                    $leader_check_task -> permission1 = $request->permission1;
                    $leader_check_task -> permission2 = $request->permission2;
                    $leader_check_task -> permission3 = $request->permission3;
                    $leader_check_task -> permission4 = $request->permission4;
                    $leader_check_task -> permission5 = $request->permission5;
                    $leader_check_task -> permission6 = $request->permission6;
                    $leader_check_task -> permission7 = $request->permission7;
                    $leader_check_task -> permission8 = $request->permission8;
                    $leader_check_task -> permission9 = $request->permission9;
                    $leader_check_task -> permission10 = $request->permission10;
                    if($leader_check_task -> save()){
                        $taskDetail = tasksView::where('task_id',$task -> id)->first();
                        $checkRoute = route('leader.task.check',['task_id' => $task -> id]);
                        $task_working_date = ConvertThaiDateLine($task -> working_date);
                        $leader = Leader::where('id',$task -> leader_id)->first();
                        $data =[$leader -> line_token,"\n                  ผู้รับเหมาลงทะเบียน\nหัวข้อ : {$taskDetail -> task_title}\nหัวหน้าผู้รับเหมา : {$taskDetail -> contractor_prefix}{$taskDetail -> contractor_first_name} {$taskDetail -> contractor_last_name} ({$taskDetail -> enterprise_name})\nผู้ควบคุมงาน : {$leader -> prefix}{$leader -> first_name} {$leader -> last_name}\nวันที่จะเข้าทำงาน : {$task_working_date}\nกรุณาตรวจสอบ Check List : {$checkRoute}"];
                        lineNotify($data);
                        Mail::to($taskDetail -> contractor_email)->send(
                            new sendQR($taskDetail)
                        );
                        $request->session()->flash('success','ลงทะเบียนสำเร็จ กรุณาตรวจสอบ QR Code ในอีเมล์ของท่าน เพื่อนำไปสแกนในวันเข้างาน');
                        return redirect(route('resultPage'));
                    }
                }else{
                    $request->session()->flash('error','ลงทะเบียนไม่สำเร็จ');
                    return redirect(route('resultPage'));
                }
            }
            else{
                $request->session()->flash('checkPermission',$contractor_N,$subcontractor_N,$assistant_subcontractor_N,$task_type);
                return view('home_v2',compact('contractor_N','subcontractor_N','assistant_subcontractor_N','task_type'));
            }

        }

    }

    public function openTask($task_id){
      $getTask = Task::find($task_id);
     if ($getTask != null){
         $getleaderCheckTask = leaderCheckTask::where('task_id',$task_id)->first();
         $getTaskDetailDate = taskDetail::select(DB::raw('max(date) as recent_date'))->where([['task_id',$task_id]])->first();
         $recent_date_task_detail = $getTaskDetailDate -> recent_date;
         // sent notify to workcontroller
         if($getleaderCheckTask -> type1 == "" || $getleaderCheckTask -> type1 == null){
             $taskDetail = tasksView::where('task_id',$task_id)->first();
             $checkRoute = route('leader.task.check',['task_id' => $task_id]);
             $leader = Leader::where('id',$getTask -> leader_id)->first();
             $data =[$leader -> line_token,"\n             ผู้รับเหมาไม่สามารถเริ่มงานได้ กรุณาเลือกประเภทงาน\nหัวข้อ : {$taskDetail -> task_title}\nหัวหน้าผู้รับเหมา : {$taskDetail -> contractor_prefix}{$taskDetail -> contractor_first_name} {$taskDetail -> contractor_last_name} ({$taskDetail -> enterprise_name})\nผู้ควบคุมงาน : {$leader -> prefix}{$leader -> first_name} {$leader -> last_name}\nกรุณาตรวจสอบ Check List : {$checkRoute}"];
             lineNotify($data);
             session()->flash('warning','ไม่สามารถเริ่มงานได้ โปรดรอผู้ควบคุมงาน ประเมินความเสียงก่อน');
             return redirect(route('resultPage'));
             //check open task once time only
         }elseif ($recent_date_task_detail != null){
             session()->flash('warning','ไม่สามารถเริ่มงานได้ เนื่องจาก QR Code สำหรับการลงทะเบียนนี้สามารถสแกนเข้างานได้เพียงหนึ่งครั้งเท่านั้น');
             return redirect(route('resultPage'));
         }else{
             $checkStatusTask = Task::find($task_id);
             if($checkStatusTask -> status != 2){
                 $taskDetail = new taskDetail();
                 $taskDetail -> task_id = $task_id;
                 $taskDetail -> status = 1;
                 $taskDetail -> date = Carbon::now();
                 if($taskDetail -> save()){
                     $confirmRoute = route('leader.task.confirm.index');
                     $TaskDetail = tasksView::where('task_id',$task_id)->first();
                     $task_scan_date = ConvertThaiDateLine(Carbon::now());
                     $leader = Leader::where('id',$TaskDetail -> leader_id)->first();
                     $checkType = leaderCheckTask::where('task_id',$task_id)->first();
                     if ($checkType -> type1 == 2 || $checkType -> type1 == 3 || $checkType -> type1 == 4 || $checkType -> type1 == 5){
                         $data =[$leader -> line_token,"\n                  ผู้รับเหมาสแกนเข้างาน (งานที่ต้องประเมินความเสี่ยง)\nหัวข้อ : {$TaskDetail -> task_title}\nผู้ควบคุมงาน : {$leader -> prefix}{$leader -> first_name} {$leader -> last_name}\nหัวหน้าผู้รับเหมา : {$TaskDetail -> contractor_prefix}{$TaskDetail -> contractor_first_name} {$TaskDetail -> contractor_last_name} ({$TaskDetail -> enterprise_name})\nสแกนวันที่ : {$task_scan_date}\nยืนยัน : {$confirmRoute}"];
                     }else{
                         $data =[$leader -> line_token,"\n                  ผู้รับเหมาสแกนเข้างาน\nหัวข้อ : {$TaskDetail -> task_title}\nผู้ควบคุมงาน : {$leader -> first_name} {$leader -> last_name}\nหัวหน้าผู้รับเหมา : {$TaskDetail -> contractor_prefix}{$TaskDetail -> contractor_first_name} {$TaskDetail -> contractor_last_name} ({$TaskDetail -> enterprise_name})\nสแกนวันที่ : {$task_scan_date}\nยืนยัน : {$confirmRoute}"];
                     }
                     $getStatus = lineNotify($data);
                     if($getStatus == 200){
                         session()->flash('success','สแกนสำเร็จ รอผู้ควบคุมงานอนุมัติเข้างาน');
                         return redirect(route('resultPage'));
                     }else{
                         session()->flash('error',"การส่ง Line แจ้งเตือนไม่สำเร็จ ({$getStatus})");
                         return redirect(route('resultPage'));
                     }
                 }
             }else{
                 session()->flash('warning','ไม่สามารถสแกนได้ เนื่องจากงานของคุณถูกระงับ');
                 return redirect(route('resultPage'));
             }
         }
     }else{
         return response()->view('errors.error', ['status' => '404','message' => 'ไม่พบหน้าที่ท่านต้องการ'], 404);
     }

    }

    public function workpermit($taskDetail_id)
    {
        $getTaskDetailNull = taskDetail::find($taskDetail_id);
        if ($getTaskDetailNull == null || $getTaskDetailNull == '') {
            return response()->view('errors.error', ['status' => '404','message' => 'ไม่พบหน้าที่ท่านต้องการ'], 404);
        } else {
            $taskDetail = taskDetail::find($taskDetail_id);
            $taskDetailView = taskDetailsView::where('task_detail_id',$taskDetail -> id)->first();

            //check cancel or unfinish task
            if($taskDetail -> status != 8 || $taskDetail -> status != 9){
                //start task
                if($taskDetail -> status == 2){
                    $taskDetail -> start_time = Carbon::now();
                    $taskDetail -> status = 3;
                    if($taskDetail -> save()){
                        session()->flash('openWorkpermit',"เริ่มทำงานได้\n - Tool Box Meeting\n- ผรม.สแกน QR Code บนใบ Work Permit เพื่อตรวจสอบความปลอดภัยร่วมครั้งที่ 1\n- ผรม.สแกน QR Code บนใบ Work Permit เพื่อตรวจสอบความปลอดภัยร่วมครั้งที่ 2\n- ผรม.สแกน QR Code ครั้งที่ 3 เพื่อแจ้งตรวจสอบปิดงานร่วมกัน\n");
                    }else{
                        session()->flash('error','สแกนไม่สำเร็จ กรุณาสแกนใหม่');
                    }
                 //check1
                }else if($taskDetail -> status == 3){

                        $contractorCheckTaskDetail = contractorCheckTaskDetail::where([
                                                                                        ['task_detail_id',$taskDetail -> id],
                                                                                        ['time',1]
                                                                                      ])
                                                                                ->first();
                        if ($contractorCheckTaskDetail == null){
                            $contractorCheckTaskDetail = new contractorCheckTaskDetail();
                            $contractorCheckTaskDetail -> task_detail_id = $taskDetail -> id;
                            $contractorCheckTaskDetail -> time = 1;
                            if($contractorCheckTaskDetail -> save()){
                                return redirect(route('contractor.checkTaskDetail',['id' => $contractorCheckTaskDetail -> id]));
                            }
                        }else{
                            return redirect(route('contractor.checkTaskDetail',['id' => $contractorCheckTaskDetail -> id]));
                        }
                //check2
                }else if($taskDetail -> status == 4){
                             $contractorCheckTaskDetail = contractorCheckTaskDetail::where([
                                                                                             ['task_detail_id',$taskDetail -> id],
                                                                                             ['time',2]
                                                                                            ])
                                                                                     ->first();
                             if ($contractorCheckTaskDetail == null) {
                                 $contractorCheckTaskDetail = new contractorCheckTaskDetail();
                                 $contractorCheckTaskDetail->task_detail_id = $taskDetail->id;
                                 $contractorCheckTaskDetail->time = 2;
                                 if ($contractorCheckTaskDetail->save()) {
                                     return redirect(route('contractor.checkTaskDetail', ['id' => $contractorCheckTaskDetail->id]));
                                 }
                             }else{
                                return redirect(route('contractor.checkTaskDetail',['id' => $contractorCheckTaskDetail -> id]));
                             }
                         //leader check
                }else if ($taskDetail -> status == 5){
                    $taskDetail -> status = 6;
                    if($taskDetail -> save()) {
                        $leaderCheckTaskDetail = new leaderCheckTaskDetail();
                        $leaderCheckTaskDetail -> task_detail_id = $taskDetail -> id;
                        if($leaderCheckTaskDetail ->save()){
                            $finishRoute = route('leader.task.finish.index');
                            $data = [$taskDetailView->leader_line_token, "\nเรียนผู้ควบคุมงาน  : {$taskDetailView -> leader_firstname} {$taskDetailView -> leader_lastname}\nกรุณาตรวจสอบหลังงานเสร็จ : {$taskDetailView -> task_title}\nของหัวหน้าผู้รับเหมา : {$taskDetailView -> contractor_firstname} {$taskDetailView -> contractor_lastname} ({$taskDetailView -> name})\nตรวจสอบ : {$finishRoute}"];
                            $getStatus = lineNotify($data);
                            if ($getStatus == 200) {
                                session()->flash('success', 'ระบบได้แจ้งผู้ควบคุมงานเรียบร้อยแล้ว เพื่อตรวจสอบปิดงานร่วมกัน กรุณารอสักครู่...');
                            } else {
                                session()->flash('error', "การส่ง Line แจ้งเตือนไม่สำเร็จ ({$getStatus})");
                            }
                        }
                    }
                }else if($taskDetail -> status == 6){
                    $finishRoute = route('leader.task.finish.index');
                    $data = [$taskDetailView->leader_line_token, "\nเรียนผู้ควบคุมงาน : {$taskDetailView -> leader_firstname} {$taskDetailView -> leader_lastname}\nกรุณาตรวจสอบหลังงานเสร็จ : {$taskDetailView -> task_title}\nของหัวหน้าผู้รับเหมา : {$taskDetailView -> contractor_firstname} {$taskDetailView -> contractor_lastname} ({$taskDetailView -> name})\nตรวจสอบ : {$finishRoute}"];
                    $getStatus = lineNotify($data);
                    if ($getStatus == 200) {
                        session()->flash('success', 'ระบบได้แจ้งผู้ควบคุมงานเรียบร้อยแล้ว เพื่อตรวจสอบปิดงานร่วมกัน กรุณารอสักครู่...');
                    } else {
                        session()->flash('error', "การส่ง Line แจ้งเตือนไม่สำเร็จ ({$getStatus})");
                    }

                } else{
                    session()->flash('error','สแกนไม่สำเร็จ กรุณาสแกนใหม่');
                }
                return redirect(route('resultPage'));
            }else{

                session()->flash('warning','ไม่สามารถสแกนได้ เนื่องจากงานของคุณถูกยกเลิก หรือ ปิดงานไม่สำเร็จไปแล้ว');
                return redirect(route('resultPage'));
            }
        }
    }

    public function workpermitPDF($id)
    {
        $task_detail = taskDetailsView::where('task_detail_id', $id)->first();
//        dd($task_detail);
        if ($task_detail != null || $task_detail != ''){
            $pdf = PDF::loadView('admin.task.workPermitPDF', ['task_detail' => $task_detail])->setPaper('a4', 'landscape');
        return @$pdf->download("{$task_detail -> task_title}(ใบ Workpermit).pdf");
//        return @$pdf->stream();
//        return view('admin.task.workPermitPDF',compact('task_detail'));
        }else{
            return response()->view('errors.error', ['status' => '404','message' => 'ไม่พบหน้าที่ท่านต้องการ'], 404);
        }
    }

    public function checkTaskDetail(Request $request,$id = null){
        $contractorCheckTaskDetail = contractorCheckTaskDetail::find($id);
        $taskDetail = taskDetail::find($contractorCheckTaskDetail -> task_detail_id);
        if ($request->isMethod('post')) {
            $contractorCheckTaskDetail -> equipment_personal = $request -> equipment_personal;
            $contractorCheckTaskDetail -> safety_glasses = $request -> safety_glasses;
            $contractorCheckTaskDetail -> safety_belt = $request -> safety_belt;
            $contractorCheckTaskDetail -> scba = $request -> scba;
            $contractorCheckTaskDetail -> oximeter = $request -> oximeter;
            $contractorCheckTaskDetail -> fan = $request -> fan;
            $contractorCheckTaskDetail -> acid_mask = $request -> acid_mask;
            $contractorCheckTaskDetail -> sling = $request -> sling;
            $contractorCheckTaskDetail -> electric1 = $request -> electric1;
            $contractorCheckTaskDetail -> electric1_1 = $request -> electric1_1;
            $contractorCheckTaskDetail -> electric1_2 = $request -> electric1_2;
            $contractorCheckTaskDetail -> cut_gas1 = $request -> cut_gas1;
            $contractorCheckTaskDetail -> cut_gas1_1 = $request -> cut_gas1_1;
            $contractorCheckTaskDetail -> cut_gas1_2 = $request -> cut_gas1_2;
            $contractorCheckTaskDetail -> cut_gas1_3 = $request -> cut_gas1_3;
            $contractorCheckTaskDetail -> cut_gas1_4 = $request -> cut_gas1_4;
             $contractorCheckTaskDetail -> cut_hand1 = $request -> cut_hand1;
             $contractorCheckTaskDetail -> cut_hand1_1 = $request -> cut_hand1_1;
             $contractorCheckTaskDetail -> cut_hand1_2 = $request -> cut_hand1_2;
             $contractorCheckTaskDetail -> cut_hand1_3 = $request -> cut_hand1_3;
             $contractorCheckTaskDetail -> fire_ready = $request -> fire_ready;
             $contractorCheckTaskDetail -> fire_amount = $request -> fire_amount;
             $contractorCheckTaskDetail -> fire_resis = $request -> fire_resis;
             $contractorCheckTaskDetail -> safety = $request -> safety;
             $contractorCheckTaskDetail -> contractor = $request -> contractor;
             $contractorCheckTaskDetail -> check_time = Carbon::now();
            if($contractorCheckTaskDetail -> save()){
                if ($taskDetail -> status == 3){
                    $taskDetail -> status = 4;
                }elseif ($taskDetail -> status == 4){
                    //check in case contractor use old link check 1 cause skip check 2 !!!
                    $getContractorCheckTaskDetail = contractorCheckTaskDetail::where([
                                                    ['task_detail_id',$contractorCheckTaskDetail -> task_detail_id],
                                                    ['time',2]
                                                    ])->first();
                    if ($getContractorCheckTaskDetail == null){
                        session()->flash('warning','กรุณาสแกนที่ใบ Workpermit');
                        return redirect(route('resultPage'));
                    }else{
                        $taskDetail -> status = 5;
//                        $taskDetail -> end_time = Carbon::now();
                    }
                }

                if($taskDetail -> save()){

                    $taskDetailView = taskDetailsView::where('task_detail_id',$contractorCheckTaskDetail -> task_detail_id)->first();
                    $checkTaskDetailRoute = route('leader.checkTaskDetail',['id' => $taskDetail ->id , 'time' => $contractorCheckTaskDetail -> time]);
                    $data = [$taskDetailView -> leader_line_token,"\nเรียนผู้ควบคุมงาน : {$taskDetailView -> leader_firstname} {$taskDetailView -> leader_lastname}\nกรุณาตรวจสอบครั้งที่ {$contractorCheckTaskDetail -> time} : {$taskDetailView -> task_title}\nของหัวหน้าผู้รับเหมา : {$taskDetailView -> contractor_firstname} {$taskDetailView -> contractor_lastname} ({$taskDetailView -> name})\nตรวจสอบ : {$checkTaskDetailRoute}"];
                    $getStatus = lineNotify($data);
                    if($getStatus == 200){
                        session()->flash('success','สแกนสำเร็จ');
                    }else{
                        session()->flash('error',"การส่ง Line แจ้งเตือนไม่สำเร็จ ({$getStatus})");
                    }

                    session()->flash('success',"ทำแบบตรวจสอบระหว่างปฏิบัติงานครั้งที่ {$contractorCheckTaskDetail -> time} สำเร็จ");
                    return redirect(route('resultPage'));
                }
            }else{
                session()->flash('error','บันทึกข้อมูลไม่สำเร็จ');
                return redirect(route('resultPage'));
            }
        }
        return view('contractor_check',compact('contractorCheckTaskDetail'));
    }

    public function searchTraining(Request $request){
        $contractors = Contractor::leftJoin('enterprises','enterprises.id','=','contractors.enterprise_id')
            ->select('contractors.id as id',
                'contractors.prefix as prefix','contractors.first_name as first_name',
                'contractors.last_name as last_name','enterprises.name as enterprise_name'
            )
            ->get();
    $training_details = null;
    if ($request->isMethod('post')) {
        $training_details = trainingDetailView::where('contractor_id',$request -> contractor_id)->get();
        if (count($training_details) <= 0){
            $training_details =null;
        }
    }

    return view('searchTraining' ,compact('contractors','training_details'));
    }

    public function searchSafetyTraining(Request $request){
        $contractors = Contractor::leftJoin('enterprises','enterprises.id','=','contractors.enterprise_id')
            ->select('contractors.id as id',
                'contractors.prefix as prefix','contractors.first_name as first_name',
                'contractors.last_name as last_name','enterprises.name as enterprise_name'
            )
            ->get();
        $safety_training = null;
        if ($request->isMethod('post')) {
            $safety_training = Contractor::select('contractors.*','enterprises.name as enterprise_name','admins.first_name as trainer_first_name','admins.last_name as trainer_last_name')
                                           ->leftjoin('enterprises','enterprises.id' ,'=' ,'contractors.enterprise_id')
                                           ->leftjoin('admins','admins.id' ,'=' ,'contractors.trainer_id')
                                           ->where('contractors.id',$request -> contractor_id)
                                           ->first();

        }

        return view('searchSafetyTraining' ,compact('contractors','safety_training'));
    }

    public function getContractors(Request $request){

        if ($request -> searchTerm != null) {
            $contractors = Contractor::leftJoin('enterprises', 'enterprises.id', '=', 'contractors.enterprise_id')
                ->select('contractors.id as id',
                    'contractors.prefix as prefix', 'contractors.first_name as first_name',
                    'contractors.last_name as last_name', 'enterprises.name as enterprise_name'
                )
                ->where('contractors.first_name', 'like', "%{$request -> searchTerm}%")
                ->get();

        }

        if ($request -> searchTerm == null) {
            $contractors = Contractor::leftJoin('enterprises', 'enterprises.id', '=', 'contractors.enterprise_id')
                ->select('contractors.id as id',
                    'contractors.prefix as prefix', 'contractors.first_name as first_name',
                    'contractors.last_name as last_name', 'enterprises.name as enterprise_name'
                )
                ->get();
        }

        return response()->json(["items"=>$contractors]);
    }

    public function getLeaders(Request $request){

        if ($request -> searchTerm != null) {
            $leaders = Leader::where('first_name', 'like', "%{$request -> searchTerm}%")->get();
        }

        if ($request -> searchTerm == null) {
            $leaders = Leader::all();
        }

        return response()->json(["items"=>$leaders]);
    }

     public function resultPage(){

        return view('resultPage');
     }
}
