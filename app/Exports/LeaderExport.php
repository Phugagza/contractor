<?php

namespace App\Exports;

use App\Leader;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class LeaderExport implements FromQuery,Responsable,WithHeadings,WithMapping,WithColumnFormatting
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query(){

            return Leader::orderBy('first_name','desc');
    }

    public function map($leaders): array
    {
        if($leaders -> line_token =="fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf"){
            $group_name = 'ME 1';
        }elseif($leaders -> line_token =="rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm"){
            $group_name = 'ME 2';
        }elseif($leaders -> line_token =="fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl"){
            $group_name = 'ME 3';
        }elseif($leaders -> line_token =="gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk"){
            $group_name = 'ME 4';
        }elseif($leaders -> line_token =="BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ"){
            $group_name = 'ME 5';
        }elseif($leaders -> line_token =="EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq"){
            $group_name = 'MRG';
        }elseif($leaders -> line_token =="NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW"){
            $group_name = 'MAG';
        }elseif($leaders -> line_token =="6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri"){
            $group_name = 'MUG';
        }elseif($leaders -> line_token =="jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m"){
            $group_name = 'EE 1';
        }elseif($leaders -> line_token =="AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH"){
            $group_name = 'EE 2';
        }elseif($leaders -> line_token =="hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d"){
            $group_name = 'EE 3';
        }elseif($leaders -> line_token =="G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT"){
            $group_name = 'PCSI';
        }elseif($leaders -> line_token =="x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ"){
            $group_name = 'EE 5';
        }elseif($leaders -> line_token =="GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc"){
            $group_name = 'ENERGY';
        }elseif($leaders -> line_token =="FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM"){
            $group_name = 'ENGINEERING';
        }elseif($leaders -> line_token =="SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf"){
            $group_name = 'PM';
        }elseif($leaders -> line_token =="AMScd53BO35HJqmyWW7Y2C2MVZBgk5sSVucKxezwuWy"){
            $group_name = 'PD 1';
        }elseif($leaders -> line_token =="JQwvJaW0OdH8i6HuoxiWQsLiggMvvbW6zqNuryMAUL1"){
            $group_name = 'PD 2';
        }elseif($leaders -> line_token =="MNR2wcZsweShOHuQEMX2s1o0Hxc8Hrs8gOfWTvuevrC"){
            $group_name = 'PD 3';
        }elseif($leaders -> line_token =="hUys8VH8x9NP0Bnrv0r31mWiAEdLMpTwvfhwgBiAPdp"){
            $group_name = 'PD 4';
        }elseif($leaders -> line_token =="gmMv6fQe9j8u7CLdTNr5PWU2As0cQuSkxYyT0z4wsWu"){
            $group_name = 'RS';
        }elseif($leaders -> line_token =="ARnNiMwv8EaQYc6PTRcEqVaitfuOFld3ZROOsfrwIlz"){
            $group_name = 'EU';
        }elseif($leaders -> line_token =="TPaOhHmy7JQsDFOw5dTTZ8Jwn5gy2V6jCNhF4aDEduJ"){
            $group_name = 'Office 1';
        }elseif($leaders -> line_token =="Ha6wlWxi2cDIRaT7p7ANVlrUEQYEB8z3IolzJRWjfJB"){
            $group_name = 'Office 2';
        }elseif($leaders -> line_token =="8Jp0bUQPYtOKYRUun8Q5gbppHvPPjCiuiJ1LONZxby5"){
            $group_name = 'Other';
        }else{
            $group_name = '-';
        }

        return [
            $leaders -> prefix.$leaders -> first_name." ".$leaders -> last_name,
            $leaders -> tel ? "'".$leaders -> tel : '-',
            $leaders -> email ? $leaders -> email : '-',
            $group_name,
        ];
    }
    public function headings(): array
    {
        return [
            'ชื่อ - นามสกุล',
            'เบอร์โทร',
            'อีเมล์',
            'กลุ่มไลน์'
        ];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'B' => "@",
        ];
    }



}
