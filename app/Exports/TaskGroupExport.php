<?php

namespace App\Exports;

use App\Contractor;
use App\Task;
use App\taskDetail;
use App\TasksView;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Auth;
class TaskGroupExport implements FromQuery,Responsable,WithHeadings,WithMapping,WithEvents
{

    use Exportable;
    protected $register_date_start;
    protected $register_date_end;
    public function __construct($register_date_start,$register_date_end)
    {
        $this->register_date_start = $register_date_start;
        $this->register_date_end = $register_date_end;
    }

    public function query()
    {
        $getLeader = Auth::guard('leader')->user();

       if ($this->register_date_start == null && $this->register_date_end == null){
           if ($getLeader -> prefix == "ED_" && $getLeader -> line_token == '') {
               //ED_ - EE_ + ME_ + EET_
               $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                   ->join('contractors','contractors.id','=','tasks.contractor_id')
                   ->join('leaders','leaders.id','=','tasks.leader_id')
                   ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                   ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                   ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                       'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                       'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                       'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                       'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                       'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                       'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                       DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                   )
                   ->orderBy('task_details.id','desc')
                   ->whereIn('leaders.line_token',[
                       'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
                       'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
                       'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
                       'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
                       'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT',
                       'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
                       'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
                       'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
                       'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
                       'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
                       'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
                       'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
                       '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri',
                       'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
                       'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
                       'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
                   ]);
           }else if($getLeader -> prefix == "EE_" && $getLeader -> line_token == ''){
               //EE_ - EE1 EE2 EE3 EE5 PCSI
               $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                   ->join('contractors','contractors.id','=','tasks.contractor_id')
                   ->join('leaders','leaders.id','=','tasks.leader_id')
                   ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                   ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                   ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                       'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                       'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                       'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                       'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                       'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                       'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                       DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                   )
                   ->orderBy('task_details.id','desc')
                   ->whereIn('leaders.line_token',[
                       'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
                       'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
                       'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
                       'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
                       'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT'
                   ]);
           }else if($getLeader -> prefix == "ME_" && $getLeader -> line_token == ''){
               //ME_ - ME1 ME2 ME3 ME4 ME5 MAG MRG MUG
               $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                   ->join('contractors','contractors.id','=','tasks.contractor_id')
                   ->join('leaders','leaders.id','=','tasks.leader_id')
                   ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                   ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                   ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                       'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                       'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                       'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                       'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                       'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                       'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                       DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                   )
                   ->orderBy('task_details.id','desc')
                   ->whereIn('leaders.line_token',[
                       'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
                       'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
                       'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
                       'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
                       'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
                       'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
                       'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
                       '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri'
                   ]);
           }else if($getLeader -> prefix == "EET_" && $getLeader -> line_token == '') {
               //EET_ - ENGINEERING PM ENERGY
               $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                   ->join('contractors','contractors.id','=','tasks.contractor_id')
                   ->join('leaders','leaders.id','=','tasks.leader_id')
                   ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                   ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                   ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                       'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                       'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                       'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                       'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                       'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                       'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                       DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                   )
                   ->orderBy('task_details.id','desc')
                   ->whereIn('leaders.line_token',[
                       'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
                       'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
                       'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
                   ]);
           }else if($getLeader -> line_token != ''){
               //workcontroller
               $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                   ->join('contractors','contractors.id','=','tasks.contractor_id')
                   ->join('leaders','leaders.id','=','tasks.leader_id')
                   ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                   ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                   ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                       'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                       'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                       'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                       'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                       'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                       'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                       DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                   )
                   ->orderBy('task_details.id','desc')
                   ->where('leaders.line_token',$getLeader -> line_token);
           }else{
               $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                   ->join('contractors','contractors.id','=','tasks.contractor_id')
                   ->join('leaders','leaders.id','=','tasks.leader_id')
                   ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                   ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                   ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                       'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                       'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                       'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                       'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                       'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                       'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                       DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                       DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                   )
                   ->orderBy('task_details.id','desc');
           }

       }

        if ($this->register_date_start != null && $this->register_date_end == null){
            if ($getLeader -> prefix == "ED_" && $getLeader -> line_token == '') {
                //ED_ - EE_ + ME_ + EET_
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->where('task_details.date',$this->register_date_start)
                    ->whereIn('leaders.line_token',[
                        'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
                        'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
                        'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
                        'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
                        'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT',
                        'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
                        'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
                        'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
                        'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
                        'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
                        'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
                        'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
                        '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri',
                        'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
                        'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
                        'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
                    ])
                    ->orderBy('task_details.id','desc');
            }else if($getLeader -> prefix == "EE_" && $getLeader -> line_token == '') {
                //EE_ - EE1 EE2 EE3 EE5 PCSI
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->where('task_details.date',$this->register_date_start)
                    ->whereIn('leaders.line_token',[
                        'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
                        'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
                        'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
                        'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
                        'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT'
                    ])
                    ->orderBy('task_details.id','desc');
            }else if($getLeader -> prefix == "ME_" && $getLeader -> line_token == '') {
                //ME_ - ME1 ME2 ME3 ME4 ME5 MAG MRG MUG
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->where('task_details.date',$this->register_date_start)
                    ->whereIn('leaders.line_token',[
                        'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
                        'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
                        'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
                        'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
                        'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
                        'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
                        'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
                        '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri'
                    ])
                    ->orderBy('task_details.id','desc');
            }else if($getLeader -> prefix == "EET_" && $getLeader -> line_token == '') {
                //EET_ - ENGINEERING PM ENERGY
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->where('task_details.date',$this->register_date_start)
                    ->whereIn('leaders.line_token',[
                        'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
                        'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
                        'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
                    ])
                    ->orderBy('task_details.id','desc');
            }else if($getLeader -> line_token != '') {
                //workcontroller
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->where('task_details.date',$this->register_date_start)
                    ->where('leaders.line_token',$getLeader -> line_token)
                    ->orderBy('task_details.id','desc');
            }else{
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->where('task_details.date',$this->register_date_start)
                    ->orderBy('task_details.id','desc');
            }

        }

        if ($this->register_date_start != null && $this->register_date_end != null){
            if ($getLeader -> prefix == "ED_" && $getLeader -> line_token == '') {
                //ED_ - EE_ + ME_ + EET_
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->whereBetween('task_details.date',[$this->register_date_start,$this->register_date_end])
                    ->whereIn('leaders.line_token',[
                        'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
                        'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
                        'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
                        'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
                        'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT',
                        'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
                        'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
                        'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
                        'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
                        'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
                        'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
                        'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
                        '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri',
                        'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
                        'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
                        'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
                    ])
                    ->orderBy('task_details.id','desc');
            }else if($getLeader -> prefix == "EE_" && $getLeader -> line_token == '') {
                //EE_ - EE1 EE2 EE3 EE5 PCSI
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->whereBetween('task_details.date',[$this->register_date_start,$this->register_date_end])
                    ->whereIn('leaders.line_token',[
                        'jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m',
                        'AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH',
                        'hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d',
                        'x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ',
                        'G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT'
                    ])
                    ->orderBy('task_details.id','desc');
            }else if($getLeader -> prefix == "ME_" && $getLeader -> line_token == '') {
                //ME_ - ME1 ME2 ME3 ME4 ME5 MAG MRG MUG
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->whereBetween('task_details.date',[$this->register_date_start,$this->register_date_end])
                    ->whereIn('leaders.line_token',[
                        'fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf',
                        'rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm',
                        'fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl',
                        'gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk',
                        'BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ',
                        'NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW',
                        'EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq',
                        '6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri'
                    ])
                    ->orderBy('task_details.id','desc');
            }else if($getLeader -> prefix == "EET_" && $getLeader -> line_token == '') {
                //EET_ - ENGINEERING PM ENERGY
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->whereBetween('task_details.date',[$this->register_date_start,$this->register_date_end])
                    ->whereIn('leaders.line_token',[
                        'FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM',
                        'SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf',
                        'GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc'
                    ])
                    ->orderBy('task_details.id','desc');
            }else if($getLeader -> line_token != '') {
                //workcontroller
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->whereBetween('task_details.date',[$this->register_date_start,$this->register_date_end])
                    ->where('leaders.line_token',$getLeader -> line_token)
                    ->orderBy('task_details.id','desc');
            }else{
                $tasks = Task::join('task_details','task_details.task_id','=','tasks.id')
                    ->join('contractors','contractors.id','=','tasks.contractor_id')
                    ->join('leaders','leaders.id','=','tasks.leader_id')
                    ->leftjoin('leader_check_tasks','leader_check_tasks.task_id','=','tasks.id')
                    ->leftjoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                    ->select('tasks.title as task_title','task_details.id as no','tasks.description as description','tasks.location as location',
                        'task_details.status as status','task_details.date as date','task_details.start_time as start_time',
                        'task_details.end_time as end_time','contractors.prefix as contractor_prefix','contractors.first_name as contractor_first_name',
                        'contractors.last_name as contractor_last_name','leaders.prefix as leader_prefix','leaders.first_name as leader_first_name','leaders.last_name as leader_last_name',
                        'enterprises.name as enterprise_name','leader_check_tasks.type1 as type1','tasks.id as task_id','leaders.line_token as line_token',
                        'tasks.subcontractor_id as task_subcontractor_id','leader_check_tasks.ppe as ppe','leader_check_tasks.crane1 as crane1',
                        'leader_check_tasks.workshop1 as workshop1','leader_check_tasks.touch as touch','leader_check_tasks.electric_check1 as electric_check1',
                        DB::raw('if(task_details.status = 7 or task_details.status = 8 ,floor(timestampdiff(hour,task_details.start_time,task_details.end_time) /24),0) as total_day'),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,hour(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))) %24 ,0) as total_hour"),
                        DB::raw("if(task_details.status = 7 or task_details.status  = 8 ,minute(timediff(date_format(task_details.start_time,'%H:%i'),date_format(task_details.end_time,'%H:%i'))),0) %60 as total_minute")

                    )
                    ->whereBetween('task_details.date',[$this->register_date_start,$this->register_date_end])
                    ->orderBy('task_details.id','desc');
            }

        }

        return $tasks;
    }


    public function map($tasks): array
    {
        if($tasks -> line_token =="fdIEbDu971lIsag3ud6TnR1byNjSxDOCDqB2lh88Egf"){
            $group_name = 'ME 1';
        }elseif($tasks -> line_token =="rit8B2PY2odAa0gRX36gWXEUoXMFEHQ9pnJfmgGTflm"){
            $group_name = 'ME 2';
        }elseif($tasks -> line_token =="fM10AQIk1HhbGrqIo6BiI2Laj2yxLmS4bo2DmrEQ6Dl"){
            $group_name = 'ME 3';
        }elseif($tasks -> line_token =="gexTQ6hSMjimIfo1yUytRhaD5abqzv8s2jr4DkivlEk"){
            $group_name = 'ME 4';
        }elseif($tasks -> line_token =="BFjqVWewCitDlY4YIYF38j0elNnGf0dMFf3IPWJF8sZ"){
            $group_name = 'ME 5';
        }elseif($tasks -> line_token =="EzKAIz6McR45LfcW2bN20COvdixCgvi2opn58GJXWSq"){
            $group_name = 'MRG';
        }elseif($tasks -> line_token =="NFpLSR22gyrpoLPkZ3VW2BDu0wfO6Gz8I9v1Fss4yrW"){
            $group_name = 'MAG';
        }elseif($tasks -> line_token =="6UUxXMc8dXLV6ydKPsD2NZvRw8gOimOM9gBdXUUG3ri"){
            $group_name = 'MUG';
        }elseif($tasks -> line_token =="jbQqTvA2rYJNMM2Xqtj0utneZfCBeuEQB48G0E9a45m"){
            $group_name = 'EE 1';
        }elseif($tasks -> line_token =="AruuC7edsY69yAXyMBrAsXgXIeszKAQ5Nkx3LeinRIH"){
            $group_name = 'EE 2';
        }elseif($tasks -> line_token =="hasFnP1uL2TkBnMPmtPBKZ9tVoF5lM4AEITcpMC7s5d"){
            $group_name = 'EE 3';
        }elseif($tasks -> line_token =="G6jVQHvjoxX3Z2XVxNqnAWTRikEEauMfmM8tn4261eT"){
            $group_name = 'PCSI';
        }elseif($tasks -> line_token =="x7xOt3LTYsqKfDdb4Zs8mx7S11MA4QwSmDR2JJcvFyQ"){
            $group_name = 'EE 5';
        }elseif($tasks -> line_token =="GKerlpsRZCRVJtQPREKIENjkhGDzLcicK4cQ2l4rxTc"){
            $group_name = 'ENERGY';
        }elseif($tasks -> line_token =="FK8OQ20Qmk149EMgXICiV6qZJOwkQFEJRxlfmT9KMcM"){
            $group_name = 'ENGINEERING';
        }elseif($tasks -> line_token =="SSeU214lKcXifSvTG65DIonsd1M7jZ8nwU0M9WaEWqf"){
            $group_name = 'PM';
        }elseif($tasks -> line_token =="AMScd53BO35HJqmyWW7Y2C2MVZBgk5sSVucKxezwuWy"){
            $group_name = 'PD 1';
        }elseif($tasks -> line_token =="JQwvJaW0OdH8i6HuoxiWQsLiggMvvbW6zqNuryMAUL1"){
            $group_name = 'PD 2';
        }elseif($tasks -> line_token =="MNR2wcZsweShOHuQEMX2s1o0Hxc8Hrs8gOfWTvuevrC"){
            $group_name = 'PD 3';
        }elseif($tasks -> line_token =="hUys8VH8x9NP0Bnrv0r31mWiAEdLMpTwvfhwgBiAPdp"){
            $group_name = 'PD 4';
        }elseif($tasks -> line_token =="gmMv6fQe9j8u7CLdTNr5PWU2As0cQuSkxYyT0z4wsWu"){
            $group_name = 'RS';
        }elseif($tasks -> line_token =="ARnNiMwv8EaQYc6PTRcEqVaitfuOFld3ZROOsfrwIlz"){
            $group_name = 'EU';
        }elseif($tasks -> line_token =="TPaOhHmy7JQsDFOw5dTTZ8Jwn5gy2V6jCNhF4aDEduJ"){
            $group_name = 'Office 1';
        }elseif($tasks -> line_token =="Ha6wlWxi2cDIRaT7p7ANVlrUEQYEB8z3IolzJRWjfJB"){
            $group_name = 'Office 2';
        }elseif($tasks -> line_token =="8Jp0bUQPYtOKYRUun8Q5gbppHvPPjCiuiJ1LONZxby5"){
            $group_name = 'Other';
        }else{
            $group_name = '-';
        }
        $subcontractors = Contractor::whereIn('id',explode(',',$tasks -> task_subcontractor_id))->get();

        if ($tasks -> type1 == 1){
            $type1 = "1";
        }elseif($tasks -> type1 == 2){
            $type1 = "2";
        }elseif($tasks -> type1 == 3){
            $type1 = "3";
        }elseif($tasks -> type1 == 4){
            $type1 = "4";
        }elseif($tasks -> type1 == 5){
            $type1 = "5";
        }else{
            $type1 = "0";
        }

        if ($tasks -> status == 1){
            $status = "รอเข้างาน";
        }elseif($tasks -> status == 2){
            $status = "อนุมัติ";
        }elseif($tasks -> status == 3){
            $status = "เริ่มทำงาน";
        }elseif($tasks -> status == 4){
            $status = "ตรวจสอบ 1";
        }elseif($tasks -> status == 5){
            $status = "ตรวจสอบ 2";
        }elseif($tasks -> status == 6){
            $status = "ตรวจสอบปิดงาน";
        }elseif($tasks -> status == 7){
            $status = "ปิดงานสำเร็จ";
        }elseif($tasks -> status == 8){
            $status = "ปิดงานไม่สำเร็จ";
        }elseif($tasks -> status == 9){
            $status = "ยกเลิก";
        }

        return [
            $tasks -> no,
            $tasks -> task_title,
            $type1,
            $tasks -> location,
            $tasks -> description,
            $status,
            $tasks -> date ? ConvertThaiDate($tasks -> date) : '-',
            $tasks -> start_time ? ConvertThaiDateHour($tasks -> start_time) : '-',
            $tasks -> end_time ? ConvertThaiDateHour($tasks -> end_time) : '-',
//            $tasks -> total_day,
            $tasks -> total_hour,
            $tasks -> total_minute,
            count($subcontractors) + 1 ,
            $tasks -> contractor_prefix.$tasks -> contractor_first_name." ".$tasks -> contractor_last_name,
            $tasks -> enterprise_name ? $tasks -> enterprise_name : '-',
            $tasks -> leader_prefix.$tasks -> leader_first_name." ".$tasks -> leader_last_name,
            $group_name,
            $tasks -> ppe ? "1" : "0",
            $tasks -> crane1 ? "1" : "0",
            $tasks -> workshop1 ? "1" : "0",
            $tasks -> electric_check1 ? "1" : "0",
            $tasks -> touch ?  $tasks -> touch : "0",
        ];
    }

    public function headings(): array
    {

        return [
            'No.',
            'ชื่องาน',
            'ประเภทงาน',
            'สถานที่ปฏิบัติงาน',
            'รายละเอียดงาน',
            'สถานะ',
            'วันที่เข้างาน',
            'เริ่ม',
            'สิ้นสุด',
//            'วัน',
            'ชั่วโมง',
            'นาที',
            'จำนวนผู้ปฏิบัติงาน',
            'หัวหน้าผู้รับเหมา',
            'บริษัท',
            'ผู้ควบคุมงาน',
            'Group',
            'แนบสำเนาใบตรวจสอบอุปกรณ์ป้องกันอันตรายส่วนบุคคล (PPE)',
            'การใช้เครนประเภทเคลี่อนที่ (รถเฮี๊ยบ รถเครน ฯลฯ)',
            'มีการใช้พื้นที่ปฏิบัติงาน (Workshop) ชั่วคราว ในเขตพื้นที่โรงงาน',
            'มีการใช้เครื่องมือหรืออุปกรณ์ไฟไฟ้าที่ใช้ไฟไฟ้าตั้งแต่ 220 V. ขึ้นไป',
            'จำนวนป้ายห้ามแตะ (Don\'t Touch) สำหรับ ผรม.',
        ];
    }
    public function registerEvents(): array
    {

        return [

////                BeforeSheet::class => function(BeforeSheet $event) use ($styleArray) {
////                    $event->sheet->getDelegate()->getStyle('A1:D4')->applyFromArray($styleArray);
////                },
//
//                 AfterSheet::class => function(AfterSheet $event) use ($styleArray){
////                     $event->sheet->getDelegate()->getStyle('A1:S4')->applyFromArray($styleArray);
////                     $event->sheet->getDelegate()->getColumnDimension('A1:S4')->setWidth(300);
//                     $event->sheet->getDelegate()->getStyle("A1:S4")->applyFromArray($styleArray);
//                },
            ];
    }

}
