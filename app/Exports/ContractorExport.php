<?php

namespace App\Exports;

use App\Contractor;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ContractorExport implements FromQuery,Responsable,WithHeadings,WithMapping
{
    use Exportable;
    protected $enterprise_id;

    public function __construct($enterprise_id)
    {
        $this->enterprise_id = $enterprise_id;
    }

    public function query(){
        if($this->enterprise_id == null){
            $contractors = Contractor::leftJoin('enterprises','enterprises.id','=','contractors.enterprise_id')
                                      ->leftJoin('admins','admins.id','=','contractors.trainer_id')
                                      ->select('contractors.prefix as prefix','contractors.first_name as first_name',
                                               'contractors.last_name as last_name','contractors.tel as tel','contractors.email as email',
                                               'contractors.citizen_id as citizen_id','enterprises.name as enterprise_name','admins.prefix as trainer_prefix',
                                               'admins.first_name as trainer_first_name','admins.last_name as trainer_last_name','contractors.safety_training_date as safety_training_date',
                                               'contractors.safety_expired_date as safety_expired_date','contractors.permission as permission',
                                               'contractors.indoor_electricity_training_date','contractors.indoor_electricity_expired_date','contractors.indoor_electricity_permission',
                                               'contractors.indoor_air_conditioner_training_date','contractors.indoor_air_conditioner_expired_date','contractors.indoor_air_conditioner_permission',
                                               'contractors.metal_arc_training_date','contractors.metal_arc_expired_date','contractors.metal_arc_permission',
                                               'contractors.tig_welding_training_date','contractors.tig_welding_expired_date','contractors.tig_welding_permission',
                                               'contractors.alloy_welding_training_date','contractors.alloy_welding_expired_date','contractors.alloy_welding_permission',
                                               'contractors.scaffolding_training_date','contractors.scaffolding_expired_date','contractors.scaffolding_permission',
                                               'contractors.worker_confine_training_date','contractors.worker_confine_expired_date','contractors.worker_confine_permission',
                                               'contractors.assistant_confine_training_date','contractors.assistant_confine_expired_date','contractors.assistant_confine_permission',
                                               'contractors.controller_confine_training_date','contractors.controller_confine_expired_date','contractors.controller_confine_permission',
                                               'contractors.warrantor_confine_training_date','contractors.warrantor_confine_expired_date','contractors.warrantor_confine_permission',
                                               'contractors.warrantor_confine_training_date','contractors.warrantor_confine_expired_date','contractors.warrantor_confine_permission'
                                              );
        }
        if ($this->enterprise_id != null){
            $contractors = Contractor::Join('enterprises','enterprises.id','=','contractors.enterprise_id')
                ->leftJoin('admins','admins.id','=','contractors.trainer_id')
                ->where('contractors.enterprise_id',$this->enterprise_id)
                ->select('contractors.prefix as prefix','contractors.first_name as first_name',
                    'contractors.last_name as last_name','contractors.tel as tel','contractors.email as email',
                    'contractors.citizen_id as citizen_id','enterprises.name as enterprise_name','admins.prefix as trainer_prefix',
                    'admins.first_name as trainer_first_name','admins.last_name as trainer_last_name','contractors.safety_training_date as safety_training_date',
                    'contractors.safety_expired_date as safety_expired_date','contractors.permission as permission',
                    'contractors.indoor_electricity_training_date','contractors.indoor_electricity_expired_date','contractors.indoor_electricity_permission',
                    'contractors.indoor_air_conditioner_training_date','contractors.indoor_air_conditioner_expired_date','contractors.indoor_air_conditioner_permission',
                    'contractors.metal_arc_training_date','contractors.metal_arc_expired_date','contractors.metal_arc_permission',
                    'contractors.tig_welding_training_date','contractors.tig_welding_expired_date','contractors.tig_welding_permission',
                    'contractors.alloy_welding_training_date','contractors.alloy_welding_expired_date','contractors.alloy_welding_permission',
                    'contractors.scaffolding_training_date','contractors.scaffolding_expired_date','contractors.scaffolding_permission',
                    'contractors.worker_confine_training_date','contractors.worker_confine_expired_date','contractors.worker_confine_permission',
                    'contractors.assistant_confine_training_date','contractors.assistant_confine_expired_date','contractors.assistant_confine_permission',
                    'contractors.controller_confine_training_date','contractors.controller_confine_expired_date','contractors.controller_confine_permission',
                    'contractors.warrantor_confine_training_date','contractors.warrantor_confine_expired_date','contractors.warrantor_confine_permission',
                    'contractors.warrantor_confine_training_date','contractors.warrantor_confine_expired_date','contractors.warrantor_confine_permission'
                );
        }
        return $contractors;
    }

    public function map($contractors): array
    {
        return [
            $contractors -> prefix.$contractors -> first_name." ".$contractors -> last_name,
            $contractors -> email,
            "'".$contractors -> tel,
            $contractors -> citizen_id,
            $contractors -> enterprise_name ? $contractors -> enterprise_name : '-',
            $contractors -> trainer_prefix.$contractors -> trainer_first_name." ".$contractors -> trainer_last_name,
            $contractors -> safety_training_date ? ConvertThaiDate($contractors -> safety_training_date) : "-",
            $contractors -> safety_expired_date ? ConvertThaiDate($contractors -> safety_expired_date) : "-",
            $contractors -> permission ? $contractors -> permission : '-',

            $contractors -> indoor_electricity_training_date ? ConvertThaiDate($contractors -> indoor_electricity_training_date) : "-",
            $contractors -> indoor_electricity_expired_date ? ConvertThaiDate($contractors -> indoor_electricity_expired_date) : "-",
            $contractors -> indoor_electricity_permission ? $contractors -> indoor_electricity_permission : '-',

            $contractors -> indoor_air_conditioner_training_date ? ConvertThaiDate($contractors -> indoor_air_conditioner_training_date) : "-",
            $contractors -> indoor_air_conditioner_expired_date ? ConvertThaiDate($contractors -> indoor_air_conditioner_expired_date) : "-",
            $contractors -> indoor_air_conditioner_permission ? $contractors -> indoor_air_conditioner_permission : '-',

            $contractors -> metal_arc_training_date ? ConvertThaiDate($contractors -> metal_arc_training_date) : "-",
            $contractors -> metal_arc_expired_date ? ConvertThaiDate($contractors -> metal_arc_expired_date) : "-",
            $contractors -> metal_arc_permission ? $contractors -> metal_arc_permission : '-',

            $contractors -> tig_welding_training_date ? ConvertThaiDate($contractors -> tig_welding_training_date) : "-",
            $contractors -> tig_welding_expired_date ? ConvertThaiDate($contractors -> tig_welding_expired_date) : "-",
            $contractors -> tig_welding_permission ? $contractors -> tig_welding_permission : '-',

            $contractors -> alloy_welding_training_date ? ConvertThaiDate($contractors -> alloy_welding_training_date) : "-",
            $contractors -> alloy_welding_expired_date ? ConvertThaiDate($contractors -> alloy_welding_expired_date) : "-",
            $contractors -> alloy_welding_permission ? $contractors -> alloy_welding_permission : '-',

            $contractors -> scaffolding_training_date ? ConvertThaiDate($contractors -> scaffolding_training_date) : "-",
            $contractors -> scaffolding_expired_date ? ConvertThaiDate($contractors -> scaffolding_expired_date) : "-",
            $contractors -> scaffolding_permission ? $contractors -> scaffolding_permission : '-',

            $contractors -> worker_confine_training_date ? ConvertThaiDate($contractors -> worker_confine_training_date) : "-",
            $contractors -> worker_confine_expired_date ? ConvertThaiDate($contractors -> worker_confine_expired_date) : "-",
            $contractors -> worker_confine_permission ? $contractors -> worker_confine_permission : '-',

            $contractors -> assistant_confine_training_date ? ConvertThaiDate($contractors -> assistant_confine_training_date) : "-",
            $contractors -> assistant_confine_expired_date ? ConvertThaiDate($contractors -> assistant_confine_expired_date) : "-",
            $contractors -> assistant_confine_permission ? $contractors -> assistant_confine_permission : '-',

            $contractors -> controller_confine_training_date ? ConvertThaiDate($contractors -> controller_confine_training_date) : "-",
            $contractors -> controller_confine_expired_date ? ConvertThaiDate($contractors -> controller_confine_expired_date) : "-",
            $contractors -> controller_confine_permission ? $contractors -> controller_confine_permission : '-',

            $contractors -> warrantor_confine_training_date ? ConvertThaiDate($contractors -> warrantor_confine_training_date) : "-",
            $contractors -> warrantor_confine_expired_date ? ConvertThaiDate($contractors -> warrantor_confine_expired_date) : "-",
            $contractors -> warrantor_confine_permission ? $contractors -> warrantor_confine_permission : '-',
        ];
    }

    public function headings(): array
    {
        return [
            'ชื่อ - นามสกุล',
            'email',
            'เบอร์โทร',
            'เลขบัตรประชาชน',
            'บริษัท',
            'ผู้ฝึกอบรม',
            'วันที่อบรม',
            'วันหมดอายุ',
            'Permission',

            'วันที่อบรมช่างไฟไฟ้าภายในอาคาร',
            'วันหมดอายุช่างไฟไฟ้าภายในอาคาร',
            'Permission',

            'วันที่อบรมช่างเครื่องปรับอากาศภายในบ้านและการพาณิชย์ขนาดเล็ก',
            'วันหมดอายุช่างเครื่องปรับอากาศภายในบ้านและการพาณิชย์ขนาดเล็ก',
            'Permission',

            'วันที่อบรมช่างเชื่อมอาร์กโลหะด้วยมือ',
            'วันหมดอายุช่างเชื่อมอาร์กโลหะด้วยมือ',
            'Permission',

            'วันที่อบรมช่างเชื่อมทิก',
            'วันหมดอายุช่างเชื่อมทิก',
            'Permission',

            'วันที่อบรมช่างเชื่อมแม็ก',
            'วันหมดอายุช่างเชื่อมแม็ก',
            'Permission',

            'วันที่อบรมช่างติดตั้งนั่งร้าน',
            'วันหมดอายุช่างติดตั้งนั่งร้าน',
            'Permission',

            'วันที่อบรมผู้ปฏิบัติงานในงานอับอากาศ',
            'วันหมดอายุผู้ปฏิบัติงานในงานอับอากาศ',
            'Permission',

            'วันที่อบรมผู้ช่วยเหลือในงานอับอากาศ',
            'วันหมดอายุผู้ช่วยเหลือในงานอับอากาศ',
            'Permission',

            'วันที่อบรมผู้ควบคุมในงานอับอากาศ',
            'วันหมดอายุผู้ควบคุมในงานอับอากาศ',
            'Permission',

            'วันที่อบรมผู้อนุญาตในงานอับอากาศ',
            'วันหมดอายุผู้อนุญาตในงานอับอากาศ',
            'Permission',
        ];
    }
}
