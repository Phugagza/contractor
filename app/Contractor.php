<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
class Contractor extends Model
{
    use SoftDeletes;
    public function enterprise(){
        return $this->hasOne('App\Enterprise','id','enterprise_id');
    }

    //safety
    public function getCountSafetyDateAttribute(){
        $safety_expired_date=date_create($this->safety_expired_date);
        $safety_training_date=date_create($this->safety_training_date);
        $date_now=date_create(date("Y-m-d"));

        $total_date=date_diff($safety_training_date,$safety_expired_date);
        $use_date=date_diff($safety_training_date,$date_now);

        return ($total_date -> days -  $use_date -> days);
    }

    public function getSafetyRangeDateAttribute(){
        $safety_expired_date=date_create($this->safety_expired_date);
        $safety_training_date=date_create($this->safety_training_date);

        $total_date=date_diff($safety_training_date,$safety_expired_date);

        return $total_date -> days;
    }

    //indoor electricity
    public function getCountIndoorElectricityDateAttribute(){
        $indoor_electricity_expired_date=date_create($this->indoor_electricity_expired_date);
        $indoor_electricity_training_date=date_create($this->indoor_electricity_training_date);
        $date_now=date_create(date("Y-m-d"));

        $total_date=date_diff($indoor_electricity_training_date,$indoor_electricity_expired_date);
        $use_date=date_diff($indoor_electricity_training_date,$date_now);

        return ($total_date -> days -  $use_date -> days);
    }

    public function getIndoorElectricityRangeDateAttribute(){
        $indoor_electricity_expired_date=date_create($this->indoor_electricity_expired_date);
        $indoor_electricity_training_date=date_create($this->indoor_electricity_training_date);

        $total_date=date_diff($indoor_electricity_training_date,$indoor_electricity_expired_date);

        return $total_date -> days;
    }

    //indoor air conditioner
    public function getCountIndoorAirConditionerDateAttribute(){
        $indoor_air_conditioner_expired_date=date_create($this->indoor_air_conditioner_expired_date);
        $indoor_air_conditioner_training_date=date_create($this->indoor_air_conditioner_training_date);
        $date_now=date_create(date("Y-m-d"));

        $total_date=date_diff($indoor_air_conditioner_training_date,$indoor_air_conditioner_expired_date);
        $use_date=date_diff($indoor_air_conditioner_training_date,$date_now);

        return ($total_date -> days -  $use_date -> days);
    }

    public function getIndoorAirConditionerRangeDateAttribute(){
        $indoor_air_conditioner_expired_date=date_create($this->indoor_air_conditioner_expired_date);
        $indoor_air_conditioner_training_date=date_create($this->indoor_air_conditioner_training_date);

        $total_date=date_diff($indoor_air_conditioner_training_date,$indoor_air_conditioner_expired_date);

        return $total_date -> days;
    }

    //metal arc
    public function getCountMetalArcDateAttribute(){
        $metal_arc_expired_date=date_create($this->metal_arc_expired_date);
        $metal_arc_training_date=date_create($this->metal_arc_training_date);
        $date_now=date_create(date("Y-m-d"));

        $total_date=date_diff($metal_arc_training_date,$metal_arc_expired_date);
        $use_date=date_diff($metal_arc_training_date,$date_now);

        return ($total_date -> days -  $use_date -> days);
    }

    public function getMetalArcRangeDateAttribute(){
        $metal_arc_expired_date=date_create($this->metal_arc_expired_date);
        $metal_arc_training_date=date_create($this->metal_arc_training_date);

        $total_date=date_diff($metal_arc_training_date,$metal_arc_expired_date);

        return $total_date -> days;
    }

    //tig welding
    public function getCountTigWeldingDateAttribute(){
        $tig_welding_expired_date=date_create($this->tig_welding_expired_date);
        $tig_welding_training_date=date_create($this->tig_welding_training_date);
        $date_now=date_create(date("Y-m-d"));

        $total_date=date_diff($tig_welding_training_date,$tig_welding_expired_date);
        $use_date=date_diff($tig_welding_training_date,$date_now);

        return ($total_date -> days -  $use_date -> days);
    }

    public function getTigWeldingRangeDateAttribute(){
        $tig_welding_expired_date=date_create($this->tig_welding_expired_date);
        $tig_welding_training_date=date_create($this->tig_welding_training_date);

        $total_date=date_diff($tig_welding_training_date,$tig_welding_expired_date);

        return $total_date -> days;
    }

    //alloy welding
    public function getCountAlloyWeldingDateAttribute(){
        $alloy_welding_expired_date=date_create($this->alloy_welding_expired_date);
        $alloy_welding_training_date=date_create($this->alloy_welding_training_date);
        $date_now=date_create(date("Y-m-d"));

        $total_date=date_diff($alloy_welding_training_date,$alloy_welding_expired_date);
        $use_date=date_diff($alloy_welding_training_date,$date_now);

        return ($total_date -> days -  $use_date -> days);
    }

    public function getAlloyWeldingRangeDateAttribute(){
        $alloy_welding_expired_date=date_create($this->alloy_welding_expired_date);
        $alloy_welding_training_date=date_create($this->alloy_welding_training_date);

        $total_date=date_diff($alloy_welding_training_date,$alloy_welding_expired_date);

        return $total_date -> days;
    }

    //scaffolding
    public function getCountScaffoldingDateAttribute(){
        $scaffolding_expired_date=date_create($this->scaffolding_expired_date);
        $scaffolding_training_date=date_create($this->scaffolding_training_date);
        $date_now=date_create(date("Y-m-d"));

        $total_date=date_diff($scaffolding_training_date,$scaffolding_expired_date);
        $use_date=date_diff($scaffolding_training_date,$date_now);

        return ($total_date -> days -  $use_date -> days);
    }

    public function getScaffoldingRangeDateAttribute(){
        $scaffolding_expired_date=date_create($this->scaffolding_expired_date);
        $scaffolding_training_date=date_create($this->scaffolding_training_date);

        $total_date=date_diff($scaffolding_training_date,$scaffolding_expired_date);

        return $total_date -> days;
    }

    //worker confine
    public function getCountWorkerConfineDateAttribute(){
        $worker_confine_expired_date=date_create($this->worker_confine_expired_date);
        $worker_confine_training_date=date_create($this->worker_confine_training_date);
        $date_now=date_create(date("Y-m-d"));

        $total_date=date_diff($worker_confine_training_date,$worker_confine_expired_date);
        $use_date=date_diff($worker_confine_training_date,$date_now);

        return ($total_date -> days -  $use_date -> days);
    }

    public function getWorkerConfineRangeDateAttribute(){
        $worker_confine_expired_date=date_create($this->worker_confine_expired_date);
        $worker_confine_training_date=date_create($this->worker_confine_training_date);

        $total_date=date_diff($worker_confine_training_date,$worker_confine_expired_date);

        return $total_date -> days;
    }

    //assistant confine
    public function getCountAssistantConfineDateAttribute(){
        $assistant_confine_expired_date=date_create($this->assistant_confine_expired_date);
        $assistant_confine_training_date=date_create($this->assistant_confine_training_date);
        $date_now=date_create(date("Y-m-d"));

        $total_date=date_diff($assistant_confine_training_date,$assistant_confine_expired_date);
        $use_date=date_diff($assistant_confine_training_date,$date_now);

        return ($total_date -> days -  $use_date -> days);
    }

    public function getAssistantConfineRangeDateAttribute(){
        $assistant_confine_expired_date=date_create($this->assistant_confine_expired_date);
        $assistant_confine_training_date=date_create($this->assistant_confine_training_date);

        $total_date=date_diff($assistant_confine_training_date,$assistant_confine_expired_date);

        return $total_date -> days;
    }

    //controller confine
    public function getCountControllerConfineDateAttribute(){
        $controller_confine_expired_date=date_create($this->controller_confine_expired_date);
        $controller_confine_training_date=date_create($this->controller_confine_training_date);
        $date_now=date_create(date("Y-m-d"));

        $total_date=date_diff($controller_confine_training_date,$controller_confine_expired_date);
        $use_date=date_diff($controller_confine_training_date,$date_now);

        return ($total_date -> days -  $use_date -> days);
    }

    public function getControllerConfineRangeDateAttribute(){
        $controller_confine_expired_date=date_create($this->controller_confine_expired_date);
        $controller_confine_training_date=date_create($this->controller_confine_training_date);

        $total_date=date_diff($controller_confine_training_date,$controller_confine_expired_date);

        return $total_date -> days;
    }

    //warrantor confine
    public function getCountWarrantorConfineDateAttribute(){
        $warrantor_confine_expired_date=date_create($this->warrantor_confine_expired_date);
        $warrantor_confine_training_date=date_create($this->warrantor_confine_training_date);
        $date_now=date_create(date("Y-m-d"));

        $total_date=date_diff($warrantor_confine_training_date,$warrantor_confine_expired_date);
        $use_date=date_diff($warrantor_confine_training_date,$date_now);

        return ($total_date -> days -  $use_date -> days);
    }

    public function getWarrantorConfineRangeDateAttribute(){
        $warrantor_confine_expired_date=date_create($this->warrantor_confine_expired_date);
        $warrantor_confine_training_date=date_create($this->warrantor_confine_training_date);

        $total_date=date_diff($warrantor_confine_training_date,$warrantor_confine_expired_date);

        return $total_date -> days;
    }

    public function getFullNameAttribute(){
        return $this->prefix.$this->first_name." ".$this->last_name;
    }

    public function getPermissionWithStyleAttribute(){
        return $this->permission == "Y" ?
             '<span class="badge badge-success">Y</span>':
             '<span class="badge badge-danger">N</span>';
    }

    public function getIndoorElectricityPermissionWithStyleAttribute(){
        return $this->indoor_electricity_permission == "Y" ?
            '<span class="badge badge-success">Y</span>':
            '<span class="badge badge-danger">N</span>';
    }

    public function getIndoorAirConditionerPermissionWithStyleAttribute(){
        return $this->indoor_air_conditioner_permission == "Y" ?
            '<span class="badge badge-success">Y</span>':
            '<span class="badge badge-danger">N</span>';
    }

    public function getMetalArcPermissionWithStyleAttribute(){
        return $this->metal_arc_permission == "Y" ?
            '<span class="badge badge-success">Y</span>':
            '<span class="badge badge-danger">N</span>';
    }

    public function getTigWeldingPermissionWithStyleAttribute(){
        return $this->tig_welding_permission == "Y" ?
            '<span class="badge badge-success">Y</span>':
            '<span class="badge badge-danger">N</span>';
    }

    public function getAlloyWeldingPermissionWithStyleAttribute(){
        return $this->alloy_welding_permission == "Y" ?
            '<span class="badge badge-success">Y</span>':
            '<span class="badge badge-danger">N</span>';
    }

    public function getScaffoldingPermissionWithStyleAttribute(){
        return $this->scaffolding_permission == "Y" ?
            '<span class="badge badge-success">Y</span>':
            '<span class="badge badge-danger">N</span>';
    }

    public function getWorkerConfinePermissionWithStyleAttribute(){
        return $this->worker_confine_permission == "Y" ?
            '<span class="badge badge-success">Y</span>':
            '<span class="badge badge-danger">N</span>';
    }

    public function getAssistantConfinePermissionWithStyleAttribute(){
        return $this->assistant_confine_permission == "Y" ?
            '<span class="badge badge-success">Y</span>':
            '<span class="badge badge-danger">N</span>';
    }

    public function getControllerConfinePermissionWithStyleAttribute(){
        return $this->controller_confine_permission == "Y" ?
            '<span class="badge badge-success">Y</span>':
            '<span class="badge badge-danger">N</span>';
    }

    public function getWarrantorConfinePermissionWithStyleAttribute(){
        return $this->warrantor_confine_permission == "Y" ?
            '<span class="badge badge-success">Y</span>':
            '<span class="badge badge-danger">N</span>';
    }
}
